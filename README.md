# 3D Playground

3D Playground is a single application that contains a collection of smaller projects, with the goal being to learn about 
specific 3D concepts found in rendering and game development. There are two main reasons why they are all inside the same application. 
One is because it makes it easy to add new concepts because a framework for low level graphics, OS interaction etc. is already present. 
It's also to see which challenges emerge as an application grows and must support different subsystems, which makes for an 
interesting system design experience. Because this is meant as an educational project rather than a professional grade one, security is not 
a priority so anything handling external data should only be used with trusted data. 

Currently the project uses OpenGL 3.3 for rendering and supports Windows. glTF is the only format currently supported for loading 
scenes/models.

### Application wide TODOs
 * Integrate OpenGL 4.5
 * Integrate Vulkan
 * Support Linux
 * Support other file formats like OBJ.
 * A proper GUI pass

### Projects:
* [glTF Loader](#gltf-loader)
* [Base64 encoder/decoder](#base64-encoderdecoder)
* [PBR Viewer](#pbr-viewer)
* [PBR Scene](#pbr-scene)
* Terrain generation(TODO)
* Fluid simulation(TODO)
* Volumetric Rendering(TODO)
* Physics/Collision detection(TODO)
* Non-Photorealistic Rendering(TODO)
* Animation(TODO)

## glTF Loader
A basic glTF loader used to load scenes/models. This is not a professional grade loader but developed as a way to learn about the glTF 
format and what it takes to load external data into an existing environment(also because it's fun). 
Currently only supports glTF format not GLB.

### Supports:
* Cameras
* Images
* Meshes
* Nodes
* Scenes
* Textures
* Samplers
* Materials

### TODOs:
* Animations
* Skins
* Extensions
* GLB
* Support fully async loading

## Base64 Encoder/decoder
A rudimentary base64 encoder/decoder mainly used to parse glTF data-URIs.

## PBR Viewer
 A renderer focused on physically based rendering with an orbiting camera. The shading is based on [Unreal's PBR model](#physically-based-rendering) presented by Brian 
 Karis but also other sources like the [course notes on moving Frostbite to PBR](#physically-based-rendering) from Sébastien Lagarde et al.
 The renderer is a fully forward renderer since there is only one direct light in addition to IBL so a more complex system isn't necessary. 

### Screenshots
![Damaged Helmet](https://wintrytbs.gitlab.io/website_test/images/damaged_helmet.PNG)
![Scifi Helmet](https://wintrytbs.gitlab.io/website_test/images/scifi_helmet.PNG)
![Metal Roughness Spheres](https://wintrytbs.gitlab.io/website_test/images/metal_roughness_spheres.PNG)
![Occlusion mapping](https://wintrytbs.gitlab.io/website_test/images/ambient_occlusion.PNG)

### Features:
 * Cook-Torrence microfacet specular BRDF & lambert diffuse
 * Metallic-Roughness workflow
 * Ambient occlusion mapping
 * Emissive mapping
 * Alpha cutoff
 * Image Based Lighting(IBL) with HDR images.
 * Tonemapping with the ability to choose between different operators(Reinhard simple, Hable, ACES etc)
 * Material Visualizer
 * Anti-Aliasing(MSAA)
 * Loading models and switching between previously loaded models
 * Loading HDR images and selecting between previously loaded HDR images.
 * Semi-async texture loading(image files are loaded asynchronously but uploaded to the GPU as textures synchronously)

## PBR Scene

 This is an extension of the PBR Viewer but with a level editor approach. This means that everything is organized into scenes
 and for each scene one can add items like meshes and lights dynamically. One other difference is that the material visualizer 
 isn't available. Mesh structure follows the structure of a glTF mesh where a mesh can contain multiple primitives 
 which contains the actual information needed for rendering.

 ### Screenshots:
 ![Terrain Shadow](https://wintrytbs.gitlab.io/website_test/images/terrain_shadow.PNG)
 ![Sponza Directional](https://wintrytbs.gitlab.io/website_test/images/sponza_directional.PNG)
 ![Sponza Cascade](https://wintrytbs.gitlab.io/website_test/images/sponza_cascades.PNG)
 ![Sponza Point Light](https://wintrytbs.gitlab.io/website_test/images/sponza_point_light.PNG)


 ### Features:
  * Point lights with the ability to choose between fall-off functions
  * Spot lights
  * Directional lights
  * Omni-directional shadow mapping for point lights
  * Shadow mapping for spot lights
  * Cascading shadow maps for directional lights(with 4 slices)
  * Scene outliner to show meshes and their hierarchical structure in a scene
  * Transform Hierarchy
  * Move, rotate and scale meshes
  * Import scenes from glTF files
  * Import meshes from glTF files into an existing scene
  * Visualize cascade slices
  * A fly-camera to move around the scene as well as to orbit objects
  * Adjustable camera properties like fov and near/far clip
  * Inspect primitive info(vertex count, material properties, material maps etc)
  * Semi-async texture loading(image files are loaded asynchronously but uploaded to the GPU as textures synchronously)


 ### TODOs
  * Use physically based units for light intensity
  * Global Illumination
  * Area lights
  * GUI gizmos for interacting with scene elements directly
  * Auto generated environment map with tunable parameters
  * Screen Space Ambient Occlusion
  * Motion Blur
  * Bloom
  * Order Independent Transparency
  * Depth of Field
  * Different anti-aliasing methods than OpenGL's built-in MSAA
  * Clustered forward/deferred rendering based on the [paper from Ola Olsson et al.](http://www.cse.chalmers.se/~uffe/clustered_shading_preprint.pdf)

## Libraries
  All the necessary libraries needed to build is included in the project.  
  The following libraries are used:
   * [Dear ImGui](https://github.com/ocornut/imgui) For everything GUI related
   * [stb_image](https://github.com/nothings/stb) Loading images and .hdr files
   * [OpenGL API and Extension Header Files](https://www.khronos.org/registry/OpenGL/index_gl.php) OpenGL definitions
   * [Meow Hash](https://github.com/cmuratori/meow_hash) For everything hashing

## Models
 * [Damaged Helmet](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/DamagedHelmet)
 * [SciFi Helmet](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/SciFiHelmet)
 * [Metal-Roughness Spheres](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/MetalRoughSpheres)
 * [Lantern](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/Lantern)
 * [Sponza](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/Sponza)
 * [Tree & Terrain(part of asset pack from Sascha Willems' Vulkan Examples)](https://github.com/SaschaWillems/Vulkan)

## HDRs
 * [St. Fagans Interior(shown in the screenshots)](https://hdrihaven.com/hdri/?h=st_fagans_interior)
 * [Barcelona Rooftops(included in the project)](http://www.hdrlabs.com/sibl/archive.html)

## Building
  To build the project run the **build.bat** file located in the **code** directory. MSVC is the default compiler so cl.exe needs to be 
  available on the command line. The project requires C++14:
  ```
  build.bat
  ```
  Clang can be invoked by providing **clang** as an argument to the build script:
  ```
  build.bat clang
  ```
  This will create a build directory at the project root containing the output files.
## Resources
  ### Physically Based Rendering:
   * [Brian Karis Real Shading in Unreal Engine 4](https://cdn2.unrealengine.com/Resources/files/2013SiggraphPresentationsNotes-26915738.pdf)
   * [Moving Frostbite to Physically Based Rendering 3.0](https://seblagarde.files.wordpress.com/2015/07/course_notes_moving_frostbite_to_pbr_v32.pdf)
   * [Physically Based Rendering in Filament](https://google.github.io/filament/Filament.html)
   * [Real-Time Rendering](http://www.realtimerendering.com/)
   * [Physics and Math of Shading by Naty Hoffman](https://blog.selfshadow.com/publications/s2013-shading-course/hoffman/s2013_pbs_physics_math_notes.pdf)
   
  ### glTF
   * [glTF Specification](https://github.com/KhronosGroup/glTF/tree/master/specification/2.0)
