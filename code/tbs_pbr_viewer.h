namespace tbs_pbr_viewer
{
    namespace local_constants
    {
        constexpr u32 MAX_MESH_COUNT                    = 512;
        constexpr u32 MAX_PBR_MATERIAL_COUNT            = 512;
        constexpr u32 MAX_GROUP_COUNT                   = 128;
    }

    enum render_mode
    {
        RENDER_MODE_Full,
        RENDER_MODE_Albedo,
        RENDER_MODE_Normal,
        RENDER_MODE_Occlusion,
        RENDER_MODE_Emissive,
        RENDER_MODE_Metallic,
        RENDER_MODE_Roughness,
        RENDER_MODE_Diffuse,
        RENDER_MODE_Specular,
        RENDER_MODE_Geometry,
        RENDER_MODE_Distribution,
        RENDER_MODE_Fresnel,
        RENDER_MODE_FresnelRoughness,
    };

    enum tonemapping_type
    {
        TONEMAPPING_TYPE_None,
        TONEMAPPING_TYPE_ReinhardSimple,
        TONEMAPPING_TYPE_ReinhardExtended,
        TONEMAPPING_TYPE_ReinhardExtendedLuminance,
        TONEMAPPING_TYPE_ReinhardJodie,
        TONEMAPPING_TYPE_Hable,
        TONEMAPPING_TYPE_ACES,

        TONEMAPPING_TYPE_Count,
    };

    enum background_option
    {
        BACKGROUND_OPTION_SolidColor,
        BACKGROUND_OPTION_EnvironmentMap,
        BACKGROUND_OPTION_EnvironmentMapBlurred,
    };

    internal const char *gui_tonemappingTypeNames[TONEMAPPING_TYPE_Count] =
    {
        "None",
        "Reinhard Simple",
        "Reinhard Extended",
        "Reinhard ExtendedLuminance",
        "Reinhard Jodie",
        "Hable",
        "ACES",
    };

    struct orbit_camera
    {
        v3 pos;
        v3 target;
        f32 radius;
        f32 phi;
        f32 theta;
        f32 clipNear;
        f32 clipFar;
    };

    struct group_link
    {
        tbs_pbr::pbr_mesh *m;
        matrix4D t;
        group_link *next;
    };

    struct mesh_group
    {
        char name[256];
        char file[256];
        group_link *first;
    };

    struct gui_state
    {
        tbs_gui::env_map_chooser_context envMapChooserCtx;
        tbs_gui::file_browser_context meshLoaderCtx;
    };

    struct pbr_viewer
    {
        // TODO(torgrim): Doesn't really make sense to have the materials
        // here since they are always connected to a specific primitive?
        u32 materialCount;
        tbs_pbr::pbr_material materials[local_constants::MAX_PBR_MATERIAL_COUNT];

        // TODO(torgrim): Remedy doesn't like it when this list comes first
        // in the struct for some reason.
        // Seems to need at least six var before it for some reason.
        // Test this more and send a crash report to remedybg
        // Seems to have something to do with pbr mesh struct.
        tbs_pbr::pbr_mesh meshes[local_constants::MAX_MESH_COUNT];
        u32 meshCount;

        mesh_group groups[local_constants::MAX_GROUP_COUNT];
        u32 groupCount;

        render_mode renderMode;

        // TODO(torgrim): This should really be initialized to
        // InvalidHandle or something although we should change
        // that to be all zeros since it would make it
        // easier to initilize to invalid value.
        tbs_opengl::opengl_resource_handle activeEnvMap;
        bool useEnvLighting;

        bool useGammaCorrection;
        bool useTonemapping;

        f32 whitePoint;
        tonemapping_type tonemappingType;

        light_data mainLight;
        orbit_camera camera;

        v2i viewportSize;
        v2i viewportPos;
        bool resizeEvent;

        u32 activeGroup;
        f32 backgroundColor[3];
        background_option backgroundOption;
        bool msaaActive;

        gui_state guiState;

        i32 CreateAndAddMeshGroupFromGltf(application_state *app, tbs_opengl::opengl_resource_manager *rm, gltf_root *gltf, char *name);
        u64 AddMaterial(tbs_pbr::pbr_material mat);
        void AddMeshGroup(mesh_group group);
    };

    internal void RenderMeshGroup(pbr_viewer *pbrViewer, tbs_opengl::opengl_resource_manager *GPUManager, u32 groupIndex, matrix4D projView);
    internal void RenderGUI(application_state *app, tbs_gui::imgui_state *imGuiState, tbs_opengl::opengl_resource_manager *rm, pbr_viewer *pbrViewer);
    internal void PostProcessTonemappingGamma(pbr_viewer *pbrViewer, tbs_opengl::opengl_resource_manager *GPUmanager, GLuint texID);
    internal void RenderBackground(pbr_viewer *pbrViewer, tbs_opengl::opengl_resource_manager *GPUManager, matrix4D proj, matrix4D view);
}
