#ifndef TBS_MATH_H

#if defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weverything"
#elif defined(_MSC_VER)
#pragma warning(push, 0)
#pragma warning(disable: 4577)
#endif

#include <cmath>
#include <limits>

#if defined(__clang__)
#pragma GCC diagnostic pop
#elif defined(_MSC_VER)
#pragma warning(pop);
#endif

#include "tbs_types.h"

constexpr f32 PI = 3.141592f;

constexpr float TBS_FLT_INF          = std::numeric_limits<float>::infinity();
constexpr float TBS_NEG_FLT_INF      = -std::numeric_limits<float>::infinity();

constexpr f32 IS_NORMALIZED_EPSILON= 0.000001f;

#define Mod2(v,n) (v & ((1 << n) - 1))

#define Mat4Addr(mat) (&mat(0,0))

// TODO(torgrim): try and organize this file so that
// you have type declarations separated from functions
// and still group functions belong to a specific type
//
// TODO(torgrim): define some safe ratio functions so that
// we don't have to worry about dividing by zero everywhere
// but just defaults to some given value

internal inline f32 Sin(f32 value)
{
    f32 result = sinf(value);
    return result;
}

internal inline f32 Cos(f32 value)
{
    f32 result = cosf(value);
    return result;
}

internal inline f32 Tan(f32 value)
{
    f32 result = tanf(value);
    return result;
}

internal inline f32 ArcSin(f32 value)
{
    ASSERT(value >= -1.0f && value <= 1.0f);
    f32 result = asinf(value);
    return result;
}

internal inline f32 ArcCos(f32 value)
{
    ASSERT(value >= -1.0f && value <= 1.0f);
    f32 result = acosf(value);
    return result;
}

internal inline f32 ArcTan2(f32 a, f32 b)
{
    f32 result = atan2f(a, b);
    return result;
}

internal inline f32 Abs(f32 a)
{
    f32 result = fabsf(a);
    return result;
}

internal inline f32 SquareRoot(f32 value)
{
    f32 result = sqrtf(value);
    return result;
}

internal inline f32 Pow2(f32 value)
{
    f32 result = value*value;
    return result;
}

internal inline f32 FloorF32(f32 value)
{
    f32 result = floorf(value);
    return result;
}

internal inline f32 CeilF32(f32 value)
{
    f32 result = ceilf(value);
    return result;
}

internal inline f32 RadianToDegree(f32 radian)
{
    f32 result = (180.0f * radian) / PI;

    return result;
}

internal inline f32 DegreeToRadians(f32 degrees)
{
    f32 result = (PI / 180.0f) * degrees;

    return result;
}

internal inline f32 Max(f32 a, f32 b)
{
    f32 result = (a >= b) ? a : b;
    return result;
}

internal inline f32 PowF32(f32 v, f32 e)
{
    f32 result = powf(v, e);
    return result;
}

internal inline f32 RoundF32(f32 value)
{
    f32 result = roundf(value);
    return result;
}

// TODO(torgrim): this can probably be done
// a lot better.
// TODO(torgrim): make this for v2, v3, v4 and also
// one that defaults to min = 0, max = 1
internal inline f32 Clamp(f32 a, f32 min, f32 max)
{
    f32 result = a;
    if(a < min)
    {
        result = min;
    }
    else if(a > max)
    {
        result = max;
    }

    return result;
}

struct v2
{
    f32 x;
    f32 y;

    v2() = default;

    v2(f32 a, f32 b)
    {
        x = a;
        y = b;
    }

    f32& operator[](i32 index)
    {
        f32& result = ((&x)[index]);
        return result;
    }
};

internal inline v2 operator+(v2 a, v2 b)
{
    v2 result = v2(a.x + b.x, a.y + b.y);
    return result;
}

internal inline v2 operator-(v2 a, v2 b)
{
    v2 result = v2(a.x - b.x, a.y - b.y);
    return result;
}

internal inline v2 operator*(v2 a, f32 scalar)
{
    v2 result = a;
    result.x *= scalar;
    result.y *= scalar;

    return result;
}

internal inline v2 operator*(v2 a, v2 b)
{
    v2 result = v2(a.x * b.x, a.y * b.y);
    return result;
}

internal inline v2 operator*(f32 scalar, v2 a)
{
    v2 result = a;
    result.x *= scalar;
    result.y *= scalar;

    return result;
}

internal inline v2 operator/(v2 a, f32 s)
{
    ASSERT(s != 0.0f);
    v2 result = a;
    result.x /= s;
    result.y /= s;

    return result;
}

internal inline bool operator==(v2 a, v2 b)
{
    bool result = (a.x == b.x && a.y == b.y);
    return result;
}

internal inline bool operator!=(v2 a, v2 b)
{
    bool result = (a.x != b.x || a.y != b.y);
    return result;
}

internal inline f32 LengthSquared(v2 a)
{
    f32 result = (a.x*a.x) + (a.y*a.y);
    return result;
}

internal inline f32 Length(v2 a)
{
    f32 l2 = LengthSquared(a);
    f32 result = SquareRoot(l2);

    return result;
}

internal inline v2 Normalize(v2 a)
{
    f32 l = Length(a);

    ASSERT(l != 0.0f);

    v2 result = a / l;

    return result;
}

internal inline v2 Lerp(v2 p0, v2 p1, f32 t)
{
    v2 result = (1.0f-t)*p0 + (t*p1);

    return result;
}

struct v2i
{
    i32 x;
    i32 y;

    v2i() = default;
    v2i(i32 a, i32 b)
    {
        x = a;
        y = b;
    }
};

internal inline v2i operator+(v2i a, v2i b)
{
    v2i result = v2i(a.x + b.x, a.y + b.y);
    return result;
}

internal inline bool operator==(v2i a, v2i b)
{
    bool result = (a.x == b.x && a.y == b.y);
    return result;
}

internal inline bool operator!=(v2i a, v2i b)
{
    bool result = (a.x != b.x || a.y != b.y);
    return result;
}

// TODO(torgrim): implement this properly
struct v3
{
    f32 x;
    f32 y;
    f32 z;

    v3() = default;

    v3(f32 a, f32 b, f32 c)
    {
        x = a;
        y = b;
        z = c;
    }

    f32& operator[](i32 index)
    {
        f32& result = ((&x)[index]);
        return result;
    }
};

internal inline v3 operator*(v3 a, v3 b)
{
    v3 result = v3(a.x * b.x, a.y * b.y, a.z * b.z);
    return result;
}

internal inline v3 operator*(v3 a, f32 s)
{
    v3 result = v3(a.x * s, a.y * s, a.z * s);
    return result;
}

internal inline v3 operator*(f32 s, v3 a)
{
    v3 result = a * s;
    return result;
}

internal inline v3 operator-(v3 a, v3 b)
{
    v3 result = v3(a.x - b.x, a.y - b.y, a.z - b.z);
    return result;
}

internal inline v3 operator-(v3 a)
{
    v3 result = v3(-a.x, -a.y, -a.z);
    return result;
}

internal inline v3 operator+(v3 a, v3 b)
{
    v3 result = v3(a.x + b.x, a.y + b.y, a.z + b.z);
    return result;
}

internal inline v3 operator/(v3 a, f32 s)
{
    ASSERT(s != 0.0f);
    v3 result = v3(a.x / s, a.y / s, a.z / s);
    return result;
}

internal inline v3& operator/=(v3 &a, f32 s)
{
    ASSERT(s != 0.0f);
    a = a / s;
    return a;
}

internal inline v3& operator+=(v3 &a, v3 b)
{
    a = a + b;

    return a;
}

internal inline v3& operator*=(v3 &a, f32 s)
{
    a = a * s;

    return a;
}

internal inline v3& operator*=(v3 &a, v3 b)
{
    a = a * b;

    return a;
}

internal inline f32 LengthSquared(v3 a)
{
    f32 ls = (a.x * a.x) + (a.y * a.y) + (a.z * a.z);
    return ls;
}

internal inline f32 Length(v3 a)
{
    f32 ls = LengthSquared(a);
    f32 l = SquareRoot(ls);

    return l;
}

internal inline f32 Dot(v3 a, v3 b)
{
    f32 result = (a.x * b.x + a.y * b.y + a.z * b.z);

    return result;
}

internal inline v3 Cross(v3 a, v3 b)
{
    v3 result = v3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
    return result;
}

internal inline v3 Normalize(v3 a)
{
    f32 l = Length(a);
    ASSERT(l != 0.0f);
    v3 result = a / l;
    return result;
}

internal inline bool IsNormalized(v3 a)
{
    f32 value = 1.0f - LengthSquared(a);
    bool result = Abs(value) < IS_NORMALIZED_EPSILON;
    return result;
}

internal inline v3 Project(v3 a, v3 b)
{
    f32 l = Length(b);
    ASSERT(l != 0.0f);
    v3 result = (Dot(a, b) / l) * b;

    return result;
}

internal inline v3 Reject(v3 a, v3 b)
{
    v3 result = a - Project(a, b);

    return result;
}

// TODO(torgrim): make this operator== instead?
internal inline bool VectorsEqual(v3 a, v3 b)
{
    bool result = (a.x == b.x && a.y == b.y && a.z == b.z);
    return result;
}

// TODO(torgrim): implement v4 properly with helper functions
// and operators
struct v4
{
    f32 x;
    f32 y;
    f32 z;
    f32 w;

    v4() = default;

    v4(f32 a, f32 b, f32 c, f32 d)
    {
        x = a;
        y = b;
        z = c;
        w = d;
    }

    v4(v3 v, f32 d)
    {
        x = v.x;
        y = v.y;
        z = v.z;
        w = d;
    }

    f32& operator[](i32 index)
    {
        f32& result = ((&x)[index]);
        return result;
    }
};

internal inline v4 operator/(v4 a, f32 s)
{
    v4 result = v4(a.x / s, a.y / s, a.z / s, a.w / s);
    return result;
}

internal inline v4 operator*(v4 a, f32 s)
{
    v4 result(a.x * s, a.y * s, a.z * s, a.w * s);
    return result;
}

internal inline v4 operator-(v4 a, v4 b)
{
    v4 result(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
    return result;
}

internal inline f32 LengthSquared(v4 a)
{
    f32 ls = (a.x * a.x) + (a.y * a.y) + (a.z * a.z) + (a.w * a.w);
    return ls;
}

internal inline f32 Length(v4 a)
{
    f32 ls = LengthSquared(a);
    f32 l = SquareRoot(ls);

    return l;
}

internal inline v4 Normalize(v4 a)
{
    f32 l = Length(a);
    ASSERT(l != 0.0f);
    v4 result = a / l;
    return result;
}

internal inline bool IsNormalized(v4 a)
{
    f32 value = 1.0f - LengthSquared(a);
    bool result = Abs(value) < IS_NORMALIZED_EPSILON;
    return result;
}

internal inline v3 V4ToV3(v4 a)
{
    v3 result(a.x, a.y, a.z);
    return result;
}

struct quaternion
{
    f32 x;
    f32 y;
    f32 z;
    f32 w;

    quaternion() = default;
    quaternion(f32 a, f32 b, f32 c, f32 d)
    {
        x = a;
        y = b;
        z = c;
        w = d;
    }
};

internal inline quaternion operator*(quaternion a, quaternion b)
{
    f32 i = a.x*b.w + a.y*b.z - a.z*b.y + a.w*b.x;
    f32 j = a.y*b.w + a.z*b.x + a.w*b.y - a.x*b.z;
    f32 k = a.z*b.w + a.w*b.z + a.x*b.y - a.y*b.x;
    f32 s = a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z;

    quaternion q;
    q.x = i;
    q.y = j;
    q.z = k;
    q.w = s;

    return q;
}

struct matrix2D
{
    private:
        f32 n[2][2];

    public:
        matrix2D() = default;

        matrix2D(f32 n00, f32 n01,
                 f32 n10, f32 n11)
        {
            n[0][0] = n00;
            n[0][1] = n10;
            n[1][0] = n01;
            n[1][1] = n11;
        }

        f32 operator()(i32 i, i32 j) const
        {
            return n[j][i];
        }

};

internal inline matrix2D operator *(const matrix2D a, const matrix2D b)
{
    matrix2D result = matrix2D(a(0,0) * b(0,0) + a(0,1) * b(1,0),
                               a(0,0) * b(0,1) + a(0,1) * b(1,1),
                               a(1,0) * b(0,0) + a(1,1) * b(1,0),
                               a(1,0) * b(0,1) + a(1,1) * b(1,1));

    return result;
}

struct matrix3D
{
    protected:
        f32 n[3][3];

    public:
        matrix3D() = default;

        matrix3D(f32 n00, f32 n01, f32 n02,
                 f32 n10, f32 n11, f32 n12,
                 f32 n20, f32 n21, f32 n22)
        {
            n[0][0] = n00;
            n[0][1] = n10;
            n[0][2] = n20;

            n[1][0] = n01;
            n[1][1] = n11;
            n[1][2] = n21;

            n[2][0] = n02;
            n[2][1] = n12;
            n[2][2] = n22;
        }

        f32& operator()(i32 i, i32 j)
        {
            return (n[j][i]);
        }

        const f32& operator()(i32 i, i32 j) const
        {
            return (n[j][i]);
        }
};

internal inline matrix3D operator*(const matrix3D a, const matrix3D b)
{
    matrix3D result = matrix3D(
                               a(0,0) * b(0,0) + a(0,1) * b(1,0) + a(0,2) * b(2,0),
                               a(0,0) * b(0,1) + a(0,1) * b(1,1) + a(0,2) * b(2,1),
                               a(0,0) * b(0,2) + a(0,1) * b(1,2) + a(0,2) * b(2,2),

                               a(1,0) * b(0,0) + a(1,1) * b(1,0) + a(1,2) * b(2,0),
                               a(1,0) * b(0,1) + a(1,1) * b(1,1) + a(1,2) * b(2,1),
                               a(1,0) * b(0,2) + a(1,1) * b(1,2) + a(1,2) * b(2,2),

                               a(2,0) * b(0,0) + a(2,1) * b(1,0) + a(2,2) * b(2,0),
                               a(2,0) * b(0,1) + a(2,1) * b(1,1) + a(2,2) * b(2,1),
                               a(2,0) * b(0,2) + a(2,1) * b(1,2) + a(2,2) * b(2,2));
    return result;
}

internal inline matrix3D operator*=(matrix3D& a, const matrix3D b)
{
    a = a * b;

    return a;
}

internal inline v3 operator*(const matrix3D m, const v3 v)
{
    v3 result = v3(m(0,0) * v.x + m(0,1) * v.y + m(0,2) * v.z,
                   m(1,0) * v.x + m(1,1) * v.y + m(1,2) * v.z,
                   m(2,0) * v.x + m(2,1) * v.y + m(2,2) * v.z);

    return result;
}

struct matrix4D
{
    protected:

        f32 n[4][4];
    public:
        matrix4D() = default;

        matrix4D(f32 n00, f32 n01, f32 n02, f32 n03,
                 f32 n10, f32 n11, f32 n12, f32 n13,
                 f32 n20, f32 n21, f32 n22, f32 n23,
                 f32 n30, f32 n31, f32 n32, f32 n33)
        {
            n[0][0] = n00;
            n[0][1] = n10;
            n[0][2] = n20;
            n[0][3] = n30;

            n[1][0] = n01;
            n[1][1] = n11;
            n[1][2] = n21;
            n[1][3] = n31;

            n[2][0] = n02;
            n[2][1] = n12;
            n[2][2] = n22;
            n[2][3] = n32;

            n[3][0] = n03;
            n[3][1] = n13;
            n[3][2] = n23;
            n[3][3] = n33;
        }

        float& operator()(i32 i, i32 j)
        {
            return (n[j][i]);
        }

        const float& operator()(i32 i, i32 j) const
        {
            return (n[j][i]);
        }
};

internal inline matrix4D operator*(const matrix4D a, const matrix4D b)
{
    matrix4D result = matrix4D(
            a(0,0) * b(0,0) + a(0,1) * b(1,0) + a(0,2) * b(2,0) + a(0,3) * b(3,0),
            a(0,0) * b(0,1) + a(0,1) * b(1,1) + a(0,2) * b(2,1) + a(0,3) * b(3,1),
            a(0,0) * b(0,2) + a(0,1) * b(1,2) + a(0,2) * b(2,2) + a(0,3) * b(3,2),
            a(0,0) * b(0,3) + a(0,1) * b(1,3) + a(0,2) * b(2,3) + a(0,3) * b(3,3),

            a(1,0) * b(0,0) + a(1,1) * b(1,0) + a(1,2) * b(2,0) + a(1,3) * b(3,0),
            a(1,0) * b(0,1) + a(1,1) * b(1,1) + a(1,2) * b(2,1) + a(1,3) * b(3,1),
            a(1,0) * b(0,2) + a(1,1) * b(1,2) + a(1,2) * b(2,2) + a(1,3) * b(3,2),
            a(1,0) * b(0,3) + a(1,1) * b(1,3) + a(1,2) * b(2,3) + a(1,3) * b(3,3),

            a(2,0) * b(0,0) + a(2,1) * b(1,0) + a(2,2) * b(2,0) + a(2,3) * b(3,0),
            a(2,0) * b(0,1) + a(2,1) * b(1,1) + a(2,2) * b(2,1) + a(2,3) * b(3,1),
            a(2,0) * b(0,2) + a(2,1) * b(1,2) + a(2,2) * b(2,2) + a(2,3) * b(3,2),
            a(2,0) * b(0,3) + a(2,1) * b(1,3) + a(2,2) * b(2,3) + a(2,3) * b(3,3),

            a(3,0) * b(0,0) + a(3,1) * b(1,0) + a(3,2) * b(2,0) + a(3,3) * b(3,0),
            a(3,0) * b(0,1) + a(3,1) * b(1,1) + a(3,2) * b(2,1) + a(3,3) * b(3,1),
            a(3,0) * b(0,2) + a(3,1) * b(1,2) + a(3,2) * b(2,2) + a(3,3) * b(3,2),
            a(3,0) * b(0,3) + a(3,1) * b(1,3) + a(3,2) * b(2,3) + a(3,3) * b(3,3));

    return result;
}

internal inline matrix4D CreateZeroMatrix4D()
{
    matrix4D result = matrix4D(0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f);

    return result;
}

internal inline matrix4D CreateIdentity4D()
{
    matrix4D result = matrix4D(1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}


internal inline matrix4D operator*=(matrix4D& a, const matrix4D b)
{
    a = a * b;

    return a;
}

internal inline v4 operator*(const matrix4D m, const v4 v)
{
    v4 result = v4(m(0,0) * v.x + m(0,1) * v.y + m(0,2) * v.z + m(0,3) * v.w,
                   m(1,0) * v.x + m(1,1) * v.y + m(1,2) * v.z + m(1,3) * v.w,
                   m(2,0) * v.x + m(2,1) * v.y + m(2,2) * v.z + m(2,3) * v.w,
                   m(3,0) * v.x + m(3,1) * v.y + m(3,2) * v.z + m(3,3) * v.w);

    return result;
}

// TODO(torgrim): Strictly not allowed in math so we might
// want to make this more explicit by making it an actual function
// instead of operator overload.
internal inline v4 operator*(const matrix4D m, const v3 v)
{
    v4 result = v4(m(0,0) * v.x + m(0,1) * v.y + m(0,2) * v.z + m(0,3) * 1.0f,
                   m(1,0) * v.x + m(1,1) * v.y + m(1,2) * v.z + m(1,3) * 1.0f,
                   m(2,0) * v.x + m(2,1) * v.y + m(2,2) * v.z + m(2,3) * 1.0f,
                   m(3,0) * v.x + m(3,1) * v.y + m(3,2) * v.z + m(3,3) * 1.0f);

    return result;
}

// NOTE(torgrim): matrix 3D methods
// TODO(torgrim): not sure about function like this
// that transforms from->to. Should the from type
// come first in the name?
internal inline matrix3D Matrix4DToMatrix3D(matrix4D m)
{
    matrix3D result = matrix3D(m(0,0), m(0,1), m(0,2),
                               m(1,0), m(1,1), m(1,2),
                               m(2,0), m(2,1), m(2,2));

    return result;
}

internal inline matrix3D Transpose3D(matrix3D mat)
{
    matrix3D result = matrix3D(mat(0,0), mat(1,0), mat(2,0),
                               mat(0,1), mat(1,1), mat(2,1),
                               mat(0,2), mat(1,2), mat(2,2));

    return result;
}

// TODO(torgrim): make this more explicit that
// it's using angles in degree.
internal inline matrix3D XRotate3D(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix3D result = matrix3D(1.0f, 0.0f, 0.0f,
                               0.0f, c,    -s,
                               0.0f, s,     c);

    return result;
}

internal inline matrix3D YRotate3D(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix3D result = matrix3D(c,       0.0f,   s,
                               0.0f,    1.0f,   0.0f,
                               -s,      0.0f,   c);

    return result;
}

internal inline matrix3D ZRotate3D(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix3D result = matrix3D(c,       -s,     0.0f,
                               s,        c,     0.0f,
                               0.0f,     0.0f,  1.0f);

    return result;
}

// TODO(torgrim): make it more explicit which order
// we are applying the rotation and add other options as well
// so that it's easier to see the difference between them
internal inline matrix3D CreateRotate3D(f32 angleX, f32 angleY, f32 angleZ)
{
    matrix3D result = ZRotate3D(angleZ) * XRotate3D(angleX) * YRotate3D(angleY);
    return result;
}

// TODO(torgrim): make it more explicit which order
// we are applying the rotation and add other options as well
// so that it's easier to see the difference between them
internal inline matrix3D CreateRotate3D(v3 angles)
{
    matrix3D result = CreateRotate3D(angles.x, angles.y, angles.z);

    return result;
}

internal inline matrix3D CreateRotateAboutAxis3D(f32 angle, v3 u)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    f32 t = 1.0f - c;


    matrix3D result = matrix3D(t * Pow2(u.x) + c, t*u.x*u.y - s*u.z, t*u.x*u.z + s*u.y,
                               t*u.x*u.y + s*u.z, t * Pow2(u.y) + c, t*u.y*u.z - s*u.x,
                               t*u.x*u.z - s*u.y, t*u.y*u.z + s*u.x, t * Pow2(u.z) + c);

    return result;
}

internal inline matrix3D CreateScale3D(f32 x, f32 y, f32 z)
{
    matrix3D result = matrix3D(x,       0.0f,   0.0f,
                               0.0f,    y,      0.0f,
                               0.0f,    0.0f,   z);

    return result;
}

internal inline matrix3D CreateScale3D(v3 v)
{
    matrix3D result = matrix3D(v.x,    0.0f,   0.0f,
                               0.0f,    v.y,   0.0f,
                               0.0f,    0.0f,   v.z);

    return result;
}

// NOTE(torgrim): matrix 4D methods
// TODO(torgrim): make it more explicit that we are doing this
// with 4d matrices.
internal inline matrix4D Transpose(matrix4D mat)
{
    matrix4D result = matrix4D(mat(0,0), mat(1,0), mat(2,0), mat(3,0),
                               mat(0,1), mat(1,1), mat(2,1), mat(3,1),
                               mat(0,2), mat(1,2), mat(2,2), mat(3,2),
                               mat(0,3), mat(1,3), mat(2,3), mat(3,3));

    return result;
}


// TODO(torgrim): make this more explicit that
// it's using angles in degree.
internal inline matrix4D XRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix4D result = matrix4D(1.0f, 0.0f, 0.0f, 0.0f,
                               0.0f, c,    -s,   0.0f,
                               0.0f, s,     c,   0.0f,
                               0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}

internal inline matrix4D YRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix4D result = matrix4D(c,       0.0f,   s,      0.0f,
                               0.0f,    1.0f,   0.0f,   0.0f,
                               -s,      0.0f,   c,      0.0f,
                               0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

internal inline matrix4D ZRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix4D result = matrix4D(c,       -s,     0.0f,   0.0f,
                               s,        c,     0.0f,   0.0f,
                               0.0f,     0.0f,  1.0f,   0.0f,
                               0.0f,     0.0f,  0.0f,   1.0f);

    return result;
}

// TODO(torgrim): make it more explicit which order
// we are applying the rotation and add other options as well
// so that it's easier to see the difference between them
internal inline matrix4D CreateRotate(f32 angleX, f32 angleY, f32 angleZ)
{
    matrix4D result = ZRotate(angleZ) * XRotate(angleX) * YRotate(angleY);
    return result;
}

// TODO(torgrim): make it more explicit which order
// we are applying the rotation and add other options as well
// so that it's easier to see the difference between them
internal inline matrix4D CreateRotate(v3 angles)
{
    matrix4D result = CreateRotate(angles.x, angles.y, angles.z);

    return result;
}

internal inline matrix4D CreateRotateAboutAxis(f32 angle, v3 u)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    f32 t = 1.0f - c;


    matrix4D result = matrix4D(t * Pow2(u.x) + c, t*u.x*u.y - s*u.z, t*u.x*u.z + s*u.y, 0,
                               t*u.x*u.y + s*u.z, t * Pow2(u.y) + c, t*u.y*u.z - s*u.x, 0,
                               t*u.x*u.z - s*u.y, t*u.y*u.z + s*u.x, t * Pow2(u.z) + c, 0,
                               0, 0, 0, 1);

    return result;
}

internal inline matrix4D CreateTranslate(f32 x, f32 y, f32 z)
{
    matrix4D result = matrix4D(1.0f, 0.0f, 0.0f, x,
                               0.0f, 1.0f, 0.0f, y,
                               0.0f, 0.0f, 1.0f, z,
                               0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}

internal inline matrix4D CreateTranslate(v3 v)
{
    matrix4D result = matrix4D(1.0f, 0.0f, 0.0f, v.x,
                               0.0f, 1.0f, 0.0f, v.y,
                               0.0f, 0.0f, 1.0f, v.z,
                               0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}

internal inline matrix4D CreateScale(f32 s)
{
    matrix4D result = matrix4D(s,       0.0f,   0.0f,   0.0f,
                               0.0f,    s,      0.0f,   0.0f,
                               0.0f,    0.0f,   s,      0.0f,
                               0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

internal inline matrix4D CreateScale(f32 x, f32 y, f32 z)
{
    matrix4D result = matrix4D(x,       0.0f,   0.0f,   0.0f,
                               0.0f,    y,      0.0f,   0.0f,
                               0.0f,    0.0f,   z,      0.0f,
                               0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

internal inline matrix4D CreateScale(v3 v)
{
    matrix4D result = matrix4D(v.x,    0.0f,   0.0f,    0.0f,
                               0.0f,    v.y,   0.0f,    0.0f,
                               0.0f,    0.0f,   v.z,    0.0f,
                               0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

// TODO(torgrim): Create correct version of this.
internal inline matrix4D CreateOrtho2D(f32 l, f32 r, f32 b, f32 t)
{
    matrix4D result = matrix4D(2.0f / (r-l), 0.0f,          0.0f, -((r+l)/(r-l)),
                               0.0f,         2.0f / (t-b),  0.0f, -((t+b)/(t-b)),
                               0.0f,         0.0f,         -1.0f, 0.0f,
                               0.0f,         0.0f,          0.0f, 1.0f);

    return result;
}

// TODO(torgrim): implement proper support for orthographic 3d viewing
// in the engine.
// TODO(torgrim): Create correct version of this.
internal inline matrix4D CreateOrtho3D(f32 l, f32 r, f32 b, f32 t, f32 n, f32 f)
{
    matrix4D result = matrix4D(2.0f / (r-l), 0.0f,          0.0f,         -(r+l) / (r-l),
                               0.0f,         2.0f / (t-b),  0.0f,         -(t+b) / (t-b),
                               0.0f,         0.0f,         -2.0f / (f-n), -(f+n) / (f-n),
                               0.0f,         0.0f,          0.0f,         1.0f);

    return result;
}

// TODO(torgrim): Create correct version of this.
internal inline matrix4D CreateOrtho3D(f32 w, f32 h, f32 d)
{
    matrix4D result = matrix4D(2.0f / w,     0.0f,      0.0f,       0.0f,
                               0.0f,         2.0f / h,  0.0f,       0.0f,
                               0.0f,         0.0f,     -2.0f / d,   0.0f,
                               0.0f,         0.0f,      0.0f,       1.0f);

    return result;
}


// TODO(torgrim): get a better understanding of how this is built
// and what each index does to a point(vertex).
// TODO(torgrim): this is currently not a rev frustum but
// a normal frustum
internal inline matrix4D CreateRevFrustum(f32 fov_y, f32 s, f32 n, f32 f)
{
    f32 g = 1.0f / tanf(fov_y * 0.5f);

    matrix4D result = matrix4D(g / s,    0.0f,    0.0f,   0.0f,
                               0.0f,     g,       0.0f,   0.0f,
                               0.0f,     0.0f,   -((f + n) / (f - n)), -((2.0f * f * n) / (f - n)),
                               0.0f,     0.0f,   -1.0f,   0.0f);

    return result;
}

internal inline matrix4D InvertTransform4D(matrix4D m)
{
    v3 a = v3(m(0,0), m(1,0), m(2,0));
    v3 b = v3(m(0,1), m(1,1), m(2,1));
    v3 c = v3(m(0,2), m(1,2), m(2,2));
    v3 d = v3(m(0,3), m(1,3), m(2,3));

    v3 s = Cross(a,b);
    v3 t = Cross(c,d);

    f32 invDet = 1.0f/Dot(s,c);

    s *= invDet;
    t *= invDet;

    v3 v = c * invDet;
    v3 r0 = Cross(b,v);
    v3 r1 = Cross(v,a);

    matrix4D result = matrix4D(r0.x,    r0.y,   r0.z,   -Dot(b,t),
                               r1.x,    r1.y,   r1.z,    Dot(a,t),
                               s.x,     s.y,    s.z,    -Dot(d,s),
                               0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

internal inline matrix4D InvertMatrix4D(matrix4D m)
{
    v3 a = v3(m(0,0), m(1,0), m(2,0));
    v3 b = v3(m(0,1), m(1,1), m(2,1));
    v3 c = v3(m(0,2), m(1,2), m(2,2));
    v3 d = v3(m(0,3), m(1,3), m(2,3));

    f32 x = m(3,0);
    f32 y = m(3,1);
    f32 z = m(3,2);
    f32 w = m(3,3);

    v3 s = Cross(a,b);
    v3 t = Cross(c,d);
    v3 u = a * y - b * x;
    v3 v = c * w - d * z;

    f32 invDet = 1.0f / (Dot(s,v) + Dot(t, u));

    s *= invDet;
    t *= invDet;
    u *= invDet;
    v *= invDet;

    v3 r0 = Cross(b,v) + t * y;
    v3 r1 = Cross(v,a) - t * x;
    v3 r2 = Cross(d,u) + s * w;
    v3 r3 = Cross(u,c) - s * z;

    matrix4D result = matrix4D(r0.x,    r0.y,   r0.z,   -Dot(b,t),
                               r1.x,    r1.y,   r1.z,    Dot(a,t),
                               r2.x,    r2.y,   r2.z,   -Dot(d,s),
                               r3.x,    r3.y,   r3.z,    Dot(c,s));

    return result;
}

// TODO(torgrim): Handle when up and direction is
// the same vector.
internal inline matrix4D LookAt(v3 origin, v3 target, v3 up)
{
    v3 dir = Normalize(origin - target);
    ASSERT(!VectorsEqual(dir, up));

    v3 right = Normalize(Cross(up, dir));
    v3 localUp = Normalize(Cross(dir, right));

    matrix4D view = matrix4D(right.x,       right.y,        right.z,        0.0f,
                             localUp.x,     localUp.y,      localUp.z,      0.0f,
                             dir.x,         dir.y,          dir.z,          0.0f,
                             0.0f,          0.0f,           0.0f,           1.0f);

    view = view * CreateTranslate(-origin.x, -origin.y, -origin.z);

    return view;
}

struct trs_holder
{
    // TODO(torgrim): Rename to position/location.
    v3 translation;
    v3 rotation;
    v3 scale;
};

// NOTE(torgrim): This also only works when the matrix have been composed of only
// a single rotation or rotations in the order zxy.
// TODO(torgrim): When we implement quaternions we want to return those instead
// of euler angles when decomposing since that is what we will be using
// as default for rotation. It's also what gltf uses for rotation.
internal inline trs_holder GetDecomposedTRSFromTRSMatrix(matrix4D m)
{
    // TODO(torgrim): Here we can see the benefit of being able
    // to treat matrix columns as vectors. Consider implementing this functionality.
    trs_holder result = {};

    result.translation = v3(m(0,3), m(1,3), m(2,3));
    v3 c0 = v3(m(0,0), m(1,0), m(2,0));
    v3 c1 = v3(m(0,1), m(1,1), m(2,1));
    v3 c2 = v3(m(0,2), m(1,2), m(2,2));

    f32 sx = Length(c0);
    f32 sy = Length(c1);
    f32 sz = Length(c2);
    result.scale = v3(sx, sy, sz);

    ASSERT(sx != 0.0f && sy != 0.0f && sz != 0.0f);
    v3 r0 = (c0 / sx);
    v3 r1 = (c1 / sy);
    v3 r2 = (c2 / sz);

    result.rotation = v3(RadianToDegree(ArcSin(r1.z)), RadianToDegree(ArcTan2(-r0.z, r2.z)), RadianToDegree(ArcTan2(-r1.x, r1.y)));

    return result;
}

internal inline matrix4D CalculateTRSMatrixFromTRS(trs_holder *trs)
{
    matrix4D result = CreateTranslate(trs->translation) * CreateRotate(trs->rotation) * CreateScale(trs->scale);
    return result;
}

// TODO(torgrim): This can only be used on pure rotation matrices right? If the matrix also
// includes a scale we need to remove the scale factor first.
// TODO(torgrim): Handle edge cases for these calculations, especially when it comes
// to quaternions.
// NOTE(torgrim): This also only works when the matrix have been composed of only
// a single rotation or rotations in the order zxy.
internal inline v3 CalcEulerFromRotation(matrix4D m)
{
    v3 result;
    result.x = RadianToDegree(ArcSin(m(2,1)));
    result.y = RadianToDegree(ArcTan2(-m(2,0), m(2,2)));
    result.z = RadianToDegree(ArcTan2(-m(0,1), m(1,1)));

    return result;
}


internal inline v3 CalcEulerFromQuaternion(quaternion a)
{
    v3 result;
    f32 xValue = 2.0f*(a.y*a.z + a.x*a.w);
    xValue = Clamp(xValue, -1.0f, 1.0f);
    result.x = RadianToDegree(ArcSin(xValue));
    result.y = RadianToDegree(ArcTan2(-2.0f*(a.x*a.z - a.y*a.w), 1.0f - 2.0f*(a.x*a.x) - 2.0f*(a.y*a.y)));
    result.z = RadianToDegree(ArcTan2(-2.0f*(a.x*a.y - a.z*a.w), 1.0f - 2.0f*(a.x*a.x) - 2.0f*(a.z*a.z)));

    return result;
}

// NOTE(torgrim): Euler angels are always stored in degrees
internal inline quaternion CalcQuaternionFromEuler(v3 eulerAngles)
{
    quaternion qx = {Sin(DegreeToRadians(eulerAngles.x*0.5f)), 0.0f, 0.0f, Cos(DegreeToRadians(eulerAngles.x*0.5f))};
    quaternion qy = {0.0f, Sin(DegreeToRadians(eulerAngles.y*0.5f)), 0.0f, Cos(DegreeToRadians(eulerAngles.y*0.5f))};
    quaternion qz = {0.0f, 0.0f, Sin(DegreeToRadians(eulerAngles.z*0.5f)), Cos(DegreeToRadians(eulerAngles.z*0.5f))};

    quaternion result = qz*qx*qy;
    return result;
}

// TODO: Find a better solution for this
#define zeroVector2D v2(0.0f, 0.0f)
#define axisVectorX v3(1.0f, 0.0f, 0.0f)
#define axisVectorY v3(0.0f, 1.0f, 0.0f)
#define axisVectorZ v3(0.0f, 0.0f, 1.0f)
#define zeroVector3D v3(0.0f, 0.0f, 0.0f)

internal inline v3 CalcXAxisFromRot(v3 rot)
{
    v3 result = CreateRotate3D(rot) * axisVectorX;
    return result;
}

internal inline v3 CalcYAxisFromRot(v3 rot)
{
    v3 result = CreateRotate3D(rot) * axisVectorY;
    return result;
}

internal inline v3 CalcZAxisFromRot(v3 rot)
{
    v3 result = CreateRotate3D(rot) * axisVectorZ;
    return result;
}


struct mesh_transform
{
    v3 translation;
    v3 rotation;
    v3 scale;
};

internal inline v2 FindMinMax(f32 *list, size_t count)
{
    f32 min = TBS_FLT_INF;
    f32 max = TBS_NEG_FLT_INF;

    for(size_t i = 0; i < count; ++i)
    {
        f32 v = list[i];
        if(v < min) { min = v;}
        if(v > max) { max = v;}
    }

    return v2(min, max);
}

// TODO(torgrim): Add Creation of a rotation matrix from quaternions

#define TBS_MATH_H

#endif
