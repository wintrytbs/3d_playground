namespace tbs_pbr
{
    // TODO(torgrim): The names here doesn't really need to have
    // pbr since it's in the namespace. Which makes it a bit redundent
    // to write tbs_pbr::pbr_mesh instead of just tbs_pbr::mesh etc.
    enum material_alpha_mode
    {
        MATERIAL_ALPHA_MODE_Opaque,
        MATERIAL_ALPHA_MODE_Mask,
        MATERIAL_ALPHA_MODE_Blend,
    };

    enum pbr_material_texture_type
    {
        PBR_MATERIAL_TEXTURE_TYPE_BaseColor,
        PBR_MATERIAL_TEXTURE_TYPE_Normal,
        PBR_MATERIAL_TEXTURE_TYPE_MetallicRoughness,
        PBR_MATERIAL_TEXTURE_TYPE_Emissive,
        PBR_MATERIAL_TEXTURE_TYPE_Occlusion,

        PBR_MATERIAL_TEXTURE_TYPE_Count,
    };

    // TODO(torgrim): GPU/opengl resource.
    struct env_map_info
    {
        char *filename;
        GLuint hdr;
        GLuint envMap;
        GLuint diffPrefilterMap;
        GLuint specPrefilterMap;
        GLuint brdfLUT;
    };

    struct pbr_material
    {
        v4 baseColor;
        f32 metallicFactor;
        f32 roughnessFactor;
        f32 normalScaleFactor;
        f32 occlusionStrengthFactor;
        v3 emissiveFactor;
        f32 alphaCutoff;
        material_alpha_mode alphaMode;
        bool isDoubleSided;


        // TODO(torgrim): If we are going to load texture lazily we need
        // some way of knowing if the textures has been loaded or not.
        // TODO(torgrim): We also need to make sure that all textures that aren't
        // bound have a texID = 0;

        u64 textures[PBR_MATERIAL_TEXTURE_TYPE_Count];
    };

    struct pbr_primitive
    {
        u64 renderDataHandle;
        u64 materialHandle;
    };

    struct pbr_mesh
    {
        tbs_array_sr<pbr_primitive> primitives;
    };

}
