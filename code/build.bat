@echo off

set code_path=%~dp0
pushd %code_path%

set lib_path="../libs/"
set imgui_path=%lib_path%imgui/

set tbs_internal_var=TBS_INTERNAL=0

if not exist "../build/" mkdir "../build/"

pushd "../build/"

if exist "gl_function_builder.exe" del gl_function_builder.exe

if "%1"=="clang" goto compile_clang

echo [Compiling with MSVC]
rem unreferenced formal parameter
set disabled_warnings=/wd4100

rem local variable initialized but not referenced
set disabled_warnings=%disabled_warnings% /wd4189
rem unreferenced inline function has been removed
set disabled_warnings=%disabled_warnings% /wd4514
rem unreferenced local function has been removed
set disabled_warnings=%disabled_warnings% /wd4505
rem Spectre mititgation for memory load if /Qspectre switch specified
set disabled_warnings=%disabled_warnings% /wd5045
rem function considered unsafe(sprintf)
set disabled_warnings=%disabled_warnings% /wd4996
rem unreferenced local variable
set disabled_warnings=%disabled_warnings% /wd4101
rem bytes padding added after data member
set disabled_warnings=%disabled_warnings% /wd4820
rem enumerator in switch of enum not handled
set disabled_warnings=%disabled_warnings% /wd4061
rem enumerator in switch of enum not handled
set disabled_warnings=%disabled_warnings% /wd4062
rem selected for automatic inline expansion
set disabled_warnings=%disabled_warnings% /wd4711
rem function not inlined
set disabled_warnings=%disabled_warnings% /wd4710

echo [Compiling OpenGL Function Builder...]
cl /nologo /std:c++14 /O2 /FC /Wall /WX %code_path%gl_function_builder.cpp /link /INCREMENTAL:NO
echo [Done]

echo.

if exist "gl_function_builder.exe" (
    echo [Building OpenGL Function Definitions...]
    call gl_function_builder.exe > %code_path%win32_opengl_function_loader.cpp
    echo [Done]
)

echo.

echo [Building Main Project...]


cl /nologo /std:c++14 /Od /Zi /FC /D%tbs_internal_var% /Wall /WX /EHa- %disabled_warnings% %tbs_intro_define% /I%lib_path% /INCREMENTAL:NO "../code/win32_playground.cpp" User32.lib Gdi32.lib opengl32.lib
echo [Done]

goto compile_finished

:compile_clang

echo [Compiling with Clang]
set clang_link_lib=-lUser32.lib -lopengl32.lib -lGdi32.lib
set clang_disabled_warn=-Wno-c++98-compat
set clang_disabled_warn=%clang_disabled_warn% -Wno-c++98-compat-pedantic
set clang_disabled_warn=%clang_disabled_warn% -Wno-float-equal

rem NOTE: These can be enabled once in while
set clang_disabled_warn=%clang_disabled_warn% -Wno-format-security
set clang_disabled_warn=%clang_disabled_warn% -Wno-unused-function
set clang_disabled_warn=%clang_disabled_warn% -Wno-cast-align
set clang_disabled_warn=%clang_disabled_warn% -Wno-switch-enum


rem NOTE: This is just temporary
set clang_disabled_warn=%clang_disabled_warn% -Wno-old-style-cast
set clang_disabled_warn=%clang_disabled_warn% -Wno-missing-prototypes
set clang_disabled_warn=%clang_disabled_warn% -Wno-zero-as-null-pointer-constant
set clang_disabled_warn=%clang_disabled_warn% -Wno-double-promotion
set clang_disabled_warn=%clang_disabled_warn% -Wno-unused-parameter
set clang_disabled_warn=%clang_disabled_warn% -Wno-unused-variable

echo [Compiling OpenGL Function Builder...]
clang -std=c++14 -Weverything -Werror -Wno-old-style-cast -Wno-c++98-compat-pedantic -O2 -o gl_function_builder.exe %code_path%gl_function_builder.cpp
echo [Done]

if exist "gl_function_builder.exe" (
    echo [Building OpenGL Function Definitions...]
    call gl_function_builder.exe > %code_path%win32_opengl_function_loader.cpp
    echo [Done]
)

echo [Building Main Project...]
clang -std=c++14 -march=native -Weverything -D%tbs_internal_var% -Werror -O0 %clang_link_lib% -o win32_playground.exe %clang_disabled_warn% -I%lib_path% %code_path%win32_playground.cpp
echo [Done]

:compile_finished

popd
popd
