/* date = June 2nd 2020 8:49 pm */

#ifndef WIN32_PLAYGROUND_H
#define WIN32_PLAYGROUND_H
// TODO(torgrim): All of the below should not really be defined in this
// header.

enum input_key
{
    INPUT_KEY_A = 0,
    INPUT_KEY_C,
    INPUT_KEY_D,
    INPUT_KEY_E,
    INPUT_KEY_F,
    INPUT_KEY_Q,
    INPUT_KEY_R,
    INPUT_KEY_S,
    INPUT_KEY_V,
    INPUT_KEY_W,
    INPUT_KEY_X,
    INPUT_KEY_Y,
    INPUT_KEY_Z,
    INPUT_KEY_Tab,
    INPUT_KEY_LeftArrow,
    INPUT_KEY_RightArrow,
    INPUT_KEY_UpArrow,
    INPUT_KEY_DownArrow,
    INPUT_KEY_PageUp,
    INPUT_KEY_PageDown,
    INPUT_KEY_Home,
    INPUT_KEY_End,
    INPUT_KEY_Insert,
    INPUT_KEY_Delete,
    INPUT_KEY_Space,
    INPUT_KEY_Enter,
    INPUT_KEY_Backspace,
    INPUT_KEY_Escape,
    INPUT_KEY_KeyPadEnter,
    INPUT_KEY_Ctrl,
    INPUT_KEY_Shift,
    INPUT_KEY_Alt,
    INPUT_KEY_Plus,
    INPUT_KEY_Minus,

    INPUT_KEY_COUNT,
};

struct keyboard_input
{
    bool isDown;
};

struct mouse_input
{
    bool leftButtonDown;
    bool rightButtonDown;
    bool middleButtonDown;
    bool pressedThisFrame;
    v2 downPos;
    v2 origPosition;
    v2 pos;
    v2 prevPos;
    v2 rButtonDownPos;
    v2 lButtonDownPos;
    v2 mButtonDownPos;
    f32 scrollDelta;
};

struct input_state
{
    keyboard_input keyboard[INPUT_KEY_COUNT];
    mouse_input mouse;
};

constexpr u32 MAX_DEBUG_MSG_SIZE = 512;
internal void PrintDebugMsg(const char *fmt, ...)
{
    va_list argList;
    va_start(argList, fmt);
    char outBuff[MAX_DEBUG_MSG_SIZE];
    int cWritten = vsnprintf(outBuff, MAX_DEBUG_MSG_SIZE, fmt, argList);
    va_end(argList);

    if(IsDebuggerPresent())
    {
        OutputDebugString(outBuff);
    }
    else
    {
        printf("%s", outBuff);
    }
}

enum app_mode
{
    APP_MODE_None,
    APP_MODE_PBRViewer,
    APP_MODE_PBRScene,
};

typedef FILETIME platform_file_time;
struct file_data
{
    u8 *content;
    u32 length;
};

struct application_state
{
    // TODO(torgrim): Consider pulling these out into their own memory_state structure or something
    // so that functions that need to make decisions can just get a pointer to this instead of the whole app
    // state.
    const memory_arena *baseArena;
    memory_arena appArena;
    memory_arena frameArena;
    memory_arena tempArena;

    // TODO(torgrim): This should be part of the some other
    // structure
    pool_memory sceneNodePool;

    HWND windowHandle;
    f32 clientW;
    f32 clientH;

    v2i viewportSize;
    v2i viewportPos;

    input_state inputState;

    f32 dt;
    f32 runtime;

    texture_worker *textureWorker;

    app_mode mode;

    const char *exePath;
    const char *appRootPath;
    const char *texturePath;
    const char *shaderPath;
    const char *hdrPath;
};

internal bool Win32EnqueueTextureWork(texture_worker *worker, texture_queue_data data);
internal file_data Win32ReadFile(memory_arena *arena, const char *fileName, bool zeroTerminate = false);

internal inline FILETIME Win32GetLastWriteTime(const char *filename)
{
    WIN32_FILE_ATTRIBUTE_DATA fileAtt = {};
    char buffer[255];
    GetCurrentDirectory(255, buffer);
    // TODO: error handling
    if(!GetFileAttributesEx(filename, GetFileExInfoStandard, &fileAtt))
    {
        Sleep(1);
        BOOL succ = GetFileAttributesEx(filename, GetFileExInfoStandard, &fileAtt);
        if(!succ)
        {
            DWORD errorCode = GetLastError();
            ASSERT(succ);
        }
    }

    FILETIME result = fileAtt.ftLastWriteTime;
    return result;
}

internal inline file_data Win32ReadAllText(memory_arena *arena, const char *filename)
{
    file_data result = Win32ReadFile(arena, filename, true);
    return result;
}

#endif //WIN32_PLAYGROUND_H
