/* date = June 9th 2020 4:42 pm */

#ifndef TBS_IMGUI_H

namespace tbs_gui
{
    struct imgui_gl_data
    {
        u32 VAOID;
        u32 VBOID;
        u32 EBOID;
    };

    struct imgui_state
    {
        imgui_gl_data glData;
        HCURSOR cursorMap[ImGuiMouseCursor_COUNT];
        u32 characterBuffer[255];
        u32 characterBufferCount;
    };

    internal bool IsInputHandledByImGui();
    internal void DefaultRenderImGuiDrawData(application_state *app, imgui_state *imGuiState, tbs_opengl::opengl_resource_manager *rm, ImDrawData *imDD);
}
#define TBS_IMGUI_H
#endif //TBS_DEBUG_GUI_H
