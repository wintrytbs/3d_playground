/* date = June 8th 2020 9:47 pm */

#ifndef TBS_PRIMITIVE_H
#define TBS_PRIMITIVE_H

struct triangle
{
    u32 index[3];
};

struct triangle_list
{
    triangle *triangles;
    size_t count;
};

internal triangle_list CreateTriangleListU32(memory_arena *arena, u32 *indices, size_t count);
internal triangle_list CreateTriangleListU16(memory_arena *arena, u16 *indices, size_t count);
internal triangle_list CreateTriangleListU8(memory_arena *arena, u8 *indices, size_t count);
internal triangle_list CreateSimpleTriangleList(memory_arena *arena, size_t vertexCount);
internal void CalcFlatNormals(v3 *vertices, triangle_list *triangleList, v3 *result);
internal void CalcMeshTangents(memory_arena *arena,
                               v3 *vertexList, size_t vertexCount,
                               triangle *triangleList, size_t triangleCount,
                               v2 *texCoordList, size_t texCoordCount,
                               v3 *normalList,
                               v4 *tangentList);
#endif //TBS_PRIMITIVE_H
