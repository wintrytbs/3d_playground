#include "tbs_imgui.h"

namespace tbs_gui
{
    internal imgui_gl_data InitImGuiOpenGL(ImGuiIO& io)
    {
        imgui_gl_data result = {};

        glGenVertexArrays(1, &result.VAOID);
        glGenBuffers(1, &result.VBOID);

        // TODO: Issue with renderdoc capture when EBOID == 17 and we call
        // glDrawElements two or more time in a row.
        glGenBuffers(1, &result.EBOID);

        i32 fontAtlasW;
        i32 fontAtlasH;
        u8 *atlasPixels;
        io.Fonts->GetTexDataAsRGBA32(&atlasPixels, &fontAtlasW, &fontAtlasH);
        GLuint atlasTexID;
        glGenTextures(1, &atlasTexID);
        glBindTexture(GL_TEXTURE_2D, atlasTexID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, fontAtlasW, fontAtlasH, 0, GL_RGBA, GL_UNSIGNED_BYTE, atlasPixels);

        io.Fonts->TexID = (ImTextureID)(intptr_t)atlasTexID;
        IM_ASSERT(io.Fonts->IsBuilt());
        return result;
    }

    internal void InitImGuiKeyMapping(ImGuiIO& io)
    {
        io.KeyMap[ImGuiKey_Tab] =         INPUT_KEY_Tab;
        io.KeyMap[ImGuiKey_LeftArrow] =   INPUT_KEY_LeftArrow;
        io.KeyMap[ImGuiKey_RightArrow] =  INPUT_KEY_RightArrow;
        io.KeyMap[ImGuiKey_UpArrow] =     INPUT_KEY_UpArrow;
        io.KeyMap[ImGuiKey_DownArrow] =   INPUT_KEY_DownArrow;
        io.KeyMap[ImGuiKey_PageUp] =      INPUT_KEY_PageUp;
        io.KeyMap[ImGuiKey_PageDown] =    INPUT_KEY_PageDown;
        io.KeyMap[ImGuiKey_Home] =        INPUT_KEY_Home;
        io.KeyMap[ImGuiKey_End] =         INPUT_KEY_End;
        io.KeyMap[ImGuiKey_Insert] =      INPUT_KEY_Insert;
        io.KeyMap[ImGuiKey_Delete] =      INPUT_KEY_Delete;
        io.KeyMap[ImGuiKey_Backspace] =   INPUT_KEY_Backspace;
        io.KeyMap[ImGuiKey_Space] =       INPUT_KEY_Space;
        io.KeyMap[ImGuiKey_Enter] =       INPUT_KEY_Enter;
        io.KeyMap[ImGuiKey_Escape] =      INPUT_KEY_Escape;
        io.KeyMap[ImGuiKey_KeyPadEnter] = INPUT_KEY_KeyPadEnter;
        // for text edit CTRL+A: select all
        // for text edit CTRL+C: copy
        // for text edit CTRL+V: paste
        // for text edit CTRL+X: cut
        // for text edit CTRL+Y: redo
        // for text edit CTRL+Z: undo
        io.KeyMap[ImGuiKey_A] =           INPUT_KEY_A;
        io.KeyMap[ImGuiKey_C] =           INPUT_KEY_C;
        io.KeyMap[ImGuiKey_V] =           INPUT_KEY_V;
        io.KeyMap[ImGuiKey_X] =           INPUT_KEY_X;
        io.KeyMap[ImGuiKey_Y] =           INPUT_KEY_Y;
        io.KeyMap[ImGuiKey_Z] =           INPUT_KEY_Z;
    }

    // NOTE(torgrim): Win32 specific at the moment
    internal void InitImGuiCursors(imgui_state *state)
    {
        state->cursorMap[ImGuiMouseCursor_Arrow] = LoadCursor(nullptr, IDC_ARROW);
        state->cursorMap[ImGuiMouseCursor_TextInput] = LoadCursor(nullptr, IDC_IBEAM);
        state->cursorMap[ImGuiMouseCursor_ResizeAll] = LoadCursor(nullptr, IDC_SIZEALL);
        state->cursorMap[ImGuiMouseCursor_ResizeNS] = LoadCursor(nullptr, IDC_SIZENS);
        state->cursorMap[ImGuiMouseCursor_ResizeEW] = LoadCursor(nullptr, IDC_SIZEWE);
        state->cursorMap[ImGuiMouseCursor_ResizeNESW] = LoadCursor(nullptr, IDC_SIZENESW);
        state->cursorMap[ImGuiMouseCursor_ResizeNWSE] = LoadCursor(nullptr, IDC_SIZENWSE);
        state->cursorMap[ImGuiMouseCursor_Hand] = LoadCursor(nullptr, IDC_HAND);
        state->cursorMap[ImGuiMouseCursor_NotAllowed] = LoadCursor(nullptr, IDC_NO);

    }
    internal imgui_state *InitImGui(memory_arena *arena, const char *settingsFilePath)
    {
        // NOTE(torgrim): Dear IMGUI init handling

        imgui_state *state = AllocStruct(arena, imgui_state);
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO();
        io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;
        io.ConfigFlags = ImGuiConfigFlags_DockingEnable;
        io.ConfigWindowsResizeFromEdges = true;
        io.IniFilename = RawStringCreate(arena, settingsFilePath, RawStringLength(settingsFilePath));

        ImGui::StyleColorsDark();
        ImGuiStyle *style = &ImGui::GetStyle();
        ImVec4 *colors = style->Colors;
        colors[ImGuiCol_WindowBg].w = 1.0f;

        state->glData = InitImGuiOpenGL(io);
        InitImGuiKeyMapping(io);
        InitImGuiCursors(state);

        // NOTE(torgrim): end Dear IMGUI init

        return state;
    }

    internal bool IsInputHandledByImGui()
    {
        ImGuiIO& io = ImGui::GetIO();
        bool result = io.WantCaptureMouse || io.WantCaptureKeyboard;

        return result;
    }

    internal void DefaultRenderImGuiDrawData(application_state *app, imgui_state *imGuiState, tbs_opengl::opengl_resource_manager *rm, ImDrawData *imDD)
    {
        glBindVertexArray(imGuiState->glData.VAOID);

        OpenGLSetActiveTextureUnit(0);

        // NOTE(torgrim): Setting up render imgui data in opengl
        opengl_state_data openglState = OpenGLGetCurrentState();
        glViewport(0, 0, (GLsizei)app->clientW, (GLsizei)app->clientH);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendEquation(GL_FUNC_ADD);
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_SCISSOR_TEST);

        matrix4D guiProj = CreateOrtho2D(imDD->DisplayPos.x,
                                         imDD->DisplayPos.x + imDD->DisplaySize.x,
                                         imDD->DisplayPos.y + imDD->DisplaySize.y,
                                         imDD->DisplayPos.y);

        GLuint progID = rm->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_DebugGui);
        glUseProgram(progID);
        OpenGLSetUniform(progID, "tex", 0);
        OpenGLSetUniform(progID, "proj", &guiProj);

        glBindBuffer(GL_ARRAY_BUFFER, imGuiState->glData.VBOID);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, imGuiState->glData.EBOID);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid *)IM_OFFSETOF(ImDrawVert, pos));
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid *)IM_OFFSETOF(ImDrawVert, uv));
        glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid *)IM_OFFSETOF(ImDrawVert, col));

        for(i32 i = 0; i < imDD->CmdListsCount; ++i)
        {
            const ImDrawList *cmdList = imDD->CmdLists[i];

            glBufferData(GL_ARRAY_BUFFER, (GLsizei)((size_t)cmdList->VtxBuffer.Size * sizeof(ImDrawVert)), cmdList->VtxBuffer.Data, GL_STREAM_DRAW);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizei)((size_t)cmdList->IdxBuffer.Size * sizeof(ImDrawIdx)), cmdList->IdxBuffer.Data, GL_STREAM_DRAW);

            for(i32 cmdIdx = 0; cmdIdx < cmdList->CmdBuffer.Size; ++cmdIdx)
            {
                const ImDrawCmd *drawCmd = &cmdList->CmdBuffer[cmdIdx];

                // Project scissor/clipping rectangles into framebuffer space
                ImVec4 clipRect;
                ImVec2 clipOff = imDD->DisplayPos;
                ImVec2 clipScale = imDD->FramebufferScale;
                clipRect.x = (drawCmd->ClipRect.x - clipOff.x) * clipScale.x;
                clipRect.y = (drawCmd->ClipRect.y - clipOff.y) * clipScale.y;
                clipRect.z = (drawCmd->ClipRect.z - clipOff.x) * clipScale.x;
                clipRect.w = (drawCmd->ClipRect.w - clipOff.y) * clipScale.y;

                if (clipRect.x < app->clientW && clipRect.y < app->clientH && clipRect.z >= 0.0f && clipRect.w >= 0.0f)
                {
                    glScissor((i32)clipRect.x,
                              (i32)(app->clientH - clipRect.w),
                              (i32)(clipRect.z - clipRect.x),
                              (i32)(clipRect.w - clipRect.y));

                    glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)drawCmd->TextureId);
                    glDrawElements(GL_TRIANGLES,
                                   (GLsizei)drawCmd->ElemCount,
                                   sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT,
                                   (void *)(intptr_t)(drawCmd->IdxOffset * sizeof(ImDrawIdx)));
                }
            }
        }

        OpenGLRestoreState(&openglState);
        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

#if 1
        glDisable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ZERO);
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_SCISSOR_TEST);
#endif
    }
}
