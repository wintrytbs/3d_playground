#if defined(VERT_ACTIVE)

layout(location = 0) in vec2 in_vert_pos;
layout(location = 1) in vec2 in_tex_coord;

out vec2 tex_coord;

void main()
{
    tex_coord = in_tex_coord;
    gl_Position = vec4(in_vert_pos, 0.0f, 1.0f);
}

#elif defined(FRAG_ACTIVE)

in vec2 tex_coord;

out vec4 result_color;

uniform sampler2DArray shadow_maps;
uniform float shadow_index;

void main()
{
    // NOTE(torgrim): compensate for depth values
    // being almost always squeezed at towards 1(max)
    // This depends on the projection used(near/far plane)

    vec3 tex_array_coord = vec3(tex_coord.xy, shadow_index);
    float depth_value = texture(shadow_maps, tex_array_coord).r;
    depth_value = pow(depth_value, 16);
    result_color = vec4(vec3(depth_value), 1.0f);
}

#endif
