#if defined(VERT_ACTIVE)
layout(location = 0) in vec3 in_vert;

uniform float scale;
uniform mat4 world_to_proj;
uniform vec3 offset;

void main()
{
    gl_Position = world_to_proj * vec4(offset + scale * in_vert, 1.0f);
}

#elif defined(FRAG_ACTIVE)

out vec4 result_color;
uniform vec3 color;

void main()
{
    result_color = vec4(color, 1.0f);
}

#endif
