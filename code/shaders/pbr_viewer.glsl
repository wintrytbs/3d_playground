#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_vert;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec4 in_tan;

out vec2 uv;
out vec3 world_pos;
out vec3 surface_normal;
out vec4 tangent;

uniform mat4 proj_view;
uniform mat4 model;

void main()
{
    world_pos = vec3(model * vec4(in_vert, 1.0f));
    uv = in_uv;

    surface_normal = (transpose(inverse(mat3(model))) * in_normal);
    tangent = vec4(normalize(mat3(model) * in_tan.xyz), in_tan.w);

    gl_Position = proj_view * vec4(world_pos, 1.0f);
}

#elif defined(FRAG_ACTIVE)
#line 32

#define PI 3.14159265359
#define e 2.71828

#define DistanceAttenuation_InvSqrSim               0
#define DistanceAttenuation_Exp                     1
#define DistanceAttenuation_CubicPolynomial         2

#define LightType_Point                             0
#define LightType_Spot                              1
#define LightType_Directional                       2

#define ActiveSamplerFlag_BaseColor                 1u << 0
#define ActiveSamplerFlag_Normal                    1u << 1
#define ActiveSamplerFlag_MetallicRoughness         1u << 2
#define ActiveSamplerFlag_Occlusion                 1u << 3
#define ActiveSamplerFlag_Emissive                  1u << 4

#define RENDER_MODE_Full                            0
#define RENDER_MODE_Albedo                          1
#define RENDER_MODE_Normal                          2
#define RENDER_MODE_Occlusion                       3
#define RENDER_MODE_Emissive                        4
#define RENDER_MODE_Metallic                        5
#define RENDER_MODE_Roughness                       6
#define RENDER_MODE_Diffuse                         7
#define RENDER_MODE_Specular                        8
#define RENDER_MODE_Geometry                        9
#define RENDER_MODE_Distribution                    10
#define RENDER_MODE_Fresnel                         11
#define RENDER_MODE_FresnelRoughness                12

#define IsSamplerFlagActive(sampler) ((active_sampler_flag & ActiveSamplerFlag_##sampler) != 0u)

struct light_properties
{
    int type;
    vec3 color;
    vec3 position;
    vec3 dir;
    int dist_att_func;
    float max_dist;
    float falloff_start_dist;
    float spot_min_cutoff;
    float spot_max_cutoff;
};

struct light_result
{
    vec3 diffuse;
    vec3 specular;
    vec3 radiance;
};

// TODO(torgrim): Group these into vert_data struct or something
in vec2 uv;
in vec3 world_pos;
in vec3 surface_normal;
in vec4 tangent;

out vec4 result_color;

uniform vec3 camera_pos;

// TODO(torgrim): Group these into pbr material struct
uniform vec4 base_color;
uniform float metallic_factor;
uniform float roughness_factor;
uniform float alpha_cutoff;
uniform bool is_double_sided;

uniform uint active_sampler_flag;
uniform int render_mode;

uniform sampler2D base_color_map;
uniform sampler2D normal_map;
uniform sampler2D metallic_roughness_map;
uniform sampler2D emissive_map;
uniform sampler2D occlusion_map;

// NOTE: Environment map related.
uniform bool use_env_lighting;
uniform samplerCube irradiance_map;
uniform samplerCube prefilter_env;
uniform sampler2D brdf_lut;

uniform light_properties main_light;

vec3 FetchNormalFromMap(vec2 tex_pos)
{
    vec3 result = texture(normal_map, tex_pos).xyz;
    return normalize(result * 2.0f - 1.0f);
}

vec3 CalcObjectNormalFromMap(vec2 sample_uv, vec3 tan, vec3 normal, float sigma)
{
    vec3 m = FetchNormalFromMap(sample_uv);
    vec3 n = normalize(normal);
    vec3 t = normalize((tan - dot(tan, n) * n));
    vec3 b = normalize(cross(normal, tan) * sigma);

    n = t * m.x + b * m.y + n * m.z;
    return normalize(n);
}

float CalcAngleAttenuation(vec3 p, light_properties l)
{
    float att = 1.0f;
    vec3 light_to_point = normalize(p - l.position);
    float cMin = l.spot_min_cutoff;
    float cMax = l.spot_max_cutoff;
    float c = dot(light_to_point, l.dir);
    float t = 1.0f - clamp((c - cMin) / (cMax - cMin), 0.0f, 1.0f);

    att = t;
    return att;
}

float CalcDistAttenuation(vec3 p, light_properties l)
{
    float att = 1.0f;

    float dist = length(p - l.position);

    float start_dist2 = pow(l.falloff_start_dist, 2);
    float max_dist2 = pow(l.max_dist, 2);
    float dist2 = pow(dist, 2);

    if(l.dist_att_func == DistanceAttenuation_InvSqrSim)
    {
        att = (start_dist2 / (max_dist2 - start_dist2)) * ((max_dist2 / dist2) - 1.0f);
    }
    else if(l.dist_att_func == DistanceAttenuation_Exp)
    {
        att = (pow(e, (-9.0f * dist2) / max_dist2) - pow(e,-9.0f)) / (1.0f - pow(e, -9));
    }
    else if(l.dist_att_func == DistanceAttenuation_CubicPolynomial)
    {
        if(dist < l.max_dist)
        {
            att = (dist2 / max_dist2) * (((2.0f * dist) / l.max_dist) - 3.0f) + 1.0f;
        }
        else
        {
            att = 0.0f;
        }
    }

    att = clamp(att, 0.0f, 1.0f);

    return att;
}

vec3 FresnelSchlickRoughness(float cos_theta, vec3 F0, float roughness)
{
    float val = max(1.0f - cos_theta, 0.0f);
    vec3 result = F0 + (max(vec3(1.0f - roughness), F0) - F0) * pow(val, 5.0f);
    return result;
}
vec3 FresnelSchlickApprox(float cos_theta, vec3 albedo, float metallic)
{
    vec3 F0 = vec3(0.04f);
    F0 = mix(F0, albedo, metallic);
    vec3 result = F0 + (1.0f - F0) * pow(1.0f - cos_theta, 5.0f);
    return result;
}

float NDFGGX(vec3 h, vec3 n, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;

    float n_dot_h = max(dot(n,h), 0.0f);
    float n_dot_h2 = n_dot_h * n_dot_h;

    float num = a2;
    float den = (n_dot_h2 * (a2 - 1.0f) + 1.0f);
    den = PI * den * den;

    float result = num / max(den, 0.000001f);
    return result;
}

float GeometrySchlickGGX(vec3 v, vec3 n, float roughness)
{
    float r = roughness + 1.0f;
    float k = (r*r) / 8.0f;
    float n_dot_v = max(dot(n,v), 0.0f);
    float den = n_dot_v * (1.0f - k) + k;

    float result = n_dot_v / den;
    return result;
}

float GeometrySmithGGX(vec3 v, vec3 l, vec3 n, float roughness)
{
    float result = GeometrySchlickGGX(v, n, roughness) * GeometrySchlickGGX(l, n, roughness);
    return result;
}

vec2 FetchMetallicRoughness()
{
    float roughness = roughness_factor;
    float metallic = metallic_factor;
    if(IsSamplerFlagActive(MetallicRoughness))
    {
        vec4 rm_sample = texture(metallic_roughness_map, uv);
        metallic *= rm_sample.b;
        roughness *= rm_sample.g;
    }

    roughness = max(roughness, 0.045f);
    return vec2(metallic, roughness);
}

light_result CalculateLight(vec3 v, vec3 l, vec3 n, vec3 albedo, float metallic, float roughness)
{
    vec3 h = normalize(l + v);
    float att = CalcDistAttenuation(world_pos, main_light);
    if(main_light.type == LightType_Spot)
    {
        att = CalcAngleAttenuation(world_pos, main_light);
    }
    else if(main_light.type == LightType_Directional)
    {
        att = 1.0f;
    }

    vec3 radiance = main_light.color * att;
    vec3 F = FresnelSchlickApprox(max(dot(v, h), 0.0f), albedo, metallic);
    float NDF = NDFGGX(h, n, roughness);
    float G = GeometrySmithGGX(v, l, n, roughness);

    vec3 num = (F*G*NDF);
    float den = max(4.0f * (dot(n, l) * dot(n, v)), 0.0001f);
    vec3 spec = num / den;

    vec3 diffuse = vec3(1.0f) - F;
    diffuse *= (1.0f - metallic);

    light_result result;
    result.diffuse = (diffuse * albedo) / PI;
    result.specular = spec;
    result.radiance = radiance;
    return result;
}

light_result CalculateLightIBL(vec3 v, vec3 n, vec3 albedo, float metallic, float roughness)
{
    vec3 F0 = vec3(0.04f);
    F0 = mix(F0, albedo, metallic);
    vec3 h = normalize(n + v);
    vec3 kS = FresnelSchlickRoughness(max(dot(h, v), 0.0f), F0, roughness);

    vec3 kD = vec3(1.0f) - kS;
    kD *= (1.0f - metallic);
    vec3 sample_n = n;
    sample_n.z = -sample_n.z;
    vec3 irr = texture(irradiance_map, sample_n).rgb;
    vec3 diff = irr * albedo;

    vec3 r = reflect(-v, n);
    r.z = -r.z;
    vec3 prefilter_reflection = textureLod(prefilter_env, r, roughness * 4.0f).rgb;
    vec2 scale_and_bias = texture(brdf_lut, vec2(max(dot(n, v), 0.0f), roughness)).xy;
    vec3 spec_env = prefilter_reflection * (F0 * scale_and_bias.x + scale_and_bias.y);

    light_result result;
    result.diffuse = kD * diff;
    result.specular = spec_env;

    return result;
}

vec3 CalculateReflectionPBR(vec3 v, vec3 n, vec3 albedo, float metallic, float roughness)
{
    vec3 lo = vec3(0.0f);

    vec3 l_pos = main_light.position;
    vec3 l = normalize(l_pos - world_pos);
    float n_dot_l = max(dot(n,l), 0.0f);
    light_result direct_light = CalculateLight(v, l, n, albedo, metallic, roughness);
    lo += (direct_light.diffuse + direct_light.specular) * direct_light.radiance * n_dot_l;

    vec3 ambient = vec3(0.0025f) * albedo;
    if(use_env_lighting)
    {

        light_result ibl_light = CalculateLightIBL(v, n, albedo, metallic, roughness);
        ambient += ibl_light.diffuse + ibl_light.specular;
    }

    if(IsSamplerFlagActive(Occlusion))
    {
        ambient *= texture(occlusion_map, uv).r;
    }

    lo += ambient;

    if(IsSamplerFlagActive(Emissive))
    {
        vec3 emissive = texture(emissive_map, uv).rgb;
        lo += emissive;
    }

    return lo;
}

void main()
{
    vec3 v = normalize(camera_pos - world_pos);
    vec3 n = normalize(surface_normal);
    float alpha = base_color.w;
    vec3 albedo = base_color.xyz;
    vec3 color = albedo;
    if(IsSamplerFlagActive(Normal))
    {
        vec3 t = normalize(tangent.xyz);
        n = CalcObjectNormalFromMap(uv, t, n, tangent.w);
    }

    if(is_double_sided && gl_FrontFacing == false)
    {
        n = -n;
    }

    if(render_mode == RENDER_MODE_Full)
    {
        if(IsSamplerFlagActive(BaseColor))
        {
            vec4 albedo_sample = texture(base_color_map, uv);
            alpha = albedo_sample.a;
            albedo = albedo_sample.rgb;
        }

        alpha = clamp(alpha, 0.0f, 1.0f);
        if(alpha_cutoff < 1.0f && alpha < alpha_cutoff)
        {
            discard;
        }

        float roughness = roughness_factor;
        float metallic = metallic_factor;
        if(IsSamplerFlagActive(MetallicRoughness))
        {
            vec4 rm_sample = texture(metallic_roughness_map, uv);
            metallic *= rm_sample.b;
            roughness *= rm_sample.g;
        }

        roughness = max(roughness, 0.045f);
        color = CalculateReflectionPBR(v, n, albedo, metallic, roughness);
    }
    else if(render_mode == RENDER_MODE_Albedo)
    {
        if(IsSamplerFlagActive(BaseColor))
        {
            vec4 albedo_sample = texture(base_color_map, uv);
            alpha = albedo_sample.a;
            color = albedo_sample.rgb;
        }
    }
    else if(render_mode == RENDER_MODE_Normal)
    {
        // TODO(torgrim): Make one that also shows the normal map on
        // geometry.
        color = n;
    }
    else if(render_mode == RENDER_MODE_Occlusion)
    {
        if(IsSamplerFlagActive(Occlusion))
        {
            color = texture(occlusion_map, uv).rrr;
        }
        else
        {
            color = vec3(0.0f);
        }
    }
    else if(render_mode == RENDER_MODE_Emissive)
    {
        if(IsSamplerFlagActive(Emissive))
        {
            color = texture(emissive_map, uv).rgb;
        }
        else
        {
            color = vec3(0.0f);
        }
    }
    else if(render_mode == RENDER_MODE_Metallic || render_mode == RENDER_MODE_Roughness)
    {
        vec2 met_rough = FetchMetallicRoughness();
        if(render_mode == RENDER_MODE_Metallic)
        {
            color = vec3(met_rough.x);
        }
        else
        {
            color = vec3(met_rough.y);
        }
    }
    else if(render_mode == RENDER_MODE_Diffuse)
    {
        if(IsSamplerFlagActive(BaseColor))
        {
            vec4 albedo_sample = texture(base_color_map, uv);
            alpha = albedo_sample.a;
            albedo = albedo_sample.rgb;
        }
        vec2 met_rough = FetchMetallicRoughness();
        vec3 l_pos = main_light.position;
        vec3 l = normalize(l_pos - world_pos);
        float n_dot_l = max(dot(n,l), 0.0f);
        light_result dir_light = CalculateLight(v, l, n, albedo, met_rough.x, met_rough.y);
        light_result ibl_light = CalculateLightIBL(v, n, albedo, met_rough.x, met_rough.y);
        color = ibl_light.diffuse;// + (dir_light.diffuse * dir_light.radiance * n_dot_l);
    }
    else if(render_mode == RENDER_MODE_Specular)
    {
        if(IsSamplerFlagActive(BaseColor))
        {
            vec4 albedo_sample = texture(base_color_map, uv);
            alpha = albedo_sample.a;
            albedo = albedo_sample.rgb;
        }
        vec2 met_rough = FetchMetallicRoughness();
        vec3 l_pos = main_light.position;
        vec3 l = normalize(l_pos - world_pos);
        float n_dot_l = max(dot(n,l), 0.0f);
        light_result dir_light = CalculateLight(v, l, n, albedo, met_rough.x, met_rough.y);
        light_result ibl_light = CalculateLightIBL(v, n, albedo, met_rough.x, met_rough.y);
        color = ibl_light.specular + (dir_light.specular * dir_light.radiance * n_dot_l);
    }
    else if(render_mode == RENDER_MODE_Geometry)
    {
        vec2 met_rough = FetchMetallicRoughness();
        vec3 l_pos = main_light.position;
        vec3 l = normalize(l_pos - world_pos);
        float G = GeometrySmithGGX(v, l, n, met_rough.y);
        color = vec3(G);
    }
    else if(render_mode == RENDER_MODE_Distribution)
    {
        vec2 met_rough = FetchMetallicRoughness();
        vec3 l_pos = main_light.position;
        vec3 l = normalize(l_pos - world_pos);
        vec3 h = normalize(l + v);
        float NDF = NDFGGX(h, n, met_rough.y);
        color = vec3(NDF);
    }
    else if(render_mode == RENDER_MODE_Fresnel)
    {
        if(IsSamplerFlagActive(BaseColor))
        {
            vec4 albedo_sample = texture(base_color_map, uv);
            alpha = albedo_sample.a;
            albedo = albedo_sample.rgb;
        }
        vec2 met_rough = FetchMetallicRoughness();
        vec3 l_pos = main_light.position;
        vec3 l = normalize(l_pos - world_pos);
        vec3 h = normalize(l + v);
        vec3 F = FresnelSchlickApprox(max(dot(l, h), 0.0f), albedo, met_rough.x);
        color = F;
    }
    else if(render_mode == RENDER_MODE_FresnelRoughness)
    {
        if(IsSamplerFlagActive(BaseColor))
        {
            vec4 albedo_sample = texture(base_color_map, uv);
            alpha = albedo_sample.a;
            albedo = albedo_sample.rgb;
        }
        vec2 met_rough = FetchMetallicRoughness();
        vec3 F0 = vec3(0.04f);
        F0 = mix(F0, albedo, met_rough.y);
        vec3 h = normalize(n+v);
        vec3 F = FresnelSchlickRoughness(max(dot(h, v), 0.0f), F0, met_rough.y);
        color = F;
    }

    result_color = vec4(color, alpha);
}

#endif
