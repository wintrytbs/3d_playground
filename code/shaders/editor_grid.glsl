#if defined(VERT_ACTIVE)

layout (location = 0) in vec3 vert_pos;

uniform mat4 proj_view;

out vec3 world_pos;
void main()
{
    gl_Position = proj_view * vec4(vert_pos, 1.0f);
    world_pos = vert_pos;
}

#elif defined(FRAG_ACTIVE)
in vec3 world_pos;

out vec4 result_color;

void main()
{
    vec2 center = vec2(0.0f);
    vec2 plane_pos = world_pos.xz;
    float distX = distance(center.x, plane_pos.x);
    float distY = distance(center.y, plane_pos.y);
    vec3 dx = dFdx(world_pos);
    vec3 dy = dFdy(world_pos);
    float alpha = 1.0f - smoothstep(1.0f, 1.0f - abs(dx.x)*4.0f, distX);
    alpha += (1.0f - smoothstep(1.0f, 1.0f - abs(dy.z)*4.0f, distY));
    result_color = vec4(0.3f, 0.3f, 0.3f, 0.7f);
    vec3 color = vec3(0.1f, 0.1f, 0.1f);
    result_color = vec4(color, alpha);
    //result_color = vec4(color, 1.0f);
}

#endif
