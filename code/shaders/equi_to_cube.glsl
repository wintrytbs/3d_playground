#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_vert_pos;
layout(location = 1) in vec2 in_tex_coord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec3 local_pos;
out vec2 tex_coord;

void main()
{
    local_pos = in_vert_pos;
    gl_Position = proj * view * vec4(in_vert_pos, 1.0f);
}

#elif defined(FRAG_ACTIVE)

#define PI 3.14159265359

in vec3 local_pos;
in vec2 tex_coord;
out vec4 result_color;

uniform sampler2D hdr_tex;

void main()
{
    vec3 n_local_pos = normalize(local_pos);
    vec2 uv = vec2(atan(n_local_pos.z, n_local_pos.x), asin(n_local_pos.y));
    uv.x = 0.5f*(uv.x / PI) + 0.5f;
    uv.y = (uv.y / PI) + 0.5f;
    result_color = texture(hdr_tex, uv);
}

#endif
