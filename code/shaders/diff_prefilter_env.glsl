#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_vert_pos;

out vec3 local_pos;

uniform mat4 proj;
uniform mat4 view;

void main()
{
    local_pos = vec3(in_vert_pos);
    gl_Position = proj * view * vec4(local_pos, 1.0f);
}

#elif defined(FRAG_ACTIVE)
#line 20

#define PI 3.14159265359

in vec3 local_pos;

out vec4 result_color;

uniform samplerCube env_map;

void main()
{
    vec3 n = normalize(local_pos);
    vec3 irr = vec3(0.0f);
    float dPhi = (2.0f * PI) / 360.0f;
    float dTheta = (PI*0.5f) / 90.0f;
    uint sample_count = 0u;

    vec3 up = vec3(0.0f, 0.0f, 1.0f);
    vec3 right = vec3(0.0f);
    if(abs(n.z) >= 0.999f)
    {
        vec3 up = vec3(1.0f, 0.0f, 0.0f);
        vec3 right = normalize(cross(up, n));
        up = cross(n, right);
        for(float phi = 0.0f; phi < (PI*2.0f); phi += dPhi)
        {
            for(float theta = 0.0f; theta < (PI*0.5f); theta += dTheta)
            {
                vec3 tmp_vec = cos(phi) * up + sin(phi) * right;
                vec3 sample_dir = cos(theta) * n + sin(theta) * tmp_vec;
                irr += texture(env_map, sample_dir).rgb * cos(theta) * sin(theta);
                ++sample_count;
            }
        }
    }
    else
    {
        vec3 up = vec3(0.0f, 0.0f, 1.0f);
        vec3 right = normalize(cross(up, n));
        up = cross(n, right);
        for(float phi = 0.0f; phi < (PI*2.0f); phi += dPhi)
        {
            for(float theta = 0.0f; theta < (PI*0.5f); theta += dTheta)
            {
                vec3 tmp_vec = cos(phi) * right + sin(phi) * up;
                vec3 sample_dir = cos(theta) * n + sin(theta) * tmp_vec;
                irr += texture(env_map, sample_dir).rgb * cos(theta) * sin(theta);
                ++sample_count;
            }
        }
    }



    irr = PI * irr * (1.0f / float(sample_count));

    result_color = vec4(irr, 1.0f);
}

#endif
