#if defined(VERT_ACTIVE)

layout(location = 0) in vec2 in_vert_pos;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec4 in_col;

out vec2 uv;
out vec4 color;

uniform mat4 proj;

void main()
{
    uv = in_uv;
    color = in_col;

    gl_Position = proj * vec4(in_vert_pos, 0.0f, 1.0f);
}

#elif defined(FRAG_ACTIVE)

in vec2 uv;
in vec4 color;

out vec4 result_color;

uniform sampler2D tex;
uniform sampler2DArray tex_array;

uniform bool is_tex_array;

void main()
{
    if(!is_tex_array)
    {
        result_color = color * texture(tex, uv);
    }
    else
    {
        result_color = color * texture(tex_array, vec3(uv, 0));
    }
}

#endif
