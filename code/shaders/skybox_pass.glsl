#if defined(VERT_ACTIVE)

// TODO(torgrim): not all these are needed
// for visualizing the light but keep them
// for now.
layout(location = 0) in vec3 in_vert_pos;
layout(location = 1) in vec2 in_tex_pos;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec4 in_tangent;

out vec3 world_pos;

uniform mat4 proj;
uniform mat4 view;

void main()
{
    world_pos = vec3(in_vert_pos);

    //vec4 result_pos = proj * mat4(mat3(view)) * vec4(world_pos, 1.0f);
    vec4 result_pos = proj * view * vec4(world_pos, 1.0f);
    result_pos = result_pos.xyww;

    gl_Position = result_pos;
}

#elif defined(FRAG_ACTIVE)

in vec3 world_pos;

out vec4 result_color;

uniform samplerCube skybox_tex;
uniform float tex_lod;

void main()
{
    vec3 uv = world_pos;

    // NOTE(torgrim): since cube maps are left handed
    // we need to remap the coordinates.
    uv.z = -uv.z;

    vec3 env = textureLod(skybox_tex, uv, tex_lod).rgb;
    result_color = vec4(env, 1.0f);
}

#endif
