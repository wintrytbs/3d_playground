#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_vert;

uniform mat4 proj_view;
uniform mat4 model;

void main()
{
    gl_Position = proj_view * model * vec4(in_vert, 1.0f);
}

#elif defined(FRAG_ACTIVE)

out vec4 result_color;

void main()
{
    result_color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
}

#endif
