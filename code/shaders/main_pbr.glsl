#if defined(VERT_ACTIVE)

#define MAX_LIGHT_COUNT 10

layout(location = 0) in vec3 in_vert_pos;
layout(location = 1) in vec2 in_tex_pos;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec4 in_tangent;
// TODO(torgrim): Support vertex color

out vec2 tex_pos;
out vec3 world_pos;
out vec3 surface_normal;
out vec4 tangent;

out vec3 view_rel_pos;

out vec4 spot_shadow_space_coords[MAX_LIGHT_COUNT];
out vec3 point_shadow_space_coords[MAX_LIGHT_COUNT];

uniform mat4 proj;
uniform mat4 view;

uniform mat4 model;

uniform int spot_shadow_count;
uniform int point_shadow_count;
uniform int dir_shadow_count;

uniform mat4 spot_shadow_MVP_list[MAX_LIGHT_COUNT];
uniform mat4 point_shadow_model_list[MAX_LIGHT_COUNT];

void main()
{
    vec4 result_pos = vec4(0.0f);
    vec4 vert_world_pos = model * vec4(in_vert_pos, 1.0f);
    world_pos = vert_world_pos.xyz;

    tex_pos = in_tex_pos;

    mat3 normalMatrix = transpose(inverse(mat3(model)));
    surface_normal =  normalize(normalMatrix * in_normal);
    tangent = vec4(normalize(mat3(model) * in_tangent.xyz), in_tangent.w);

    mat4 shadow_scale_bias = mat4(vec4(0.5f, 0.0f, 0.0f, 0.0f),
                                  vec4(0.0f, 0.5f, 0.0f, 0.0f),
                                  vec4(0.0f, 0.0f, 0.5f, 0.0f),
                                  vec4(0.5f, 0.5f, 0.5f, 1.0f));

    for(int i = 0; i < spot_shadow_count; ++i)
    {
        spot_shadow_space_coords[i] = shadow_scale_bias * spot_shadow_MVP_list[i] * vert_world_pos;
    }

    for(int i = 0; i < point_shadow_count; ++i)
    {
        point_shadow_space_coords[i] = vec3(inverse(point_shadow_model_list[i]) * vec4(world_pos, 1.0f));
    }

    view_rel_pos = vec3(view * vert_world_pos);
    result_pos = proj * view * vert_world_pos;

    gl_Position = result_pos;
}

#elif defined(FRAG_ACTIVE)
// TODO(torgrim): there is a bug when the light is aligned with
// a surface that creates artifacts on that surface with a noisy
// color. This can be shown if the light y = 1 and
// the default cube pos = center origin
#line 72

#define PI 3.14159265359
#define e 2.71828

#define DISTANCE_ATTENUATION_InvSqrSim              0
#define DISTANCE_ATTENUATION_Exp                    1
#define DISTANCE_ATTENUATION_CubicPolynomial        2

#define LIGHT_TYPE_Point                            0
#define LIGHT_TYPE_Spot                             1
#define LIGHT_TYPE_Directional                      2

#define ACTIVE_SAMPLER_FLAG_BaseColor               1u << 0
#define ACTIVE_SAMPLER_FLAG_Normal                  1u << 1
#define ACTIVE_SAMPLER_FLAG_MetallicRoughness       1u << 2
#define ACTIVE_SAMPLER_FLAG_Emissive                1u << 3
#define ACTIVE_SAMPLER_FLAG_Occlusion               1u << 4

#define GLOBAL_SETTINGS_FLAG_UseAmbientEnvLighting  1u << 0

#define MAX_LIGHT_COUNT                             10

struct light_properties
{
    int type;
    vec3 color;
    vec3 position;
    vec3 prim_dir;
    int dist_att_func;
    float max_dist;
    float falloff_start_dist;
    float spot_min_cutoff;
    float spot_max_cutoff;
    float strength;
};

struct point_shadow_properties
{
    vec2 trans;
    samplerCubeShadow map;
};

in vec2 tex_pos;
in vec3 surface_normal;
in vec3 world_pos;
in vec4 tangent;
in vec4 spot_shadow_space_coords[MAX_LIGHT_COUNT];
in vec3 point_shadow_space_coords[MAX_LIGHT_COUNT];

in vec3 view_rel_pos;

out vec4 result_color;

uniform int spot_shadow_count;
uniform int point_shadow_count;
uniform int dir_shadow_count;

uniform vec4 base_color;
uniform vec3 view_pos;

uniform float metallic_factor;
uniform float roughness_factor;

uniform float alpha_cutoff;

uniform float ambient_strength;

uniform uint glob_settings_flags;
uniform uint active_sampler_flag;

uniform bool use_spot_sample_shadow;
uniform bool use_point_sample_shadow;

// TODO(torgrim): Are these easier to handle if they are defined as a texture array instead?
uniform sampler2D base_color_map;
uniform sampler2D normal_map;
uniform sampler2D metallic_roughness_map;
uniform sampler2D emissive_map;
uniform sampler2D occlusion_map;

// TODO(torgrim): Rename to environment_map or something.
uniform samplerCube irradiance_map;
uniform samplerCube prefilter_env;
uniform sampler2D brdf_lut;

uniform sampler2DArrayShadow spot_shadow_map_array;
uniform sampler2DArrayShadow dir_shadow_map_array;
uniform point_shadow_properties point_shadow_list[MAX_LIGHT_COUNT];

uniform light_properties light_list[MAX_LIGHT_COUNT * 3];

uniform int light_count;

uniform vec4 cascade_splits;
uniform mat4 dir_shadow_MVP_list[4];

vec3 FetchNormalVector(vec2 texPos)
{
    // NOTE(torgrim): this can be done if we are just
    // storing x and y of the normal map to retrieve
    // the z value
    //vec2 sample = texture(normal_map, texPos).st;
    //float zValue = sqrt(1.0f - (sample.x * sample.x) - (sample.y * sample.y));
    //vec3 result = vec3(sample, zValue);

    vec3 result = texture(normal_map, texPos).xyz;
    return normalize(result * 2.0f - 1.0f);
}

vec3 CalcObjectNormalVector(vec2 texPos, vec3 tan, vec3 normal, float sigma)
{
    vec3 m = FetchNormalVector(texPos);
    vec3 n = normalize(normal);
    vec3 t = normalize((tan - dot(tan, n) * n));
    vec3 b = normalize(cross(normal, tan) * sigma);

    n = t * m.x + b * m.y + n * m.z;

    return normalize(n);
}

// TODO(torgrim): Currently an error when we are approaching
// 90 degrees in cutoff angle. Need to find out why.
float CalcSpotShadow(vec4 shadow_coord, int lookup_index)
{
    // TODO(torgrim): Find a better way of saying that
    // no shadows should be checked.
    if(lookup_index >= spot_shadow_count)
    {
        return 1.0f;
    }

    vec2 shadowOffset = 1.0f / textureSize(spot_shadow_map_array, 0).xy;
    vec4 p = vec4(shadow_coord.xyz / shadow_coord.w, lookup_index);
    p = vec4(p.x, p.y, p.w, p.z);

    float result = texture(spot_shadow_map_array, p);

    if(use_spot_sample_shadow)
    {
        // TODO(torgrim): make numbers of samples a per light
        // parameter.
        p.xy -= shadowOffset;
        result += texture(spot_shadow_map_array, p);

        p.x += shadowOffset.x * 2.0f;
        result += texture(spot_shadow_map_array, p);

        p.y += shadowOffset.y * 2.0f;
        result += texture(spot_shadow_map_array, p);

        p.x -= shadowOffset.x * 2.0f;
        result += texture(spot_shadow_map_array, p);

        result = (result * 0.2f);
    }

    return result;
}

float CalcDirShadow(uint cascade_index)
{
    // TODO(torgrim): Do a better blending between
    // different cascades.
    const mat4 shadow_scale_bias = mat4(vec4(0.5f, 0.0f, 0.0f, 0.0f),
                                        vec4(0.0f, 0.5f, 0.0f, 0.0f),
                                        vec4(0.0f, 0.0f, 0.5f, 0.0f),
                                        vec4(0.5f, 0.5f, 0.5f, 1.0f));

    uint casc1 = min(cascade_index + 1u, 3u);
    vec4 shadow_coord0 = vec4(shadow_scale_bias * dir_shadow_MVP_list[cascade_index] * vec4(world_pos, 1.0f));
    vec4 shadow_coord1 = vec4(shadow_scale_bias * dir_shadow_MVP_list[casc1] * vec4(world_pos, 1.0f));
    shadow_coord0 = shadow_coord0 / shadow_coord0.w;
    shadow_coord1 = shadow_coord1 / shadow_coord1.w;
    vec2 xy0 = shadow_coord0.xy;
    vec2 xy1 = shadow_coord1.xy;
    float dx = 1.0f / textureSize(dir_shadow_map_array, 0).x;
    float dy = 1.0f / textureSize(dir_shadow_map_array, 0).y;

    float origin = (cascade_index == 0u ? 0.0f : cascade_splits[cascade_index - 1u]);
    float range = abs(cascade_splits[cascade_index]) - abs(origin);
    float dist = abs(view_rel_pos.z) - abs(origin);
    float weight = clamp(dist / range, 0.0f, 1.0f);

    int count = 0;
    float result0 = 0.0f;
    float result1 = 0.0f;
#if 1
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            vec2 offset0 = xy0 + vec2(dx*x,dy*y);
            vec2 offset1 = xy1 + vec2(dx*x,dy*y);
            vec4 p0 = vec4(offset0, cascade_index, shadow_coord0.z);
            vec4 p1 = vec4(offset1, casc1, shadow_coord1.z);
            result0 += texture(dir_shadow_map_array, p0);
            result1 += texture(dir_shadow_map_array, p1);
            count++;
        }
    }

    float result = mix(result0, result1, weight) / float(count);
#else
    vec4 p0 = vec4(xy0, cascade_index, shadow_coord0.z);
    vec4 p1 = vec4(xy1, casc1, shadow_coord1.z);
    result0 += texture(dir_shadow_map_array, p0);
    result1 += texture(dir_shadow_map_array, p1);
    float result = mix(result0, result1, weight);
#endif

    return result;
}

float CalcPointShadow(int lookup_index)
{
    // TODO(torgrim): Find a better way of saying that
    // no shadows should be checked.

    if(lookup_index >= point_shadow_count)
    {
        return 1.0f;
    }
    vec2 trans = point_shadow_list[lookup_index].trans;
    vec3 lsc = point_shadow_space_coords[lookup_index];

    vec3 abs_coord = abs(lsc);
    float max_xy = max(abs_coord.x, abs_coord.y);
    float ma = max(max_xy, abs_coord.z);

    float uv_depth = trans.x + (trans.y / -ma);
    uv_depth = 1.0f - ((uv_depth * 0.5f) + 0.5f);

    vec2 texSize = textureSize(point_shadow_list[lookup_index].map, 0).xy;

    vec4 map_uv = vec4(lsc, uv_depth);
    float result = texture(point_shadow_list[lookup_index].map, map_uv);

    // TODO(torgrim): Find more/better ways of sampling a point light shadow map.
    if(use_point_sample_shadow)
    {
        // TODO(torgrim): Get a better understanding of
        // this calculation before we say that we are finished
        // with shadows for now.
        // TODO(torgrim): Make numbers of samples a per light
        // parameter.
        // TODO(torgrim): Support non square shadow maps
        float offset = (2.0f / (texSize.x)) * ma;
        float dxy = (max_xy > abs_coord.z) ? offset : 0.0f;
        float dx = (abs_coord.x > abs_coord.y) ? dxy : 0.0f;
        vec2 oxy = vec2(offset - dx, dx);
        vec2 oyz = vec2(offset - dxy, dxy);

        float offset_epsilon = 1.0 / texSize.x;
        vec3 lim = vec3(ma, ma, ma);
        lim.xy -= (oxy * offset_epsilon);
        lim.yz -= (oyz * offset_epsilon);
        vec3 offset_coord = lsc;
#if 1
        offset_coord.xy -= oxy;
        offset_coord.yz -= oyz;
        offset_coord = clamp(offset_coord, -lim, lim);

        map_uv = vec4(offset_coord, uv_depth);
        result += texture(point_shadow_list[lookup_index].map, map_uv);

        offset_coord.xy += oxy * 2.0;
        offset_coord = clamp(offset_coord, -lim, lim);

        map_uv = vec4(offset_coord, uv_depth);
        result += texture(point_shadow_list[lookup_index].map, map_uv);

        offset_coord.yz += oyz * 2.0f;
        offset_coord = clamp(offset_coord, -lim, lim);

        map_uv = vec4(offset_coord, uv_depth);
        result += texture(point_shadow_list[lookup_index].map, map_uv);
#endif

        offset_coord.xy -= oxy * 2.0f;
        offset_coord = clamp(offset_coord, -lim, lim);

        map_uv = vec4(offset_coord, uv_depth);
        result += texture(point_shadow_list[lookup_index].map, map_uv);

        result = (result * 0.2f);
    }

    return result;
}

float CalcAngleAttenuation(vec3 p, int light_index)
{
    float attenuation = 1.0f;

    light_properties l = light_list[light_index];
    vec3 point_to_light = normalize(l.position - p);
    float cMin = l.spot_min_cutoff;
    float cMax = l.spot_max_cutoff;
    float c = dot(-point_to_light, normalize(l.prim_dir));

    float t = 1.0f - clamp((c - cMin) / (cMax - cMin), 0, 1);

    attenuation = t;
    return attenuation;
}

float CalcDistanceAttenuation(vec3 p, int light_index)
{
    float attenuation = 1.0f;

    light_properties l = light_list[light_index];
    float dist = length(p - l.position);

    float startDist2 = pow(l.falloff_start_dist, 2);
    float maxDist2 = pow(l.max_dist, 2);
    float dist2 = pow(dist, 2);

    if(l.dist_att_func == DISTANCE_ATTENUATION_InvSqrSim)
    {
        attenuation = (startDist2 / (maxDist2 - startDist2)) * ((maxDist2 / dist2) - 1.0f);
        attenuation = clamp(attenuation, 0, 1);
    }
    else if(l.dist_att_func == DISTANCE_ATTENUATION_Exp)
    {
        attenuation = (pow(e, (-9 * dist2) / maxDist2) - pow(e, -9)) / (1- pow(e, -9));
        attenuation = clamp(attenuation, 0, 1);
    }
    else if(l.dist_att_func == DISTANCE_ATTENUATION_CubicPolynomial)
    {
        // TODO(torgrim): this is the smooth attenuation in FGED2 but
        // it doesn't cut off(become negative when dist > max_dist).
        // Is this intended or is the function incorrect. For now we explicitly
        // check if dist > max and set the value = 0
        if(dist < l.max_dist)
        {
            attenuation = (dist2 / maxDist2) * (((2.0f * dist) / l.max_dist) - 3.0f) + 1.0f;
            attenuation = clamp(attenuation, 0, 1);
        }
        else
        {
            attenuation = 0;
        }
    }

    return attenuation;
}

vec3 FresnelSchlickRoughness(float cos_theta, vec3 F0, float roughness)
{
    float val = max(1.0f - cos_theta, 0.0f);
    vec3 result = F0 + (max(vec3(1.0f - roughness), F0) - F0) * pow(val, 5.0f);
    return result;
}

vec3 FresnelSchlickApprox(float cos_theta, vec3 albedo, float metallic)
{
    // TODO(torgrim): F0 calculation doesn't really have
    // to be calculated per light but rather per object.
    vec3 F0 = vec3(0.04f);
    F0 = mix(F0, albedo, metallic); 
    vec3 result = F0 + (1.0f - F0) * pow(1.0f - cos_theta, 5.0f);
    return result;
}

float NDFGGX(vec3 h, vec3 n, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;

    float NdotH = max(dot(n,h), 0.0f);
    float NdotH2 = NdotH*NdotH;

    float num = a2;
    float den = (NdotH2 * (a2 - 1.0f) + 1.0f);
    den = PI * den*den;

    float result = num / max(den, 0.000001f);

    return result;
}

float GeometrySchlickGGX(vec3 v, vec3 n, float roughness)
{
    float r = (roughness + 1.0f);
    float k = (r*r) / 8.0f;
    float NdotV = max(dot(n,v), 0.0f);
    float num = NdotV;
    float den = NdotV * (1.0f-k) + k;

    float result = num / den;
    return result;
}

float GeometrySmithGGX(vec3 v, vec3 l, vec3 n, float roughness)
{
    float result = GeometrySchlickGGX(v, n, roughness) * GeometrySchlickGGX(l, n, roughness);
    return result;
}

vec3 CalculateReflectionPBR(vec3 v, vec3 n, vec3 albedo, float metallic, float roughness)
{
    uint cascade_index = 0u;
    for(uint i = 0u; i < 3u; ++i)
    {
        if(view_rel_pos.z < cascade_splits[i])
        {
            cascade_index = i + 1u;
        }
    }

    vec3 Lo = vec3(0.0f);
    // TODO(torgrim): Since we are now supporting HDR environment lighting
    // we also need to move all of our lights into HDR otherwise
    // the ambient environment lighting will always dominate.
#if 1
    int spot_shadow_index = 0;
    int point_shadow_index = 0;
    int dir_shadow_index = 0;
    for(int i = 0; i < light_count; ++i)
    {
        vec3 l_pos = light_list[i].position;
        vec3 l = normalize(l_pos - world_pos);
        vec3 h = normalize(l + v);
        float attenuation = CalcDistanceAttenuation(world_pos, i);
        float shadow_factor = 1.0f;
        if(light_list[i].type == LIGHT_TYPE_Spot)
        {
            attenuation *= CalcAngleAttenuation(world_pos, i);
            shadow_factor = CalcSpotShadow(spot_shadow_space_coords[spot_shadow_index], spot_shadow_index);
            spot_shadow_index++;
        }
        else if(light_list[i].type == LIGHT_TYPE_Directional)
        {
            shadow_factor = CalcDirShadow(cascade_index);
            attenuation = 1.0f;
            l = -normalize(light_list[i].prim_dir);
            h = normalize(l + v);
            dir_shadow_index++;

        }
        else if(light_list[i].type == LIGHT_TYPE_Point)
        {
            shadow_factor = CalcPointShadow(point_shadow_index);
            ++point_shadow_index;
        }

        vec3 radiance = light_list[i].color * light_list[i].strength * attenuation;
        vec3 F = FresnelSchlickApprox(max(dot(v,h), 0.0f), albedo, metallic);
        float NDF = NDFGGX(h, n, roughness);
        float G = GeometrySmithGGX(v, l, n, roughness);

        vec3 num = (F*G*NDF);
        float den = 4.0f * max(dot(n,l), 0.0f) * max(dot(n,v), 0.0f);
        vec3 spec_reflection = num / max(den, 0.001f);

        vec3 diffuse_reflection = vec3(1.0f) - F;
        diffuse_reflection *= (1.0f - metallic);
        float NdotL = max(dot(n,l), 0.0f);
        Lo += ((diffuse_reflection * albedo / PI) + spec_reflection) * radiance * NdotL * shadow_factor;
    }
#endif

    vec3 ambient = albedo * ambient_strength;
    if((glob_settings_flags & GLOBAL_SETTINGS_FLAG_UseAmbientEnvLighting) != 0u)
    {
        vec3 F0 = vec3(0.04f);
        F0 = mix(F0, albedo, metallic);
        vec3 kS = FresnelSchlickRoughness(max(dot(n, v), 0.0f), F0, roughness);

        vec3 kD = 1.0f - kS;
        kD *= 1.0f - metallic;
        vec3 sNor = n;
        sNor.z = -sNor.z;
        vec3 irr = texture(irradiance_map, sNor).rgb;
        vec3 diff = irr * albedo;

        vec3 r = reflect(-v, n);
        r.z = -r.z;
        vec3 prefilteredReflection = textureLod(prefilter_env, r, roughness * 4.0f).rgb;
        vec2 scaleAndBias = texture(brdf_lut, vec2(max(dot(n, v), 0.0f), roughness)).rg;
        vec3 spec = prefilteredReflection * (F0 * scaleAndBias.r + scaleAndBias.g);
        ambient += (kD * diff + spec);
    }

    if((active_sampler_flag & ACTIVE_SAMPLER_FLAG_Occlusion) != 0u)
    {
        ambient *= texture(occlusion_map, tex_pos).r;
    }

    Lo += ambient;

    // TODO(torgrim): Add bloom.
    if((active_sampler_flag & ACTIVE_SAMPLER_FLAG_Emissive) != 0u)
    {
        vec3 emissive = texture(emissive_map, tex_pos).rgb;
        Lo += emissive;
    }

    return Lo;
}

void main()
{
    // TODO(torgrim): Take into account the alpha value here as well?
    vec3 color = base_color.xyz;
    vec3 v = normalize(view_pos - world_pos);
    vec3 n = normalize(surface_normal);

    // TODO(torgrim): try to create a reverse the normal map
    // into a hight map

    vec3 surface_color = base_color.xyz;
    float alpha = base_color.w;


    if((active_sampler_flag & ACTIVE_SAMPLER_FLAG_BaseColor) != 0u)
    {
        vec4 base_sample = texture(base_color_map, tex_pos);
        surface_color = base_sample.xyz;
        alpha = base_sample.w;
        color = surface_color;
    }

    alpha = clamp(alpha, 0.0f, 1.0f);
    if(alpha_cutoff < 1.0f && alpha < alpha_cutoff)
    {
        discard;
    }

    if((active_sampler_flag & ACTIVE_SAMPLER_FLAG_Normal) != 0u)
    {
        vec3 tt = normalize(tangent.xyz);
        n = CalcObjectNormalVector(tex_pos, tt, surface_normal, tangent.w);
    }

    // TODO(torgrim): base_color here should be change to base_color_factor since
    // we can have both a base_color_factor and a base_color_map(albedo texture). If both
    // of these are used we want to multiply these together.
    float r = roughness_factor;
    float m = metallic_factor;
    if((active_sampler_flag & ACTIVE_SAMPLER_FLAG_MetallicRoughness) != 0u)
    {
        vec2 met_rough = texture(metallic_roughness_map, tex_pos).bg;
        m *= met_rough.x;
        r *= met_rough.y;
    }

    r = max(r, 0.045f);
    color = CalculateReflectionPBR(v, n, surface_color, m, r);

    result_color = vec4(color, 1.0f);
}

#endif
