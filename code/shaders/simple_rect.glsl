#if defined(VERT_ACTIVE)

layout(location = 0) in vec2 in_vert_pos;

uniform mat4 proj;
uniform mat4 model;

void main()
{
    //gl_Position = proj * model * vec4(in_vert_pos, 0.0f, 1.0f);
    gl_Position =  vec4(in_vert_pos, 0.0f, 1.0f);
}

#elif defined(FRAG_ACTIVE)

out vec4 result_color;

void main()
{
    result_color = vec4(1.0f, 1.0f, 0.0f, 1.0f);
}

#endif
