#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_vert_pos;
layout(location = 1) in vec2 in_uv;

out vec2 uv;

uniform mat4 proj_view;
uniform mat4 model;

void main()
{
    uv = in_uv;
    gl_Position = proj_view * model * vec4(in_vert_pos, 1.0f);
}

#elif defined(FRAG_ACTIVE)

in vec2 uv;

out vec4 result_color;

uniform sampler2D base_color_map;
uniform float alpha_cutoff;
uniform bool has_base_color;

void main()
{
    if(!has_base_color)
    {
        result_color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
    }
    else
    {
        vec4 base_color = texture(base_color_map, uv);
        if(base_color.a < alpha_cutoff)
        {
            discard;
        }
        result_color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
    }
}

#endif
