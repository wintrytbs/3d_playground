#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_vert_pos;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec4 in_tangent;

out vec2 uv;
out vec3 normal;
out vec3 view_pos;
out vec3 world_pos;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

void main()
{
    uv = in_uv;
    vec3 world_pos = vec3(model * vec4(in_vert_pos, 1.0f));
    view_pos = vec3(view * vec4(world_pos, 1.0f));
    //normal = normalize(inverse(transpose(mat3(model))) * in_normal);
    normal = in_normal;
    gl_Position = proj * vec4(view_pos, 1.0f);
}

#elif defined(FRAG_ACTIVE)

const vec3 cascade_palett[4] = vec3[]
(
    vec3(1.0f, 0.05f, 0.05f),
    vec3(0.05f, 1.0f, 0.05f),
    vec3(0.05f, 0.05f, 1.0f),
    vec3(1.0f, 1.0f, 0.05f)
);

in vec2 uv;
in vec3 normal;
in vec3 view_pos;
in vec3 world_pos;

out vec4 result_color;

uniform vec4 cascade_splits;
uniform vec3 cam_pos;
uniform float alpha_cutoff;
uniform bool texture_present;
uniform vec4 base_color;

uniform sampler2D base_color_map;

void main()
{
    vec3 v = normalize(cam_pos - world_pos);
    vec3 n = normalize(normal);

    uint cascade_index = 0u;
    for(uint i = 0u; i < 3u; ++i)
    {
        if(view_pos.z < cascade_splits[i])
        {
            cascade_index = i + 1u;
        }
    }

    vec3 surface_color = base_color.rgb;
    float alpha = base_color.a;
    if(texture_present)
    {
        vec4 base_sample = texture(base_color_map, uv);
        surface_color = base_sample.rgb;
        alpha = base_sample.w;
    }

    if(alpha < alpha_cutoff)
    {
        discard;
    }

    vec3 c_col = cascade_palett[cascade_index];
    vec3 color = surface_color * c_col * max(dot(v,n), 0.5f);
    result_color = vec4(pow(color, vec3(1.0f / 2.2f)), 1.0f);
}

#endif
