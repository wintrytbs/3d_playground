#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_verts;

uniform mat4 proj_view;
uniform mat4 model;

void main()
{
    gl_Position = proj_view * model * vec4(in_verts, 1.0f);
}

#elif defined(FRAG_ACTIVE)

out vec4 result_color;

uniform vec4 quad_color;

void main()
{
    result_color = quad_color;
}

#endif
