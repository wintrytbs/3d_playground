#if defined(VERT_ACTIVE)

layout(location = 0) in vec2 in_vert;
layout(location = 1) in vec2 in_uv;

out vec2 uv;

void main()
{
    uv = in_uv;
    gl_Position = vec4(in_vert, 0.0f, 1.0f);
}

#elif defined(FRAG_ACTIVE)

#define TONEMAPPING_TYPE_None 0
#define TONEMAPPING_TYPE_ReinhardSimple 1
#define TONEMAPPING_TYPE_ReinhardExtended 2
#define TONEMAPPING_TYPE_ReinhardExtendedLuminance 3
#define TONEMAPPING_TYPE_ReinhardJodie 4
#define TONEMAPPING_TYPE_Hable 5
#define TONEMAPPING_TYPE_ACES 6

in vec2 uv;

out vec4 result_color;

uniform bool use_gamma;
uniform bool use_tonemapping;
uniform int tonemapping_type;
uniform float max_white;
uniform sampler2D tex;

float rgb_to_luminance(vec3 c)
{
    float result = dot(c, vec3(0.2126f, 0.7152f, 0.0722f));
    return result;
}

vec3 hable_tonemap_partial(vec3 x)
{
    float a = 0.15f;
    float b = 0.50f;
    float c = 0.10f;
    float d = 0.20f;
    float e = 0.02f;
    float f = 0.30f;
    return ((x*(a*x+c*b)+d*e)/(x*(a*x+b)+d*f))-e/f;
}

// sRGB => XYZ => D65_2_D60 => AP1 => RRT_SAT
const mat3 aces_input_map = mat3
(
    0.59719, 0.35458, 0.04823,
    0.07600, 0.90834, 0.01566,
    0.02840, 0.13383, 0.83777
);

// ODT_SAT => XYZ => D60_2_D65 => sRGB
const mat3 aces_output_mat = mat3
(
     1.60475f, -0.53108f, -0.07367f,
    -0.10208f,  1.10813f, -0.00605f,
    -0.00327f, -0.07276f,  1.07602f
);

vec3 rrt_and_odt_fit(vec3 v)
{
    vec3 a = v * (v + 0.0245786f) - 0.000090537f;
    vec3 b = v * (0.983729f * v + 0.4329510f) + 0.238081f;
    return a / b;
}

vec3 aces_fitted(vec3 c)
{
    c = c * aces_input_map;
    // Apply RRT and ODT
    c = rrt_and_odt_fit(c);
    c = c * aces_output_mat;
    // Clamp to [0, 1]
    c = clamp(c, 0.0f, 1.0f);

    return c;
}

void main()
{
    vec3 sample_color = texture(tex, uv).rgb;
    if(use_tonemapping)
    {
        // NOTE: Taken from https://64.github.io/tonemapping/
        if(tonemapping_type == TONEMAPPING_TYPE_ReinhardSimple)
        {
            sample_color = sample_color / (1.0f + sample_color);
        }
        else if(tonemapping_type == TONEMAPPING_TYPE_ReinhardExtended)
        {
            vec3 num = sample_color * (1.0f + (sample_color / vec3(max_white * max_white)));
            sample_color = num / (1.0f + sample_color);
        }
        else if(tonemapping_type == TONEMAPPING_TYPE_ReinhardExtendedLuminance)
        {
            float l_old = rgb_to_luminance(sample_color);
            float num = l_old * (1.0f + (l_old / (max_white * max_white)));
            float l_new = num / (1.0f + l_old);
            sample_color = sample_color * (l_new / l_old);
        }
        else if(tonemapping_type == TONEMAPPING_TYPE_ReinhardJodie)
        {
            float l = rgb_to_luminance(sample_color);
            vec3 tv = sample_color / (1.0f + sample_color);
            sample_color = mix(sample_color / (1.0f + l), tv, tv);
        }
        else if(tonemapping_type == TONEMAPPING_TYPE_Hable)
        {
            float exposure_bias = 2.0f;
            vec3 part1 = hable_tonemap_partial(sample_color * exposure_bias);
            vec3 w = vec3(11.2f);
            vec3 white_scale = vec3(1.0f) / hable_tonemap_partial(w);
            sample_color = part1 * white_scale;
        }
        else if(tonemapping_type == TONEMAPPING_TYPE_ACES)
        {
            sample_color = aces_fitted(sample_color);
        }
    }
    if(use_gamma)
    {
        vec3 inv_gamma = vec3(1.0f / 2.2f);
        sample_color = vec3(pow(sample_color, inv_gamma));
    }

    result_color = vec4(sample_color, 1.0f);
}

#endif
