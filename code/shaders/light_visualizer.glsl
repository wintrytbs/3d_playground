#if defined(VERT_ACTIVE)

// TODO(torgrim): not all these are needed
// for visualizing the light but keep them
// for now.
layout(location = 0) in vec3 in_vert_pos;
layout(location = 1) in vec2 in_tex_pos;
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec4 in_tangent;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

void main()
{
    vec4 result_pos = proj * view * model * vec4(in_vert_pos, 1.0f);
    gl_Position = result_pos;
}

#elif defined(FRAG_ACTIVE)

out vec4 result_color;

uniform vec3 light_color;

void main()
{
    result_color = vec4(light_color, 1.0f);
}

#endif
