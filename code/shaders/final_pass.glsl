#if defined(VERT_ACTIVE)

layout(location = 0) in vec2 in_vert_pos;
layout(location = 1) in vec2 in_tex_coord;

out vec2 tex_coord;

void main()
{
    tex_coord = in_tex_coord;
    gl_Position = vec4(in_vert_pos, 0.0f, 1.0f);
}

#elif defined(FRAG_ACTIVE)

in vec2 tex_coord;

out vec4 result_color;

uniform sampler2D tex;

void main()
{
    result_color = texture(tex, tex_coord);

}

#endif
