#if defined(VERT_ACTIVE)

layout(location = 0) in vec2 in_vert_pos;
layout(location = 1) in vec2 in_tex_coord;

out vec2 tex_coord;
void main()
{
    tex_coord = in_tex_coord;
    gl_Position = vec4(in_vert_pos, 0.0f, 1.0f);
}

#elif defined(FRAG_ACTIVE)
#line 16

#define PI 3.14159265359f

in vec2 tex_coord;
out vec2 result_color;

// Hammersley Points from Holger Dammertz:
// http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
float RadicalInverseVCD(uint bits)
{
    bits = (bits << 16u) | (bits >> 16u);
    bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
    bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
    bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);

    float result = float(bits) * 2.3283064365386963e-10;
    return result;
}

vec2 Hammersley(uint s_index, uint s_count)
{
    vec2 result = vec2(float(s_index) / float(s_count), RadicalInverseVCD(s_index));
    return result;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float a = roughness;
    float k = (a*a) / 2.0f;

    float nom = NdotV;
    float denom = NdotV * (1.0f - k) + k;

    return nom / denom;
}

vec3 ImportanceSampleGGX(vec2 xi, vec3 n, float roughness)
{
    float a = roughness * roughness;
    float phi = 2.0f * PI * xi.x;
    float cosTheta = sqrt((1.0f - xi.y) / (1.0f + (a*a - 1.0f) * xi.y));
    float sinTheta = sqrt(1.0f - cosTheta * cosTheta);

    vec3 H = vec3(cos(phi)*sinTheta, sin(phi)*sinTheta, cosTheta);
    vec3 up = abs(n.z) < 0.999f ? vec3(0.0f, 0.0f, 1.0f) : vec3(1.0f, 0.0f, 0.0f);
    vec3 tangent = normalize(cross(up, n));
    vec3 bitangent = cross(n, tangent);
    vec3 sampleVec = tangent * H.x + bitangent * H.y + n * H.z;
    return normalize(sampleVec);
}

float GeometrySmith(vec3 n, vec3 v, vec3 l, float roughness)
{
    float NdotV = max(dot(n, v), 0.0f);
    float NdotL = max(dot(n, l), 0.0f);

    float ggx_0 = GeometrySchlickGGX(NdotV, roughness);
    float ggx_1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx_0 * ggx_1;
}

vec2 IntegrateBRDF(float NdotV, float roughness)
{
    vec3 v;
    v.x = sqrt(1.0f - NdotV*NdotV);
    v.y = 0.0f;
    v.z = NdotV;

    float a = 0.0f;
    float b = 0.0f;

    vec3 n = vec3(0.0f, 0.0f, 1.0f);

    const uint sample_count = 1024u;
    for(uint i = 0u; i < sample_count; ++i)
    {
        vec2 xi = Hammersley(i, sample_count);
        vec3 h = ImportanceSampleGGX(xi, n, roughness);
        vec3 l = 2.0f * dot(v, h) * h - v;

        float NdotL = max(l.z, 0.0f);
        float NdotH = max(h.z, 0.0f);
        float VdotH = max(dot(v, h), 0.0f);

        if(NdotL > 0.0f)
        {
            float g = GeometrySmith(n, v, l, roughness);
            float g_vis = (g * VdotH) / (NdotH * NdotV);

            float Fc = pow(1.0f - VdotH, 5.0f);
            a += (1.0f - Fc) * g_vis;
            b += Fc * g_vis;
        }
    }

    return vec2(a, b) / float(sample_count);
}

void main()
{
    result_color = IntegrateBRDF(tex_coord.x, tex_coord.y);
}

#endif
