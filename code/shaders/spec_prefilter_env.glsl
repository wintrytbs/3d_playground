#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_vert_pos;

uniform mat4 proj;
uniform mat4 view;

out vec3 local_pos;

void main()
{
    local_pos = in_vert_pos;
    gl_Position = proj * view * vec4(in_vert_pos, 1.0f);
}

#elif defined(FRAG_ACTIVE)
#line 12

#define PI 3.14159265359f

in vec3 local_pos;
out vec4 result_color;

uniform float roughness;
uniform samplerCube env_map;

// Hammersley Points from Holger Dammertz:
// http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
float RadicalInverseVDC(uint bits)
{
    bits = (bits << 16u) | (bits >> 16u);
    bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
    bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
    bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);

    float result = float(bits) * 2.3283064365386963e-10;
    return result;
}

vec2 Hammersley(uint s_index, uint s_count)
{
    vec2 result = vec2(float(s_index) / float(s_count), RadicalInverseVDC(s_index));
    return result;
}

vec3 ImportanceSampleGGX(vec2 xi, vec3 n, float roughness)
{
    float a = roughness * roughness;
    float phi = 2.0f * PI * xi.x;
    float cosTheta = sqrt((1.0f - xi.y) / (1.0f + (a*a - 1.0f) * xi.y));
    float sinTheta = sqrt(1.0f - cosTheta * cosTheta);

    vec3 H = vec3(cos(phi)*sinTheta, sin(phi)*sinTheta, cosTheta);
    vec3 up = abs(n.z) > 0.999f ? vec3(0.0f, 0.0f, 1.0f) : vec3(1.0f, 0.0f, 0.0f);
    vec3 tangent = normalize(cross(up, n));
    vec3 bitangent = cross(n, tangent);
    vec3 sampleVec = tangent * H.x + bitangent * H.y + n * H.z;
    return normalize(sampleVec);
}

float NDFGGX(vec3 h, vec3 n, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;

    float n_dot_h = max(dot(n,h), 0.0f);
    float n_dot_h2 = n_dot_h * n_dot_h;

    float num = a2;
    float den = (n_dot_h2 * (a2 - 1.0f) + 1.0f);
    den = PI * den * den;

    float result = num / max(den, 0.000001f);
    return result;
}

void main()
{
    vec3 n = normalize(local_pos);
    vec3 r = n;
    vec3 v = r;

    const uint sample_count = 1024u;
    float weightSum = 0.0f;
    vec3 prefilteredColor = vec3(0.0f);
    float envMapDim = textureSize(env_map, 0).s;
    float envMapDim2 = envMapDim * envMapDim;
    for(uint i = 0u; i < sample_count; ++i)
    {
        vec2 xi = Hammersley(i, sample_count);
        vec3 h = ImportanceSampleGGX(xi, n, roughness);
        vec3 l = 2.0f * dot(v, h) * h - v;
        l = normalize(l);
        float NdotL = max(dot(n,l), 0.0f);

        if(NdotL > 0.0f)
        {
            float mipLevel = 0.0f;
            if(roughness > 0.0f)
            {
                float VdotH = max(dot(v, h), 0.0f);
                float NdotH = max(dot(n, h), 0.0f);

                float pdf = (NDFGGX(h, n, roughness) * NdotH) / (4.0f * VdotH + 0.0001f);
                float omegaS = 1.0f / (float(sample_count) * pdf + 0.0001f);
                float omegaP = (4.0f * PI) / (6.0f * envMapDim2);
                float mipBias = 1.0f;
                mipLevel = max(0.5f * log2(omegaS / omegaP) + mipBias, 0.0f);
            }

            prefilteredColor += textureLod(env_map, l, mipLevel).rgb * NdotL;
            weightSum += NdotL;
        }
    }

    prefilteredColor /= weightSum;

    result_color = vec4(prefilteredColor, 1.0f);
}

#endif
