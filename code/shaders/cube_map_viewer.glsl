#if defined(VERT_ACTIVE)

layout(location = 0) in vec2 in_vert_pos;
layout(location = 1) in vec2 in_tex_coord;

out vec2 tex_coord;

uniform mat4 proj;
uniform int face;

void main()
{
    if(face == 2)
    {
        tex_coord.x = (in_tex_coord.x * 2.0f) - 1.0f;
        tex_coord.y = (in_tex_coord.y *-2.0f) + 1.0f;
    }
    else if(face == 0 || face == 5)
    {
        tex_coord.x = (in_tex_coord.x *-2.0f) + 1.0f;
        tex_coord.y = (in_tex_coord.y * 2.0f) - 1.0f;
    }
    else
    {
        tex_coord.x = (in_tex_coord.x * 2.0f) - 1.0f;
        tex_coord.y = (in_tex_coord.y * 2.0f) - 1.0f;
    }

    gl_Position = proj * vec4(in_vert_pos, 0.0f, 1.0f);
}

#elif defined(FRAG_ACTIVE)

in vec2 tex_coord;

out vec4 result_color;

uniform samplerCube cube_map;
uniform int face;

void main()
{
    vec3 face_vector = vec3(0);
    switch(face)
    {
        case 0:
        {
            face_vector = vec3(1, tex_coord.y, tex_coord.x);
        } break;
        case 1:
        {
            face_vector = vec3(-1, tex_coord.y, tex_coord.x);
        } break;
        case 2:
        {
            face_vector = vec3(tex_coord.x, 1,  tex_coord.y);
        } break;
        case 3:
        {
            face_vector = vec3(tex_coord.x, -1, tex_coord.y);
        } break;
        case 4:
        {
            face_vector = vec3(tex_coord.x, tex_coord.y, 1);
        } break;
        case 5:
        {
            face_vector = vec3(tex_coord.x, tex_coord.y, -1.0);
        } break;
    }

    vec3 s = vec3(pow(texture(cube_map, face_vector).r, 16));
    //vec3 s = texture(cube_map, face_vector).rgb;
    result_color = vec4(s, 1.0f);

}

#endif
