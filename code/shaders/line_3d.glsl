#if defined(VERT_ACTIVE)

layout(location = 0) in vec3 in_vert_pos;

uniform mat4 proj_view;
uniform mat4 model;

void main()
{
    gl_Position = proj_view * model * vec4(in_vert_pos, 1.0f);
}

#elif defined(FRAG_ACTIVE)

out vec4 result_color;

uniform vec3 color;

void main()
{
    result_color = vec4(color, 1.0f);
}

#endif
