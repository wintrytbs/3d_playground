#ifndef TBS_SCENE_RENDERER_H

namespace tbs_scene
{
    struct shadow_map_render_result
    {
        matrix4D spotShadowMVPs[tbs_opengl::local_constants::PBR_SHADER_MAX_SPOT_COUNT];
        matrix4D directionalShadowMVPs[local_constants::CASCADE_SHADOW_SPLIT_COUNT];
        matrix4D pointShadowMVPs[tbs_opengl::local_constants::PBR_SHADER_MAX_POINT_COUNT];
        v2 pointShadowDepthTransforms[tbs_opengl::local_constants::PBR_SHADER_MAX_POINT_COUNT];

        u32 spotCount;
        u32 pointCount;
        u32 directionalCount;

        v4 f1;
        v4 f2;
        v4 f3;

        matrix4D cascadeProj[4];
        matrix4D cascadeView[4];
        matrix4D cascadeMVP[4];
        v3 cascadeScale[3];
        v3 cascadeOffset[3];
        v4 cascadeSplits;

        GLuint spotShadowTexArrayID;
        GLuint directionalShadowTexArrayID;
        // TODO(torgrim): When we update to opengl 4.5 we can start to use
        // texture arrays for cube maps as well.
        GLuint *pointShadowTextures;
    };

    struct draw_list
    {
        tbs_pbr::pbr_mesh **meshes;
        light_data **lights;
        matrix4D *meshTransforms;
        matrix4D *lightTransforms;
        u32 meshCount;
        u32 lightCount;

        u32 cameraCount;
        u32 *cameras;
        matrix4D *cameraTransforms;
    };


    internal shadow_map_render_result *RenderShadowMaps(memory_arena *arena, light_data *lightList, u32 lightCount, draw_list *drawList, tbs_opengl::opengl_resource_manager *manager);
    internal void DrawListRender_Shadow(pbr_scene *pbrScene, tbs_opengl::opengl_resource_manager *manager, draw_list *drawList, matrix4D projView);
    internal void CreateDrawListFromSceneNodes(pbr_scene *pbrScene, scene_node *node, draw_list *drawList, matrix4D parentTransform);
    internal draw_list CreateDrawList(memory_arena *arena, pbr_scene *pbrScene, scene_node *rootNode);
}

#define TBS_SCENE_RENDERER_H
#endif
