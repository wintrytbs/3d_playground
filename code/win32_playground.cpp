#if !defined(_MSC_VER) && !defined(__clang__)
#error "Unsupported compiler"
#endif

#if defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weverything"
#elif defined(_MSC_VER)
#pragma warning(push, 0)
#pragma warning(disable : 4626 4625 5026 5027 4530 4577)
#pragma warning(disable : 4365 4774 4710 5219)
#endif

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <process.h>
#include <cstdio>
#include <cstdint>

#include <gl/GL.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

#include <imgui/imgui.cpp>
#include <imgui/imgui_draw.cpp>
#include <imgui/imgui_widgets.cpp>

#include <meow_hash/meow_hash_x64_aesni.h>

#include <opengl/glext.h>
#include <opengl/wglext.h>

#if defined(__clang__)
#pragma GCC diagnostic pop
#elif defined(_MSC_VER)
#pragma warning(pop)
#endif

#include "tbs_types.h"
#include "tbs_math.h"
#include "base64_endec.h"

#include "tbs_base_memory.h"
#include "tbs_string.h"

#include "tbs_texture_worker.h"
#include "win32_playground.h"

#pragma warning(push)
#pragma warning(disable : 4191)
#include "win32_opengl_function_loader.cpp"
#pragma warning(pop)
#include "win32_opengl.cpp"

#include "tbs_base_memory.cpp"
#include "tbs_containers.cpp"
#include "tbs_pbr.h"

#include "tbs_image.cpp"
#include "tbs_json.cpp"
#include "tbs_gltf.cpp"

#include "tbs_primitive.cpp"
#include "tbs_light.cpp"

#include "tbs_opengl.cpp"
#include "tbs_opengl_resource_manager.cpp"

#include "tbs_imgui.cpp"
#include "tbs_imgui_widgets.cpp"

#include "tbs_scene.cpp"
#include "tbs_pbr_viewer.cpp"

global bool globalRunning = false;
global bool resizeEvent = false;

#if defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
#endif
// TODO(torgrim): Consider using std:: thread library instead/as well
internal texture_worker *Win32CreateTextureWorker(application_state *app, texture_worker_handler handler, bool StartWorker)
{
    texture_worker *worker = AllocStruct(&app->appArena, texture_worker);
    worker->inQueueSem = CreateSemaphoreA(NULL, 0, MAX_TEXTURE_QUEUE_COUNT, NULL);
    worker->outQueueSem = CreateSemaphoreA(NULL, MAX_TEXTURE_QUEUE_COUNT, MAX_TEXTURE_QUEUE_COUNT, NULL);
    worker->inQueueMtx = CreateMutexA(NULL, FALSE, NULL);
    worker->outQueueMtx = CreateMutexA(NULL, FALSE, NULL);

    worker->threadHandle = _beginthreadex(NULL, 0, handler, worker, 0, NULL);
    return worker;
}

internal bool Win32EnqueueTextureWork(texture_worker *worker, texture_queue_data data)
{
    // TODO(torgrim): Handle cases where the queue is full.
    WaitForSingleObject(worker->inQueueMtx, INFINITE);
    worker->inQueue.Enqueue(data);
    ReleaseMutex(worker->inQueueMtx);
    ReleaseSemaphore(worker->inQueueSem, 1, NULL);

    return true;
}

internal texture_worker_result Win32GetNextTextureWorkerResult(texture_worker *worker)
{
    texture_worker_result result = {};
    WaitForSingleObject(worker->outQueueMtx, 1);
    if(worker->outQueue.count > 0)
    {
        result.data = worker->outQueue.Dequeue();
        result.hasValue = true;
    }
    ReleaseMutex(worker->outQueueMtx);
    return result;
}

// TODO(torgrim): Consider creating a separate OpenGL context for
// this thread and create a shared list so that this thread is
// responsible for the whole of texture creation instead of just
// loading the image.
internal TEXTURE_WORKER_HANDLER(Win32TextureQueueHandler)
{
    texture_worker *worker = (texture_worker *)params;
    PrintDebugMsg("Created Texture Processing Thread...\n");
    while(globalRunning)
    {
        PrintDebugMsg("Waiting For Texture Input...\n");
        WaitForSingleObject(worker->inQueueSem, INFINITE);
        PrintDebugMsg("Waiting For Texture Input Queue Mutex...\n");
        WaitForSingleObject(worker->inQueueMtx, INFINITE);
        texture_queue_data input = worker->inQueue.Dequeue();
        ReleaseMutex(worker->inQueueMtx);
        PrintDebugMsg("Loading Texture Image\n");
        image_data image = LoadImageData((char *)input.path, false);

        input.image = image;
        PrintDebugMsg("Waiting For Texture Output Queue Mutex...\n");
        WaitForSingleObject(worker->outQueueMtx, INFINITE);
        PrintDebugMsg("Adding Item To The Output Queue\n");
        // TODO(torgrim): If the queue is full here we need to wait until
        // the main thread dequeues an item.
        worker->outQueue.Enqueue(input);
        ReleaseMutex(worker->outQueueMtx);
        PrintDebugMsg("Done Processing Texture Image\n");

    }
    return 0;
}

internal inline LARGE_INTEGER Win32GetWallclockTime()
{
    LARGE_INTEGER result;
    QueryPerformanceCounter(&result);

    return result;
}

internal inline f32 Win32GetElapsedSeconds(LARGE_INTEGER startTime, LARGE_INTEGER endTime)
{
    LONGLONG elapsedTicks = endTime.QuadPart - startTime.QuadPart;

    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    LONGLONG elapsedMicroseconds = elapsedTicks * 1000000;
    elapsedMicroseconds /= freq.QuadPart;

    f32 seconds = (f32)elapsedMicroseconds / 1000000.0f;

    return seconds;
}

internal file_data Win32ReadFile(memory_arena *arena, const char *fileName, bool zeroTerminate)
{
    file_data result = {};
    HANDLE fileHandle = CreateFileA(fileName,
                                    GENERIC_READ,
                                    FILE_SHARE_READ,
                                    0,
                                    OPEN_EXISTING,
                                    0,
                                    0);

    if(fileHandle == INVALID_HANDLE_VALUE)
    {
        DWORD errorCode = GetLastError();

        // NOTE: access denied. This is most often caused by when we hot reload
        // shader files because the editor where the edit was made is holding
        // a lock on the file for some time. If this is the cause we try sleep and
        // try again.
        if(errorCode == 32)
        {
            Sleep(1000);

            fileHandle = CreateFile(fileName,
                                    GENERIC_READ,
                                    FILE_SHARE_READ,
                                    0,
                                    OPEN_EXISTING,
                                    0,
                                    0);
        }

        if(fileHandle == INVALID_HANDLE_VALUE)
        {
            ASSERT(false);
            PrintDebugMsg("Win32 ERROR::Invalid File Handle, Error Code: %ld\n", GetLastError());

            return result;
        }
    }

    DWORD fileSize = GetFileSize(fileHandle, NULL);
    size_t allocSize = fileSize;
    if(zeroTerminate)
    {
        allocSize += 1;
    }
    u8 *file_content = AllocSize(arena, allocSize);
    DWORD bytesRead;
    BOOL readSuccess = ReadFile(fileHandle, (void *)file_content, fileSize, &bytesRead, NULL);
    if(readSuccess == FALSE)
    {
        ASSERT(false);
        PrintDebugMsg("Could not read file\n");
    }
    else if(zeroTerminate)
    {
        file_content[bytesRead] = '\0';
        result.content = file_content;
        result.length = bytesRead + 1;
    }
    else
    {
        result.content = file_content;
        result.length = bytesRead;
    }

    CloseHandle(fileHandle);

    return result;
}

internal void ProcessKeyEvent(u64 keyCode, bool isDown, input_state *inputState)
{
    switch(keyCode)
    {
        case 'A':
        {
            inputState->keyboard[INPUT_KEY_A].isDown = isDown;
        } break;
        case 'C':
        {
            inputState->keyboard[INPUT_KEY_C].isDown = isDown;
        } break;
        case 'D':
        {
            inputState->keyboard[INPUT_KEY_D].isDown = isDown;
        } break;
        case 'E':
        {
            inputState->keyboard[INPUT_KEY_E].isDown = isDown;
        } break;
        case 'F':
        {
            inputState->keyboard[INPUT_KEY_F].isDown = isDown;
        } break;
        case 'Q':
        {
            inputState->keyboard[INPUT_KEY_Q].isDown = isDown;
        } break;
        case 'R':
        {
            inputState->keyboard[INPUT_KEY_R].isDown = isDown;
        } break;
        case 'S':
        {
            inputState->keyboard[INPUT_KEY_S].isDown = isDown;
        } break;
        case 'W':
        {
            inputState->keyboard[INPUT_KEY_W].isDown = isDown;
        } break;
        case 'X':
        {
            inputState->keyboard[INPUT_KEY_X].isDown = isDown;
        } break;
        case 'Y':
        {
            inputState->keyboard[INPUT_KEY_Y].isDown = isDown;
        } break;
        case 'Z':
        {
            inputState->keyboard[INPUT_KEY_Z].isDown = isDown;
        } break;
        case VK_TAB:
        {
            inputState->keyboard[INPUT_KEY_Tab].isDown = isDown;
        } break;
        case VK_LEFT:
        {
            inputState->keyboard[INPUT_KEY_LeftArrow].isDown = isDown;
        } break;
        case VK_RIGHT:
        {
            inputState->keyboard[INPUT_KEY_RightArrow].isDown = isDown;
        } break;
        case VK_UP:
        {
            inputState->keyboard[INPUT_KEY_UpArrow].isDown = isDown;
        } break;
        case VK_DOWN:
        {
            inputState->keyboard[INPUT_KEY_DownArrow].isDown = isDown;
        } break;
        case VK_PRIOR:
        {
            inputState->keyboard[INPUT_KEY_PageUp].isDown = isDown;
        } break;
        case VK_NEXT:
        {
            inputState->keyboard[INPUT_KEY_PageDown].isDown = isDown;
        } break;
        case VK_HOME:
        {
            inputState->keyboard[INPUT_KEY_Home].isDown = isDown;
        } break;
        case VK_END:
        {
            inputState->keyboard[INPUT_KEY_End].isDown = isDown;
        } break;
        case VK_INSERT:
        {
            inputState->keyboard[INPUT_KEY_Insert].isDown = isDown;
        } break;
        case VK_DELETE:
        {
            inputState->keyboard[INPUT_KEY_Delete].isDown = isDown;
        } break;
        case VK_SPACE:
        {
            inputState->keyboard[INPUT_KEY_Space].isDown = isDown;
        } break;
        case VK_RETURN:
        {
            inputState->keyboard[INPUT_KEY_Enter].isDown = isDown;

            // TODO(torgrim): this can be checked for explictly
            // by checking for extended keys
            inputState->keyboard[INPUT_KEY_KeyPadEnter].isDown = isDown;
        } break;
        case VK_BACK:
        {
            inputState->keyboard[INPUT_KEY_Backspace].isDown = isDown;
        } break;
        case VK_ESCAPE:
        {
            // TODO(torgrim): this should just be mapped like
            // the rest of the keys and then we handle this
            // in our main loop instead.
            globalRunning = false;
        } break;
        case VK_CONTROL:
        {
            inputState->keyboard[INPUT_KEY_Ctrl].isDown = isDown;
        } break;
        case VK_SHIFT:
        {
            inputState->keyboard[INPUT_KEY_Shift].isDown = isDown;
        } break;
        case VK_MENU:
        {
            inputState->keyboard[INPUT_KEY_Alt].isDown = isDown;
        } break;
        case VK_ADD:
        {
            inputState->keyboard[INPUT_KEY_Plus].isDown = isDown;
        } break;
        case VK_SUBTRACT:
        {
            inputState->keyboard[INPUT_KEY_Minus].isDown = isDown;
        } break;
    }

}

internal LRESULT CALLBACK MainWindowCallback(HWND window, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;
    switch(msg)
    {
        case WM_CLOSE:
        case WM_DESTROY:
        {
            PostQuitMessage(0);
        } break;
        case WM_SIZE:
        {
            resizeEvent = true;
        } break;
        default:
        {
            result = DefWindowProc(window, msg, wParam, lParam);
        }
    }

    return result;
}

internal void PrintMeowHash(meow_u128 hash)
{
    PrintDebugMsg("    %08X-%08X-%08X-%08X\n",
                       MeowU32From(hash, 3),
                       MeowU32From(hash, 2),
                       MeowU32From(hash, 1),
                       MeowU32From(hash, 0));
}

internal char *Win32CreateExePath()
{
    char *exePath = (char *)malloc(sizeof(char) * MAX_PATH);
    GetModuleFileNameA(NULL, exePath, MAX_PATH);
    i32 offset = GetParentDirectoryOffset(exePath);
    ASSERT(offset > -1);
    ASSERT(offset < MAX_PATH-1);
    memset(exePath+offset+1, 0, (size_t)(MAX_PATH-(offset+1)));

    return exePath;
}

// TODO: Clean up the handling of these
internal char *CreateAppRootPath(application_state *app)
{
    char *rootPath = (char *)malloc(sizeof(char) * MAX_PATH);
    i32 offset = GetParentDirectoryOffset(app->exePath);
    ASSERT(offset > -1);
    RawStringCopy(rootPath, app->exePath, (size_t)(offset+1));
    return rootPath;
}

internal char *CreateTexturePath(application_state *app)
{
    char *texPath = (char *)malloc(sizeof(char) * MAX_PATH);
    RawStringConcat(texPath, app->appRootPath, "assets\\textures\\");
    return texPath;
}

internal char *CreateShaderPath(application_state *app)
{
    char *shaderPath = (char *)malloc(sizeof(char) * MAX_PATH);
    RawStringConcat(shaderPath, app->appRootPath, "code\\shaders\\");
    return shaderPath;
}

internal char *CreateHDRPath(application_state *app)
{
    char *hdrPath = (char *)malloc(sizeof(char) * MAX_PATH);
    RawStringConcat(hdrPath, app->appRootPath, "assets\\HDR\\");
    return hdrPath;
}

int WinMain(HINSTANCE instance, HINSTANCE prevInstance, PSTR cmdLine, int cmdShow)
{
    HWND windowHandle = Win32InitOpenGL(instance, MainWindowCallback, 1080, 900);
    if(windowHandle != NULL)
    {
        // TODO(torgrim): The appState should not be allocated on the stack. Allocate it onto our app arena.
        // TODO(torgrim): Or move things that we don't really want on the appstate into a different structure
        // so that appstate can just be allocated on the stack and then most other things that we use is allocated
        // from an arena.
        application_state appState = {};
        appState.exePath = Win32CreateExePath();
        appState.appRootPath = CreateAppRootPath(&appState);
        appState.texturePath = CreateTexturePath(&appState);
        appState.shaderPath = CreateShaderPath(&appState);
        appState.hdrPath = CreateHDRPath(&appState);

        appState.windowHandle = windowHandle;
        // TODO(torgrim): Consider having a base address in debug mode to make debugging memory related issues
        // easier.
        memory_arena baseArena = {};
        baseArena.size = GB(2);
        baseArena.base =(u8 *)VirtualAlloc(NULL, baseArena.size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
        baseArena.offset = 0;
        baseArena.type = ArenaType_Base;

        appState.baseArena = &baseArena;
        appState.appArena = MakeSubArena(&baseArena, MB(500), ArenaType_App);
        appState.frameArena = MakeSubArena(&baseArena, MB(50), ArenaType_Frame);
        appState.sceneNodePool = CreateStructPool(&baseArena, tbs_scene::scene_node, tbs_scene::local_constants::MAX_SCENE_NODE_COUNT);
        appState.tempArena = MakeTailArena(&baseArena, ArenaType_Temp);

        tbs_gui::imgui_state *imGuiState = nullptr;
        {
            char settingsFilePath[MAX_PATH];
            RawStringConcat(settingsFilePath, appState.exePath, "tbs_imgui.ini");
            imGuiState = tbs_gui::InitImGui(&appState.appArena, settingsFilePath);
        }

        RECT clientRect;
        GetClientRect(windowHandle, &clientRect);
        appState.clientW = (f32)clientRect.right;
        appState.clientH = (f32)clientRect.bottom;
        appState.viewportSize = v2i(clientRect.right, clientRect.bottom);

        // TODO(torgrim): Do not like this where the manager belongs to appstate and then the appstate
        // also needs to be passed to the resource manager(because we need memory allocation)??
        // Should really look into separating out memory from the appState into it's own thing?
        tbs_opengl::opengl_resource_manager *rm = AllocStruct(&appState.appArena, tbs_opengl::opengl_resource_manager);
        rm->Init(&appState);

        tbs_scene::pbr_scene *pbrScene = AllocStruct(&appState.appArena, tbs_scene::pbr_scene);
        pbrScene->Init();
        pbrScene->scenes[pbrScene->sceneCount++] = tbs_scene::CreateDefaultScene(&appState, pbrScene);
        pbrScene->activeSceneIndex = 0;
        pbrScene->viewMode = tbs_scene::VIEW_MODE_Default;


        appState.mode = APP_MODE_PBRViewer;

        tbs_pbr_viewer::pbr_viewer *pbrViewer = AllocStruct(&appState.appArena, tbs_pbr_viewer::pbr_viewer);
        pbrViewer->camera.pos = v3(0.0f, 0.0f, 3.0f);
        pbrViewer->camera.radius = 3.0f;
        pbrViewer->camera.target = zeroVector3D;
        pbrViewer->camera.clipNear = 0.1f;
        pbrViewer->camera.clipFar = 1000.0f;
        pbrViewer->camera.phi = 90.0f;
        pbrViewer->camera.theta = 90.0f;
        pbrViewer->useEnvLighting = true;
        pbrViewer->backgroundColor[0] = 0.1f;
        pbrViewer->backgroundColor[1] = 0.1f;
        pbrViewer->backgroundColor[2] = 0.1f;
        pbrViewer->viewportSize = v2i(1920, 1080);
        {
            tbs_pbr::pbr_mesh defaultMesh = {};
            defaultMesh.primitives = tbs_array_sr<tbs_pbr::pbr_primitive>(&appState.appArena, 1);
            tbs_pbr::pbr_primitive defaultPrim = {};
            defaultPrim.renderDataHandle = tbs_opengl::BASE_PRIMITIVE_TYPE_Cube;
            defaultPrim.materialHandle = 0;
            defaultMesh.primitives.Add(defaultPrim);
            pbrViewer->meshes[pbrViewer->meshCount++] = defaultMesh;
            pbrViewer->materials[pbrViewer->materialCount++] = pbrScene->materials[0];


            pbrViewer->mainLight.position = v3(3.0f, 5.0f, 0.0f);
            pbrViewer->mainLight.color = v3(1.0f, 1.0f, 1.0f);
            pbrViewer->mainLight.type = LIGHT_TYPE_Point;

            pbrViewer->whitePoint = 1.0f;

            tbs_pbr_viewer::group_link *link = AllocStruct(&appState.appArena, tbs_pbr_viewer::group_link);
            *link = {pbrViewer->meshes, CreateIdentity4D(), nullptr};
            pbrViewer->groups[pbrViewer->groupCount++] = {"Default Cube", "\0", link};
            pbrViewer->activeGroup = 0;

        }

        glClearColor(0.07f, 0.07f, 0.07f, 1.0f);
        globalRunning = true;

        appState.textureWorker = Win32CreateTextureWorker(&appState, Win32TextureQueueHandler, true);

#if 0
        {
            tbs_scene::scene_state *testScene = tbs_scene::CreateEmptyScene(&appState);
            testScene->AddFromGltfFile(&appState, "C:/programming/example_projects/Vulkan/data/models/terrain_gridlines.gltf");
            testScene->AddFromGltfFile(&appState, "C:/programming/example_projects/Vulkan/data/models/oaktree.gltf");
            light_data defaultLight = CreateLight(LIGHT_TYPE_Directional);
            defaultLight.position.x = 10.0f;
            defaultLight.position.y = 10.0f;
            defaultLight.rotation.x = -45.0f;
            auto defaultLightHandle = appState.pbrScene->AddLight(defaultLight);
            testScene->CreateAndAddTopLevelNode(&appState.sceneNodePool, tbs_opengl::HandleToVoid(defaultLightHandle), "Default Light", tbs_scene::SCENE_NODE_TYPE_Light);
            u32 newSceneIndex = appState.pbrScene->sceneCount;
            appState.pbrScene->scenes[appState.pbrScene->sceneCount++] = testScene;
            appState.pbrScene->activeSceneIndex = newSceneIndex;
        }
#endif

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        ShowWindow(windowHandle, SW_MAXIMIZE);
        while(globalRunning)
        {
            imGuiState->characterBufferCount = 0;
            appState.inputState.mouse.scrollDelta = 0;
            appState.inputState.mouse.pressedThisFrame = false;

            RECT tmpSize;
            GetClientRect(windowHandle, &tmpSize);

            MSG msg;
            while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
            {
                TranslateMessage(&msg);
                switch(msg.message)
                {
                    case WM_CHAR:
                    {
                        ASSERT(imGuiState->characterBufferCount < ARRAY_COUNT(imGuiState->characterBuffer));
                        imGuiState->characterBuffer[imGuiState->characterBufferCount++] = (u32)msg.wParam;
                    } break;
                    case WM_KEYDOWN:
                    case WM_KEYUP:
                    {
                        u64 keyCode = msg.wParam;
                        bool wasDown = (((msg.lParam >> 30) & 1) != 0);
                        bool isDown = (((msg.lParam >> 31) & 1) == 0);
                        if(wasDown != isDown)
                        {
                            ProcessKeyEvent(keyCode, isDown, &appState.inputState);
                        }
                    } break;
                    case WM_LBUTTONDOWN:
                    {
                        appState.inputState.mouse.leftButtonDown = true;
                    } break;
                    case WM_LBUTTONUP:
                    {
                        appState.inputState.mouse.leftButtonDown = false;
                    } break;
                    case WM_RBUTTONDOWN:
                    {
                        appState.inputState.mouse.rightButtonDown = true;
                        appState.inputState.mouse.pressedThisFrame = true;
                        appState.inputState.mouse.downPos = v2(LOWORD(msg.lParam), HIWORD(msg.lParam));
                    } break;
                    case WM_RBUTTONUP:
                    {
                        appState.inputState.mouse.rightButtonDown = false;
                    } break;
                    case WM_MBUTTONDOWN:
                    {
                        appState.inputState.mouse.middleButtonDown = true;
                    } break;
                    case WM_MBUTTONUP:
                    {
                        appState.inputState.mouse.middleButtonDown = false;
                    } break;
                    case WM_MOUSEWHEEL:
                    {
                        appState.inputState.mouse.scrollDelta = GET_WHEEL_DELTA_WPARAM(msg.wParam);
                    } break;
                    case WM_QUIT:
                    {
                        globalRunning = false;
                    } break;
                    default:
                    {
                        DispatchMessage(&msg);
                    }
                }
            }

            LARGE_INTEGER frameStart = Win32GetWallclockTime();

            POINT cPos;
            GetCursorPos(&cPos);
            ScreenToClient(windowHandle, &cPos);
            appState.inputState.mouse.prevPos = appState.inputState.mouse.pos;
            appState.inputState.mouse.pos = v2((f32)cPos.x, (f32)cPos.y);

            rm->CheckAndUpdateModifiedShaders(&appState);

            GetClientRect(windowHandle, &clientRect);
            appState.clientW = (f32)clientRect.right;
            appState.clientH = (f32)clientRect.bottom;

            texture_worker_result texWorkerResult = Win32GetNextTextureWorkerResult(appState.textureWorker);
            if(texWorkerResult.hasValue)
            {
                OutputDebugString("Creating Texture from Texture Output Queue\n");
                texture_queue_data data = texWorkerResult.data;
                rm->LoadTexture((tbs_opengl::opengl_resource_handle)data.handle, &data.image);
                OutputDebugString("Done Creating Texture\n");
                FreeImageData(&data.image);
            }

            switch(appState.mode)
            {
                case APP_MODE_PBRViewer:
                {
                    tbs_pbr_viewer::UpdateAndRenderPBRViewer(&appState, imGuiState, rm, pbrViewer);
                } break;
                case APP_MODE_PBRScene:
                {
                    tbs_scene::UpdateAndRenderPBRScene(&appState, imGuiState, rm, pbrScene);
                } break;
                default:
                {
                    ASSERT(false);
                }
            }

            HDC deviceContext = GetDC(windowHandle);
            SwapBuffers(deviceContext);

            LARGE_INTEGER frameEnd = Win32GetWallclockTime();

            appState.dt = Win32GetElapsedSeconds(frameStart, frameEnd);
            appState.runtime += appState.dt;
            appState.frameArena.offset = 0;
        }
    }

    PrintDebugMsg("Exiting application\n");

    return 0;
}

#if defined(__clang__)
#pragma GCC diagnostic pop
#endif
