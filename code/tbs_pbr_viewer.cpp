#include "tbs_pbr_viewer.h"

namespace tbs_pbr_viewer
{
    u64 pbr_viewer::AddMaterial(tbs_pbr::pbr_material mat)
    {
        ASSERT(this->materialCount < ARRAY_COUNT(this->materials));
        u64 handle = this->materialCount;
        this->materials[this->materialCount++] = mat;
        return handle;
    }

    i32 pbr_viewer::CreateAndAddMeshGroupFromGltf(application_state *app, tbs_opengl::opengl_resource_manager *rm, gltf_root *gltf, char *name)
    {
        struct stack_value
        {
            gltf_node *node;
            u64 visitCount;
            matrix4D transform;
        };

        mesh_group group = {};
        group_link dummyLink = {};
        group_link *currentLink = &dummyLink;
        ASSERT(gltf->scenes.count == 1);

        temp_memory scratch(app);
        tbs_array_sr<i64> rootNodes = gltf->scenes.GetItem(0u)->rootNodes;
        limit_stack<stack_value> nodeStack(scratch.arena, 100);
        for(size_t i = 0; i < rootNodes.count; ++i)
        {
            i64 nIndex = rootNodes.data[i];
            gltf_node *node = gltf->nodes.GetItemI64(nIndex);
            matrix4D rootTrans = CreateTranslate(node->translation) * CreateRotate(node->rotation) * CreateScale(node->scale);
            nodeStack.push({node, 0, rootTrans});
            while(nodeStack.count > 0)
            {
                gltf_node *topNode = nodeStack.top->node;
                if(nodeStack.top->visitCount < topNode->children.count)
                {
                    i64 childIndex = topNode->children.data[nodeStack.top->visitCount];
                    gltf_node *childNode = gltf->nodes.GetItemI64(childIndex);
                    matrix4D childTrans = nodeStack.top->transform * CreateTranslate(childNode->translation) * CreateRotate(childNode->rotation) * CreateScale(childNode->scale);
                    nodeStack.top->visitCount++;

                    nodeStack.push({childNode, 0, childTrans});

                }
                else
                {
                    if(topNode->meshIndex > -1)
                    {
                        gltf_mesh *gltfMesh = gltf->meshes.GetItemI64(topNode->meshIndex);
                        tbs_pbr::pbr_mesh mesh;
                        mesh.primitives = tbs_array_sr<tbs_pbr::pbr_primitive>(&app->appArena, gltfMesh->primitives.count);
                        for(size_t pIndex = 0; pIndex < gltfMesh->primitives.count; ++pIndex)
                        {
                            gltf_primitive *gltfPrim = gltfMesh->primitives.GetItem(pIndex);
                            // TODO(torgrim): Positions are required so if we don't have
                            // positions it's considered invalid.

                            ASSERT(gltfPrim->mode == GLTF_PRIMITIVE_MODE_Triangles);

                            triangle_list triangleList = {};

                            ASSERT(gltfPrim->positions.size % sizeof(v3) == 0);
                            size_t vertexAttribCount = gltfPrim->positions.size / sizeof(v3);

                            tbs_opengl::data_buffer posBuffer = {};
                            tbs_opengl::data_buffer norBuffer = {};
                            tbs_opengl::data_buffer tanBuffer = {};
                            tbs_opengl::data_buffer uv0Buffer = {};
                            tbs_opengl::data_buffer indBuffer = {};

                            if(gltfPrim->indices.size > 0)
                            {
                                indBuffer.buff = gltfPrim->indices.data;
                                indBuffer.size = gltfPrim->indices.size;
                                indBuffer.dataType = gltfPrim->indices.componentType;

                                if(indBuffer.dataType == GL_UNSIGNED_BYTE)
                                {
                                    size_t count = gltfPrim->indices.size / sizeof(u8);
                                    triangleList = CreateTriangleListU8(scratch.arena, (u8 *)indBuffer.buff, count);
                                }
                                else if(indBuffer.dataType == GL_UNSIGNED_SHORT)
                                {
                                    size_t count = gltfPrim->indices.size / sizeof(u16);
                                    triangleList = CreateTriangleListU16(scratch.arena, (u16 *)indBuffer.buff, count);
                                }
                                else if(indBuffer.dataType == GL_UNSIGNED_INT)
                                {
                                    size_t count = gltfPrim->indices.size / sizeof(u32);
                                    triangleList = CreateTriangleListU32(scratch.arena, (u32 *)indBuffer.buff, count);
                                }
                                else
                                {
                                    ASSERT(false);
                                }
                            }
                            else
                            {
                                triangleList = CreateSimpleTriangleList(scratch.arena, vertexAttribCount);
                            }


                            if(gltfPrim->positions.size > 0)
                            {
                                posBuffer.buff = gltfPrim->positions.data;
                                posBuffer.size = gltfPrim->positions.size;
                            }
                            else
                            {
                                ASSERT(false);
                            }

                            bool recalculateTangents = false;
                            if(gltfPrim->normals.size > 0)
                            {
                                norBuffer.buff = gltfPrim->normals.data;
                                norBuffer.size = gltfPrim->normals.size;
                            }
                            else
                            {
                                recalculateTangents = true;

                                norBuffer.buff = AllocSize(scratch.arena, posBuffer.size);
                                norBuffer.size = posBuffer.size;
                                CalcFlatNormals((v3 *)posBuffer.buff, &triangleList, (v3 *)norBuffer.buff);
                            }

                            if(gltfPrim->uv0.size > 0)
                            {
                                uv0Buffer.buff = gltfPrim->uv0.data;
                                uv0Buffer.size = gltfPrim->uv0.size;

                                if(!recalculateTangents && gltfPrim->tangents.size > 0)
                                {
                                    tanBuffer.buff = gltfPrim->tangents.data;
                                    tanBuffer.size = gltfPrim->tangents.size;
                                }
                                else
                                {
                                    tanBuffer.size = vertexAttribCount * sizeof(v4);
                                    tanBuffer.buff = AllocSize(scratch.arena, tanBuffer.size);
                                    CalcMeshTangents(scratch.arena,
                                                     (v3 *)posBuffer.buff, posBuffer.size / sizeof(v3),
                                                     triangleList.triangles, triangleList.count,
                                                     (v2 *)uv0Buffer.buff, uv0Buffer.size / sizeof(v2),
                                                     (v3 *)norBuffer.buff,
                                                     (v4 *)tanBuffer.buff);
                                }
                            }

                            tbs_opengl::primitive_buffer_collection bufferCollection = {};
                            bufferCollection.posBuffer = posBuffer;
                            bufferCollection.uv0Buffer = uv0Buffer;
                            bufferCollection.norBuffer = norBuffer;
                            bufferCollection.tanBuffer = tanBuffer;
                            bufferCollection.indBuffer = indBuffer;
                            auto primHandle = rm->CreateRenderablePrimitive(bufferCollection, gltfPrim->mode);


                            gltf_material *gltfMat = gltf->materials.GetItemOrNullI64(gltfPrim->materialIndex);
                            ASSERT(gltfMat != nullptr);
                            tbs_pbr::pbr_material mat = {};

                            // NOTE(torgrim): Initialize all textures to an invalid texture handle.
                            for(size_t texIndex = 0; texIndex < ARRAY_COUNT(mat.textures); ++texIndex)
                            {
                                mat.textures[texIndex] = tbs_opengl::invalidHandle;
                            }

                            gltf_texture *gltfNorTex = gltf->textures.GetItemOrNullI64(gltfMat->normalTextureIndex);
                            gltf_texture *gltfOccTex = gltf->textures.GetItemOrNullI64(gltfMat->occlusionTextureIndex);
                            gltf_texture *gltfEmiTex = gltf->textures.GetItemOrNullI64(gltfMat->emissiveTexture.texIndex);
                            gltf_texture *gltfBasTex = gltf->textures.GetItemOrNullI64(gltfMat->pbr.baseColorTexture.texIndex);
                            gltf_texture *gltfMetTex = gltf->textures.GetItemOrNullI64(gltfMat->pbr.metallicRoughnessTexture.texIndex);

                            // TODO(torgrim): All of these textures can have specific tex coord set that should
                            // be used.
                            auto texHandle = tbs_scene::EnqueueGltfTexture(app, rm, gltf, gltfNorTex);
                            if(texHandle != tbs_opengl::invalidHandle)
                            {
                                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_Normal] = texHandle;
                                mat.normalScaleFactor = (f32)gltfMat->normalScaleFactor;
                                ASSERT(mat.normalScaleFactor == 1.0f);
                            }
                            texHandle = tbs_scene::EnqueueGltfTexture(app, rm, gltf, gltfOccTex);
                            if(texHandle != tbs_opengl::invalidHandle)
                            {
                                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_Occlusion] = texHandle;
                                mat.occlusionStrengthFactor = (f32)gltfMat->occlusionStrengthFactor;
                                ASSERT(mat.occlusionStrengthFactor == 1.0f);
                            }
                            texHandle = tbs_scene::EnqueueGltfTexture(app, rm, gltf, gltfEmiTex, tbs_opengl::TEXTURE_INTERNAL_FORMAT_SRGBA);
                            if(texHandle != tbs_opengl::invalidHandle)
                            {
                                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_Emissive] = texHandle;
                                mat.emissiveFactor = gltfMat->emissiveFactor;
                                ASSERT(mat.emissiveFactor.x == 1.0f);
                                ASSERT(mat.emissiveFactor.y == 1.0f);
                                ASSERT(mat.emissiveFactor.z == 1.0f);
                            }
                            texHandle = tbs_scene::EnqueueGltfTexture(app, rm, gltf, gltfBasTex, tbs_opengl::TEXTURE_INTERNAL_FORMAT_SRGBA);
                            if(texHandle != tbs_opengl::invalidHandle)
                            {
                                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_BaseColor] = texHandle;
                            }
                            texHandle = tbs_scene::EnqueueGltfTexture(app, rm, gltf, gltfMetTex);
                            if(texHandle != tbs_opengl::invalidHandle)
                            {
                                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_MetallicRoughness] = texHandle;
                            }

                            mat.isDoubleSided = gltfMat->doubleSided;
                            mat.baseColor = gltfMat->pbr.baseColorFactor;
                            mat.metallicFactor = (f32)gltfMat->pbr.metallicFactor;
                            mat.roughnessFactor = (f32)gltfMat->pbr.roughnessFactor;
                            mat.alphaCutoff = (f32)gltfMat->alphaCutoff;
                            mat.alphaMode = (tbs_pbr::material_alpha_mode)gltfMat->alphaMode;

                            auto matHandle = this->AddMaterial(mat);

                            tbs_pbr::pbr_primitive prim = {};
                            prim.materialHandle = matHandle;
                            prim.renderDataHandle = primHandle;
                            mesh.primitives.Add(prim);
                        }

                        ASSERT(this->meshCount < ARRAY_COUNT(this->meshes));
                        tbs_pbr::pbr_mesh *meshSlot = &this->meshes[this->meshCount++];
                        *meshSlot = mesh;
                        group_link *nextLink = AllocStruct(&app->appArena, group_link);
                        nextLink->m = meshSlot;
                        nextLink->t = nodeStack.top->transform;
                        currentLink->next = nextLink;
                        currentLink = nextLink;
                    }

                    nodeStack.pop();
                }
            }
        }

        i32 resultIndex = -1;
        if(dummyLink.next != nullptr)
        {
            ASSERT(this->groupCount < ARRAY_COUNT(this->groups));
            RawStringCopy(group.name, name);
            // TODO(torgrim): Make separate argument for filename
            RawStringCopy(group.file, name);
            group.first = dummyLink.next;
            resultIndex = (i32)this->groupCount;
            this->groups[this->groupCount++] = group;
        }

        return resultIndex;
    }

    internal void UpdateAndRenderPBRViewer(application_state *app, tbs_gui::imgui_state *imGuiState, tbs_opengl::opengl_resource_manager *rm, pbr_viewer *pbrViewer)
    {
        input_state *inputState = &app->inputState;
        orbit_camera *camera = &pbrViewer->camera;

        if(!tbs_gui::IsInputHandledByImGui())
        {
            if(inputState->mouse.middleButtonDown)
            {
                v2 delta = inputState->mouse.pos - inputState->mouse.prevPos;

                camera->phi -= delta.y * app->dt * 100.0f;
                camera->theta += delta.x * app->dt * 100.0f;

                if(camera->phi <= 0.0f)
                {
                    camera->phi = 0.1f;
                }
                else if(camera->phi >= 180.0f)
                {
                    camera->phi = 179.9f;
                }

                f32 radPhi = DegreeToRadians(camera->phi);
                f32 radTheta = DegreeToRadians(camera->theta);
                camera->pos.x = camera->radius * Sin(radPhi) * Cos(radTheta);
                camera->pos.z = camera->radius * Sin(radPhi) * Sin(radTheta);
                camera->pos.y = camera->radius * Cos(radPhi);
            }
            else if(inputState->mouse.scrollDelta != 0.0f)
            {
                // TODO(torgrim): Adjust the speed here based on the distance
                // from the target(close slow, far away fast).
                camera->radius += -inputState->mouse.scrollDelta * app->dt;
                if(camera->radius <= 0.0f)
                {
                    camera->radius = 0.1f;
                }
                f32 radPhi = DegreeToRadians(camera->phi);
                f32 radTheta = DegreeToRadians(camera->theta);
                camera->pos.x = camera->radius * Sin(radPhi) * Cos(radTheta);
                camera->pos.z = camera->radius * Sin(radPhi) * Sin(radTheta);
                camera->pos.y = camera->radius * Cos(radPhi);
            }

        }

        GLuint msaaFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_PBRViewerOffscreenMsaa);
        GLuint swapFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_Swap);
        GLuint tonemapGammaFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_PBRViewerTonemappingGamma);

        if(pbrViewer->resizeEvent)
        {
            OpenGLResizeFramebufferAttachmentsMsaa(msaaFBO, pbrViewer->viewportSize.x, pbrViewer->viewportSize.y, GL_RGBA32F);
            OpenGLResizeFramebufferAttachments(swapFBO, pbrViewer->viewportSize.x, pbrViewer->viewportSize.y, GL_RGBA32F);
            OpenGLResizeFramebufferAttachments(tonemapGammaFBO, pbrViewer->viewportSize.x, pbrViewer->viewportSize.y, GL_RGBA8);
            pbrViewer->resizeEvent = true;
        }

        pbrViewer->mainLight.position.x = Cos(app->runtime) * 4.0f;
        pbrViewer->mainLight.position.z = Sin(app->runtime) * 4.0f;

        matrix4D view = LookAt(camera->pos, camera->target, axisVectorY);

        f32 aspect = (f32)pbrViewer->viewportSize.x / (f32)pbrViewer->viewportSize.y;
        //ASSERT(aspect > 1.0f);
        f32 fov = PI / 3.0f;
        matrix4D proj = CreateRevFrustum(fov, aspect, camera->clipNear, camera->clipFar);

        glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
        if(pbrViewer->msaaActive)
        {
            glEnable(GL_MULTISAMPLE);
        }

        glClearColor(pbrViewer->backgroundColor[0], pbrViewer->backgroundColor[1], pbrViewer->backgroundColor[2], 1.0f);
        glViewport(0, 0, pbrViewer->viewportSize.x, pbrViewer->viewportSize.y);

        // NOTE: Render Geometry/Meshes into msaa framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, msaaFBO);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        if(pbrViewer->backgroundOption != BACKGROUND_OPTION_SolidColor)
        {
            RenderBackground(pbrViewer, rm, proj, view);
        }
        RenderMeshGroup(pbrViewer, rm, pbrViewer->activeGroup, proj * view);

        // NOTE: Blit MSAA pass
        glBindFramebuffer(GL_READ_FRAMEBUFFER, msaaFBO);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, swapFBO);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBlitFramebuffer(0, 0, pbrViewer->viewportSize.x, pbrViewer->viewportSize.y,
                          0, 0, pbrViewer->viewportSize.x, pbrViewer->viewportSize.y,
                          GL_COLOR_BUFFER_BIT,
                          GL_LINEAR);

        //glViewport(0, 0, 512, 768);
        // NOTE: Tonemapping and gamma correction
        GLuint texID = OpenGLGetFramebufferColorAttachment(swapFBO);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, tonemapGammaFBO);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, tonemapGammaFBO);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        PostProcessTonemappingGamma(pbrViewer, rm, texID);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, tonemapGammaFBO);

        // NOTE: Blit to default framebuffer
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBlitFramebuffer(0, 0,
                          pbrViewer->viewportSize.x, pbrViewer->viewportSize.y,
                          pbrViewer->viewportPos.x, pbrViewer->viewportPos.y,
                          pbrViewer->viewportPos.x + pbrViewer->viewportSize.x,
                          pbrViewer->viewportPos.y + pbrViewer->viewportSize.y,
                          GL_COLOR_BUFFER_BIT,
                          GL_LINEAR);

        // NOTE: Render GUI directly to default framebuffer
        glViewport(0, 0, (i32)app->clientW, (i32)app->clientH);
        RenderGUI(app, imGuiState, rm, pbrViewer);

        glDisable(GL_MULTISAMPLE);
        glDisable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    }

    internal void RenderBackground(pbr_viewer *pbrViewer, tbs_opengl::opengl_resource_manager *manager, matrix4D proj, matrix4D view)
    {
        auto *prim = manager->GetBasePrimitive(tbs_opengl::BASE_PRIMITIVE_TYPE_Cube);
        ASSERT(prim->VAOID > 0);

        GLuint progID = manager->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_Skybox);
        glUseProgram(progID);

        auto *envMap = manager->GetEnvMap(pbrViewer->activeEnvMap);
        glUniform1i(glGetUniformLocation(progID, "skybox_tex"), 0);
        if(pbrViewer->backgroundOption == BACKGROUND_OPTION_EnvironmentMap)
        {
            glBindTexture(GL_TEXTURE_CUBE_MAP, envMap->envMap);
            glUniform1f(glGetUniformLocation(progID, "tex_lod"), 0.0f);
        }
        else
        {
            glBindTexture(GL_TEXTURE_CUBE_MAP, envMap->specPrefilterMap);
            glUniform1f(glGetUniformLocation(progID, "tex_lod"), 1.5f);
        }

        matrix4D skyboxView = view;
        skyboxView(0,3) = 0.0f;
        skyboxView(1,3) = 0.0f;
        skyboxView(2,3) = 0.0f;

        glUniformMatrix4fv(glGetUniformLocation(progID, "proj"), 1, GL_FALSE, Mat4Addr(proj));
        glUniformMatrix4fv(glGetUniformLocation(progID, "view"), 1, GL_FALSE, Mat4Addr(skyboxView));

        glDepthFunc(GL_LEQUAL);
        glDepthMask(GL_FALSE);
        glBindVertexArray(prim->VAOID);
        glDrawArrays(GL_TRIANGLES, 0, (GLsizei)prim->vertexCount);
        glDepthMask(GL_TRUE);
        glDepthFunc(GL_LESS);
        glBindVertexArray(0);
    }

    internal void PostProcessTonemappingGamma(pbr_viewer *pbrViewer, tbs_opengl::opengl_resource_manager *rm, GLuint texID)
    {
        GLuint programID = rm->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_TonemappingGamma);
        glUseProgram(programID);

        OpenGLSetActiveTextureUnit(0);
        glBindTexture(GL_TEXTURE_2D, texID);
        glUniform1i(glGetUniformLocation(programID, "tex"), 0);
        glUniform1i(glGetUniformLocation(programID, "use_gamma"), pbrViewer->useGammaCorrection);
        glUniform1i(glGetUniformLocation(programID, "use_tonemapping"), pbrViewer->useTonemapping);
        glUniform1f(glGetUniformLocation(programID, "max_white"), pbrViewer->whitePoint);
        glUniform1i(glGetUniformLocation(programID, "tonemapping_type"), pbrViewer->tonemappingType);

        f32 vertexAttribs[] =
        {
            -1.0f, -1.0f, 0.0f, 0.0f,
             1.0f, -1.0f, 1.0f, 0.0f,
             1.0f,  1.0f, 1.0f, 1.0f,

             1.0f,  1.0f, 1.0f, 1.0f,
            -1.0f,  1.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f,
        };

        GLuint fullscreenVAOID;
        GLuint fullscreenVBOID;
        glGenVertexArrays(1, &fullscreenVAOID);
        glBindVertexArray(fullscreenVAOID);
        glGenBuffers(1, &fullscreenVBOID);
        glBindBuffer(GL_ARRAY_BUFFER, fullscreenVBOID);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexAttribs), vertexAttribs, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(f32) * 4, nullptr);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(f32) * 4, (void *)(sizeof(f32) * 2));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);


        glDrawArrays(GL_TRIANGLES, 0, 6);


        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteVertexArrays(1, &fullscreenVAOID);
        glDeleteBuffers(1, &fullscreenVBOID);

    }

    internal void RenderGUI(application_state *app, tbs_gui::imgui_state *imGuiState, tbs_opengl::opengl_resource_manager *rm, pbr_viewer *pbrViewer)
    {
        auto *localGuiState = &pbrViewer->guiState;

        ImGuiIO& io = ImGui::GetIO();

        io.DisplaySize = ImVec2(app->clientW, app->clientH);
        io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);
        io.DeltaTime = app->dt;

        io.MouseDown[0] = app->inputState.mouse.leftButtonDown;
        io.MouseDown[1] = app->inputState.mouse.rightButtonDown;
        io.MouseWheel = app->inputState.mouse.scrollDelta * 0.01f;

        if((io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange) == 0)
        {
            ImGuiMouseCursor imguiCursor = ImGui::GetMouseCursor();
            if(imguiCursor == ImGuiMouseCursor_None || io.MouseDrawCursor)
            {
                ASSERT(false);
            }
            else
            {
                // TODO: Windows specific
                SetCursor(imGuiState->cursorMap[imguiCursor]);
            }
        }


        for(i32 keyIndex= 0; keyIndex < INPUT_KEY_COUNT; ++keyIndex)
        {
            io.KeysDown[keyIndex] = app->inputState.keyboard[keyIndex].isDown;
        }

        io.KeyCtrl = io.KeysDown[INPUT_KEY_Ctrl];
        io.KeyShift = io.KeysDown[INPUT_KEY_Shift];
        io.KeyAlt = io.KeysDown[INPUT_KEY_Alt];

        if(imGuiState->characterBufferCount != 0)
        {
            for(u32 cIndex = 0; cIndex < imGuiState->characterBufferCount; ++cIndex)
            {
                io.AddInputCharacter(imGuiState->characterBuffer[cIndex]);
            }
        }

        // TODO(torgrim): Not sure what this is suppose to do. Find out.
        // TODO(torgrim): This is Windows specific.
        if(io.WantSetMousePos)
        {
            POINT screenPos = {(i32)io.MousePos.x, (i32)io.MousePos.y};
            ClientToScreen(app->windowHandle, &screenPos);
            SetCursorPos((i32)io.MousePos.x, (i32)io.MousePos.y);
        }
        else
        {
            POINT curPos;
            GetCursorPos(&curPos);
            ScreenToClient(app->windowHandle, &curPos);
            f32 curX = (f32)curPos.x;
            // NOTE(torgrim): IMGUI uses (0,0) = top,left
            f32 curY = (f32)curPos.y;
            io.MousePos = ImVec2(curX, curY);
        }

        ImGui::NewFrame();

        // TODO(torgrim): Try to also just render our main viewport into
        // a window.
        ImGuiWindowFlags mainDockWindowFlags = ImGuiWindowFlags_None;
        mainDockWindowFlags |= ImGuiWindowFlags_NoTitleBar;
        mainDockWindowFlags |= ImGuiWindowFlags_NoCollapse;
        mainDockWindowFlags |= ImGuiWindowFlags_NoResize;
        mainDockWindowFlags |= ImGuiWindowFlags_NoMove;
        mainDockWindowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
        mainDockWindowFlags |= ImGuiWindowFlags_NoNavFocus;
        mainDockWindowFlags |= ImGuiWindowFlags_NoBackground;
        mainDockWindowFlags |= ImGuiWindowFlags_MenuBar;
        mainDockWindowFlags |= ImGuiWindowFlags_NoDocking;
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        bool ignored = true;
        ImGuiViewport *viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetWorkPos());
        ImGui::SetNextWindowSize(viewport->GetWorkSize());
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::Begin("DockSpace Dummy", &ignored, mainDockWindowFlags);
        ImGui::PopStyleVar(3);

        ImGuiDockNodeFlags mainDockSpaceFlags = ImGuiDockNodeFlags_None;
        mainDockSpaceFlags |= ImGuiDockNodeFlags_PassthruCentralNode;
        ImGuiID mainDockSpaceID = ImGui::GetID("MainDockSpace");
        ImGui::DockSpace(mainDockSpaceID, ImVec2(0.0f, 0.0f), mainDockSpaceFlags);

        ImGuiContext *imCtx = ImGui::GetCurrentContext();
        ImGuiDockNode *mainNode = ImGui::DockContextFindNodeByID(imCtx, mainDockSpaceID);
        v2i mainViewportSize = v2i((i32)mainNode->CentralNode->Size.x, (i32)mainNode->CentralNode->Size.y);
        pbrViewer->viewportPos  = v2i((i32)mainNode->CentralNode->Pos.x, (i32)(app->clientH - mainNode->CentralNode->Pos.y) - mainViewportSize.y);
        if(mainViewportSize != pbrViewer->viewportSize)
        {
            pbrViewer->viewportSize = mainViewportSize;
            pbrViewer->resizeEvent = true;
        }

        local_persist bool FileMenu_LoadedGroups = false;
        local_persist bool FileMenu_LoadFileGLTF = false;
        local_persist bool ViewMenu_ViewerMode = true;
        local_persist bool ViewMenu_GammaTonemapping = true;
        local_persist bool ViewMenu_Settings = true;
        if(ImGui::BeginMenuBar())
        {
            if(ImGui::BeginMenu("App"))
            {
                if(ImGui::MenuItem("PBR Scene"))
                {
                    app->mode = APP_MODE_PBRScene;
                }

                ImGui::EndMenu();
            }
            if(ImGui::BeginMenu("File"))
            {
                ImGui::MenuItem("Loaded Groups...", nullptr, &FileMenu_LoadedGroups);
                ImGui::MenuItem("Load GLTF file...", nullptr, &FileMenu_LoadFileGLTF);
                ImGui::MenuItem("Environment Maps", nullptr, &localGuiState->envMapChooserCtx.open);
                ImGui::EndMenu();
            }
            if(ImGui::BeginMenu("View"))
            {
                ImGui::MenuItem("Render Mode", nullptr, &ViewMenu_ViewerMode);
                ImGui::MenuItem("Gamma/Tonemapping", nullptr, &ViewMenu_GammaTonemapping);
                ImGui::MenuItem("Settings", nullptr, &ViewMenu_Settings);
                ImGui::EndMenu();
            }

            ImGui::EndMenuBar();
        }


        if(localGuiState->envMapChooserCtx.open)
        {
            localGuiState->envMapChooserCtx.activeIndex = tbs_opengl::HandleToIndexU32(pbrViewer->activeEnvMap);
            if(tbs_gui::ShowEnvMapChooserWidget(app, rm, &pbrViewer->guiState.envMapChooserCtx))
            {
                pbrViewer->activeEnvMap = pbrViewer->guiState.envMapChooserCtx.result;
            }
        }

        if(FileMenu_LoadedGroups)
        {
            if(ImGui::Begin("Group List", &FileMenu_LoadedGroups))
            {
                ImGui::BeginChild("group_list", ImVec2(0,-ImGui::GetFrameHeightWithSpacing()), true);
                ImGui::Columns(2, "group_list_columns");
                ImGui::Separator();
                ImGui::Text("Name");
                ImGui::NextColumn();
                ImGui::Text("File");
                ImGui::NextColumn();
                ImGui::Separator();
                local_persist i32 selectedGIndex = -1;
                local_persist i32 renameGIndex = -1;
                for(i32 gIndex = 0; gIndex < (i32)pbrViewer->groupCount; ++gIndex)
                {
                    mesh_group *g = pbrViewer->groups + gIndex;
                    if(ImGui::Selectable(g->name, selectedGIndex == gIndex, ImGuiSelectableFlags_SpanAllColumns))
                    {
                        selectedGIndex = gIndex;
                    }
                    if(ImGui::BeginPopupContextItem())
                    {
                        if(ImGui::Selectable("Rename"))
                        {
                            renameGIndex = gIndex;
                        }
                        ImGui::EndPopup();
                    }

                    ImGui::NextColumn();
                    if(g->file[0])
                    {
                        ImGui::Text(g->file);
                    }
                    else
                    {
                        ImGui::Text("BUILTIN");
                    }
                    ImGui::NextColumn();
                }

                if(renameGIndex > -1)
                {
                    mesh_group *g = pbrViewer->groups + renameGIndex;
                    ImGui::OpenPopup("Rename Group##rename_group_modal");
                    if(ImGui::BeginPopupModal("Rename Group##rename_group_modal"))
                    {
                        ImGui::InputText("New Name", g->name, 256);
                        if(ImGui::Button("Close"))
                        {
                            ImGui::CloseCurrentPopup();
                            renameGIndex = -1;
                        }
                        ImGui::EndPopup();
                    }
                }

                ImGui::EndChild();
                if(selectedGIndex == -1)
                {
                    ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                    ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
                }

                if(ImGui::Button("Load"))
                {
                    if(selectedGIndex > -1)
                    {
                        pbrViewer->activeGroup = (u32)selectedGIndex;
                    }
                }
                if(selectedGIndex == -1)
                {
                    ImGui::PopStyleVar();
                    ImGui::PopItemFlag();
                }

                ImGui::SameLine();
                if(ImGui::Button("Close"))
                {
                    FileMenu_LoadedGroups = false;
                }
            }

            ImGui::End();
        }

        if(FileMenu_LoadFileGLTF)
        {
            tbs_gui::file_browser_context *meshLoaderCtx = &pbrViewer->guiState.meshLoaderCtx;
            if(!meshLoaderCtx->init)
            {
#if TBS_INTERNAL
                RawStringCopy(meshLoaderCtx->searchDir, "C:/programming/asset_collection/gltf_sample_models/2.0/");
#else
                RawStringCopy(meshLoaderCtx->searchDir, app->appRootPath);
#endif
                RawStringCopy(meshLoaderCtx->extension, "gltf");
                meshLoaderCtx->init = true;
                meshLoaderCtx->result.dialogResult = tbs_gui::DIALOG_RESULT_None;
                memset(meshLoaderCtx->result.fullFilename, 0, sizeof(meshLoaderCtx->result.fullFilename));
            }

            if(tbs_gui::ShowFileBrowserWidget(meshLoaderCtx))
            {
                if(meshLoaderCtx->result.dialogResult == tbs_gui::DIALOG_RESULT_Open)
                {
                    PrintDebugMsg("Selected file: %s\n", meshLoaderCtx->result.fullFilename);
                    temp_memory scratch(app);
                    file_data selectedFile = Win32ReadAllText(scratch.arena, meshLoaderCtx->result.fullFilename);
                    base_json_object *jsonData = ParseJsonSimple(scratch.arena, &selectedFile);
                    gltf_context ctx = {};
                    ctx.arena = scratch.arena;
                    ctx.root = jsonData;
                    i32 dirOffset = GetParentDirectoryOffset(meshLoaderCtx->result.fullFilename);
                    ASSERT(dirOffset > 0);
                    RawStringCopy(ctx.path, meshLoaderCtx->result.fullFilename, (size_t)dirOffset + 1);
                    gltf_root gltf = gltf_ProcessGltf(&ctx);

                    i32 groupIndex = pbrViewer->CreateAndAddMeshGroupFromGltf(app, rm, &gltf, meshLoaderCtx->result.fullFilename);
                    if(groupIndex > -1)
                    {
                        pbrViewer->activeGroup = (u32)groupIndex;
                    }
                }

                meshLoaderCtx->init = false;
                FileMenu_LoadFileGLTF = false;
            }
        }

        if(ViewMenu_ViewerMode)
        {
            if(ImGui::Begin("Viewer Mode", &ViewMenu_ViewerMode))
            {
                local_persist i32 e = RENDER_MODE_Full;
                ImGui::RadioButton("Full Render",       &e, RENDER_MODE_Full);
                ImGui::RadioButton("Albedo",            &e, RENDER_MODE_Albedo);
                ImGui::RadioButton("Normal",            &e, RENDER_MODE_Normal);
                ImGui::RadioButton("Occlusion",         &e, RENDER_MODE_Occlusion);
                ImGui::RadioButton("Emissive",          &e, RENDER_MODE_Emissive);
                ImGui::RadioButton("Metallic",          &e, RENDER_MODE_Metallic);
                ImGui::RadioButton("Roughness",         &e, RENDER_MODE_Roughness);
                ImGui::RadioButton("Diffuse",           &e, RENDER_MODE_Diffuse);
                ImGui::RadioButton("Specular",          &e, RENDER_MODE_Specular);
                ImGui::RadioButton("Geometry",          &e, RENDER_MODE_Geometry);
                ImGui::RadioButton("Distribution",      &e, RENDER_MODE_Distribution);
                ImGui::RadioButton("Fresnel",           &e, RENDER_MODE_Fresnel);
                ImGui::RadioButton("Fresnel Roughness", &e, RENDER_MODE_FresnelRoughness);
                pbrViewer->renderMode = (render_mode)e;
            }

            ImGui::End();
        }

        if(ViewMenu_GammaTonemapping)
        {
            if(ImGui::Begin("Gamma And Tonemapping", &ViewMenu_GammaTonemapping, ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_AlwaysAutoResize))
            {
                ImGui::Checkbox("Use Gamma", &pbrViewer->useGammaCorrection);
                ImGui::Checkbox("Use Tonemapping", &pbrViewer->useTonemapping);
                if(ImGui::BeginCombo("Tonemapping Operator", gui_tonemappingTypeNames[pbrViewer->tonemappingType]))
                {
                    for(size_t typeIndex = 0; typeIndex < ARRAY_COUNT(gui_tonemappingTypeNames); ++typeIndex)
                    {
                        if(ImGui::Selectable(gui_tonemappingTypeNames[typeIndex], pbrViewer->tonemappingType == typeIndex))
                        {
                            pbrViewer->tonemappingType = (tonemapping_type)typeIndex;
                        }
                    }
                    ImGui::EndCombo();
                }

                ImGui::SliderFloat("White Point", &pbrViewer->whitePoint, 0.0f, 100.0f);
            }

            ImGui::End();
        }

        if(ViewMenu_Settings)
        {
            if(ImGui::Begin("Settings", &ViewMenu_Settings, ImGuiWindowFlags_AlwaysAutoResize))
            {
                ImGui::Checkbox("MSAA", &pbrViewer->msaaActive);
                ImGui::Separator();
                i32 bgOpt = pbrViewer->backgroundOption;
                ImGui::RadioButton("Use Environment Map", &bgOpt, BACKGROUND_OPTION_EnvironmentMap);
                ImGui::RadioButton("Use Environment Map Blurred", &bgOpt, BACKGROUND_OPTION_EnvironmentMapBlurred);
                ImGui::RadioButton("Use Solid Color", &bgOpt, BACKGROUND_OPTION_SolidColor);
                pbrViewer->backgroundOption = (background_option)bgOpt;
                ImGui::ColorPicker3("Background Color", pbrViewer->backgroundColor);
            }

            ImGui::End();
        }

        ImGui::End();
        ImGui::EndFrame();
        ImGui::Render();

        ImDrawData *imDD = ImGui::GetDrawData();
        tbs_gui::DefaultRenderImGuiDrawData(app, imGuiState, rm, imDD);
    }

    internal void RenderMeshGroup(pbr_viewer *pbrViewer, tbs_opengl::opengl_resource_manager *rm, u32 groupIndex, matrix4D projView)
    {
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);

        if(pbrViewer->meshCount < 1)
        {
            return;
        }

        ASSERT(groupIndex < pbrViewer->groupCount);
        mesh_group group = pbrViewer->groups[groupIndex];
        if(group.first == nullptr)
        {
            return;
        }

        GLuint progID = rm->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_PBRViewer);
        glUseProgram(progID);

        S_ASSERT(ARRAY_COUNT(rm->pbrViewerSamplerLocations) == tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_Count, "Invalid definition of sampler binding locations");
        for(i32 bindPoint = 0; bindPoint < tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_Count; ++bindPoint)
        {
            GLint loc = rm->pbrViewerSamplerLocations[bindPoint];
            //ASSERT(loc != -1);
            if(loc != -1)
            {
                glUniform1i(loc, bindPoint);
            }
        }

        if(pbrViewer->useEnvLighting)
        {
            tbs_pbr::env_map_info *envMap = rm->GetEnvMap(pbrViewer->activeEnvMap);
            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_IrradianceMap);
            glBindTexture(GL_TEXTURE_CUBE_MAP, envMap->diffPrefilterMap);
            glUniform1i(glGetUniformLocation(progID, "irradiance_map"), tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_IrradianceMap);

            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_PrefilterMap);
            glBindTexture(GL_TEXTURE_CUBE_MAP, envMap->specPrefilterMap);
            glUniform1i(glGetUniformLocation(progID, "prefilter_env"), tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_PrefilterMap);

            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_BrdfLut);
            glBindTexture(GL_TEXTURE_2D, envMap->brdfLUT);
            glUniform1i(glGetUniformLocation(progID, "brdf_lut"), tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_BrdfLut);

            glUniform1ui(glGetUniformLocation(progID, "use_env_lighting"), 1);
        }
        else
        {
            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_IrradianceMap);
            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_PrefilterMap);
            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_VIEWER_SAMPLER_BINDING_POINT_BrdfLut);
            glBindTexture(GL_TEXTURE_2D, 0);
            glUniform1ui(glGetUniformLocation(progID, "use_env_lighting"), 0);
        }

        glUniform3fv(glGetUniformLocation(progID, "camera_pos"), 1, &pbrViewer->camera.pos.x);
        glUniformMatrix4fv(glGetUniformLocation(progID, "proj_view"), 1, GL_FALSE, Mat4Addr(projView));

        glUniform3fv(glGetUniformLocation(progID, "main_light.position"), 1, &pbrViewer->mainLight.position.x);
        glUniform3fv(glGetUniformLocation(progID, "main_light.color"), 1, &pbrViewer->mainLight.color.x);
        glUniform1i(glGetUniformLocation(progID, "main_light.type"), pbrViewer->mainLight.type);
        glUniform1f(glGetUniformLocation(progID, "main_light.max_dist"), 100.0f);
        glUniform1f(glGetUniformLocation(progID, "main_light.falloff_start_dist"), 2.0f);
        glUniform1i(glGetUniformLocation(progID, "main_light.dist_att_func"), 1);

        glUniform1i(glGetUniformLocation(progID, "render_mode"), pbrViewer->renderMode);

        group_link *link = group.first;
        while(link)
        {
            tbs_pbr::pbr_mesh *mesh = link->m;
            matrix4D model = link->t;
            for(size_t pIndex = 0; pIndex < mesh->primitives.count; ++pIndex)
            {
                tbs_opengl::renderable_primitive *prim = rm->GetPrimitive(mesh->primitives.data[pIndex].renderDataHandle);
                tbs_pbr::pbr_material *mat = pbrViewer->materials + mesh->primitives.data[pIndex].materialHandle;
                u32 samplerFlags = 0;
                for(u32 texIndex = 0; texIndex < 5; ++texIndex)
                {
                    OpenGLSetActiveTextureUnit(texIndex);
                    auto texHandle = mat->textures[texIndex];
                    tbs_opengl::texture_info *texInfo = rm->GetTexture(texHandle);
                    if(texInfo != nullptr && texInfo->loaded)
                    {
                        ASSERT(texInfo->texID > 0);
                        ASSERT(texInfo->target != 0);
                        glBindTexture(texInfo->target, texInfo->texID);
                        samplerFlags |= tbs_opengl::pbrTextureToPbrViewerActiveSamplerFlag[texIndex];
                    }
                    else
                    {
                        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
                        glBindTexture(GL_TEXTURE_2D, 0);
                    }
                }

                glUniform1i(glGetUniformLocation(progID, "is_double_sided"), mat->isDoubleSided);
                OpenGLSetUniform(progID, "base_color", &mat->baseColor);
                OpenGLSetUniform(progID, "model", &model);
                OpenGLSetUniform(progID, "metallic_factor", mat->metallicFactor);
                OpenGLSetUniform(progID, "roughness_factor", mat->roughnessFactor);
                OpenGLSetUniform(progID, "emissive_factor", &mat->emissiveFactor);
                OpenGLSetUniform(progID, "active_sampler_flag", samplerFlags);

                // TODO(torgrim): Handle blending properly.
                if(mat->alphaMode == tbs_pbr::MATERIAL_ALPHA_MODE_Mask || mat->alphaMode == tbs_pbr::MATERIAL_ALPHA_MODE_Blend)
                {
                    OpenGLSetUniform(progID, "alpha_cutoff", mat->alphaCutoff);
                    glEnable(GL_BLEND);
                }
                else
                {
                    OpenGLSetUniform(progID, "alpha_cutoff", 1.0f);
                    glDisable(GL_BLEND);
                }

                glBindVertexArray(prim->VAOID);
                if(prim->target == GL_ELEMENT_ARRAY_BUFFER)
                {
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, prim->indEBOID);
                    glDrawElements(prim->mode, (GLsizei)prim->indexCount, prim->indexDataType, nullptr);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                }
                else
                {
                    glDrawArrays(prim->mode, 0, (GLsizei)prim->vertexCount);
                }
            }

            link = link->next;

        }

        glDisable(GL_DEPTH_TEST);
    }
}
