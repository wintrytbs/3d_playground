#ifndef TBS_SCENE_H

namespace tbs_scene
{
    namespace local_constants
    {
        // TODO: Not sure which of these belong here
        // vs in the resource manager
        constexpr u32 MAX_SCENE_COUNT                   = 32;
        constexpr u32 MAX_PRIMITIVE_COUNT               = 1024;
        constexpr u32 MAX_MESH_COUNT                    = 1024;
        constexpr u32 MAX_MATERIAL_COUNT                = 1024;
        constexpr u32 MAX_LIGHT_COUNT                   = 1024;

        constexpr u32 CASCADE_SHADOW_SPLIT_COUNT        = 4;
        constexpr i32 SHADOW_MAP_WIDTH                  = 2048;
        constexpr i32 SHADOW_MAP_HEIGHT                 = 2048;
        constexpr i32 DIRECTIONAL_SHADOW_SIZE           = 2048;
        constexpr i32 MAX_SCENE_NODE_COUNT              = 1000;
    }

    struct simple_camera
    {
        v3 pos;
        // TODO(torgrim): We don't really use rot vector
        // anymore. We just spherical coordinates to control
        // the camera instead.
        v3 rot;
        v3 dir;

        f32 fov;

        f32 speed;
        f32 rotationSpeed;

        f32 aspect;
        f32 clipNear;
        f32 clipFar;

        bool hasFocus;
        v3 target;

        f32 phi;
        f32 theta;
        f32 radius;

        // TODO(torgrim): Not sure we want these here.
        matrix4D proj;
        matrix4D view;
    };

    enum scene_node_type
    {
        SCENE_NODE_TYPE_None,
        // TODO(torgrim): Need to decide what we consider a mesh and what we consider
        // a primitive. Right now mesh type in a gltf node = mesh in scene node, but a primitive can also be
        // a mesh node??
        SCENE_NODE_TYPE_Mesh,
        SCENE_NODE_TYPE_Light,
        SCENE_NODE_TYPE_Parent,
        SCENE_NODE_TYPE_Camera,
    };

    struct scene_node_list
    {
        struct scene_node **data;
        i64 count;
    };

    struct scene_node
    {
        char name[256];
        scene_node_type type;
        void *handle;

        mesh_transform transform;
        scene_node *firstChild;
        scene_node *next;
        scene_node *parent;
        u64 groupID;
    };

    global u64 groupIDCounter = 1;
    struct scene_state
    {
        char name[256];

        // NOTE: Currently only used for internal testing
        v3 boundingSphereO[10];
        f32 boundingSphereR[10];
        i32 boundingSphereCount;
        v3 boundingSphereColor[10];

        scene_node *firstNode;
        tbs_opengl::opengl_resource_handle envMap;

        bool useAmbientEnvLighting;
        bool usePointSampleShadow;
        bool useSpotSampleShadow;

        simple_camera editorCamera;

        u32 cameraCount;
        simple_camera cameras[10];

        void AddTopLevelNode(scene_node *node);
        scene_node *CreateAndAddTopLevelNode(pool_memory *pool, void *dataHandle, const char *nodeName, scene_node_type type);
        matrix4D CalculateCompleteNodeTransform(scene_node *node);
    };

    enum view_mode
    {
        VIEW_MODE_Default,
        VIEW_MODE_Cascade,
    };

    enum tonemapping_type
    {
        TONEMAPPING_TYPE_None,
        TONEMAPPING_TYPE_ReinhardSimple,
        TONEMAPPING_TYPE_ReinhardExtended,
        TONEMAPPING_TYPE_ReinhardExtendedLuminance,
        TONEMAPPING_TYPE_ReinhardJodie,
        TONEMAPPING_TYPE_Hable,
        TONEMAPPING_TYPE_ACES,

        TONEMAPPING_TYPE_Count,
    };

    typedef u64 resource_handle;

    struct pbr_scene
    {
        tbs_pbr::pbr_material materials[local_constants::MAX_MATERIAL_COUNT];
        u32 materialCount;

        tbs_pbr::pbr_mesh meshes[local_constants::MAX_MESH_COUNT];
        u32 meshCount;

        light_data lights[local_constants::MAX_LIGHT_COUNT];
        u32 lightCount;

        scene_state *scenes[local_constants::MAX_SCENE_COUNT];
        u32 sceneCount;
        u32 activeSceneIndex;

        resource_handle defaultMaterialHandle;

        GLuint spotShadowTexArray;
        GLuint dirShadowTexArray;
        GLuint pointShadowTexList[tbs_opengl::local_constants::PBR_SHADER_MAX_POINT_COUNT];

        v2i viewportPos;
        v2i viewportSize;

        view_mode viewMode;

        bool useGammaCorrection;
        bool useTonemapping;

        f32 whitePoint;
        tonemapping_type tonemappingType;

        f32 ambientStrength;
        bool msaaActive;

        scene_gui_state guiState;

        // NOTE: Events
        bool NeedViewportResize;

        // NOTE: Init handling
        void Init();
        void SetupDefaultMaterialPBR();
        void SetupShadowTextures();

        // NOTE: Mesh/primitive/material handling
        resource_handle AddMesh(tbs_pbr::pbr_mesh mesh);
        resource_handle CreateAndAddBuiltinMesh(memory_arena *arena, tbs_opengl::base_primitive_type type);
        tbs_pbr::pbr_mesh *GetMesh(resource_handle handle);
        tbs_pbr::pbr_material *GetMaterial(resource_handle handle);
        resource_handle AddMaterial(tbs_pbr::pbr_material mat);

        // NOTE: Misc
        resource_handle AddLight(light_data light);
        light_data *GetLight(resource_handle handle);
        scene_state *GetActiveScene();
        void AddScene(scene_state *scene);

    };

    global constexpr resource_handle invalidHandle = UINT32_MAX;

    internal inline void *HandleToVoid(resource_handle handle)
    {
        return ((void *)(uintptr_t)handle);
    }

    internal inline resource_handle VoidToHandle(void *handle)
    {
        return (resource_handle)(uintptr_t)handle;
    }

    internal scene_node *CreateSceneNodeAndMesh(application_state *app, tbs_scene::pbr_scene *pbrScene, tbs_opengl::base_primitive_type type, const char *name);
    internal scene_node *CreateSceneNodeTreeFromGltf(application_state *app, tbs_opengl::opengl_resource_manager *rm, pbr_scene *pbrScene, gltf_root *gltf, i64 sceneIndex);
    internal scene_node *CreateSceneNodesRecurs(pool_memory *pool, tbs_array_sr<gltf_node> *completeNodeList, tbs_array_sr<tbs_opengl::opengl_resource_handle> *meshMap, tbs_array_sr<char*> *meshNameMap, tbs_array_sr<i64> *nodeIndices, u64 groupID);
    internal scene_state *CreateDefaultScene(application_state *app, pbr_scene *pbrScene);
    internal scene_state *CreateScene(application_state *app, pbr_scene *pbrScene, scene_node *firstNode);
}

#define TBS_SCENE_H
#endif
