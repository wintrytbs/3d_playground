/* date = June 8th 2020 8:19 pm */

#ifndef TBS_STRING_H

internal inline size_t RawStringLength(const char *str)
{
    size_t result = 0;
    const char *c = str;
    while(*c != 0)
    {
        ++result;
        ++c;
    }

    return result;
}

constexpr size_t CompileRawStringLength(const char *str)
{
    size_t result = 0;
    const char *c = str;
    while(*c != 0)
    {
        ++result;
        ++c;
    }

    return result;
}

internal inline void RawStringConcat(char *dst, const char *s1, const char *s2)
{
    size_t s1L = RawStringLength(s1);
    size_t s2L = RawStringLength(s2);

    memcpy(dst, s1, s1L);
    memcpy(dst+s1L, s2, s2L);

    dst[s1L+s2L] = '\0';
}

internal inline char *RawStringConcat(memory_arena *arena, const char *s1, const char *s2)
{
    char *result = nullptr;

    size_t s1L = RawStringLength(s1);
    size_t s2L = RawStringLength(s2);

    result = AllocArray(arena, char, s1L+s2L+1);
    memcpy(result, s1, s1L);
    memcpy(result+s1L, s2, s2L);

    result[s1L+s2L] = '\0';

    return result;
}

internal inline char *RawStringCreate(const char *s)
{
    size_t l = RawStringLength(s);
    ASSERT(l > 0);
    char *result = (char *)malloc(l+1);
    memcpy(result, s, l);
    result[l] = '\0';
    return result;
}

internal inline char *RawStringCreate(memory_arena *arena, const char *s, size_t l)
{
    ASSERT(l > 0);
    char *result = AllocArray(arena, char, l+1);
    memcpy(result, s, l);
    result[l] = '\0';

    return result;
}

internal inline void RawStringCopy(char *dst, const char *src)
{
    size_t l = RawStringLength(src);
    ASSERT(l > 0);
    memcpy(dst, src, l);
    dst[l] = '\0';
}

internal inline void RawStringCopy(char *dst, const char *src, size_t l)
{
    memcpy(dst, src, l);
    dst[l] = '\0';
}

internal bool RawStringEqual(const char *s1, const char *s2)
{
    bool result = false;

    while(*s1 != '\0' && *s2 != '\0')
    {
        if(*s1 != *s2)
        {
            break;
        }

        ++s1;
        ++s2;
    }

    if(*s1 == '\0' && *s1 == *s2)
    {
        result = true;
    }

    return result;
}

struct tbs_tmp_string
{
    char* str;
    size_t size;

    tbs_tmp_string() : str(nullptr), size(0)
    {
    }
    tbs_tmp_string(const char *s)
    {
        this->str = RawStringCreate(s);
        size_t l = RawStringLength(s);
        this->str = (char *)malloc(l+1);
        memcpy(this->str, s, l);
        this->size = l;
        this->str[l] = '\0';
    }
    tbs_tmp_string(const char *s1, const char *s2)
    {
        size_t s1L = RawStringLength(s1);
        size_t s2L = RawStringLength(s2);
        this->str = (char *)malloc(s1L + s2L + 1);
        this->size = s1L + s2L;
        memcpy(this->str, s1, s1L);
        memcpy(this->str+s1L, s2, s2L);
        this->str[this->size] = '\0';
    }

    ~tbs_tmp_string()
    {
        free(this->str);
    }

    tbs_tmp_string(const tbs_tmp_string &src) = delete;
    tbs_tmp_string & operator = (const tbs_tmp_string &src) = delete;
};

// TODO(torgrim): These are more file system functions and should probably be moved to
// a different file.
internal i32 GetParentDirectoryOffset(const char *path)
{
    i32 l = (i32)RawStringLength(path);
    ASSERT(l > 0);

    i32 result = -1;
    i32 startPos = l-1;
    if(path[startPos] == '/' || path[startPos] == '\\')
    {
        startPos--;
    }
    for(i32 i = startPos; i > 0; --i)
    {
        if(path[i] == '/' || path[i] == '\\')
        {
            result = i;
            break;
        }
    }

    return result;
}

// TODO(torgrim): Ignore casing?
// TODO(torgrim): Move this since it's not doesn't really belong in a string context.
internal bool MatchFileExtension(const char *fileName, const char *matchExtension)
{
    bool result = false;

    size_t l = RawStringLength(fileName);
    size_t matchLength = RawStringLength(matchExtension);
    ASSERT(l > 0);
    ASSERT(matchLength > 0);

    const char *extensionOffset = nullptr;
    size_t extensionLength = 0;
    for(size_t i = l-1; i > 0; --i)
    {
        ++extensionLength;
        if(fileName[i] == '.')
        {
            extensionLength = l - (i+1);
            extensionOffset = fileName + (i+1);
            break;
        }
    }

    if(extensionOffset != nullptr && extensionLength == matchLength)
    {
        result = true;
        for(size_t i = 0; i < extensionLength; ++i)
        {
            if(extensionOffset[i] != matchExtension[i])
            {
                result = false;
                break;
            }
        }
    }

    return result;
}

internal void AppendToPath(char *path, char *addition, bool endWithSeparator)
{
    size_t pathLength = RawStringLength(path);
    size_t addLength = RawStringLength(addition);
    ASSERT(pathLength > 0);
    ASSERT(addLength > 0);

    size_t offset = pathLength;
    char separator = '/';
    if(path[pathLength-1] != '\\' && path[pathLength-1] != '/')
    {
        path[pathLength] = '\\';
        ++offset;
    }

    memcpy(path+offset, addition, addLength);
    if(endWithSeparator)
    {
        path[offset+addLength] = separator;
        path[offset+addLength+1] = '\0';
    }
    else
    {
        path[offset+addLength] = '\0';
    }
}

internal inline bool IsWhitespace(u8 c)
{
    bool result = (c == ' ' ||
                   c == '\n' ||
                   c == '\r' ||
                   c == '\t');

    return result;
}

internal inline bool IsAlpha(u8 c)
{
    bool result = (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
    return result;
}

internal inline bool IsNumber(u8 c)
{
    bool result = (c >= '0' && c <= '9');
    return result;
}

#define TBS_STRING_H

#endif //TBS_STRING_H
