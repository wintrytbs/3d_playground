#include "tbs_image.h"

internal image_data LoadImageDataFromMemory(u8 *buffer, int size, bool flip)
{
    stbi_set_flip_vertically_on_load(flip);

    image_data result = {};

    i32 w, h, cc;
    i32 wantedChannels = 0;
    u8 *pixels = stbi_load_from_memory(buffer, size, &w, &h, &cc, wantedChannels);
    if(pixels == nullptr)
    {
        PrintDebugMsg("STB::ERROR:: could not load image data");
        ASSERT(false);

        return result;
    }

    ASSERT(cc == 1 || cc == 3 || cc == 4);

    result.pixels = pixels;
    result.width = w;
    result.height = h;
    result.channelCount = cc;

    stbi_set_flip_vertically_on_load(false);

    return result;
}
internal image_data LoadImageData(const char *filename, bool flip)
{
    stbi_set_flip_vertically_on_load(flip);

    image_data result = {};

    i32 w, h, cc;
    i32 wantedChannels = 0;
    u8 *pixels = stbi_load(filename, &w, &h, &cc, wantedChannels);
    if(pixels == nullptr)
    {
        PrintDebugMsg("STB::ERROR:: could not load image data");
        ASSERT(false);

        return result;
    }

    ASSERT(cc == 1 || cc == 3 || cc == 4);

    result.pixels = pixels;
    result.width = w;
    result.height = h;
    result.channelCount = cc;

    stbi_set_flip_vertically_on_load(false);

    return result;
}

internal image_data_hdr LoadImageDataHDR(const char *filename, bool flip, bool storeFilename)
{
    stbi_set_flip_vertically_on_load(flip);

    image_data_hdr result = {};
    if(storeFilename)
    {
        result.filename = RawStringCreate(filename);
    }

    i32 w, h, cc;
    i32 wantedChannels = 0;
    float *pixels = stbi_loadf(filename, &w, &h, &cc, wantedChannels);
    if(pixels == nullptr)
    {
        PrintDebugMsg("STB::ERROR:: could not load HDR image data");
        ASSERT(false);

        return result;
    }

    ASSERT(cc == 1 || cc == 3 || cc == 4);

    result.pixels = pixels;
    result.width = w;
    result.height = h;
    result.channelCount = cc;

    stbi_set_flip_vertically_on_load(false);

    return result;
}

internal inline void FreeImageData(image_data *img)
{
    stbi_image_free(img->pixels);

    *img = {};
}

internal inline void FreeImageData(image_data_hdr *img)
{
    stbi_image_free(img->pixels);
    free(img->filename);

    *img = {};
}
