#include "tbs_primitive.h"

internal void CalcFlatNormals(v3 *vertices, triangle_list *triangleList, v3 *result)
{
    i32 resultOffset = 0;
    for(size_t i = 0; i < triangleList->count; ++i)
    {
        triangle t = triangleList->triangles[i];
        v3 p0 = vertices[t.index[0]];
        v3 p1 = vertices[t.index[1]];
        v3 p2 = vertices[t.index[2]];

        v3 a = p1 - p0;
        v3 b = p2 - p0;

        v3 c = Cross(a,b);
        v3 normal = v3(0.0f, 0.0f, 0.0f);
        // TODO(torgrim): Do we want to do something if this is the case?
        // Do we try to offet the triangle a bit so that we can get a normal or something
        // else?
        // TODO(torgrim): A lot of gltf models seems to have this problem of generating very small triangles(could be something
        // wrong with our triangle generation). Maybe applying a scale to the vertices would solve this
        // until we find a better way?
        // TODO(torgrim): This is just a hack for ruling out those triangles. Should find a better solution.
        f32 cl = Length(c);
        if(cl > 0)
        {
            normal = Normalize(c);
        }

        // TODO(torgrim): This is still not quite correct
        // when triangles share the same vertex index. Then only
        // the last normal will be written. When this is the case we actually
        // have to expand the vertex list so better to detect this before
        // so that this function expects that all triangles indices are unique.
        result[t.index[0]] = normal;
        result[t.index[1]] = normal;
        result[t.index[2]] = normal;

        resultOffset += 3;
    }
}

internal triangle_list CreateTriangleListU32(memory_arena *arena, u32 *indices, size_t count)
{
    ASSERT(count % 3 == 0);

    size_t triangleCount = count / 3;
    triangle_list result = {};
    result.count = triangleCount;
    result.triangles = AllocArray(arena, triangle, triangleCount);
    u32 i = 0;
    for(size_t rIdx = 0; rIdx < triangleCount; ++rIdx)
    {
        result.triangles[rIdx] = {indices[i], indices[i+1], indices[i+2]};
        i += 3;
    }

    return result;
}

internal triangle_list CreateTriangleListU16(memory_arena *arena, u16 *indices, size_t count)
{

    ASSERT(count % 3 == 0);

    size_t triangleCount = count / 3;
    triangle_list result = {};
    result.count = triangleCount;
    result.triangles = AllocArray(arena, triangle, triangleCount);
    u32 i = 0;
    for(size_t rIdx = 0; rIdx < triangleCount; ++rIdx)
    {
        result.triangles[rIdx] = {indices[i], indices[i+1], indices[i+2]};
        i += 3;
    }

    return result;

}

internal triangle_list CreateTriangleListU8(memory_arena *arena, u8 *indices, size_t count)
{

    ASSERT(count % 3 == 0);

    size_t triangleCount = count / 3;
    triangle_list result = {};
    result.count = triangleCount;
    result.triangles = AllocArray(arena, triangle, triangleCount);
    u32 i = 0;
    for(size_t rIdx = 0; rIdx < triangleCount; ++rIdx)
    {
        result.triangles[rIdx] = {indices[i], indices[i+1], indices[i+2]};
        i += 3;
    }

    return result;

}

internal triangle_list CreateSimpleTriangleList(memory_arena *arena, size_t vertexCount)
{
    ASSERT(vertexCount % 3 == 0);

    size_t triangleCount = vertexCount / 3;
    triangle_list result = {};
    result.count = triangleCount;
    result.triangles = AllocArray(arena, triangle, triangleCount);
    u32 i = 0;
    for(size_t rIdx = 0; rIdx < triangleCount; ++rIdx)
    {
        result.triangles[rIdx] = {i, i+1, i+2};
        i += 3;
    }

    return result;

}

// TODO(torgrim): Rename this it's not really calculating a mesh tangents, but rather primitive
// tangents.
// TODO(torgrim): Some models seems to have bad triangles where the some triangles are lines instead...
// This is a problem that needs to be handled farther up the pipeline where we fix those kind of triangles.
internal void CalcMeshTangents(memory_arena *arena,
                               v3 *vertexList, size_t vertexCount,
                               triangle *triangleList, size_t triangleCount,
                               v2 *texCoordList, size_t texCoordCount,
                               v3 *normalList,
                               v4 *tangentList)
{
    // TODO(torgrim): Do we want to pass in memory here even though we know
    // from this function that the allocated memory is always just temporary?
    // Should we in that case pass a temp_memory struct?
    v3 *tangent = AllocArray(arena, v3, vertexCount*2);
    v3 *bitangent = tangent + vertexCount;

    for(size_t i = 0; i < vertexCount; ++i)
    {
        tangent[i] = v3(0.0f, 0.0f, 0.0f);
        bitangent[i] = v3(0.0f, 0.0f, 0.0f);
    }

    for(size_t i = 0; i < triangleCount; ++i)
    {
        u32 i0 = triangleList[i].index[0];
        u32 i1 = triangleList[i].index[1];
        u32 i2 = triangleList[i].index[2];

        f32 x1 = texCoordList[i1].x - texCoordList[i0].x;
        f32 x2 = texCoordList[i2].x - texCoordList[i0].x;

        f32 y1 = texCoordList[i1].y - texCoordList[i0].y;
        f32 y2 = texCoordList[i2].y - texCoordList[i0].y;

        f32 den = (x1*y2) - (x2*y1);
        f32 reciprocal = 0.5f;
        //ASSERT(den != 0.0f);
        reciprocal = 1.0f / den;
        v3 e1 = vertexList[i1] - vertexList[i0];
        v3 e2 = vertexList[i2] - vertexList[i0];

        v3 t = (e1*y2 - e2*y1) * reciprocal;
        v3 b = (e2*x1 - e1*x2) * reciprocal;

        tangent[i0] += t;
        tangent[i1] += t;
        tangent[i2] += t;

        bitangent[i0] += b;
        bitangent[i1] += b;
        bitangent[i2] += b;
    }

    for(size_t i = 0; i < vertexCount; ++i)
    {
        v3 oldT = tangent[i];
        v3 b = bitangent[i];
        v3 n = normalList[i];
        v3 orthoNormT = Normalize(Reject(oldT, n));
        tangentList[i].x = orthoNormT.x;
        tangentList[i].y = orthoNormT.y;
        tangentList[i].z = orthoNormT.z;
        tangentList[i].w = (Dot(Cross(b, oldT), n) > 0.0f) ? 1.0f : -1.0f;
    }
}

