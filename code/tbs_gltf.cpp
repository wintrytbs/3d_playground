#include "tbs_gltf.h"

internal inline gltf_camera_type gltf_CameraTypeStringToEnum(const char *s)
{
    if(RawStringEqual(s, GLTF_CAMERA_TYPE_STR_Perspective))
    {
        return GLTF_CAMERA_TYPE_Perspective;
    }
    else if(RawStringEqual(s, GLTF_CAMERA_TYPE_STR_Orthographic))
    {
        return GLTF_CAMERA_TYPE_Orthographic;
    }
    else
    {
        ASSERT(false);
    }

    return GLTF_CAMERA_TYPE_None;
}

internal inline size_t gltf_GetComponentTypeSize(gltf_component_type type)
{
    size_t result = 0;
    if(type != GLTF_COMPONENT_TYPE_None)
    {
        size_t index = (size_t)type - 5120;
        result = gltf_component_type_byte_sizes[index];
    }

    return result;
}

internal inline size_t gltf_GetAttributeTypeCount(gltf_internal_attribute_type type)
{
    size_t result = gltf_internal_attribute_type_count[type];
    return result;
}

internal gltf_material_alpha_mode gltf_AlphaModeStringToEnum(const char *s)
{
    // TODO(torgrim): Since opaque is the default we don't really
    // have to compare that string.
    // Is there any cases where we actually care that each
    // string match?
    if(s != nullptr)
    {
        if(RawStringEqual(s, GLTF_ALPHA_MODE_STRING_Opaque))
        {
            return GLTF_MATERIAL_ALPHA_MODE_Opaque;
        }
        else if(RawStringEqual(s, GLTF_ALPHA_MODE_STRING_Mask))
        {
            return GLTF_MATERIAL_ALPHA_MODE_Mask;
        }
        else if(RawStringEqual(s, GLTF_ALPHA_MODE_STRING_Blend))
        {
            return GLTF_MATERIAL_ALPHA_MODE_Blend;
        }
    }

    return GLTF_MATERIAL_ALPHA_MODE_Opaque;
}

internal gltf_internal_attribute_type gltf_AttributeTypeStringToEnum(const char *s)
{
    gltf_internal_attribute_type result = GLTF_INTERNAL_ATTRIBUTE_TYPE_None;
    size_t l = RawStringLength(s);
    if(l == 6)
    {
        if(s[0] == GLTF_ATTRIBUTE_TYPE_STRING_Scalar[0] &&
           s[1] == GLTF_ATTRIBUTE_TYPE_STRING_Scalar[1] &&
           s[2] == GLTF_ATTRIBUTE_TYPE_STRING_Scalar[2] &&
           s[3] == GLTF_ATTRIBUTE_TYPE_STRING_Scalar[3] &&
           s[4] == GLTF_ATTRIBUTE_TYPE_STRING_Scalar[4] &&
           s[5] == GLTF_ATTRIBUTE_TYPE_STRING_Scalar[5])
        {
            return GLTF_INTERNAL_ATTRIBUTE_TYPE_Scalar;
        }
    }
    else if(l == 4)
    {
        if(s[0] == GLTF_ATTRIBUTE_TYPE_STRING_Vec2[0] &&
           s[1] == GLTF_ATTRIBUTE_TYPE_STRING_Vec2[1] &&
           s[2] == GLTF_ATTRIBUTE_TYPE_STRING_Vec2[2])
        {
            if(s[3] == '2')
            {
                result = GLTF_INTERNAL_ATTRIBUTE_TYPE_Vec2;
            }
            if(s[3] == '3')
            {
                result = GLTF_INTERNAL_ATTRIBUTE_TYPE_Vec3;
            }
            if(s[3] == '4')
            {
                result = GLTF_INTERNAL_ATTRIBUTE_TYPE_Vec4;
            }
        }
        if(s[0] == GLTF_ATTRIBUTE_TYPE_STRING_Mat2[0] &&
           s[1] == GLTF_ATTRIBUTE_TYPE_STRING_Mat2[1] &&
           s[2] == GLTF_ATTRIBUTE_TYPE_STRING_Mat2[2])
        {
            if(s[3] == '2')
            {
                result = GLTF_INTERNAL_ATTRIBUTE_TYPE_Mat2;
            }
            if(s[3] == '3')
            {
                result = GLTF_INTERNAL_ATTRIBUTE_TYPE_Mat3;
            }
            if(s[3] == '4')
            {
                result = GLTF_INTERNAL_ATTRIBUTE_TYPE_Mat4;
            }
        }

    }

    return result;
}

internal char *gltf_GetStringValueOrNull(const base_json_object *object, const char *memberName)
{
    char *result = nullptr;
    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        ASSERT(member->type == JSON_TYPE_String);
        result = member->stringValue;
    }

    return result;
}

internal bool gltf_GetBoolValueOrDefault(const base_json_object *object, const char *memberName, bool defaultValue)
{
    bool result = defaultValue;
    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        ASSERT(member->type == JSON_TYPE_Bool);
        result = member->boolValue;
    }

    return result;
}

internal i64 gltf_GetIntValueOrDefault(const base_json_object *object, const char *memberName, i64 defaultValue)
{
    i64 result = defaultValue;
    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        ASSERT(member->type == JSON_TYPE_Integer);
        result = member->intValue;
    }

    return result;
}

internal f64 gltf_GetFloatValueOrDefault(const base_json_object *object, const char *memberName, f64 defaultValue)
{
    f64 result = defaultValue;
    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        ASSERT(member->type == JSON_TYPE_Float);
        result = member->floatValue;
    }

    return result;
}

internal f64 gltf_GetFloatValueOrDefaultFromNumber(const base_json_object *object, const char *memberName, f64 defaultValue)
{
    f64 result = defaultValue;
    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        ASSERT(member->type == JSON_TYPE_Float || member->type == JSON_TYPE_Integer);
        if(member->type == JSON_TYPE_Float)
        {
            result = member->floatValue;
        }
        else if(member->type == JSON_TYPE_Integer)
        {
            result = (f64)member->intValue;
        }
    }

    return result;
}

internal v3 gltf_GetV3OrDefault(const base_json_object *object, const char *memberName, v3 defaultValue)
{
    v3 result = defaultValue;

    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        // TODO(torgrim): Better handling of float/integer difference.
        ASSERT(member->type == JSON_TYPE_Array);
        ASSERT(member->array.count == 3);
        ASSERT(member->array[0]->type == JSON_TYPE_Float || member->array[0]->type == JSON_TYPE_Integer);
        ASSERT(member->array[1]->type == JSON_TYPE_Float || member->array[1]->type == JSON_TYPE_Integer);
        ASSERT(member->array[2]->type == JSON_TYPE_Float || member->array[2]->type == JSON_TYPE_Integer);

        result[0] = (member->array[0]->type == JSON_TYPE_Float ? (f32)member->array[0]->floatValue : (f32)member->array[0]->intValue);
        result[1] = (member->array[1]->type == JSON_TYPE_Float ? (f32)member->array[1]->floatValue : (f32)member->array[1]->intValue);
        result[2] = (member->array[2]->type == JSON_TYPE_Float ? (f32)member->array[2]->floatValue : (f32)member->array[2]->intValue);

    }

    return result;
}

internal v4 gltf_GetV4OrDefault(const base_json_object *object, const char *memberName, v4 defaultValue)
{
    v4 result = defaultValue;

    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        ASSERT(member->type == JSON_TYPE_Array);
        ASSERT(member->array.count == 4);
        ASSERT(member->array[0]->type == JSON_TYPE_Float);
        ASSERT(member->array[1]->type == JSON_TYPE_Float);
        ASSERT(member->array[2]->type == JSON_TYPE_Float);
        ASSERT(member->array[3]->type == JSON_TYPE_Float);

        result[0] = (f32)member->array[0]->floatValue;
        result[1] = (f32)member->array[1]->floatValue;
        result[2] = (f32)member->array[2]->floatValue;
        result[3] = (f32)member->array[3]->floatValue;
    }

    return result;
}

internal quaternion gltf_GetQuaternionOrDefault(const base_json_object *object, const char *memberName, quaternion defaultValue)
{
    quaternion result = defaultValue;
    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        // TODO(torgrim): Better handling of float/integer difference.
        ASSERT(member->type == JSON_TYPE_Array);
        ASSERT(member->array.count == 4);
        ASSERT(member->array[0]->type == JSON_TYPE_Float || member->array[0]->type == JSON_TYPE_Integer);
        ASSERT(member->array[1]->type == JSON_TYPE_Float || member->array[1]->type == JSON_TYPE_Integer);
        ASSERT(member->array[2]->type == JSON_TYPE_Float || member->array[2]->type == JSON_TYPE_Integer);
        ASSERT(member->array[3]->type == JSON_TYPE_Float || member->array[3]->type == JSON_TYPE_Integer);

        if(member->array[0]->type == JSON_TYPE_Float)
        {
            result.x = (f32)member->array[0]->floatValue;
        }
        else
        {
            result.x = (f32)member->array[0]->intValue;
        }
        if(member->array[1]->type == JSON_TYPE_Float)
        {
            result.y = (f32)member->array[1]->floatValue;
        }
        else
        {
            result.y = (f32)member->array[1]->intValue;
        }
        if(member->array[2]->type == JSON_TYPE_Float)
        {
            result.z = (f32)member->array[2]->floatValue;
        }
        else
        {
            result.z = (f32)member->array[2]->intValue;
        }
        if(member->array[3]->type == JSON_TYPE_Float)
        {
            result.w = (f32)member->array[3]->floatValue;
        }
        else
        {
            result.w = (f32)member->array[3]->intValue;
        }
    }

    return result;
}

internal matrix4D gltf_GetMatrix4DOrDefault(const base_json_object *object, const char *memberName, matrix4D defaultValue)
{
    // TODO(torgrim): This is not a great way of doing it. Here we might
    // be copying 16 floats only to have them overwritten moment later.
    matrix4D result = defaultValue;

    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        ASSERT(member->type == JSON_TYPE_Array);
        ASSERT(member->array.count == 16);

        for(u32 i = 0; i < member->array.count; ++i)
        {
            const base_json_object *item = member->array[i];
            ASSERT(item->type == JSON_TYPE_Float || item->type == JSON_TYPE_Integer);
            i32 mod = (i32)Mod2(i, 2);
            i32 div = (i32)(i >> 2);
            if(item->type == JSON_TYPE_Float)
            {
                result(mod, div) = (f32)item->floatValue;
            }
            else
            {
                result(mod, div) = (f32)item->intValue;
            }
        }

    }

    return result;
}

internal matrix4D gltf_GetMatrix4DOrIdentity(const base_json_object *object, const char *memberName)
{
    matrix4D result = CreateIdentity4D();

    const base_json_object *member = JSON_GetMember_CString(object, memberName);
    if(member != nullptr)
    {
        ASSERT(member->type == JSON_TYPE_Array);
        ASSERT(member->array.count == 16);

        for(u32 i = 0; i < member->array.count; ++i)
        {
            const base_json_object *item = member->array[i];
            ASSERT(item->type == JSON_TYPE_Float || item->type == JSON_TYPE_Integer);
            i32 mod = (i32)Mod2(i, 2);
            i32 div = (i32)(i >> 2);
            if(item->type == JSON_TYPE_Float)
            {
                result(mod, div) = (f32)item->floatValue;
            }
            else
            {
                result(mod, div) = (f32)item->intValue;
            }
        }

    }

    return result;
}

internal const base_json_object *gltf_GetObjectFromRootList(base_json_object *root, const char *listName, i64 index)
{
    const base_json_object *result = nullptr;
    if(index > -1)
    {
        const base_json_object *listObject = JSON_GetMember_CString(root, listName);
        if(listObject)
        {
            result = JSON_GetListItemByIndexI64(listObject, index);
        }
    }

    return result;
}

internal gltf_buffer_cache_data gltf_ReadBufferFile(memory_arena *arena, const char *fullPath, u64 size)
{
    gltf_buffer_cache_data result = {};
    file_data file = Win32ReadFile(arena, fullPath);
    result.data = file.content;
    result.size = file.length;

    ASSERT(result.size == size);

    return result;
}

internal gltf_buffer_cache_data gltf_GetBufferDataByIndex(gltf_context *ctx, i64 index)
{
    gltf_buffer_cache_data result = {};

    const base_json_object *bufferObject = gltf_GetObjectFromRootList(ctx->root, "buffers", index);
    if(bufferObject)
    {
        char *uri = gltf_GetStringValueOrNull(bufferObject, "uri");
        i64 bufferSize = gltf_GetIntValueOrDefault(bufferObject, "byteLength", 0);
        char *name = gltf_GetStringValueOrNull(bufferObject, "name");
        ASSERT(uri != nullptr);
        size_t uriLength = RawStringLength(uri);
        ASSERT(uriLength > 4);
        if(uri[0] == 'd' && uri[1] == 'a' && uri[2] == 't' && uri[3] == 'a' && uri[4] == ':')
        {
            ASSERT(uriLength > 37);
            // TODO(torgrim): Do proper parsing of data uri
            char *data = uri + 37;
            PrintDebugMsg("Uri Length: %zu\n", uriLength);
            tbs_base64::b64_dec_result b64Result = tbs_base64::base64_decode((u8 *)data, uriLength - 37);
            result.data = AllocSize(ctx->arena, b64Result.size);
            memcpy(result.data, b64Result.data, b64Result.size);
            result.size = b64Result.size;

            ASSERT(b64Result.size == (u64)bufferSize);
            free(b64Result.data);
        }
        else
        {
            tbs_tmp_string fullPath(ctx->path, uri);
            result = gltf_ReadBufferFile(ctx->arena, fullPath.str, (u64)bufferSize);
        }

    }

    return result;
}

internal gltf_buffer_cache_data *gltf_CreateLoadedBuffer(gltf_context *ctx, i64 bufferIndex)
{
    gltf_buffer_cache_data buffer = gltf_GetBufferDataByIndex(ctx, bufferIndex);
    buffer.loaded = true;
    gltf_buffer_cache_data *result = ctx->bufferCache.SetSlotI64(bufferIndex, buffer);
    return result;
}

internal gltf_buffer_cache_data gltf_LoadBufferData(gltf_context *ctx,
                                                    i64 bufferIndex,
                                                    size_t count,
                                                    size_t byteOffset,
                                                    size_t stride,
                                                    size_t attribSize)
{
    gltf_buffer_cache_data result = {};
    if(bufferIndex > -1 && count > 0)
    {
        gltf_buffer_cache_data *buffer = ctx->bufferCache.GetSlotI64(bufferIndex);
        if(buffer == nullptr || buffer->loaded == false)
        {
            buffer = gltf_CreateLoadedBuffer(ctx, bufferIndex);
            if(buffer == nullptr)
            {
                // Return empty object
                ASSERT(false);
            }
        }

        size_t totalSize = attribSize * count;
        ASSERT(buffer->size >= byteOffset + totalSize);
        result.data = AllocSize(ctx->arena, totalSize);
        if(stride == 0)
        {
            memcpy(result.data, buffer->data + byteOffset, totalSize);
        }
        else
        {
            ASSERT(buffer->size >= byteOffset + (stride*(count-1)));
            u8 *dst = result.data;
            u8 *src = buffer->data + byteOffset;
            for(size_t c = 0; c < count; ++c)
            {
                memcpy(dst, src, attribSize);
                src += stride;
                dst += attribSize;
            }
        }

        result.size = totalSize;
    }

    return result;
}

internal gltf_buffer_view gltf_GetBufferView(gltf_context *ctx, i64 bufferViewIndex)
{
    gltf_buffer_view result = {};
    if(bufferViewIndex > -1)
    {
        const base_json_object *bufferViewList = JSON_GetMember_CString(ctx->root, "bufferViews");
        if(bufferViewList)
        {
            const base_json_object *bufferView = JSON_GetListItemByIndexI64(bufferViewList, bufferViewIndex);
            ASSERT(bufferView != nullptr);
            result.bufferIndex = gltf_GetIntValueOrDefault(bufferView, "buffer", -1);
            result.byteOffset = gltf_GetIntValueOrDefault(bufferView, "byteOffset", 0);
            result.byteLength = gltf_GetIntValueOrDefault(bufferView, "byteLength", 0);
            result.byteStride = gltf_GetIntValueOrDefault(bufferView, "byteStride", 0);
            result.target = (gltf_buffer_view_target)gltf_GetIntValueOrDefault(bufferView, "target", (i64)GLTF_BUFFER_VIEW_TARGET_None);
            // NOTE(torgrim): We don't really care about names at the moment.
            char *name = gltf_GetStringValueOrNull(bufferView, "name");
        }
    }

    return result;
}



internal tbs_array_sr<gltf_camera> gltf_ProcessCameraList(gltf_context *ctx)
{
    tbs_array_sr<gltf_camera> result;
    const base_json_object *list = JSON_GetMember_CString(ctx->root, "cameras");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_camera> camList(ctx->arena, list->array.count);
        for(u64 camIndex = 0; camIndex < list->array.count; ++camIndex)
        {
            const base_json_object *camObject = JSON_GetListItemByIndexU64(list, camIndex);
            gltf_camera cam = {};
            char *camTypeStr = gltf_GetStringValueOrNull(camObject, "type");
            cam.type = gltf_CameraTypeStringToEnum(camTypeStr);
            if(cam.type == GLTF_CAMERA_TYPE_Perspective)
            {
                const base_json_object *persp = JSON_GetMember_CString(camObject, "perspective");
                cam.aspectRatio = gltf_GetFloatValueOrDefaultFromNumber(persp, "aspectRatio", 0.0);
                cam.yFov = gltf_GetFloatValueOrDefaultFromNumber(persp, "yfov", 0.0);
                cam.zFar = gltf_GetFloatValueOrDefaultFromNumber(persp, "zfar", 0.0);
                cam.zNear = gltf_GetFloatValueOrDefaultFromNumber(persp, "znear", 0.0);
            }
            else
            {
                const base_json_object *ortho = JSON_GetMember_CString(camObject, "orthographic");
                cam.xMag = gltf_GetFloatValueOrDefaultFromNumber(ortho, "xmag", 0.0);
                cam.yMag = gltf_GetFloatValueOrDefaultFromNumber(ortho, "ymag", 0.0);
                cam.zFar = gltf_GetFloatValueOrDefaultFromNumber(ortho, "zfar", 0.0);
                cam.zNear = gltf_GetFloatValueOrDefaultFromNumber(ortho, "znear", 0.0);
            }

            camList.Add(cam);
        }

        result = camList;
    }

    return result;
}

internal tbs_array_sr<gltf_image> gltf_ProcessImageList(gltf_context *ctx)
{
    tbs_array_sr<gltf_image> result;
    const base_json_object *list = JSON_GetMember_CString(ctx->root, "images");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_image> imgList(ctx->arena, list->array.count);
        for(u64 imgIndex = 0; imgIndex < list->array.count; ++imgIndex)
        {
            const base_json_object *imgObject = JSON_GetListItemByIndexU64(list, imgIndex);
            gltf_image img = {};
            // TODO(torgrim): GLTF image can referer to either a file or a buffer.
            // currently only supports file.
            char *relUri = gltf_GetStringValueOrNull(imgObject, "uri");
            i64 bufferViewIndex = gltf_GetIntValueOrDefault(imgObject, "bufferView", -1);
            if(relUri)
            {
                img.uri = RawStringConcat(ctx->arena, ctx->path, relUri);
            }
            else if(bufferViewIndex > -1)
            {
                gltf_buffer_view bufferView = gltf_GetBufferView(ctx, bufferViewIndex);
                ASSERT(bufferView.byteLength > 0);

                ASSERT(bufferView.byteStride == 0);

                gltf_buffer_cache_data bufferData  = gltf_LoadBufferData(ctx,
                                                                         bufferView.bufferIndex,
                                                                         (size_t)bufferView.byteLength,
                                                                         (size_t)bufferView.byteOffset,
                                                                         (size_t)bufferView.byteStride,
                                                                         1);

                img.imageBuffer.data = bufferData.data;
                img.imageBuffer.size = bufferData.size;
                img.imageBuffer.componentType = GLTF_COMPONENT_TYPE_Byte;
                img.imageBuffer.count = 1;

            }
            else
            {
                ASSERT(false);
            }
            // TODO(torgrim): enum?
            img.mimeType = gltf_GetStringValueOrNull(imgObject, "mimeType");
            img.name = gltf_GetStringValueOrNull(imgObject, "name");

            imgList.Add(img);
        }

        result = imgList;
    }

    return result;
}

internal gltf_buffer_data gltf_LoadAccessor(gltf_context *ctx, i64 accessorIndex)
{
    gltf_buffer_data result = {};
    if(accessorIndex > -1)
    {
        const base_json_object *accessorList = JSON_GetMember_CString(ctx->root, "accessors");
        if(accessorList)
        {
            const base_json_object *accessor = JSON_GetListItemByIndexI64(accessorList, accessorIndex);
            ASSERT(accessor != nullptr);
            i64 bufferViewIndex = gltf_GetIntValueOrDefault(accessor, "bufferView", -1);
            i64 byteOffset = gltf_GetIntValueOrDefault(accessor, "byteOffset", 0);
            gltf_component_type componentType = (gltf_component_type)gltf_GetIntValueOrDefault(accessor, "componentType", GLTF_COMPONENT_TYPE_None);
            bool normalized = gltf_GetBoolValueOrDefault(accessor, "normalized", false);
            i64 count = gltf_GetIntValueOrDefault(accessor, "count", 0);
            char *typeStr = gltf_GetStringValueOrNull(accessor, "type");
            gltf_internal_attribute_type type = gltf_AttributeTypeStringToEnum(typeStr);

            char *name = gltf_GetStringValueOrNull(accessor, "name");

            ASSERT(normalized == false);
            ASSERT(bufferViewIndex > -1);
            ASSERT(componentType != GLTF_COMPONENT_TYPE_None);
            ASSERT(type != GLTF_INTERNAL_ATTRIBUTE_TYPE_None);
            ASSERT(count > 0);

            gltf_buffer_view bufferView = gltf_GetBufferView(ctx, bufferViewIndex);
            if(bufferView.byteLength > 0)
            {
                size_t compSize = gltf_GetComponentTypeSize(componentType);
                size_t attributeSize = gltf_GetAttributeTypeCount(type) * compSize;
                i64 totalByteOffset = bufferView.byteOffset + byteOffset;


                gltf_buffer_cache_data bufferData  = gltf_LoadBufferData(ctx,
                                                                         bufferView.bufferIndex,
                                                                         (size_t)count,
                                                                         (size_t)totalByteOffset,
                                                                         (size_t)bufferView.byteStride,
                                                                         attributeSize);

                result.data = bufferData.data;
                result.size = bufferData.size;
                result.componentType = componentType;
                result.count = count;
            }
#if 0
            // TODO(torgrim): Support these
            number[16] min = gltf_GetIntValueOrDefault(accessor, "min", -1);
            number[16] max = gltf_GetIntValueOrDefault(accessor, "max", -1);
            object sparse = gltf_GetIntValueOrDefault(accessor, "sparse", -1);
#endif
        }
    }

    return result;
}

internal tbs_array_sr<gltf_primitive> gltf_ProcessPrimitiveList(gltf_context *ctx, const base_json_object *meshObject)
{
    tbs_array_sr<gltf_primitive> result;
    const base_json_object *list = JSON_GetMember_CString(meshObject, "primitives");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_primitive> primList(ctx->arena, list->array.count);
        for(u64 primIndex = 0; primIndex < list->array.count; ++primIndex)
        {
            const base_json_object *primObject = JSON_GetListItemByIndexU64(list, primIndex);
            gltf_primitive prim = {};
            const base_json_object *attributesObject = JSON_GetMember_CString(primObject, "attributes");
            if(attributesObject != nullptr)
            {
                ASSERT(attributesObject->type == JSON_TYPE_Object);
                ASSERT(attributesObject->members.count > 0);
                i64 positionAccessor = gltf_GetIntValueOrDefault(attributesObject, GLTF_PRIMITIVE_ATTRIBUTE_Position, -1);
                i64 normalAccessor = gltf_GetIntValueOrDefault(attributesObject, GLTF_PRIMITIVE_ATTRIBUTE_Normal, -1);
                i64 tangentAccessor = gltf_GetIntValueOrDefault(attributesObject, GLTF_PRIMITIVE_ATTRIBUTE_Tangent, -1);
                i64 texCoord0Accessor = gltf_GetIntValueOrDefault(attributesObject, GLTF_PRIMITIVE_ATTRIBUTE_TexCoord0, -1);

                prim.positions = gltf_LoadAccessor(ctx, positionAccessor);
                prim.normals = gltf_LoadAccessor(ctx, normalAccessor);
                prim.tangents = gltf_LoadAccessor(ctx, tangentAccessor);
                prim.uv0 = gltf_LoadAccessor(ctx, texCoord0Accessor);

                // TODO(torgrim): Support Joints, Weights, Colors and TexCoords1
            }

            i64 indicesAccessor = gltf_GetIntValueOrDefault(primObject, "indices", -1);
            prim.indices = gltf_LoadAccessor(ctx, indicesAccessor);
            prim.materialIndex = gltf_GetIntValueOrDefault(primObject, "material", -1);
            prim.mode = (gltf_primitive_mode)gltf_GetIntValueOrDefault(primObject, "mode", GLTF_PRIMITIVE_MODE_Triangles);

            primList.Add(prim);
            // TODO(torgrim): Support targets.
        }

        result = primList;
    }

    return result;
}

internal tbs_array_sr<gltf_mesh> gltf_ProcessMeshList(gltf_context *ctx)
{
    tbs_array_sr<gltf_mesh> result;
    const base_json_object *list = JSON_GetMember_CString(ctx->root, "meshes");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_mesh> meshList(ctx->arena, list->array.count);
        for(u64 meshIndex = 0; meshIndex < list->array.count; ++meshIndex)
        {
            const base_json_object *meshObject = JSON_GetListItemByIndexU64(list, meshIndex);
            gltf_mesh mesh = {};
            mesh.primitives = gltf_ProcessPrimitiveList(ctx, meshObject);
            mesh.name = gltf_GetStringValueOrNull(meshObject, "name");

            // TODO:
            //mesh->weights = gltf_ProcessNumberList(ctx, meshObject);

            meshList.Add(mesh);
        }

        result = meshList;
    }

    return result;
}

internal tbs_array_sr<gltf_node> gltf_ProcessNodeList(gltf_context *ctx)
{
    tbs_array_sr<gltf_node> result;
    const base_json_object *list = JSON_GetMember_CString(ctx->root, "nodes");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_node> nodeList(ctx->arena, list->array.count);
        for(u64 nodeIndex = 0; nodeIndex < list->array.count; ++nodeIndex)
        {
            const base_json_object *nodeObject = JSON_GetListItemByIndexU64(list, nodeIndex);
            gltf_node node = {};
            node.cameraIndex = gltf_GetIntValueOrDefault(nodeObject, "camera", -1);
            node.meshIndex = gltf_GetIntValueOrDefault(nodeObject, "mesh", -1);
            node.skinIndex = gltf_GetIntValueOrDefault(nodeObject, "skin", -1);
            if(JSON_GetMember_CString(nodeObject, "matrix"))
            {
                matrix4D m = gltf_GetMatrix4DOrIdentity(nodeObject, "matrix");
                trs_holder trs = GetDecomposedTRSFromTRSMatrix(m);
                node.rotation = trs.rotation;
                node.translation = trs.translation;
                node.scale = trs.scale;
            }
            else
            {

                quaternion q = gltf_GetQuaternionOrDefault(nodeObject, "rotation", quaternion(0.0f, 0.0f, 0.0f, 1.0f));
                node.rotation = CalcEulerFromQuaternion(q);
                node.translation = gltf_GetV3OrDefault(nodeObject, "translation", v3(0.0f, 0.0f, 0.0f));
                node.scale = gltf_GetV3OrDefault(nodeObject, "scale", v3(1.0f, 1.0f, 1.0f));
            }

            node.name = gltf_GetStringValueOrNull(nodeObject, "name");

            const base_json_object *childrenObject = JSON_GetMember_CString(nodeObject, "children");
            if(childrenObject != nullptr && childrenObject->array.count > 0)
            {
                tbs_array_sr<i64> children(ctx->arena, childrenObject->array.count);
                for(u64 childIndex = 0; childIndex < childrenObject->array.count; ++childIndex)
                {
                    const base_json_object *childObject = JSON_GetListItemByIndexU64(childrenObject, childIndex);
                    children.Add(childObject->intValue);
                }

                node.children = children;
            }

            nodeList.Add(node);
        }

        result = nodeList;
    }

    return result;
}

internal tbs_array_sr<gltf_scene> gltf_ProcessSceneList(gltf_context *ctx)
{
    tbs_array_sr<gltf_scene> result;
    const base_json_object *list = JSON_GetMember_CString(ctx->root, "scenes");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_scene> sceneList(ctx->arena, list->array.count);
        for(u64 sceneIndex = 0; sceneIndex < list->array.count; ++sceneIndex)
        {
            const base_json_object *sceneObject = JSON_GetListItemByIndexU64(list, sceneIndex);
            gltf_scene scene = {};
            scene.name = gltf_GetStringValueOrNull(sceneObject, "name");
            const base_json_object *nodeList = JSON_GetMember_CString(sceneObject, "nodes");
            if(nodeList != nullptr)
            {
                ASSERT(nodeList->type == JSON_TYPE_Array);
                ASSERT(nodeList->array.count > 0);
                scene.rootNodes = tbs_array_sr<i64>(ctx->arena, nodeList->array.count);
                for(u64 nodeIndex = 0; nodeIndex < nodeList->array.count; ++nodeIndex)
                {
                    const base_json_object *indexObject = JSON_GetListItemByIndexU64(nodeList, nodeIndex);
                    ASSERT(indexObject->type == JSON_TYPE_Integer);
                    i64 index = indexObject->intValue;
                    scene.rootNodes.Add(index);
                }
            }

            sceneList.Add(scene);
        }

        result = sceneList;
    }

    return result;
}

internal gltf_texture_info gltf_GetTextureInfo(const base_json_object *object, const char *memberName)
{
    const base_json_object *to = JSON_GetMember_CString(object, memberName);
    gltf_texture_info result;
    result.texIndex = gltf_GetIntValueOrDefault(to, "index", -1);
    result.texCoordsIndex = gltf_GetIntValueOrDefault(to, "texCoord", 0);

    return result;
}

internal tbs_array_sr<gltf_texture> gltf_ProcessTextureList(gltf_context *ctx)
{
    tbs_array_sr<gltf_texture> result;
    const base_json_object *list = JSON_GetMember_CString(ctx->root, "textures");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_texture> textureList(ctx->arena, list->array.count);
        for(u64 texIndex = 0; texIndex < list->array.count; ++texIndex)
        {
            const base_json_object *texObject = JSON_GetListItemByIndexU64(list, texIndex);
            gltf_texture tex;
            tex.name = gltf_GetStringValueOrNull(texObject, "name");
            tex.samplerIndex = gltf_GetIntValueOrDefault(texObject, "sampler", -1);
            tex.imageIndex = gltf_GetIntValueOrDefault(texObject, "source", -1);

            textureList.Add(tex);
        }

        result = textureList;
    }

    return result;
}

internal tbs_array_sr<gltf_sampler> gltf_ProcessSamplerList(gltf_context *ctx)
{
    tbs_array_sr<gltf_sampler> result;
    const base_json_object *list = JSON_GetMember_CString(ctx->root, "samplers");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_sampler> samplerList(ctx->arena, list->array.count);
        for(u64 samplerIndex = 0; samplerIndex < list->array.count; ++samplerIndex)
        {
            const base_json_object *samplerObject = JSON_GetListItemByIndexU64(list, samplerIndex);
            gltf_sampler sampler;
            sampler.name = gltf_GetStringValueOrNull(samplerObject, "name");
            sampler.magFilter = (gltf_sampler_filter_type)gltf_GetIntValueOrDefault(samplerObject, "magFilter", GLTF_SAMPLER_FILTER_TYPE_Linear);
            sampler.minFilter = (gltf_sampler_filter_type)gltf_GetIntValueOrDefault(samplerObject, "minFilter", GLTF_SAMPLER_FILTER_TYPE_Linear);
            sampler.wrapS = (gltf_sampler_wrap_type)gltf_GetIntValueOrDefault(samplerObject, "wrapS", GLTF_SAMPLER_WRAP_TYPE_Repeat);
            sampler.wrapT = (gltf_sampler_wrap_type)gltf_GetIntValueOrDefault(samplerObject, "wrapT", GLTF_SAMPLER_WRAP_TYPE_Repeat);

            samplerList.Add(sampler);
        }

        result = samplerList;
    }

    return result;
}

internal gltf_pbr_metallic_roughness gltf_GetMaterialPbrMetallicRoughness(const base_json_object *matObject)
{
    const base_json_object *pbrObject = JSON_GetMember_CString(matObject, "pbrMetallicRoughness");
    gltf_pbr_metallic_roughness result = {};
    result.baseColorFactor = gltf_GetV4OrDefault(pbrObject, "baseColorFactor", v4(1.0, 1.0, 1.0, 1.0));
    result.metallicFactor = gltf_GetFloatValueOrDefaultFromNumber(pbrObject, "metallicFactor", 1.0);
    result.roughnessFactor = gltf_GetFloatValueOrDefaultFromNumber(pbrObject, "roughnessFactor", 1.0);
    result.roughnessFactor = gltf_GetFloatValueOrDefaultFromNumber(pbrObject, "roughnessFactor", 1.0);
    result.baseColorTexture = gltf_GetTextureInfo(pbrObject, "baseColorTexture");
    result.metallicRoughnessTexture = gltf_GetTextureInfo(pbrObject, "metallicRoughnessTexture");

    return result;
}

internal tbs_array_sr<gltf_material> gltf_ProcessMaterialList(gltf_context *ctx)
{
    tbs_array_sr<gltf_material> result;
    const base_json_object *list = JSON_GetMember_CString(ctx->root, "materials");
    if(list != nullptr)
    {
        // TODO(torgrim): These cases shouldn't terminate the process but returned information
        // indicating invalid gltf file.
        ASSERT(list->type == JSON_TYPE_Array);
        ASSERT(list->array.count > 0);
        tbs_array_sr<gltf_material> materialList(ctx->arena, list->array.count);
        for(u64 matIndex = 0; matIndex < list->array.count; ++matIndex)
        {
            const base_json_object *matObject = JSON_GetListItemByIndexU64(list, matIndex);
            gltf_material material = {};
            material.name = gltf_GetStringValueOrNull(matObject, "name");
            material.emissiveFactor = gltf_GetV3OrDefault(matObject, "emissiveFactor", v3(0.0f, 0.0f, 0.0f));
            material.alphaMode = gltf_AlphaModeStringToEnum(gltf_GetStringValueOrNull(matObject, "alphaMode"));
            material.alphaCutoff = gltf_GetFloatValueOrDefaultFromNumber(matObject, "alphaCutoff", 0.5);
            material.doubleSided = gltf_GetBoolValueOrDefault(matObject, "doubleSided", false);

            material.emissiveTexture = gltf_GetTextureInfo(matObject, "emissiveTexture");
            material.pbr = gltf_GetMaterialPbrMetallicRoughness(matObject);

            const base_json_object *normalTexObject = JSON_GetMember_CString(matObject, "normalTexture");
            if(normalTexObject)
            {
                material.normalTextureIndex = gltf_GetIntValueOrDefault(normalTexObject, "index", -1);
                material.normalTexCoordsIndex = gltf_GetIntValueOrDefault(normalTexObject, "texCoord", 0);
                material.normalScaleFactor = gltf_GetFloatValueOrDefaultFromNumber(normalTexObject, "scale", 1.0);
            }
            else
            {
                material.normalTextureIndex = -1;
            }

            const base_json_object *occlusionTexObject = JSON_GetMember_CString(matObject, "occlusionTexture");
            if(occlusionTexObject)
            {
                material.occlusionTextureIndex = gltf_GetIntValueOrDefault(occlusionTexObject, "index", -1);
                material.occlusionTexCoordsIndex = gltf_GetIntValueOrDefault(occlusionTexObject, "texCoord", 0);
                material.occlusionStrengthFactor = gltf_GetFloatValueOrDefaultFromNumber(occlusionTexObject, "strength", 1.0);
            }
            else
            {
                material.occlusionTextureIndex = -1;
            }

            materialList.Add(material);
        }

        result = materialList;
    }

    return result;
}

internal gltf_asset gltf_ProcessAsset(gltf_context *ctx)
{
    gltf_asset result = {};
    const base_json_object *assetObject = JSON_GetMember_CString(ctx->root, "asset");
    if(assetObject)
    {
        result.copyright = gltf_GetStringValueOrNull(assetObject, "copyright");
        result.generator = gltf_GetStringValueOrNull(assetObject, "generator");
        result.version = gltf_GetStringValueOrNull(assetObject, "version");
        result.minVersion = gltf_GetStringValueOrNull(assetObject, "minVersion");
    }

    return result;
}

internal void gltf_InitBufferCache(gltf_context *ctx)
{
    const base_json_object *bufferList = JSON_GetMember_CString(ctx->root, "buffers");
    if(bufferList != nullptr)
    {
        ASSERT(bufferList->type == JSON_TYPE_Array && bufferList->array.count > 0);
        ctx->bufferCache = tbs_array_sr<gltf_buffer_cache_data>(ctx->arena, bufferList->array.count);
    }
}

internal gltf_root gltf_ProcessGltf(gltf_context *ctx)
{
    ASSERT(ctx != nullptr && ctx->root != nullptr && RawStringLength(ctx->path) > 0);

    gltf_InitBufferCache(ctx);
    // TODO(torgrim): Check that this calls the constructor of subobjects.
    // TODO(torgrim): Allocate this from the ctx arena so that
    // we only have to return a pointer instead of a full struct
    // This also makes it clearer that all data is related to the arena.
    // Can use a placement new to make sure that the arrays are initialized
    // properly.
    gltf_root root = {};
    root.asset = gltf_ProcessAsset(ctx);
    root.cameras = gltf_ProcessCameraList(ctx);
    root.images = gltf_ProcessImageList(ctx);
    root.meshes = gltf_ProcessMeshList(ctx);
    root.nodes = gltf_ProcessNodeList(ctx);
    root.scenes = gltf_ProcessSceneList(ctx);
    root.textures = gltf_ProcessTextureList(ctx);
    root.samplers = gltf_ProcessSamplerList(ctx);
    root.materials = gltf_ProcessMaterialList(ctx);

    root.defaultScene = gltf_GetIntValueOrDefault(ctx->root, "scene", -1);

    return root;
}

