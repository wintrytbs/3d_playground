#ifndef TBS_JSON_H

#include "tbs_types.h"

enum json_type
{
    JSON_TYPE_None,
    JSON_TYPE_Array,
    JSON_TYPE_Object,
    JSON_TYPE_String,
    JSON_TYPE_Integer,
    JSON_TYPE_Float,
    JSON_TYPE_Null,
    JSON_TYPE_Bool,
};

struct base_json_object
{
    json_type type;
    union
    {
        i64 intValue;
        f64 floatValue;
        char *stringValue;
        bool boolValue;
    };

    // NOTE(torgrim): Name is not really needed, just there
    // for easier debugging.
    char *name;
    u32 nameHash;

    ss_dynamic_array<base_json_object *> array;
    simple_dict<base_json_object *> members;
};

#define JSON_GetMember_CString(object, name) JSON_GetMember(object, name)
internal const base_json_object *JSON_GetMember(const base_json_object *obj, const char *memberName);
internal const base_json_object *JSON_GetListItemByIndexU64(const base_json_object *list, u64 index);
internal const base_json_object *JSON_GetListItemByIndexI64(const base_json_object *list, i64 index);
internal base_json_object *ParseJsonSimple(memory_arena *arena, file_data *file);

#define TBS_JSON_H
#endif
