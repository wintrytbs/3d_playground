/* date = June 8th 2020 8:03 pm */

#ifndef TBS_LIGHT_H


// TODO(torgrim): have a none_type?
enum light_type
{
    LIGHT_TYPE_Point,
    LIGHT_TYPE_Spot,
    LIGHT_TYPE_Directional,

    LIGHT_TYPE_Count,
};

// TODO(torgrim): have a none_type?
enum distance_attenuation_function
{
    DISTANCE_ATTENUATION_FUNCTION_InvSqrSim = 0,
    DISTANCE_ATTENUATION_FUNCTION_Exp,
    DISTANCE_ATTENUATION_FUNCTION_CubicPolynomial,

    DISTANCE_ATTENUATION_FUNCTION_Count,
};


struct light_data
{
    v3 position;
    v3 rotation;
    v3 color;
    distance_attenuation_function distAttFunc;
    f32 strength;
    f32 maxDist;
    f32 falloffStartDist;
    f32 spotMinCutoff;
    f32 spotMaxCutoff;
    light_type type;
    bool isEnabled;
};

internal matrix4D CreatePointLightCameraMatrix(GLenum face, v3 pos);
internal light_data CreateLight(light_type type);

#define TBS_LIGHT_H

#endif //TBS_LIGHT_H
