#include "tbs_imgui_widgets.h"

namespace tbs_gui
{
    internal bool ShowFileBrowserWidget(file_browser_context *ctx)
    {
        // TODO(torgrim): We probably don't want to query
        // the OS for FindFirstFile etc each frame.
        // Just cache the result and update when changing
        // directory. Might subscribe to notifications
        // for changes in the current directory as well.
        bool result = false;

        ImGui::OpenPopup("File Browser##tbs_file_browser");
        ImGui::SetNextWindowSize(ImVec2(600, 400), ImGuiCond_FirstUseEver);
        if(ImGui::BeginPopupModal("File Browser##tbs_file_browser"))
        {

            // TODO(torgrim): Move this so that we don't need a local persist, maybe into
            // the gui state.
            char *searchDir = ctx->searchDir;
            local_persist i32 selectedDirItem = -1;

            char queryString[MAX_PATH];
            sprintf(queryString, "%s*", searchDir);
            WIN32_FIND_DATAA fileList[255];
            HANDLE searchHandle = FindFirstFileA(queryString, fileList);
            i32 dirCount = 1;
            if(searchHandle != INVALID_HANDLE_VALUE)
            {
                while(FindNextFile(searchHandle, fileList + dirCount))
                {
                    ASSERT(dirCount < 255);
                    ++dirCount;
                }

                FindClose(searchHandle);
            }

            if(dirCount > 0)
            {

                ImGui::Text("Directory: %s", searchDir);

                ImGui::PushStyleColor(ImGuiCol_ChildBg, IM_COL32(100, 100, 100, 100));
                ImGui::Columns(1, "tbs_file_viewer_columns");
                ImGui::Separator();
                ImGui::Text("Name");
                ImGui::Separator();
                ImGui::BeginChild("tbs_file_viewer", ImVec2(0, -ImGui::GetFrameHeightWithSpacing()));
                for(i32 i = 0; i < dirCount; ++i)
                {
                    if((fileList[i].dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) || ctx->extension[0] == '\0' || MatchFileExtension(fileList[i].cFileName, ctx->extension))
                    {
                        if(ImGui::Selectable(fileList[i].cFileName, selectedDirItem == i, ImGuiSelectableFlags_SpanAllColumns))
                        {
                            selectedDirItem = i;
                        }
                    }
                }

                ImGui::Columns(1);
                ImGui::Separator();
                ImGui::PopStyleColor();
                ImGui::EndChild();

                if(selectedDirItem != -1 && (fileList[selectedDirItem].dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
                {
                    char *dirName = fileList[selectedDirItem].cFileName;
                    size_t nameLength = RawStringLength(dirName);
                    if(nameLength != 1 || dirName[0] != '.')
                    {
                        if(nameLength == 2 && dirName[0] == '.' && dirName[1] == '.')
                        {
                            i32 parentDirOffset = GetParentDirectoryOffset(searchDir);
                            if(parentDirOffset != -1)
                            {
                                searchDir[parentDirOffset+1] = '\0';
                            }
                        }
                        else
                        {
                            AppendToPath(searchDir, fileList[selectedDirItem].cFileName, true);
                        }
                    }

                    selectedDirItem = -1;
                }

                if(ImGui::Button("Open"))
                {
                    ASSERT(selectedDirItem != -1);
                    if(ctx->extension[0] == '\0' || MatchFileExtension(fileList[selectedDirItem].cFileName, ctx->extension))
                    {
                        ImGui::CloseCurrentPopup();

                        ctx->result.dialogResult = DIALOG_RESULT_Open;
                        RawStringConcat(ctx->result.fullFilename, searchDir, fileList[selectedDirItem].cFileName);
                        selectedDirItem = -1;

                        result = true;
                    }
                }

                ImGui::SameLine();

                if(ImGui::Button("Cancel"))
                {
                    ImGui::CloseCurrentPopup();

                    result = true;
                    ctx->result.dialogResult = DIALOG_RESULT_Cancel;
                    selectedDirItem = -1;
                }
                ImGui::EndPopup();
            }
        }

        return result;
    }



    internal bool ShowEnvMapChooserWidget(application_state *app,
                                          tbs_opengl::opengl_resource_manager *GPUManager,
                                          env_map_chooser_context *ctx)
    {
        // TODO(torgrim): Do we want to make this more like
        // blender which shows the maps mapped onto spheres?
        bool result = false;
        if(!ctx->init)
        {
            ctx->chooserIndex = ctx->activeIndex;
            ctx->mode = ENV_MAP_CHOOSER_MODE_Normal;
            ctx->init = true;
        }

        ImGui::OpenPopup("Environment Map Chooser##tbs_env_map_chooser");
        ImGui::SetNextWindowSize(ImVec2(700, 500), ImGuiCond_FirstUseEver);
        if(ImGui::BeginPopupModal("Environment Map Chooser##tbs_env_map_chooser"))
        {
            if(ImGui::Button("Load HDR"))
            {
                ctx->mode = ENV_MAP_CHOOSER_MODE_FileBrowser;
            }

            if(ctx->mode == ENV_MAP_CHOOSER_MODE_FileBrowser)
            {
                if(ctx->fbCtx.init == false)
                {
                    RawStringCopy(ctx->fbCtx.searchDir, app->hdrPath);
                    RawStringCopy(ctx->fbCtx.extension, "hdr");
                    ctx->fbCtx.result.dialogResult = DIALOG_RESULT_None;
                    memset(ctx->fbCtx.result.fullFilename, 0, sizeof(ctx->fbCtx.result.fullFilename));
                    ctx->fbCtx.init = true;
                }

                if(ShowFileBrowserWidget(&ctx->fbCtx))
                {
                    if(ctx->fbCtx.result.dialogResult == DIALOG_RESULT_Cancel)
                    {
                        ctx->fbCtx.init = false;
                        ctx->mode = ENV_MAP_CHOOSER_MODE_Normal;
                    }
                    else if(ctx->fbCtx.result.dialogResult == DIALOG_RESULT_Open)
                    {
                        image_data_hdr hdrImage = LoadImageDataHDR(ctx->fbCtx.result.fullFilename, true, true);
                        auto envHandle = GPUManager->AddEnvMapHDR(app, &hdrImage);
                        ctx->chooserIndex = tbs_opengl::HandleToIndexU32(envHandle);
                        FreeImageData(&hdrImage);
                        ctx->fbCtx.init = false;
                        ctx->mode = ENV_MAP_CHOOSER_MODE_Normal;
                    }
                }
            }

            ImGui::Text("Current:");
            // TODO(torgrim): Find a better solution for handling cases
            // where there is no currently active env map.
            if(ctx->activeIndex != UINT32_MAX)
            {
                tbs_pbr::env_map_info *active = GPUManager->environmentMaps + ctx->activeIndex;
                ImGui::Image((void *)(intptr_t)active->hdr, ImVec2(64, 64), ImVec2(0, 1), ImVec2(1, 0));
                if(ImGui::IsItemHovered())
                {
                    ImGui::BeginTooltip();
                    ImGui::Text(active->filename);
                    ImGui::EndTooltip();
                }
            }
            else
            {
                ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "No Current Environment Map");
            }
            f32 windowMaxX = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;
            ImGui::BeginChild("HDR List##hdr_viewer_list", ImVec2(0, -ImGui::GetFrameHeightWithSpacing()), true);
            local_persist i32 hoverIndex = -1;
            bool wasHovered = false;
            for(i32 envIndex = 0; envIndex < (i32)GPUManager->environmentMapCount; ++envIndex)
            {
                // TODO(torgrim): Do something special for the active index
                // to show it's the currently active line/index.
                tbs_pbr::env_map_info *envMap = GPUManager->environmentMaps + envIndex;
                ImGui::BeginGroup();
                if(envIndex == hoverIndex)
                {
                    ImGui::Image((void *)(intptr_t)envMap->hdr, ImVec2(128, 128), ImVec2(0, 1), ImVec2(1, 0), ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(0.0f, 1.0f, 1.0f, 1.0f));
                }
                else if(envIndex == (i32)ctx->chooserIndex)
                {
                    ImGui::Image((void *)(intptr_t)envMap->hdr, ImVec2(128, 128), ImVec2(0, 1), ImVec2(1, 0), ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(1.0f, 1.0f, 0.0f, 1.0f));
                }
                else
                {
                    ImGui::Image((void *)(intptr_t)envMap->hdr, ImVec2(128, 128), ImVec2(0, 1), ImVec2(1, 0), ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(0.0f, 0.0f, 0.0f, 1.0f));
                }
                //ImGui::Text(envMap->filename);
                //ImGui::PopTextWrapPos();


                ImGui::EndGroup();

                f32 prevButtonX = ImGui::GetItemRectMax().x;
                f32 nextButtonX = prevButtonX  + ImGui::GetStyle().ItemSpacing.x + 128;
                if(nextButtonX < windowMaxX)
                {
                    ImGui::SameLine();
                }

                if(ImGui::IsItemClicked())
                {
                    ctx->chooserIndex = (u32)envIndex;
                }
                if(ImGui::IsItemHovered())
                {
                    ImGui::BeginTooltip();
                    ImGui::Text(envMap->filename);
                    ImGui::EndTooltip();
                    hoverIndex = envIndex;
                    wasHovered = true;
                }
#if 0
                if(ImGui::Selectable((GPUManager->environmentMaps+envIndex)->filename, ctx->chooserIndex == envIndex, 0))
                {
                    ctx->chooserIndex = envIndex;
                }
#endif
            }

            if(!wasHovered)
            {
                hoverIndex = -1;
            }
            ImGui::EndChild();

#if 0
            if(ctx->chooserIndex < GPUManager->environmentMapCount)
            {
                ImGui::SameLine();
                ImGui::BeginChild("HDR Preview##hdr_viewer_preview", ImVec2(0, -ImGui::GetFrameHeightWithSpacing()));
                tbs_pbr::env_map_info *envMap = GPUManager->environmentMaps + ctx->chooserIndex;
                ImGui::Image((void *)(intptr_t)envMap->hdr, ImVec2(128, 128), ImVec2(0, 1), ImVec2(1, 0));
                ImGui::EndChild();
#if 0
                if(ImGui::BeginTabBar("hdr_previews"))
                {
                    tbs_pbr::env_map_info *envMap = GPUManager->environmentMaps + ctx->chooserIndex;
                    if(ImGui::BeginTabItem("Original HDR"))
                    {
                        ImGui::BeginChild("hdr_image_preview_window");
                        ImGui::Image((void *)(intptr_t)envMap->hdr, ImVec2(128, 128), ImVec2(0, 1), ImVec2(1, 0));
                        ImGui::EndChild();
                        ImGui::EndTabItem();
                    }

                    ImGui::EndTabBar();
                }
                ImGui::EndChild();
#endif
            }
#endif

            if(ImGui::Button("Select"))
            {
                ctx->init = false;
                ctx->open = false;
                result = true;
                ctx->result = tbs_opengl::IndexU32ToHandle(ctx->chooserIndex);
            }
            ImGui::SameLine();
            if(ImGui::Button("Cancel"))
            {
                ctx->init = false;
                ctx->open = false;
            }

            ImGui::EndPopup();


        }
        return result;
    }
}
