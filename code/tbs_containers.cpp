#include "tbs_containers.h"

internal inline u32 DictHashDefault(const char *str)
{
    meow_u128 hash = MeowHash(MeowDefaultSeed, RawStringLength(str), str);
    u32 shortHash = (u32)MeowU32From(hash, 0);

    return shortHash;
}

// TODO(torgrim): Make a better version of this.
internal inline u32 ModHash(u32 hash)
{
    u64 a = 0x9E3779B9ull;
    u64 result = ((a*hash) & ((1llu << 31llu) - 1llu)) >> (32llu - 8llu);
    ASSERT(result < DEFAULT_DICT_COUNT);
    return (u32)result;
}

template <typename T>
tbs_array_sr<T>::tbs_array_sr() : data(nullptr), count(0), capacity(0)
{
}

template <typename T>
tbs_array_sr<T>::tbs_array_sr(memory_arena *arena, size_t cap)
{
    ASSERT(cap > 0);

    this->data = nullptr;
    this->capacity = cap;
    this->count = 0;
    this->data = AllocArray(arena, T, cap);
}

template <typename T>
void tbs_array_sr<T>::Add(T item)
{
    ASSERT(this->data != nullptr);
    ASSERT(this->count < this->capacity);
    this->data[count++] = item;
}

template <typename T>
T *tbs_array_sr<T>::GetItem(size_t index)
{
    ASSERT(this->data != nullptr);
    ASSERT(index < this->count);
    ASSERT(index < this->capacity);

    T *result = this->data + index;
    return result;
}

template <typename T>
T *tbs_array_sr<T>::GetItemI64(i64 index)
{
    ASSERT(index > -1);
    T *result = this->GetItem((size_t)index);
    return result;
}

template <typename T>
T *tbs_array_sr<T>::GetItemOrNull(size_t index)
{
    ASSERT(this->data != nullptr);
    T *result = nullptr;
    if(index < this->count)
    {
        result = this->data + index;
    }

    return result;
}

template <typename T>
T *tbs_array_sr<T>::GetItemOrNullI64(i64 index)
{
    T *result = nullptr;
    if(index > -1)
    {
        result = this->GetItemOrNull((size_t)index);
    }

    return result;
}

template <typename T>
T *tbs_array_sr<T>::GetSlot(size_t index)
{
    ASSERT(this->data != nullptr);
    ASSERT(index < this->capacity);

    T *result = this->data + index;
    return result;
}

template <typename T>
T *tbs_array_sr<T>::GetSlotI64(i64 index)
{
    ASSERT(index > -1);
    T *result = this->GetSlot((size_t)index);
    return result;
}


template <typename T>
T tbs_array_sr<T>::GetValueOrDefaultI64(i64 index, T defaultValue)
{
    T result = defaultValue;
    if(index > -1 && (size_t)index < this->count)
    {
        result = this->data[index];
    }

    return result;
}



template <typename T>
tbs_array_sr<T*>::tbs_array_sr() : data(nullptr), count(0), capacity(0)
{

}

template <typename T>
tbs_array_sr<T*>::tbs_array_sr(memory_arena *arena, size_t cap)
{
    this->data = nullptr;
    this->capacity = cap;
    this->count = 0;
    if(cap > 0)
    {
        this->data = AllocPointerArray(arena, T, cap);
    }
}

template <typename T>
void tbs_array_sr<T*>::Add(T *item)
{
    ASSERT(this->data != nullptr);
    ASSERT(this->count < this->capacity);
    this->data[this->count++] = item;
}

template <typename T>
T *tbs_array_sr<T*>::GetItem(size_t index)
{
    ASSERT(this->data != nullptr);
    ASSERT(index < this->count);
    T *result = this->data[index];
    return result;
}

template <typename T>
T *tbs_array_sr<T*>::GetItemI64(i64 index)
{
    ASSERT(index > -1);
    T *result = this->GetItem((size_t)index);
    return result;
}

template <typename T>
T *tbs_array_sr<T*>::GetItemOrNull(size_t index)
{
    ASSERT(this->data != nullptr);
    T *result = nullptr;
    if(index < this->count)
    {
        result = this->data[index];
    }

    return result;
}

template <typename T>
T *tbs_array_sr<T*>::GetItemOrNullI64(i64 index)
{
    T *result = nullptr;
    if(index > -1)
    {
        result = this->GetItemOrNull((size_t)index);
    }

    return result;
}

template <typename T>
T *tbs_array_sr<T>::SetSlot(size_t index, T item)
{
    ASSERT(this->data != nullptr);
    ASSERT(index < this->capacity);
    // NOTE(torgrim): When using slot api it should never
    // have been used as list before.
    ASSERT(this->count == 0);

    T *result = this->data + index;
    *result = item;
    return result;
}

template <typename T>
T *tbs_array_sr<T>::SetSlotI64(i64 index, T item)
{
    ASSERT(index > -1);
    T *result = this->SetSlot((size_t)index, item);
    return result;
}


template <typename T>
ss_dynamic_array<T*>::ss_dynamic_array() : data(nullptr), count(0), capacity(0), arena(nullptr), relocationCount(0)
{
}

template <typename T>
ss_dynamic_array<T*>::ss_dynamic_array(memory_arena *srcArena)
{
    this->data = AllocPointerArray(srcArena, T, DEFAULT_DYN_ARRAY_COUNT);
    this->count = 0;
    this->capacity = DEFAULT_DYN_ARRAY_COUNT;
    this->arena = srcArena;
    this->relocationCount = 0;
}

template <typename T>
const T *ss_dynamic_array<T*>::operator[](size_t i) const
{
    ASSERT(i < this->count);
    T *result = this->data[i];
    return result;
}

template <typename T>
void ss_dynamic_array<T*>::Add(T *item)
{
    ASSERT(this->data != nullptr);
    ASSERT(this->capacity > 0);
    size_t currentSize = this->count * sizeof(T);
    if(this->count+1 > this->capacity)
    {
        T **newMem = AllocPointerArray(this->arena, T, this->count * 2u);
        memcpy(newMem, this->data, currentSize);
        this->data = (T **)newMem;
        this->capacity = this->count * 2u;
        this->relocationCount++;
    }

    this->data[count++] = item;
}

template <typename T>
simple_dict<T*>::simple_dict() : data(nullptr), count(0)
{
}

template <typename T>
simple_dict<T*>::simple_dict(memory_arena *arena)
{
    this->data = AllocArray(arena, pair, DEFAULT_DICT_COUNT);
    this->count = 0;
}

template <typename T>
const T *simple_dict<T*>::operator [](u32 hash) const
{
    T *result = nullptr;
    if(this->count > 0)
    {
        ASSERT(this->data != nullptr);
        u32 index = ModHash(hash);
        pair p = this->data[index];
        if(p.key == hash)
        {
            result = p.value;
        }
    }

    return result;
}

template <typename T>
const T *simple_dict<T*>::operator [](const char *str) const
{
    T *result = nullptr;
    if(this->count > 0)
    {
        ASSERT(this->data != nullptr);
        u32 hash = DictHashDefault(str);
        u32 index = ModHash(hash);
        pair p = this->data[index];
        if(p.key == hash)
        {
            result = p.value;
        }
    }

    return result;
}

template <typename T>
void simple_dict<T*>::Add(u32 hash, T *value)
{
    ASSERT(hash > 0);
    // TODO(torgrim): Handle collision.
    u32 index = ModHash(hash);
    ASSERT(this->data != nullptr);
    ASSERT(this->count < DEFAULT_DICT_COUNT);

    // TODO(torgrim): This is just as a test for now since we don't really
    // now what data this is. So how do we check for collision. Does the data
    // need to be a pointer type?
    ASSERT(this->data[index].key == 0);

    this->data[index].value = value;
    this->data[index].key = hash;

    ++count;
}

template <typename T>
limit_stack<T>::limit_stack(memory_arena *arena, u32 max)
{
    this->data = AllocArray(arena, T, max);
    this->count = 0;
    this->top = nullptr;
    this->maxCount = max;
}

template <typename T>
void limit_stack<T>::pop()
{
    ASSERT(this->count > 0);
    --this->count;
    if(this->count > 0)
    {
        this->top = this->data + (this->count-1);
    }
    else
    {
        this->top = nullptr;
    }
}

template <typename T>
void limit_stack<T>::push(T header)
{
    ASSERT(this->data != nullptr);
    ASSERT(this->count < this->maxCount);
    this->data[this->count] = header;
    this->top = this->data + this->count;
    this->count++;
}


template <typename T>
limit_stack<T*>::limit_stack(memory_arena *arena, u32 max)
{
    this->data = AllocPointerArray(arena, T, max);
    this->count = 0;
    this->top = nullptr;
    this->maxCount = max;
}

template <typename T>
void limit_stack<T*>::pop()
{
    ASSERT(this->count > 0);
    --this->count;
    if(this->count > 0)
    {
        this->top = this->data[this->count-1];
    }
    else
    {
        this->top = nullptr;
    }
}

template <typename T>
void limit_stack<T*>::push(T *header)
{
    ASSERT(this->data != nullptr);
    ASSERT(this->count < this->maxCount);
    this->data[this->count] = header;
    this->top = this->data[this->count++];
}

