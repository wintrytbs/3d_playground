#include "tbs_image.h"
#include "tbs_opengl_resource_manager.h"


namespace tbs_opengl
{
    global const pbr_active_sampler_flag samplerIndexToActiveSamplerFlag[] =
    {
        PBR_ACTIVE_SAMPLER_FLAG_BaseColor,
        PBR_ACTIVE_SAMPLER_FLAG_Normal,
        PBR_ACTIVE_SAMPLER_FLAG_MetallicRoughness,
        PBR_ACTIVE_SAMPLER_FLAG_Emissive,
        PBR_ACTIVE_SAMPLER_FLAG_Occlusion,
    };

    global const pbr_viewer_active_sampler_flag pbrTextureToPbrViewerActiveSamplerFlag[] =
    {
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_BaseColor,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_Normal,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_MetallicRoughness,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_Emissive,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_Occlusion,
    };

    internal char *GetShaderFullPath(application_state *app, memory_arena *arena, const char *name)
    {
        char *result = RawStringConcat(arena, app->shaderPath, name);
        return result;
    }

    void opengl_resource_manager::Init(application_state *app)
    {
        temp_memory scratch(app);
        // TODO: Create default/invalid shader.
        // TODO: Need to make sure that all shaders that aren't initialized and
        // don't have valid data are either set to some default shader or have zero values.
        this->shaders[TBS_SHADER_TYPE_None]               = {};
        this->shaders[TBS_SHADER_TYPE_PrimitivePBR]       = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "main_pbr.glsl"));
        this->shaders[TBS_SHADER_TYPE_Line3D]             = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "line_3d.glsl"));
        this->shaders[TBS_SHADER_TYPE_Rect]               = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "simple_rect.glsl"));
        this->shaders[TBS_SHADER_TYPE_FullscreenRect]     = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "final_pass.glsl"));
        this->shaders[TBS_SHADER_TYPE_ShadowMap]          = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "shadow_map_pass.glsl"));
        this->shaders[TBS_SHADER_TYPE_Skybox]             = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "skybox_pass.glsl"));
        this->shaders[TBS_SHADER_TYPE_Light]              = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "light_visualizer.glsl"));
        this->shaders[TBS_SHADER_TYPE_ShadowMapViewer]    = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "shadow_map_viewer.glsl"));
        this->shaders[TBS_SHADER_TYPE_CubeMapViewer]      = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "cube_map_viewer.glsl"));
        this->shaders[TBS_SHADER_TYPE_DebugGui]           = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "tbs_imgui.glsl"));
        this->shaders[TBS_SHADER_TYPE_EditorGrid]         = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "editor_grid.glsl"));
        this->shaders[TBS_SHADER_TYPE_HdrToCubeMap]       = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "equi_to_cube.glsl"));
        this->shaders[TBS_SHADER_TYPE_DiffPrefilter]      = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "diff_prefilter_env.glsl"));
        this->shaders[TBS_SHADER_TYPE_SpecPrefilter]      = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "spec_prefilter_env.glsl"));
        this->shaders[TBS_SHADER_TYPE_BrdfLut]            = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "brdf_convolution.glsl"));
        this->shaders[TBS_SHADER_TYPE_Camera]             = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "camera_visualizer.glsl"));
        this->shaders[TBS_SHADER_TYPE_Quad3D]             = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "simple_quad_3d.glsl"));
        this->shaders[TBS_SHADER_TYPE_PBRViewer]          = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "pbr_viewer.glsl"));
        this->shaders[TBS_SHADER_TYPE_TonemappingGamma]   = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "tonemapping_gamma.glsl"));
        this->shaders[TBS_SHADER_TYPE_CascadeVisualize]   = OpenGLCreateProgramFromSingleFile(app, GetShaderFullPath(app, scratch.arena, "cascade_visualize.glsl"));


        RelocateAllSamplers();

        // NOTE(torgrim): We don't really care about the size here since we will resize the window
        // when it's ready and resize the framebuffer to the correct size then.
        this->framebuffers[FRAMEBUFFER_TYPE_PBRSceneOffscreen]          = OpenGLCreateFramebufferDefault(1920, 1080, GL_RGBA16F);
        this->framebuffers[FRAMEBUFFER_TYPE_PBRSceneOffscreenMsaa]      = OpenGLCreateFramebufferMsaa(1920, 1080, GL_RGBA16F);
        this->framebuffers[FRAMEBUFFER_TYPE_PBRViewerOffscreenMsaa]     = OpenGLCreateFramebufferMsaa(1920, 1080, GL_RGBA16F);
        this->framebuffers[FRAMEBUFFER_TYPE_Swap]                       = OpenGLCreateFramebufferDefault(1920, 1080, GL_RGBA16F);
        this->framebuffers[FRAMEBUFFER_TYPE_PBRViewerTonemappingGamma]  = OpenGLCreateFramebufferDefault(1920, 1080, GL_RGBA8);

        this->framebuffers[FRAMEBUFFER_TYPE_PBRSceneCamera]             = OpenGLCreateFramebufferDefault(1920, 1080, GL_RGBA8);

        this->SetupBasePrimitives(app);

        char defaultHDRPath[MAX_PATH];
        RawStringConcat(defaultHDRPath, app->hdrPath, "Barce_Rooftop_C_3k.hdr");
        image_data_hdr hdrImage = LoadImageDataHDR(defaultHDRPath, true, true);
        this->AddEnvMapHDR(app, &hdrImage);
        FreeImageData(&hdrImage);

    }

    void opengl_resource_manager::RelocateAllSamplers()
    {
        {
            GLuint progID = this->GetProgramID(TBS_SHADER_TYPE_PrimitivePBR);
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_Normal]                      = glGetUniformLocation(progID, "normal_map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_BaseColor]                   = glGetUniformLocation(progID, "base_color_map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_MetallicRoughness]           = glGetUniformLocation(progID, "metallic_roughness_map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_Occlusion]                   = glGetUniformLocation(progID, "occlusion_map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_Emissive]                    = glGetUniformLocation(progID, "emissive_map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_DiffPrefilterMap]            = glGetUniformLocation(progID, "irradiance_map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_SpecPrefilterMap]            = glGetUniformLocation(progID, "prefilter_env");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_BrdfLUT]                     = glGetUniformLocation(progID, "brdf_lut");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_SpotShadowMap]               = glGetUniformLocation(progID, "spot_shadow_map_array");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_DirShadowMap]                = glGetUniformLocation(progID, "dir_shadow_map_array");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap0]             = glGetUniformLocation(progID, "point_shadow_list[0].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap1]             = glGetUniformLocation(progID, "point_shadow_list[1].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap2]             = glGetUniformLocation(progID, "point_shadow_list[2].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap3]             = glGetUniformLocation(progID, "point_shadow_list[3].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap4]             = glGetUniformLocation(progID, "point_shadow_list[4].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap5]             = glGetUniformLocation(progID, "point_shadow_list[5].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap6]             = glGetUniformLocation(progID, "point_shadow_list[6].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap7]             = glGetUniformLocation(progID, "point_shadow_list[7].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap8]             = glGetUniformLocation(progID, "point_shadow_list[8].map");
            this->pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_PointShadowMap9]             = glGetUniformLocation(progID, "point_shadow_list[9].map");


            progID = this->GetProgramID(TBS_SHADER_TYPE_PBRViewer);
            this->pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_BaseColor]             = glGetUniformLocation(progID, "base_color_map");
            this->pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_NormalMap]             = glGetUniformLocation(progID, "normal_map");
            this->pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_MetallicRoughnessMap]  = glGetUniformLocation(progID, "metallic_roughness_map");
            this->pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_EmissiveMap]           = glGetUniformLocation(progID, "emissive_map");
            this->pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_OcclusionMap]          = glGetUniformLocation(progID, "occlusion_map");
            this->pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_IrradianceMap]         = glGetUniformLocation(progID, "irradiance_map");
            this->pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_PrefilterMap]          = glGetUniformLocation(progID, "prefilter_env");
            this->pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_BrdfLut]               = glGetUniformLocation(progID, "brdf_lut");
        }
    }

    bool opengl_resource_manager::CheckAndUpdateModifiedShaders(application_state *app)
    {
        bool anyUpdate = false;
        for(i32 shaderIndex = 1; shaderIndex < TBS_SHADER_TYPE_Count; ++shaderIndex)
        {
            opengl_shader_info *si = this->shaders + shaderIndex;
            // TODO(torgrim): In the cases where the file is locked(most likely due to editor locking
            // the file while writing) do we just want to sleep for a bit and try again?
            FILETIME lastWriteTime = si->lastWriteTime;
            FILETIME currentWriteTime = Win32GetLastWriteTime(si->filename);
            if(CompareFileTime(&currentWriteTime, &lastWriteTime) == 1)
            {
                OpenGLReloadShader(app, si);
                si->lastWriteTime = currentWriteTime;
                anyUpdate = true;
            }
        }

        if(anyUpdate)
        {
            this->RelocateAllSamplers();
        }

        return anyUpdate;
    }

    GLuint opengl_resource_manager::GetProgramID(tbs_shader_type type)
    {
        opengl_shader_info *shader = this->shaders + type;
        ASSERT(glIsProgram(shader->programID));

        return shader->programID;
    }

    GLuint opengl_resource_manager::GetFramebuffer(framebuffer_type framebufferType)
    {
        GLuint result = this->framebuffers[framebufferType];
        ASSERT(glIsFramebuffer(result));
        return result;
    }

    renderable_primitive *opengl_resource_manager::GetLightPrimitive()
    {
        ASSERT(this->primitiveCount > 0);
        renderable_primitive *result = this->primitives + BASE_PRIMITIVE_TYPE_Cube;
        return result;
    }

    opengl_resource_handle opengl_resource_manager::CreateUnloadedTexture(char *imagePath, sampler_config config, GLenum textureTarget, texture_internal_format internalFormat)
    {
        ASSERT(this->textureCount < local_constants::MAX_TEXTURE_COUNT);
        texture_info ti = {};
        ti.path = imagePath;
        ti.target = textureTarget;
        ti.samplerConfig = config;
        ti.internalFormat = internalFormat;

        auto handle = this->textureCount;
        this->textures[this->textureCount++] = ti;
        return handle;
    }

    texture_info *opengl_resource_manager::GetTexture(opengl_resource_handle handle)
    {
        texture_info *result = nullptr;
        if(handle != invalidHandle)
        {
            ASSERT(handle < this->textureCount);
            result = this->textures + handle;
        }

        return result;
    }

    void opengl_resource_manager::LoadTexture(opengl_resource_handle handle, image_data *image)
    {
        texture_info *ti = this->GetTexture(handle);

        ASSERT(ti->loaded == false);
        ASSERT(glIsTexture(ti->texID) == GL_FALSE);

        // TODO(torgrim): Support multiple target types.
        ASSERT(ti->target == GL_TEXTURE_2D);

        glGenTextures(1, &ti->texID);
        glBindTexture(ti->target, ti->texID);

        GLenum pixelFormat = OpenGLGetTextureFormatFromChannelCount(image->channelCount);

        // TODO(torgrim): Always use GL_LINEAR_MIPMAP_LINEAR when generating mipmaps but for
        // some textures we probably don't want to create a mip chain so should give the caller a choice.
        // So we currently don't use the filtering specified in the sampler config(which
        // most of the time comes from a gltf file).
        //glTexParameteri(ti->target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        //glTexParameteri(ti->target, GL_TEXTURE_MIN_FILTER, ti->samplerConfig.minFilter);
        //glTexParameteri(ti->target, GL_TEXTURE_MAG_FILTER, ti->samplerConfig.magFilter);
        glTexParameteri(ti->target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(ti->target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(ti->target, GL_TEXTURE_WRAP_S, ti->samplerConfig.wrapS);
        glTexParameteri(ti->target, GL_TEXTURE_WRAP_T, ti->samplerConfig.wrapT);

        glTexImage2D(ti->target, 0, ti->internalFormat, image->width, image->height, 0, pixelFormat, GL_UNSIGNED_BYTE, image->pixels);

        glGenerateMipmap(ti->target);
        glBindTexture(ti->target, 0);

        ti->loaded = true;
    }

    opengl_resource_handle opengl_resource_manager::CreateRenderablePrimitive(primitive_buffer_collection buffers, GLenum mode)
    {
        ASSERT(this->primitiveCount < local_constants::MAX_PRIMITIVE_COUNT);
        renderable_primitive prim = Internal_CreateRenderablePrimitive(buffers, mode);
        auto handle = this->primitiveCount;
        this->primitives[this->primitiveCount++] = prim;
        return handle;
    }

    renderable_primitive *opengl_resource_manager::GetPrimitive(opengl_resource_handle handle)
    {
        ASSERT(handle != invalidHandle);
        ASSERT(handle < this->primitiveCount);
        auto result = this->primitives + handle;
        return result;
    }

    renderable_primitive *opengl_resource_manager::GetBasePrimitive(base_primitive_type type)
    {
        ASSERT(this->primitiveCount >= BASE_PRIMITIVE_TYPE_Count);
        renderable_primitive *prim = this->primitives + type;
        return prim;
    }

    void opengl_resource_manager::SetupBasePrimitives(application_state *app)
    {
        ASSERT(this->primitiveCount == 0);
        temp_memory scratch(app);
        primitive_buffer_collection cubeBuffers = Internal_CreateBasePrimitiveBuffers(scratch.arena, BASE_PRIMITIVE_TYPE_Cube);
        primitive_buffer_collection planeBuffers = Internal_CreateBasePrimitiveBuffers(scratch.arena, BASE_PRIMITIVE_TYPE_Plane);
        renderable_primitive cubePrim = Internal_CreateRenderablePrimitive(cubeBuffers, GL_TRIANGLES);

        renderable_primitive planePrim = Internal_CreateRenderablePrimitive(planeBuffers, GL_TRIANGLES);

        this->primitives[BASE_PRIMITIVE_TYPE_Cube] = cubePrim;
        this->primitives[BASE_PRIMITIVE_TYPE_Plane] = planePrim;

        this->primitiveCount += BASE_PRIMITIVE_TYPE_Count;

        ASSERT(this->primitiveCount == BASE_PRIMITIVE_TYPE_Count);
    }

    opengl_resource_handle opengl_resource_manager::AddEnvMapHDR(application_state *app, image_data_hdr *img)
    {
        auto result = invalidHandle;
        if(this->environmentMapCount < local_constants::MAX_ENVIRONMENT_MAP_COUNT)
        {
            result = this->environmentMapCount;
            this->environmentMaps[this->environmentMapCount++] = Internal_CreateEnvMapFromHDR(app, this, img);
        }

        return result;
    }

    tbs_pbr::env_map_info *opengl_resource_manager::GetEnvMap(opengl_resource_handle handle)
    {
        ASSERT(handle != invalidHandle);
        ASSERT(handle < this->environmentMapCount);
        tbs_pbr::env_map_info *result = this->environmentMaps + handle;
        return result;
    }

    internal renderable_primitive Internal_CreateRenderablePrimitive(primitive_buffer_collection buffers, GLenum mode)
    {
        renderable_primitive prim = {};
        prim.mode = mode;

        GLuint VAOID = 0;
        GLuint posVBOID = 0;
        GLuint norVBOID = 0;
        GLuint tanVBOID = 0;
        GLuint uv0VBOID = 0;
        GLuint indEBOID = 0;

        glGenVertexArrays(1, &VAOID);
        glGenBuffers(1, &posVBOID);
        glGenBuffers(1, &norVBOID);
        glGenBuffers(1, &tanVBOID);
        glGenBuffers(1, &uv0VBOID);

        data_buffer posBuffer = buffers.posBuffer;
        data_buffer uv0Buffer = buffers.uv0Buffer;
        // TODO: Handle the cases where there is two uv buffers
        //data_buffer uv1Buffer = buffers.uv1Buffer;
        data_buffer norBuffer = buffers.norBuffer;
        data_buffer tanBuffer = buffers.tanBuffer;
        data_buffer indBuffer = buffers.indBuffer;

        glBindVertexArray(VAOID);

        glBindBuffer(GL_ARRAY_BUFFER, posVBOID);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)posBuffer.size, posBuffer.buff, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(0);

        if(uv0Buffer.size > 0)
        {
            glBindBuffer(GL_ARRAY_BUFFER, uv0VBOID);
            glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)uv0Buffer.size, uv0Buffer.buff, GL_STATIC_DRAW);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(1);

            ASSERT(tanBuffer.size > 0);
            glBindBuffer(GL_ARRAY_BUFFER, tanVBOID);
            glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)tanBuffer.size, tanBuffer.buff, GL_STATIC_DRAW);
            glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(3);

        }
        else
        {
            glDisableVertexAttribArray(1);
        }

        ASSERT(norBuffer.size > 0);
        glBindBuffer(GL_ARRAY_BUFFER, norVBOID);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)norBuffer.size, norBuffer.buff, GL_STATIC_DRAW);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(2);

        if(indBuffer.size > 0)
        {
            glGenBuffers(1, &indEBOID);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indEBOID);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)indBuffer.size, indBuffer.buff, GL_STATIC_DRAW);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            prim.target = GL_ELEMENT_ARRAY_BUFFER;
            prim.indexDataType = indBuffer.dataType;
            if(indBuffer.dataType == GL_UNSIGNED_BYTE || indBuffer.dataType == GL_BYTE)
            {
                prim.indexCount = (u32)indBuffer.size / sizeof(u8);
            }
            else if(indBuffer.dataType == GL_UNSIGNED_SHORT || indBuffer.dataType == GL_SHORT)
            {
                prim.indexCount = (u32)indBuffer.size / sizeof(u16);
            }
            else
            {
                prim.indexCount = (u32)indBuffer.size / sizeof(u32);
            }
        }
        else
        {
            prim.target = GL_ARRAY_BUFFER;
        }

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        prim.VAOID = VAOID;
        prim.posVBOID = posVBOID;
        prim.uv0VBOID = uv0VBOID;
        prim.norVBOID = norVBOID;
        prim.tanVBOID = tanVBOID;
        prim.indEBOID = indEBOID;

        prim.vertexCount = (u32)posBuffer.size / sizeof(v3);

        return prim;
    }

    internal primitive_buffer_collection Internal_CreateBasePrimitiveBuffers(memory_arena *arena, base_primitive_type type)
    {
        primitive_buffer_collection result = {};

        if(type == BASE_PRIMITIVE_TYPE_Cube)
        {
            v3 vertices[] =
            {
                // back
                v3( 1.0f, -1.0f, -1.0f),
                v3(-1.0f, -1.0f, -1.0f),
                v3(-1.0f,  1.0f, -1.0f),
                v3(-1.0f,  1.0f, -1.0f),
                v3( 1.0f,  1.0f, -1.0f),
                v3( 1.0f, -1.0f, -1.0f),

                // front
                v3(-1.0f, -1.0f,  1.0f),
                v3( 1.0f, -1.0f,  1.0f),
                v3( 1.0f,  1.0f,  1.0f),
                v3( 1.0f,  1.0f,  1.0f),
                v3(-1.0f,  1.0f,  1.0f),
                v3(-1.0f, -1.0f,  1.0f),

                // left
                v3(-1.0f,  1.0f,  1.0f),
                v3(-1.0f,  1.0f, -1.0f),
                v3(-1.0f, -1.0f, -1.0f),
                v3(-1.0f, -1.0f, -1.0f),
                v3(-1.0f, -1.0f,  1.0f),
                v3(-1.0f,  1.0f,  1.0f),

                // right
                v3( 1.0f, -1.0f,  1.0f),
                v3( 1.0f, -1.0f, -1.0f),
                v3( 1.0f,  1.0f, -1.0f),
                v3( 1.0f,  1.0f, -1.0f),
                v3( 1.0f,  1.0f,  1.0f),
                v3( 1.0f, -1.0f,  1.0f),

                // bottom
                v3(-1.0f, -1.0f, -1.0f),
                v3( 1.0f, -1.0f, -1.0f),
                v3( 1.0f, -1.0f,  1.0f),
                v3( 1.0f, -1.0f,  1.0f),
                v3(-1.0f, -1.0f,  1.0f),
                v3(-1.0f, -1.0f, -1.0f),

                // top
                v3(-1.0f,  1.0f,  1.0f),
                v3( 1.0f,  1.0f,  1.0f),
                v3( 1.0f,  1.0f, -1.0f),
                v3( 1.0f,  1.0f, -1.0f),
                v3(-1.0f,  1.0f, -1.0f),
                v3(-1.0f,  1.0f,  1.0f),
            };

            v2 texCoords[] =
            {
                v2(0.0f, 0.0f),
                v2(1.0f, 0.0f),
                v2(1.0f, 1.0f),
                v2(1.0f, 1.0f),
                v2(0.0f, 1.0f),
                v2(0.0f, 0.0f),


                v2(0.0f, 0.0f),
                v2(1.0f, 0.0f),
                v2(1.0f, 1.0f),
                v2(1.0f, 1.0f),
                v2(0.0f, 1.0f),
                v2(0.0f, 0.0f),

                v2(1.0f, 0.0f),
                v2(1.0f, 1.0f),
                v2(0.0f, 1.0f),
                v2(0.0f, 1.0f),
                v2(0.0f, 0.0f),
                v2(1.0f, 0.0f),

                v2(1.0f, 0.0f),
                v2(1.0f, 1.0f),
                v2(0.0f, 1.0f),
                v2(0.0f, 1.0f),
                v2(0.0f, 0.0f),
                v2(1.0f, 0.0f),

                v2(0.0f, 1.0f),
                v2(1.0f, 1.0f),
                v2(1.0f, 0.0f),
                v2(1.0f, 0.0f),
                v2(0.0f, 0.0f),
                v2(0.0f, 1.0f),

                v2(0.0f, 1.0f),
                v2(1.0f, 1.0f),
                v2(1.0f, 0.0f),
                v2(1.0f, 0.0f),
                v2(0.0f, 0.0f),
                v2(0.0f, 1.0f),
            };

            triangle_list triangleList = CreateSimpleTriangleList(arena, ARRAY_COUNT(vertices));

            v3 *normals = (v3 *)AllocSize(arena, sizeof(vertices));
            CalcFlatNormals(vertices, &triangleList, normals);


            v4 *tangents = AllocArray(arena, v4, ARRAY_COUNT(vertices));
            CalcMeshTangents(arena,
                             vertices, ARRAY_COUNT(vertices),
                             triangleList.triangles, (size_t)triangleList.count,
                             texCoords, ARRAY_COUNT(texCoords),
                             normals,
                             tangents);

            data_buffer posBuffer = {};
            data_buffer uv0Buffer = {};
            data_buffer norBuffer = {};
            data_buffer tanBuffer = {};

            posBuffer.size = sizeof(vertices);
            posBuffer.buff = AllocSize(arena, posBuffer.size);
            memcpy(posBuffer.buff, vertices, posBuffer.size);

            uv0Buffer.size = sizeof(texCoords);
            uv0Buffer.buff = AllocSize(arena, uv0Buffer.size);
            memcpy(uv0Buffer.buff, texCoords, uv0Buffer.size);

            norBuffer.size = sizeof(vertices);
            norBuffer.buff = normals;

            tanBuffer.size = sizeof(v4) * ARRAY_COUNT(vertices);
            tanBuffer.buff = tangents;

            result.posBuffer = posBuffer;
            result.uv0Buffer = uv0Buffer;
            result.norBuffer = norBuffer;
            result.tanBuffer = tanBuffer;
        }
        else if(type == BASE_PRIMITIVE_TYPE_Plane)
        {
            v3 vertices[] =
            {
                v3(-1.0f,  0.0f,  1.0f),
                v3( 1.0f,  0.0f,  1.0f),
                v3( 1.0f,  0.0f, -1.0f),

                v3( 1.0f,  0.0f, -1.0f),
                v3(-1.0f,  0.0f, -1.0f),
                v3(-1.0f,  0.0f,  1.0f),
            };

            v2 texCoords[] =
            {
                v2(0.0f, 0.0f),
                v2(1.0f, 0.0f),
                v2(1.0f, 1.0f),

                v2(1.0f, 1.0f),
                v2(0.0f, 1.0f),
                v2(0.0f, 0.0f),
            };

            v3 normals[] =
            {
                v3(0.0f, 1.0f, 0.0f),
                v3(0.0f, 1.0f, 0.0f),
                v3(0.0f, 1.0f, 0.0f),
                v3(0.0f, 1.0f, 0.0f),
                v3(0.0f, 1.0f, 0.0f),
                v3(0.0f, 1.0f, 0.0f),
            };

            triangle_list triangleList = CreateSimpleTriangleList(arena, ARRAY_COUNT(vertices));
            v4 *tangents = AllocArray(arena, v4, ARRAY_COUNT(vertices));
            CalcMeshTangents(arena,
                             vertices, ARRAY_COUNT(vertices),
                             triangleList.triangles, (size_t)triangleList.count,
                             texCoords, ARRAY_COUNT(texCoords),
                             normals,
                             tangents);

            data_buffer posBuffer = {};
            data_buffer uv0Buffer = {};
            data_buffer norBuffer = {};
            data_buffer tanBuffer = {};

            posBuffer.size = sizeof(vertices);
            posBuffer.buff = AllocSize(arena, posBuffer.size);
            memcpy(posBuffer.buff, vertices, posBuffer.size);

            uv0Buffer.size = sizeof(texCoords);
            uv0Buffer.buff = AllocSize(arena, uv0Buffer.size);
            memcpy(uv0Buffer.buff, texCoords, uv0Buffer.size);

            norBuffer.size = sizeof(vertices);
            norBuffer.buff = AllocSize(arena, norBuffer.size);
            memcpy(norBuffer.buff, normals, norBuffer.size);

            tanBuffer.size = sizeof(v4) * ARRAY_COUNT(vertices);
            tanBuffer.buff = tangents;

            result.posBuffer = posBuffer;
            result.uv0Buffer = uv0Buffer;
            result.norBuffer = norBuffer;
            result.tanBuffer = tanBuffer;
        }

        return result;
    }

    internal renderable_primitive Internal_CreateBasePrimitiveRenderData(application_state *app, base_primitive_type type)
    {
        renderable_primitive result = {};
        temp_memory scratch(app);
        if(type == BASE_PRIMITIVE_TYPE_Cube)
        {
            primitive_buffer_collection cubeBuffers = Internal_CreateBasePrimitiveBuffers(scratch.arena, BASE_PRIMITIVE_TYPE_Cube);
            result = Internal_CreateRenderablePrimitive(cubeBuffers, GL_TRIANGLES);
        }
        else if(type == BASE_PRIMITIVE_TYPE_Plane)
        {
            primitive_buffer_collection planeBuffers = Internal_CreateBasePrimitiveBuffers(scratch.arena, BASE_PRIMITIVE_TYPE_Plane);
            result = Internal_CreateRenderablePrimitive(planeBuffers, GL_TRIANGLES);
        }

        return result;
    }

    internal tbs_pbr::env_map_info Internal_CreateEnvMapFromHDR(application_state *app, opengl_resource_manager *rm, image_data_hdr *hdr)
    {
        // TODO(torgrim): BRDF lut doesn't really need to be generated each time but only
        // once since it's not dependent on anything else than our brdf functions.

        GLsizei envMapResolution = 512;
        GLsizei diffPrefilterResolution = 64;
        GLsizei specPrefilterResolution = 512;
        GLsizei brdfLutResoulution = 512;

        // NOTE: Set up textures we need.
        GLuint hdrTexID;
        glGenTextures(1, &hdrTexID);
        glBindTexture(GL_TEXTURE_2D, hdrTexID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, hdr->width, hdr->height, 0, GL_RGB, GL_FLOAT, hdr->pixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);

        GLuint envTexID;
        glGenTextures(1, &envTexID);
        glBindTexture(GL_TEXTURE_CUBE_MAP, envTexID);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        for(i32 faceIndex = GL_TEXTURE_CUBE_MAP_POSITIVE_X; faceIndex <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++faceIndex)
        {
            glTexImage2D((GLenum)faceIndex, 0, GL_RGB16F, envMapResolution, envMapResolution, 0, GL_RGB, GL_FLOAT, nullptr);
        }

        GLuint diffPrefilterTexID;
        glGenTextures(1, &diffPrefilterTexID);

        glBindTexture(GL_TEXTURE_CUBE_MAP, diffPrefilterTexID);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        for(i32 faceIndex = GL_TEXTURE_CUBE_MAP_POSITIVE_X; faceIndex <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++faceIndex)
        {
            glTexImage2D((GLenum)faceIndex, 0, GL_RGB16F, diffPrefilterResolution, diffPrefilterResolution, 0, GL_RGB, GL_FLOAT, nullptr);
        }

        GLuint specPrefilterTexID;
        glGenTextures(1, &specPrefilterTexID);
        glBindTexture(GL_TEXTURE_CUBE_MAP, specPrefilterTexID);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        for(i32 faceIndex = GL_TEXTURE_CUBE_MAP_POSITIVE_X; faceIndex <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++faceIndex)
        {
            glTexImage2D((GLenum)faceIndex, 0, GL_RGB16F, specPrefilterResolution, specPrefilterResolution, 0, GL_RGB, GL_FLOAT, nullptr);
        }

        glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

        GLuint brdfLUT;
        glGenTextures(1, &brdfLUT);
        glBindTexture(GL_TEXTURE_2D, brdfLUT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, brdfLutResoulution, brdfLutResoulution, 0, GL_RG, GL_FLOAT, nullptr);

        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // NOTE: Render to all the textures
        GLuint NDCQuad = OpenGLCreateNDCQuad();
        renderable_primitive hdrCube = Internal_CreateBasePrimitiveRenderData(app, BASE_PRIMITIVE_TYPE_Cube);

        GLuint hdrToCubeProgID      = rm->GetProgramID(TBS_SHADER_TYPE_HdrToCubeMap);
        GLuint diffPrefilterProgID  = rm->GetProgramID(TBS_SHADER_TYPE_DiffPrefilter);
        GLuint specPrefilterProgID  = rm->GetProgramID(TBS_SHADER_TYPE_SpecPrefilter);
        GLuint brdfLutShaderProgID  = rm->GetProgramID(TBS_SHADER_TYPE_BrdfLut);

        ASSERT(glIsProgram(hdrToCubeProgID));
        ASSERT(glIsProgram(diffPrefilterProgID));
        ASSERT(glIsProgram(specPrefilterProgID));
        ASSERT(glIsProgram(brdfLutShaderProgID));

        OpenGLSetActiveTextureUnit(0);

        GLuint envFBO = OpenGLCreateFramebufferHDR();
        glBindFramebuffer(GL_FRAMEBUFFER, envFBO);

        GLuint envRBO;
        glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, (GLint *)&envRBO);

        glBindRenderbuffer(GL_RENDERBUFFER, (GLuint)envRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, envMapResolution, envMapResolution);

        OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
        matrix4D proj = CreateRevFrustum(DegreeToRadians(90.0f), 1.0f, 0.1f, 10.0f);
        glViewport(0, 0, envMapResolution, envMapResolution);

        glUseProgram(hdrToCubeProgID);
        glBindTexture(GL_TEXTURE_2D, hdrTexID);
        OpenGLSetUniform(hdrToCubeProgID, "hdr_tex", 0);
        OpenGLSetUniform(hdrToCubeProgID, "proj", &proj);

        for(i32 cubeFace = GL_TEXTURE_CUBE_MAP_POSITIVE_X; cubeFace <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++cubeFace)
        {
            matrix4D capView = CreatePointLightCameraMatrix((GLenum)cubeFace, v3(0.0f, 0.0f, 0.0f));
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, (GLenum)cubeFace, envTexID, 0);
            OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            OpenGLSetUniform(hdrToCubeProgID, "view", &capView);
            glBindVertexArray(hdrCube.VAOID);
            glDrawArrays(GL_TRIANGLES, 0, (GLsizei)hdrCube.vertexCount);
        }

        glBindTexture(GL_TEXTURE_CUBE_MAP, envTexID);
        glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, diffPrefilterResolution, diffPrefilterResolution);
        proj = CreateRevFrustum(DegreeToRadians(90.0f), 1.0f, 0.1f, 20.0f);
        glViewport(0, 0, diffPrefilterResolution, diffPrefilterResolution);

        OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

        glUseProgram(diffPrefilterProgID);
        glBindTexture(GL_TEXTURE_CUBE_MAP, envTexID);
        OpenGLSetUniform(diffPrefilterProgID, "proj", &proj);
        OpenGLSetUniform(diffPrefilterProgID, "env_map", 0);

        for(i32 cubeFace = GL_TEXTURE_CUBE_MAP_POSITIVE_X; cubeFace <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++cubeFace)
        {
            matrix4D capView = CreatePointLightCameraMatrix((GLenum)cubeFace, v3(0.0f, 0.0f, 0.0f));
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, (GLenum)cubeFace, diffPrefilterTexID, 0);
            OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            OpenGLSetUniform(diffPrefilterProgID, "view", &capView);
            glBindVertexArray(hdrCube.VAOID);
            glDrawArrays(GL_TRIANGLES, 0, (GLsizei)hdrCube.vertexCount);

        }

        glUseProgram(specPrefilterProgID);
        proj = CreateRevFrustum(DegreeToRadians(90.0f), 1.0f, 0.1f, 10.0f);

        glBindTexture(GL_TEXTURE_CUBE_MAP, envTexID);
        OpenGLSetUniform(specPrefilterProgID, "proj", &proj);
        OpenGLSetUniform(specPrefilterProgID, "env_map", 0);

        GLsizei mipSize = specPrefilterResolution;
        for(i32 level = 0; level < 5; ++level)
        {
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mipSize, mipSize);
            glViewport(0, 0, mipSize, mipSize);

            OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
            f32 roughnessFactor = (f32)level / 4.0f;
            OpenGLSetUniform(specPrefilterProgID, "roughness", roughnessFactor);
            for(i32 cubeFace = GL_TEXTURE_CUBE_MAP_POSITIVE_X; cubeFace <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++cubeFace)
            {
                matrix4D capView = CreatePointLightCameraMatrix((GLenum)cubeFace, v3(0.0f, 0.0f, 0.0f));
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, (GLenum)cubeFace, specPrefilterTexID, level);
                OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                OpenGLSetUniform(specPrefilterProgID, "view", &capView);
                glBindVertexArray(hdrCube.VAOID);
                glDrawArrays(GL_TRIANGLES, 0, (GLsizei)hdrCube.vertexCount);

            }

            mipSize /= 2;
        }

        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, brdfLutResoulution, brdfLutResoulution);
        glViewport(0, 0, brdfLutResoulution, brdfLutResoulution);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, brdfLUT, 0);
        glUseProgram(brdfLutShaderProgID);
        glClear(GL_COLOR_BUFFER_BIT);
        glDisable(GL_DEPTH_TEST);
        glBindVertexArray(NDCQuad);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glEnable(GL_DEPTH_TEST);

        DEBUG_OpenGLCheckError();

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDeleteRenderbuffers(1, &envRBO);
        glDeleteFramebuffers(1, &envFBO);

        tbs_pbr::env_map_info result = {};
        result.hdr = hdrTexID;
        result.envMap = envTexID;
        result.diffPrefilterMap = diffPrefilterTexID;
        result.specPrefilterMap = specPrefilterTexID;
        result.brdfLUT = brdfLUT;
        result.filename = RawStringCreate(hdr->filename);

        return result;
    }
}
