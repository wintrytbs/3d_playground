#pragma warning(disable: 4710 4711)
#pragma warning(push, 0)
#include <cstdio>
#include <memory.h>
#include <cstdlib>
#pragma warning(pop)


struct tbs_gl_func
{
    const char *funcName;
    const char *funcMacro;
};

static char *Upp(const char *s)
{
    size_t i = 0;
    char c = s[i];

    while(s[i] != '\0')
    {
        ++i;
    }

    size_t l = i+1u;
    char *result = (char *)malloc(l);
    memcpy(result, s, l);
    i = 0;

    while(c != '\0')
    {
        if(c >= 'a' && c <= 'z')
        {
            result[i] = 'A' + (c - 'a');
        }
        else
        {
            result[i] = c;
        }

        ++i;
        c = s[i];
    }

    return result;
}

#define gs(name) {#name, "PFN" #name "PROC"}
#define array_count(arr) (sizeof(arr) / sizeof((arr)[0]))

static tbs_gl_func funcList[]
{
    gs(wglChoosePixelFormatARB),
    gs(wglCreateContextAttribsARB),
    gs(wglSwapIntervalEXT),
    gs(glCreateShader),
    gs(glShaderSource),
    gs(glCompileShader),
    gs(glGetShaderiv),
    gs(glGetShaderInfoLog),
    gs(glCreateProgram),
    gs(glAttachShader),
    gs(glDetachShader),
    gs(glLinkProgram),
    gs(glGetProgramiv),
    gs(glGetProgramInfoLog),
    gs(glDeleteShader),
    gs(glGenBuffers),
    gs(glBindBuffer),
    gs(glBufferData),
    gs(glVertexAttribPointer),
    gs(glEnableVertexAttribArray),
    gs(glDisableVertexAttribArray),
    gs(glUseProgram),
    gs(glBindVertexArray),
    gs(glGenVertexArrays),
    gs(glGetUniformLocation),
    gs(glUniformMatrix4fv),
    gs(glUniform1i),
    gs(glUniform1ui),
    gs(glUniform1f),
    gs(glUniform2f),
    gs(glUniform2fv),
    gs(glUniform3fv),
    gs(glUniform3f),
    gs(glUniform4f),
    gs(glUniform4fv),
    gs(glBlendFuncSeparate),
    gs(glBlendEquation),
    gs(glBufferSubData),
    gs(glActiveTexture),
    gs(glDeleteVertexArrays),
    gs(glBlendEquationSeparate),
    gs(glGenFramebuffers),
    gs(glBindFramebuffer),
    gs(glFramebufferTexture),
    gs(glCheckFramebufferStatus),
    gs(glFramebufferTexture2D),
    gs(glGenRenderbuffers),
    gs(glBindRenderbuffer),
    gs(glRenderbufferStorage),
    gs(glFramebufferRenderbuffer),
    gs(glDeleteBuffers),
    gs(glFramebufferTextureLayer),
    gs(glGenerateMipmap),
    gs(glTexImage3D),
    gs(glTexImage2DMultisample),
    gs(glBlitFramebuffer),
    gs(glIsFramebuffer),
    gs(glRenderbufferStorageMultisample),
    gs(glGetRenderbufferParameteriv),
    gs(glGetFramebufferAttachmentParameteriv),
    gs(glDeleteFramebuffers),
    gs(glDeleteRenderbuffers),
    gs(glIsProgram),
    gs(glGetAttribLocation),
    gs(glMapBuffer),
    gs(glUnmapBuffer),
    gs(glGetIntegeri_v),
};


int main()
{
    for(size_t i = 0; i < array_count(funcList); ++i)
    {
        funcList[i].funcMacro = Upp(funcList[i].funcMacro);
    }
    for(size_t i = 0; i < array_count(funcList); ++i)
    {
        printf("static %s %s;\n", funcList[i].funcMacro, funcList[i].funcName);
    }

    printf("\nstatic void LoadGLFunctions()\n");
    printf("{\n");
    for(size_t i = 0; i < array_count(funcList); ++i)
    {
        tbs_gl_func fn = funcList[i];
        printf("    %s = (%s)wglGetProcAddress(\"%s\");\n", fn.funcName, fn.funcMacro, fn.funcName);
    }
    printf("}\n");
}
