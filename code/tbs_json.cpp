#include "tbs_json.h"

internal inline const base_json_object *JSON_GetMember(const base_json_object *obj, const char *memberName)
{
    const base_json_object *result = nullptr;
    if(obj != nullptr)
    {
        result = obj->members[memberName];
    }

    return result;
}

internal inline const base_json_object *JSON_GetListItemByIndexU64(const base_json_object *listObj, u64 index)
{
    // TODO(torgrim): Return a null pointer here instead of all these asserts
    ASSERT(listObj != nullptr);
    ASSERT(listObj->type == JSON_TYPE_Array);

    const base_json_object *result = listObj->array[index];
    return result;
}

internal inline const base_json_object *JSON_GetListItemByIndexI64(const base_json_object *listObj, i64 index)
{
    const base_json_object *result = nullptr;
    if(index > -1)
    {
        result = JSON_GetListItemByIndexU64(listObj, (u64)index);
    }

    return result;
}

internal base_json_object *ParseJsonText(memory_arena *arena, file_data *file)
{
    struct
    {
        u8 *base;
        u8 *at;
    } c;

    c.base = file->content;
    c.at =  c.base;

    base_json_object *root = nullptr;

    constexpr size_t maxDepth = 40;
    auto stack = limit_stack<base_json_object *>(arena, maxDepth);

    bool nextStringIsMemberName = false;
    bool parsing = true;

    char *nextMemberName = nullptr;

    while(parsing)
    {
        while(*c.at != '\0' && IsWhitespace(*c.at))
        {
            ++c.at;
        }

        base_json_object *j = nullptr;
        switch(c.at[0])
        {
            case '[':
                {
                    j = AllocStruct(arena, base_json_object);
                    j->type = JSON_TYPE_Array;

                    ++c.at;
                } break;
            case '{':
                {
                    j = AllocStruct(arena, base_json_object);
                    j->type = JSON_TYPE_Object;

                    ++c.at;
                    nextStringIsMemberName = true;
                } break;
            case '"':
                {
                    c.at += 1;
                    u8 *tmpCursor = c.at;
                    size_t l = 0;
                    // TODO(torgrim): Handle escaped sequences as well.
                    while(tmpCursor[l] != '"' && tmpCursor[l] != '\0')
                    {
                        l++;
                    }

                    // TODO(torgrim): Deal with error handling like this in a more
                    // graceful way.
                    ASSERT(tmpCursor[l] == '"');
                    c.at += l+1;


                    char *str = RawStringCreate(arena, (char *)tmpCursor, l);
                    if(nextStringIsMemberName)
                    {
                        nextStringIsMemberName = false;
                        nextMemberName = str;
                    }
                    else
                    {
                        j = AllocStruct(arena, base_json_object);
                        j->type = JSON_TYPE_String;
                        j->stringValue = str;
                    }
                } break;
            case ']':
                {
                    ASSERT(stack.top != nullptr && stack.top->type == JSON_TYPE_Array);

                    stack.pop();
                    ++c.at;
                } break;
            case '}':
                {
                    ASSERT(stack.top != nullptr && stack.top->type == JSON_TYPE_Object);
                    stack.pop();
                    ++c.at;
                } break;
            case ',':
                {
                    ASSERT(stack.top != nullptr);

                    if(stack.top->type == JSON_TYPE_Object)
                    {
                        nextStringIsMemberName = true;
                    }

                    ++c.at;
                } break;
            case ':':
                {
                    ++c.at;
                } break;
            case '\0':
                {
                    parsing = false;
                } break;
            default:
                {
                    j = AllocStruct(arena, base_json_object);
                    u8 *old = c.at;
                    while(IsAlpha(c.at[0]))
                    {
                        ++c.at;
                    }

                    i64 l = c.at - old;
                    if(l == 5)
                    {
                        if(old[0] == 'f' &&
                                old[1] == 'a' &&
                                old[2] == 'l' &&
                                old[3] == 's' &&
                                old[4] == 'e')
                        {
                            j->type = JSON_TYPE_Bool;
                            j->boolValue = false;
                        }
                        else
                        {
                            ASSERT(false);
                        }
                    }
                    else if(l == 4)
                    {
                        if(old[0] == 't' &&
                                old[1] == 'r' &&
                                old[2] == 'u' &&
                                old[3] == 'e')
                        {
                            j->type = JSON_TYPE_Bool;
                            j->boolValue = true;
                        }
                        else if(old[0] == 'n' &&
                                old[1] == 'u' &&
                                old[2] == 'l' &&
                                old[3] == 'l')
                        {
                            j->type = JSON_TYPE_Null;
                        }
                        else
                        {
                            ASSERT(false);
                        }
                    }
                    else
                    {

                        // TODO(torgrim): Slow verions of parsing numbers from a stream.
                        c.at = old;

                        bool isNegative = false;
                        // TODO: support plain integers as well since
                        // float -> integer is expensive and since this is
                        // primarily used for parsing gltf json where there
                        // are a lot of indices(which are integer) that need
                        // to be processed. If the data struct represents this
                        // via a float it's faster to go the other way.
                        // (signed)int -> float
                        if(c.at[0] == '-')
                        {
                            isNegative = true;
                            ++c.at;
                        }
                        u64 intPart = 0;
                        f64 fractionalPart = 0;
                        i64 fractionalExponent = 1;
                        bool hasFractionalPart = false;
                        bool isNegativeExponent = false;
                        u64 exponent = 0;

                        if(IsNumber(c.at[0]))
                        {
                            intPart = (intPart * 10) + (c.at[0] - '0');
                            ++c.at;
                            while(IsNumber(c.at[0]))
                            {
                                intPart = (intPart * 10) + (c.at[0] - '0');
                                ++c.at;
                            }
                        }
                        else
                        {
                            // TODO: Incorrect number format
                            ASSERT(false);
                        }


                        if(c.at[0] == '.')
                        {
                            hasFractionalPart = true;
                            ++c.at;

                            // TODO: Clean up this. A number MUST follow a .
                            if(IsNumber(c.at[0]))
                            {
                                fractionalExponent *= 10;
                                fractionalPart = (fractionalPart * 10) + (c.at[0] - '0');
                                ++c.at;

                                while(IsNumber(c.at[0]))
                                {
                                    fractionalExponent *= 10;
                                    fractionalPart = (fractionalPart * 10) + (c.at[0] - '0');
                                    ++c.at;
                                }
                            }
                            else
                            {
                                ASSERT(false);
                            }
                        }

                        if(c.at[0] == 'e' || c.at[0] == 'E')
                        {
                            ++c.at;
                            if(c.at[0] == '-')
                            {
                                isNegativeExponent = true;
                                ++c.at;
                            }
                            else if(c.at[0] == '+')
                            {
                                ++c.at;
                            }

                            // TODO: Clean up this part. A number MUST follow an e/E
                            if(IsNumber(c.at[0]))
                            {
                                exponent = (exponent * 10) + (c.at[0] - '0');
                                ++c.at;
                                while(IsNumber(c.at[0]))
                                {
                                    exponent = (exponent * 10) + (c.at[0] - '0');
                                    ++c.at;
                                }
                            }
                            else
                            {
                                ASSERT(false);
                            }
                        }

                        ASSERT(c.at != old);

                        ASSERT(fractionalExponent != 0);
                        f64 invFracExp = 1.0 / (f64)fractionalExponent;
                        f64 exp = pow(10.0, (f64)exponent);
                        if(isNegativeExponent)
                        {
                            exp = 1.0 / exp;
                        }

                        f64 finalValue = ((f64)intPart + (fractionalPart * invFracExp)) * exp;
                        if(isNegative)
                        {
                            finalValue *= -1.0;
                        }

                        if(hasFractionalPart == false && isNegativeExponent == false)
                        {
                            j->type = JSON_TYPE_Integer;
                            j->intValue = (i64)finalValue;
                        }
                        else
                        {
                            j->type = JSON_TYPE_Float;
                            j->floatValue = finalValue;
                        }
                    }
                } break;
        }

        if(j != nullptr)
        {
            if(stack.top == nullptr)
            {
                // TODO(torgrim): If this is not a array/object type
                // we can break out here since we only allow one basic type
                // in the root.
                root = j;
            }
            else if(stack.top->type == JSON_TYPE_Array)
            {
                if(stack.top->array.count == 0)
                {
                    stack.top->array = ss_dynamic_array<base_json_object *>(arena);
                }

                stack.top->array.Add(j);
            }
            else if(stack.top->type == JSON_TYPE_Object)
            {
                if(stack.top->members.count == 0)
                {
                    stack.top->members = simple_dict<base_json_object *>(arena);
                }
            }

            if(nextMemberName != nullptr)
            {
                j->name = nextMemberName;
                u32 hash = DictHashDefault(nextMemberName);
                j->nameHash = hash;
                stack.top->members.Add(hash, j);

                nextMemberName = nullptr;
            }

            if(j->type == JSON_TYPE_Array || j->type == JSON_TYPE_Object)
            {
                stack.push(j);
            }
        }

        if(*c.at == '\0')
        {
            parsing = false;
        }
    }

    ASSERT(root != nullptr);

    return root;
}

internal base_json_object *ParseJsonSimple(memory_arena *arena, file_data *file)
{
    base_json_object *root = ParseJsonText(arena, file);
    return root;
}
