#include "tbs_scene_gui.h"

namespace tbs_scene
{
    internal const char *guiAttenuationFunctionNames[DISTANCE_ATTENUATION_FUNCTION_Count] =
    {
        "Inverse Square Approximation",
        "Exponential",
        "Cubic Polynomial",
    };

    internal const char *guiPrimitiveTypeNames[tbs_opengl::BASE_PRIMITIVE_TYPE_Count] =
    {
        "Cube",
        "Plane",
    };

    internal const char *guiLightTypeNames[LIGHT_TYPE_Count] =
    {
        "Point",
        "Spot",
        "Directional",
    };

    internal const char *guiPbrTextureNames[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_Count] =
    {
        "Base Color Map",
        "Normal Map",
        "Metallic Roughness Map",
        "Emissive Map",
        "Occlusion Map",
    };

    internal const char *guiTonemappingTypeNames[TONEMAPPING_TYPE_Count] =
    {
        "None",
        "Reinhard Simple",
        "Reinhard Extended",
        "Reinhard ExtendedLuminance",
        "Reinhard Jodie",
        "Hable",
        "ACES",
    };

    internal void gui_CreateSceneNodeTreeList(scene_node *node, scene_node **selected)
    {
        ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_OpenOnArrow;
        if(*selected == node)
        {
            flags |= ImGuiTreeNodeFlags_Selected;
        }

        if(node->firstChild == nullptr)
        {
            flags |= ImGuiTreeNodeFlags_Leaf;
        }

        bool nodeOpen = ImGui::TreeNodeEx(node, flags, (char *)node->name);
        if(ImGui::IsItemClicked())
        {
            *selected = node;
        }
        if(nodeOpen)
        {

            scene_node *cNode = node->firstChild;
            if(cNode)
            {
                gui_CreateSceneNodeTreeList(cNode, selected);
                cNode = cNode->next;
            }

            ImGui::TreePop();
        }

        if(node->next != nullptr)
        {
            gui_CreateSceneNodeTreeList(node->next, selected);
        }
    }

    internal void gui_CreateTexturePreview(tbs_opengl::texture_info *texInfo, ImVec2& previewSize)
    {
        // TODO(torgrim): Separate out this into its own function
        // so that we easily can preview textures in a uniform manner.
        ImGui::BeginGroup();
        ImGui::Image((void *)(intptr_t)texInfo->texID, previewSize);
        ImGui::EndGroup();
        ImGui::SameLine();
        ImGui::BeginGroup();
        if(texInfo->path != nullptr)
        {
            ImGui::Text((char *)texInfo->path);
        }
        else
        {
            ImGui::Text("");
        }
        ImGui::Text("Loaded: ");
        ImGui::SameLine();
        if(texInfo->loaded)
        {
            ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), BOOL_STRING(texInfo->loaded));
        }
        else
        {
            ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), BOOL_STRING(texInfo->loaded));
        }
        ImGui::Text("ID: %u", texInfo->texID);
        ImGui::Text("target: %u", texInfo->target);
        ImGui::EndGroup();
    }

    // TODO(torgrim): A lot here is currently Win32 specific. This should be handled in a platform agnostic way
    // so that the same can be used on all platforms.
    internal void RenderSceneGUI(application_state *appState, tbs_gui::imgui_state *imGuiState, tbs_opengl::opengl_resource_manager *rm, pbr_scene *pbrScene)
    {

        scene_state *sceneState = pbrScene->GetActiveScene();
        scene_gui_state *sceneGuiState = &pbrScene->guiState;
        ImGuiIO& io = ImGui::GetIO();

        // NOTE(torgrim): imgui rendering
        io.MouseDown[0] = appState->inputState.mouse.leftButtonDown;
        io.MouseDown[1] = appState->inputState.mouse.rightButtonDown;
        io.MouseWheel = appState->inputState.mouse.scrollDelta * 0.01f;

        if((io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange) == 0)
        {
            ImGuiMouseCursor imguiCursor = ImGui::GetMouseCursor();
            if(imguiCursor == ImGuiMouseCursor_None || io.MouseDrawCursor)
            {
                // TODO(torgrim): hide cursor here
                ASSERT(false);
            }
            else
            {
                // TODO(torgrim): Make sure that cursor is drawn
                SetCursor(imGuiState->cursorMap[imguiCursor]);
            }
        }

        for(i32 keyIndex= 0; keyIndex < INPUT_KEY_COUNT; ++keyIndex)
        {
            io.KeysDown[keyIndex] = appState->inputState.keyboard[keyIndex].isDown;
        }

        io.KeyCtrl = io.KeysDown[INPUT_KEY_Ctrl];
        io.KeyShift = io.KeysDown[INPUT_KEY_Shift];
        io.KeyAlt = io.KeysDown[INPUT_KEY_Alt];

        if(imGuiState->characterBufferCount != 0)
        {
            for(u32 cIndex = 0; cIndex < imGuiState->characterBufferCount; ++cIndex)
            {
                io.AddInputCharacter(imGuiState->characterBuffer[cIndex]);
            }
        }

        // TODO(torgrim): Not sure what this is suppose to do. Find out.
        if(io.WantSetMousePos)
        {
            POINT screenPos = {(i32)io.MousePos.x, (i32)io.MousePos.y};
            ClientToScreen(appState->windowHandle, &screenPos);
            SetCursorPos((i32)io.MousePos.x, (i32)io.MousePos.y);
        }
        else
        {
            POINT curPos;
            GetCursorPos(&curPos);
            ScreenToClient(appState->windowHandle, &curPos);
            f32 curX = (f32)curPos.x;
            // NOTE(torgrim): IMGUI uses (0,0) = top,left
            f32 curY = (f32)curPos.y;
            io.MousePos = ImVec2(curX, curY);
        }

        io.DisplaySize = ImVec2(appState->clientW, appState->clientH);
        io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);
        io.DeltaTime = appState->dt;

        ImGui::NewFrame();

        ImGuiWindowFlags mainDockWindowFlags = ImGuiWindowFlags_None;
        mainDockWindowFlags |= ImGuiWindowFlags_NoTitleBar;
        mainDockWindowFlags |= ImGuiWindowFlags_NoCollapse;
        mainDockWindowFlags |= ImGuiWindowFlags_NoResize;
        mainDockWindowFlags |= ImGuiWindowFlags_NoMove;
        mainDockWindowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
        mainDockWindowFlags |= ImGuiWindowFlags_NoNavFocus;
        mainDockWindowFlags |= ImGuiWindowFlags_NoBackground;
        mainDockWindowFlags |= ImGuiWindowFlags_MenuBar;
        mainDockWindowFlags |= ImGuiWindowFlags_NoDocking;
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        bool ignored = true;
        ImGuiViewport *viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetWorkPos());
        ImGui::SetNextWindowSize(viewport->GetWorkSize());
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::Begin("DockSpace Dummy", &ignored, mainDockWindowFlags);
        ImGui::PopStyleVar(3);

        ImGuiDockNodeFlags mainDockSpaceFlags = ImGuiDockNodeFlags_None;
        mainDockSpaceFlags |= ImGuiDockNodeFlags_PassthruCentralNode;
        ImGuiID mainDockSpaceID = ImGui::GetID("MainDockSpace");
        ImGui::DockSpace(mainDockSpaceID, ImVec2(0.0f, 0.0f), mainDockSpaceFlags);


        // TODO(torgrim): Temp solution for fixing data that needs to be refreshed
        // reset after scene change.
        bool resetSceneData = false;
        local_persist scene_node *selected = nullptr;

        local_persist bool ViewMenu_Settings = true;
        local_persist bool ViewMenu_SceneOutliner = true;
        local_persist bool ViewMenu_Properties = true;
        local_persist bool ViewMenu_CameraSettings = false;
        local_persist bool ViewMenu_Memory = false;
        local_persist bool ViewMenu_Metrics = false;
        local_persist bool ViewMenu_ApplicationInfo = false;
        local_persist bool ViewMenu_Renderer = false;
        local_persist bool ViewMenu_OpenGLInfo = false;
        local_persist bool ViewMenu_HDRInfo = false;

        local_persist bool SceneMenu_NewSceneEmpty = false;
        local_persist bool SceneMenu_NewSceneGLTF = false;

        if(ImGui::BeginMenuBar())
        {
            if(ImGui::BeginMenu("App"))
            {
                if(ImGui::MenuItem("PBR Viewer"))
                {
                    appState->mode = APP_MODE_PBRViewer;
                }

                ImGui::EndMenu();
            }
            if(ImGui::BeginMenu("File"))
            {
                ImGui::MenuItem("TODO");
                ImGui::EndMenu();
            }

            if(ImGui::BeginMenu("View"))
            {
                ImGui::MenuItem("Settings", nullptr, &ViewMenu_Settings);
                ImGui::MenuItem("Scene Outliner", nullptr, &ViewMenu_SceneOutliner);
                ImGui::MenuItem("Properties", nullptr, &ViewMenu_Properties);
                ImGui::MenuItem("Editor Camera", nullptr, &ViewMenu_CameraSettings);
                ImGui::MenuItem("Memory", nullptr, &ViewMenu_Memory);
                ImGui::MenuItem("Metrics", nullptr, &ViewMenu_Metrics);
                ImGui::MenuItem("Application Info", nullptr, &ViewMenu_ApplicationInfo);
                ImGui::MenuItem("OpenGL Info", nullptr, &ViewMenu_OpenGLInfo);
                ImGui::MenuItem("Renderer State", nullptr, &ViewMenu_Renderer);
                ImGui::MenuItem("HDR Info", nullptr, &ViewMenu_HDRInfo);
                ImGui::EndMenu();
            }

            if(ImGui::BeginMenu("Scene"))
            {
                ImGui::Text("Current Scene: %s", sceneState->name);
                ImGui::Separator();
                if(ImGui::BeginMenu("New Scene"))
                {
                    ImGui::MenuItem("Empty Scene", nullptr, &SceneMenu_NewSceneEmpty);
                    ImGui::MenuItem("Import GLTF Scene", nullptr, &SceneMenu_NewSceneGLTF);
                    ImGui::EndMenu();
                }
                if(ImGui::BeginMenu("Loaded Scenes"))
                {
                    for(u32 sceneIndex = 0; sceneIndex < pbrScene->sceneCount; ++sceneIndex)
                    {
                        if(ImGui::MenuItem(pbrScene->scenes[sceneIndex]->name, nullptr, pbrScene->activeSceneIndex == sceneIndex))
                        {
                            pbrScene->activeSceneIndex = sceneIndex;
                            sceneState = pbrScene->GetActiveScene();
                            imGuiState->characterBufferCount = 0;
                            resetSceneData = true;
                        }
                    }

                    ImGui::EndMenu();
                }

                ImGui::EndMenu();
            }

            ImGui::EndMenuBar();
        }

        ImGuiContext *imCtx = ImGui::GetCurrentContext();
        ImGuiDockNode *mainNode = ImGui::DockContextFindNodeByID(imCtx, mainDockSpaceID);

        ASSERT(mainNode != nullptr);
        ASSERT(mainNode->CentralNode != nullptr);

        v2i newViewportSize = v2i((i32)mainNode->CentralNode->Size.x, (i32)mainNode->CentralNode->Size.y);
        if(newViewportSize != sceneGuiState->viewport.size)
        {
            sceneGuiState->viewport.size = newViewportSize;
            pbrScene->viewportSize = newViewportSize;
            pbrScene->NeedViewportResize = true;
        }

        sceneGuiState->viewport.pos.x = (i32)mainNode->CentralNode->Pos.x;
        sceneGuiState->viewport.pos.y = (i32)(appState->clientH - mainNode->CentralNode->Pos.y) - sceneGuiState->viewport.size.y;
        if(sceneGuiState->viewport.pos != pbrScene->viewportPos)
        {
            // NOTE(torgrim): IMGUI is top to bottom
            pbrScene->viewportPos = sceneGuiState->viewport.pos;
        }


#if 1
        ImGui::SetNextWindowPos(ImVec2(10, 30), ImGuiCond_FirstUseEver);


        POINT cursorScreenPos;
        GetCursorPos(&cursorScreenPos);
        POINT cursorClientPos = cursorScreenPos;
        ScreenToClient(appState->windowHandle, &cursorClientPos);

        if(ViewMenu_Settings)
        {
            if(ImGui::Begin("Settings##scene_settings", &ViewMenu_Settings))
            {
                ImGui::Checkbox("MSAA", &pbrScene->msaaActive);
                // TODO(torgrim): Need to decide which of these should be per
                // scene settings vs global settings.
                ImGui::Checkbox("Use Point Shadow Sample", &sceneState->usePointSampleShadow);
                ImGui::Checkbox("Use Spot Shadow Sample", &sceneState->useSpotSampleShadow);
                ImGui::Checkbox("Use Ambient Environment Lighting", &sceneState->useAmbientEnvLighting);

                ImGui::Separator();
                ImGui::SliderFloat("Ambient Strength", &pbrScene->ambientStrength, 0.0f, 1.0f);

                ImGui::Separator();
                ImGui::Checkbox("Use Gamma", &pbrScene->useGammaCorrection);
                ImGui::Checkbox("Use Tonemapping", &pbrScene->useTonemapping);
                if(ImGui::BeginCombo("Tonemapping Operator", tbs_scene::guiTonemappingTypeNames[pbrScene->tonemappingType]))
                {
                    for(size_t typeIndex = 0; typeIndex < ARRAY_COUNT(tbs_scene::guiTonemappingTypeNames); ++typeIndex)
                    {
                        if(ImGui::Selectable(tbs_scene::guiTonemappingTypeNames[typeIndex], pbrScene->tonemappingType == typeIndex))
                        {
                            pbrScene->tonemappingType = (tbs_scene::tonemapping_type)typeIndex;
                        }
                    }
                    ImGui::EndCombo();
                }

                ImGui::SliderFloat("White Point", &pbrScene->whitePoint, 0.0f, 100.0f);
                ImGui::Separator();

                i32 e = pbrScene->viewMode;
                ImGui::RadioButton("Default", &e, tbs_scene::VIEW_MODE_Default);
                ImGui::RadioButton("Cascade", &e, tbs_scene::VIEW_MODE_Cascade);
                pbrScene->viewMode = (tbs_scene::view_mode)e;

            }
            ImGui::End();
        }

        if(ViewMenu_Memory)
        {
            ImGui::Begin("Memory", &ViewMenu_Memory);
            if(ImGui::BeginTabBar("memory_overview_tab_bar"))
            {
                if(ImGui::BeginTabItem("Overview"))
                {
                    ImGui::Text("TODO [Debugging Features]");

                    ImGui::EndTabItem();
                }

                ImGui::EndTabBar();
            }

            ImGui::End();
        }

        if(ViewMenu_Metrics)
        {
            ImGui::Begin("Metrics", &ViewMenu_Metrics);
            ImGui::Text("Delta Time: %.2f ms", (f64)appState->dt * 1000.0);
            ImGui::Separator();
            ImGui::Text("TODO (Debugging Metrics)");
            ImGui::End();
        }

        if(ViewMenu_ApplicationInfo)
        {
            ImGui::Begin("Application", &ViewMenu_ApplicationInfo);
            ImGui::Text("Client WxH, %dx%d", (i32)appState->clientW, (i32)appState->clientH);
            ImGui::Text("Mouse Client Pos, X: %ld, Y: %ld", cursorClientPos.x, cursorClientPos.y);
            ImGui::Text("Mouse Screen Pos, X: %ld, Y: %ld", cursorScreenPos.x, cursorScreenPos.y);

            ImGui::End();
        }

        if(ViewMenu_OpenGLInfo)
        {
            ImGui::Begin("OpenGL Info", &ViewMenu_OpenGLInfo);
            opengl_state_data currentOpenGLState = OpenGLGetCurrentState();

            // TODO(torgrim): All of these can be fetch only once a then
            // saved by the application since they are the same as long
            // as the context isn't changed.
            // Some also aren't valid for all version of opengl
#define IMGUI_GL_VAR_I(id) ImGui::Text("%s: %d", #id, OpenGLGetVarI(id))
#define IMGUI_GL_VAR_S(id) ImGui::Text("%s: %s", #id, glGetString(id))
#define IMGUI_GL_VAR_B(id) ImGui::Text("%s: %d", #id, OpenGLGetVarB(id))
#define IMGUI_GL_VAR_F(id) ImGui::Text("%s: %f", #id, OpenGLGetVarF(id))

            IMGUI_GL_VAR_S(GL_VENDOR);
            IMGUI_GL_VAR_S(GL_RENDERER);
            IMGUI_GL_VAR_S(GL_VERSION);
            IMGUI_GL_VAR_S(GL_SHADING_LANGUAGE_VERSION);
            ImGui::Separator();

            IMGUI_GL_VAR_B(GL_DOUBLEBUFFER);
            IMGUI_GL_VAR_B(GL_STEREO);

            ImGui::Separator();

            IMGUI_GL_VAR_I(GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS);
            IMGUI_GL_VAR_I(GL_MAX_COMPUTE_UNIFORM_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_COMPUTE_ATOMIC_COUNTERS);
            IMGUI_GL_VAR_I(GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS);
            IMGUI_GL_VAR_I(GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS);

#if 0
            IMGUI_GL_VAR_I_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT);
            IMGUI_GL_VAR_I_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT);
            IMGUI_GL_VAR_I_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT);
            IMGUI_GL_VAR_I_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE);
            IMGUI_GL_VAR_I_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE);
            IMGUI_GL_VAR_I_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE);
#endif

            IMGUI_GL_VAR_I(GL_MAX_DEBUG_GROUP_STACK_DEPTH);

            IMGUI_GL_VAR_I(GL_MAJOR_VERSION);
            IMGUI_GL_VAR_I(GL_MINOR_VERSION);

            IMGUI_GL_VAR_I(GL_MAX_3D_TEXTURE_SIZE);
            IMGUI_GL_VAR_I(GL_MAX_ARRAY_TEXTURE_LAYERS);
            IMGUI_GL_VAR_I(GL_MAX_CLIP_DISTANCES);
            IMGUI_GL_VAR_I(GL_MAX_COLOR_TEXTURE_SAMPLES);
            IMGUI_GL_VAR_I(GL_MAX_COMBINED_ATOMIC_COUNTERS);
            IMGUI_GL_VAR_I(GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS);
            IMGUI_GL_VAR_I(GL_MAX_COMBINED_UNIFORM_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_CUBE_MAP_TEXTURE_SIZE);
            IMGUI_GL_VAR_I(GL_MAX_DEPTH_TEXTURE_SAMPLES);
            IMGUI_GL_VAR_I(GL_MAX_DRAW_BUFFERS);
            IMGUI_GL_VAR_I(GL_MAX_DUAL_SOURCE_DRAW_BUFFERS);
            IMGUI_GL_VAR_I(GL_MAX_ELEMENTS_INDICES);
            IMGUI_GL_VAR_I(GL_MAX_ELEMENTS_VERTICES);
            IMGUI_GL_VAR_I(GL_MAX_FRAGMENT_ATOMIC_COUNTERS);
            IMGUI_GL_VAR_I(GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_FRAGMENT_INPUT_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_FRAGMENT_UNIFORM_VECTORS);
            IMGUI_GL_VAR_I(GL_MAX_FRAGMENT_UNIFORM_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_FRAMEBUFFER_WIDTH);
            IMGUI_GL_VAR_I(GL_MAX_FRAMEBUFFER_HEIGHT);
            IMGUI_GL_VAR_I(GL_MAX_FRAMEBUFFER_LAYERS);
            IMGUI_GL_VAR_I(GL_MAX_FRAMEBUFFER_SAMPLES);
            IMGUI_GL_VAR_I(GL_MAX_GEOMETRY_ATOMIC_COUNTERS);
            IMGUI_GL_VAR_I(GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_GEOMETRY_INPUT_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_GEOMETRY_OUTPUT_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS);
            IMGUI_GL_VAR_I(GL_MAX_GEOMETRY_UNIFORM_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_GEOMETRY_UNIFORM_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_INTEGER_SAMPLES);
            IMGUI_GL_VAR_I(GL_MIN_MAP_BUFFER_ALIGNMENT);
            IMGUI_GL_VAR_I(GL_MAX_LABEL_LENGTH);
            IMGUI_GL_VAR_I(GL_MAX_PROGRAM_TEXEL_OFFSET);
            IMGUI_GL_VAR_I(GL_MIN_PROGRAM_TEXEL_OFFSET);
            IMGUI_GL_VAR_I(GL_MAX_RECTANGLE_TEXTURE_SIZE);
            IMGUI_GL_VAR_I(GL_MAX_RENDERBUFFER_SIZE);
            IMGUI_GL_VAR_I(GL_MAX_SAMPLE_MASK_WORDS);
            IMGUI_GL_VAR_I(GL_MAX_SERVER_WAIT_TIMEOUT);
            IMGUI_GL_VAR_I(GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS);
            IMGUI_GL_VAR_I(GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS);
            IMGUI_GL_VAR_I(GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS);
            IMGUI_GL_VAR_I(GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_TEXTURE_BUFFER_SIZE);
            IMGUI_GL_VAR_I(GL_MAX_TEXTURE_IMAGE_UNITS);
            IMGUI_GL_VAR_I(GL_MAX_TEXTURE_SIZE);
            IMGUI_GL_VAR_I(GL_MAX_UNIFORM_BUFFER_BINDINGS);
            IMGUI_GL_VAR_I(GL_MAX_UNIFORM_BLOCK_SIZE);
            IMGUI_GL_VAR_I(GL_MAX_UNIFORM_LOCATIONS);
            IMGUI_GL_VAR_I(GL_MAX_VARYING_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_VARYING_VECTORS);
            IMGUI_GL_VAR_I(GL_MAX_VARYING_FLOATS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_ATOMIC_COUNTERS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_ATTRIBS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_UNIFORM_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_UNIFORM_VECTORS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_OUTPUT_COMPONENTS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_UNIFORM_BLOCKS);
            IMGUI_GL_VAR_I(GL_MAX_VIEWPORT_DIMS);
            IMGUI_GL_VAR_I(GL_MAX_VIEWPORTS);
            IMGUI_GL_VAR_I(GL_NUM_EXTENSIONS);
            IMGUI_GL_VAR_I(GL_NUM_PROGRAM_BINARY_FORMATS);
            IMGUI_GL_VAR_I(GL_NUM_SHADER_BINARY_FORMATS);
            IMGUI_GL_VAR_I(GL_SUBPIXEL_BITS);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET);
            IMGUI_GL_VAR_I(GL_MAX_VERTEX_ATTRIB_BINDINGS);

            ImGui::End();
        }

        if(ViewMenu_Renderer)
        {
            ImGui::Begin("Renderer", &ViewMenu_Renderer);
            if(ImGui::BeginTabBar("renderer_tabs"))
            {
                if(ImGui::BeginTabItem("Meshes"))
                {
                    local_persist i32 RendererWindow_SelectedMesh = -1;
                    ImGui::Text("Total Count %u", pbrScene->meshCount);
                    ImGui::BeginChild("renderer_mesh_list", ImVec2(300.0f, 0.0f), true);

                    ImGui::Columns(2, "mesh_list_columns", true);
                    ImGui::Text("Index");
                    ImGui::NextColumn();
                    ImGui::Text("Primitive Count");
                    ImGui::NextColumn();
                    ImGui::Separator();
                    for(u32 mIndex = 0; mIndex < pbrScene->meshCount; ++mIndex)
                    {
                        auto m = pbrScene->meshes + mIndex;
                        char lab[32];
                        sprintf(lab, "%u", mIndex);
                        if(ImGui::Selectable(lab, RendererWindow_SelectedMesh == (i32)mIndex, ImGuiSelectableFlags_SpanAllColumns))
                        {
                            RendererWindow_SelectedMesh = (i32)mIndex;
                        }
                        ImGui::NextColumn();
                        ImGui::Text("%zu", m->primitives.count);
                        ImGui::NextColumn();
                        ImGui::Separator();
                    }

                    ImGui::EndChild();

                    if(RendererWindow_SelectedMesh > -1)
                    {
                        auto m = pbrScene->meshes + RendererWindow_SelectedMesh;
                        ImGui::SameLine();
                        ImGui::BeginChild("renderer_primitive_list", ImVec2(0.0f, 0.0f), true);
                        for(u32 pIndex = 0; pIndex < m->primitives.count; ++pIndex)
                        {
                            auto *p = rm->GetPrimitive(m->primitives.data[pIndex].renderDataHandle);
                            auto *mat = pbrScene->GetMaterial(m->primitives.data[pIndex].materialHandle);
                            ASSERT(mat != nullptr);
                            char lab[32];
                            sprintf(lab, "Primitive_%u", pIndex);
                            ImGui::PushID(lab);
                            if(ImGui::CollapsingHeader(lab))
                            {
                                if(ImGui::BeginTabBar("primitive_tab_bar##testing_tab_id"))
                                {
                                    if(ImGui::BeginTabItem("Primitive Info##primitive_info_tab_item"))
                                    {
                                        ImGui::Text("Vertex Count %u", p->vertexCount);
                                        ImGui::Text("Index Count %u", p->indexCount);
                                        ImGui::EndTabItem();
                                    }
                                    if(ImGui::BeginTabItem("Primitive Material##primitive_material_tab_item"))
                                    {
                                        ImGui::Text("Metallic Factor: %f", (f64)mat->metallicFactor);
                                        ImGui::Text("Rougness Factor: %f", (f64)mat->roughnessFactor);
                                        ImGui::Text("Emissive Factor: %f", (f64)mat->roughnessFactor);
                                        ImGui::Text("Normal Scale Factor: %f", (f64)mat->normalScaleFactor);
                                        ImGui::Text("Occlusion Strengh Factor: %f", (f64)mat->occlusionStrengthFactor);
                                        ImGui::Text("Alpha Cutoff: %f", (f64)mat->alphaCutoff);

                                        for(u32 tIndex = 0; tIndex < ARRAY_COUNT(mat->textures); ++tIndex)
                                        {
                                            auto texHandle = mat->textures[tIndex];
                                            auto texInfo = rm->GetTexture(texHandle);
                                            ImVec2 previewSize = ImVec2(128, 128);
                                            if(texInfo != nullptr && texInfo->loaded)
                                            {
                                                ImGui::Separator();
                                                gui_CreateTexturePreview(texInfo, previewSize);
                                                ImGui::Text(guiPbrTextureNames[tIndex]);
                                            }
                                        }

                                        ImGui::EndTabItem();
                                    }

                                    ImGui::EndTabBar();
                                }
                            }
                            ImGui::PopID();
                        }

                        ImGui::EndChild();
                    }

                    ImGui::EndTabItem();
                }

                if(ImGui::BeginTabItem("Materials"))
                {
                    ImGui::Text("Count %u", pbrScene->materialCount);

                    ImGui::EndTabItem();
                }

                if(ImGui::BeginTabItem("Textures"))
                {
                    ImVec2 previewSize = ImVec2(128, 128);
                    ImGui::Text("Count %u", rm->textureCount);
                    ImGui::Separator();
                    for(u32 tIndex = 0; tIndex < rm->textureCount; ++tIndex)
                    {
                        auto texInfo = rm->textures + tIndex;
                        ImGui::Separator();
                        gui_CreateTexturePreview(texInfo, previewSize);
                    }

                    ImGui::EndTabItem();
                }

                ImGui::EndTabBar();
            }

            ImGui::End();
        }

        sceneGuiState->envMapChooserCtx.open = ViewMenu_HDRInfo;
        if(ViewMenu_HDRInfo)
        {
            sceneGuiState->envMapChooserCtx.activeIndex = tbs_opengl::HandleToIndexU32(sceneState->envMap);
            if(tbs_gui::ShowEnvMapChooserWidget(appState, rm, &sceneGuiState->envMapChooserCtx))
            {
                if(sceneGuiState->envMapChooserCtx.result != tbs_opengl::invalidHandle)
                {
                    sceneState->envMap = sceneGuiState->envMapChooserCtx.result;
                }
            }

            ViewMenu_HDRInfo = sceneGuiState->envMapChooserCtx.open;
        }

        if(SceneMenu_NewSceneGLTF)
        {
            if(sceneGuiState->fbCtx.init == false)
            {
#if TBS_INTERNAL
                RawStringCopy(sceneGuiState->fbCtx.searchDir, "c:/programming/asset_collection/gltf_sample_models/2.0/");
#else
                RawStringCopy(sceneGuiState->fbCtx.searchDir, appState->appRootPath);
#endif
                RawStringCopy(sceneGuiState->fbCtx.extension, "gltf");
                sceneGuiState->fbCtx.result.dialogResult = tbs_gui::DIALOG_RESULT_None;
                memset(sceneGuiState->fbCtx.result.fullFilename, 0, sizeof(sceneGuiState->fbCtx.result.fullFilename));
                sceneGuiState->fbCtx.init = true;
            }

            if(tbs_gui::ShowFileBrowserWidget(&sceneGuiState->fbCtx))
            {
                if(sceneGuiState->fbCtx.result.dialogResult == tbs_gui::DIALOG_RESULT_Cancel)
                {
                    SceneMenu_NewSceneGLTF = false;
                    sceneGuiState->fbCtx.init = false;
                }
                else if(sceneGuiState->fbCtx.result.dialogResult == tbs_gui::DIALOG_RESULT_Open)
                {
                    temp_memory scratch(appState);
                    file_data selectedFile = Win32ReadAllText(scratch.arena, sceneGuiState->fbCtx.result.fullFilename);

                    base_json_object *jsonData = ParseJsonSimple(scratch.arena, &selectedFile);

                    // TODO(torgrim): Check that the version is valid and that other
                    // the parse was successful
                    gltf_context ctx = {};
                    ctx.arena = scratch.arena;
                    ctx.root = jsonData;
                    sprintf(ctx.path, "%s", sceneGuiState->fbCtx.searchDir);

                    gltf_root gltf = gltf_ProcessGltf(&ctx);

                    ASSERT(gltf.defaultScene >= 0 && gltf.defaultScene < (i64)gltf.scenes.count);
                    i64 newActiveScene = pbrScene->sceneCount + gltf.defaultScene;
                    for(size_t sIndex = 0; sIndex < gltf.scenes.count; ++sIndex)
                    {
                        scene_node *firstNode = CreateSceneNodeTreeFromGltf(appState, rm, pbrScene, &gltf, (i64)sIndex);
                        scene_state *newScene = CreateScene(appState, pbrScene, firstNode);
                        pbrScene->AddScene(newScene);
                    }

                    pbrScene->activeSceneIndex = (u32)newActiveScene;
                    sceneState = pbrScene->GetActiveScene();
                    imGuiState->characterBufferCount = 0;
                    resetSceneData = true;

                    SceneMenu_NewSceneGLTF = false;
                    sceneGuiState->fbCtx.init = false;
                }
            }
        }

        if(ViewMenu_CameraSettings)
        {
            simple_camera *cam = &sceneState->editorCamera;
            ImGui::Begin("Editor Camera", &ViewMenu_CameraSettings);
            ImGui::InputFloat("Camera Speed", &cam->speed);
            ImGui::InputFloat("Camera Rotation Speed", &cam->rotationSpeed);
            ImGui::InputFloat("Camera FOV", &cam->fov);
            ImGui::Text("Camera Pos, X: %.2f, Y: %.2f, Z: %.2f", (f64)cam->pos.x, (f64)cam->pos.y, (f64)cam->pos.z);
            ImGui::Text("Camera Direction, X: %.2f, Y: %.2f, Z: %.2f", (f64)cam->dir.x, (f64)cam->dir.y, (f64)cam->dir.z);
            ImGui::Text("Camera Pitch: %.2f", (f64)cam->rot.x);
            ImGui::Text("Camera Yaw: %.2f", (f64)cam->rot.y);
            ImGui::Text("Camera Roll: %.2f", (f64)cam->rot.z);
            ImGuiSliderFlags sliderFlags = ImGuiSliderFlags_ClampOnInput;
            ImGui::SliderFloat("Clip Near", &sceneState->editorCamera.clipNear, 0.1f, 5.0f, "%.3f", sliderFlags);
            ImGui::SliderFloat("Clip Far", &sceneState->editorCamera.clipFar, 6.0f, 1000.0f, "%.3f", sliderFlags);

            ImGui::End();
        }

        if(ViewMenu_SceneOutliner)
        {
            ImGui::Begin("Scene Outliner", &ViewMenu_SceneOutliner);

            if(resetSceneData)
            {
                SceneMenu_NewSceneEmpty = false;
                selected = nullptr;
            }

            if(selected == nullptr && sceneState->firstNode != nullptr)
            {
                selected = sceneState->firstNode;
            }

            local_persist bool addingGltfMesh = false;
            if(ImGui::Button("Add"))
            {
                ImGui::OpenPopup("add_scene_element_popup");
            }
            if(ImGui::BeginPopup("add_scene_element_popup"))
            {
                if(ImGui::BeginMenu("Mesh##add_mesh_menu"))
                {
                    for(size_t nIndex = 0; nIndex < ARRAY_COUNT(guiPrimitiveTypeNames); nIndex++)
                    {
                        if(ImGui::Selectable(guiPrimitiveTypeNames[nIndex]))
                        {
                            scene_node *newNode = CreateSceneNodeAndMesh(appState,
                                                                         pbrScene,
                                                                         (tbs_opengl::base_primitive_type)nIndex,
                                                                         "New Mesh");

                            newNode->groupID = groupIDCounter++;
                            sceneState->AddTopLevelNode(newNode);
                            selected = newNode;
                        }
                    }

                    ImGui::EndMenu();
                }

                if(ImGui::BeginMenu("Light##add_light_menu"))
                {
                    for(size_t nIndex = 0; nIndex < ARRAY_COUNT(guiLightTypeNames); ++nIndex)
                    {
                        if(ImGui::Selectable(guiLightTypeNames[nIndex]))
                        {
                            // TODO(torgrim): Create helper function for this
                            // so that we can assert that integer is in range
                            // of enum.
                            light_data newLight = CreateLight((light_type)nIndex);
                            auto lightHandle = pbrScene->AddLight(newLight);
                            auto newNode = sceneState->CreateAndAddTopLevelNode(&appState->sceneNodePool, tbs_opengl::HandleToVoid(lightHandle), "New Light", SCENE_NODE_TYPE_Light);
                            selected = newNode;
                        }
                    }

                    ImGui::EndMenu();
                }

                if(ImGui::Button("Camera##add_camera_menu"))
                {
                    if(sceneState->cameraCount < 10)
                    {
                        u32 handle = sceneState->cameraCount;
                        simple_camera newCam = {};
                        newCam.fov = 60.0f;
                        newCam.aspect = 16.0f / 9.0f;
                        newCam.clipNear = 0.1f;
                        newCam.clipFar = 100.0f;
                        newCam.theta = 90.0f;

                        sceneState->cameras[sceneState->cameraCount++] = newCam;

                        auto newNode = sceneState->CreateAndAddTopLevelNode(&appState->sceneNodePool, (void *)(uintptr_t)(handle), "New Camera", SCENE_NODE_TYPE_Camera);
                        // TODO(trogrim): This is not how we want to do it since rendering
                        // the camera is a very specific thing only the position and rotation
                        // should be relevant for this transform.
                        newNode->transform.scale = v3(0.4f, 0.4f, 0.4f);
                        selected = newNode;
                    }

                    ImGui::CloseCurrentPopup();
                }

                if(ImGui::Button("GLTF##add_gltf_menu"))
                {
                    addingGltfMesh = true;
                    ImGui::CloseCurrentPopup();
                }

                ImGui::EndPopup();


            }

            if(addingGltfMesh)
            {
                if(sceneGuiState->fbCtx.init == false)
                {
#if TBS_INTERNAL
                    RawStringCopy(sceneGuiState->fbCtx.searchDir, "c:/programming/asset_collection/gltf_sample_models/2.0/");
#else
                    RawStringCopy(sceneGuiState->fbCtx.searchDir, appState->appRootPath);
#endif
                    RawStringCopy(sceneGuiState->fbCtx.extension, "gltf");
                    sceneGuiState->fbCtx.result.dialogResult = tbs_gui::DIALOG_RESULT_None;
                    memset(sceneGuiState->fbCtx.result.fullFilename, 0, sizeof(sceneGuiState->fbCtx.result.fullFilename));
                    sceneGuiState->fbCtx.init = true;
                }

                if(tbs_gui::ShowFileBrowserWidget(&sceneGuiState->fbCtx))
                {
                    if(sceneGuiState->fbCtx.result.dialogResult == tbs_gui::DIALOG_RESULT_Cancel)
                    {
                        addingGltfMesh = false;
                        sceneGuiState->fbCtx.init = false;
                    }
                    else if(sceneGuiState->fbCtx.result.dialogResult == tbs_gui::DIALOG_RESULT_Open)
                    {
                        const char *filename = sceneGuiState->fbCtx.result.fullFilename;
                        temp_memory scratch(appState);
                        file_data selectedFile = Win32ReadAllText(scratch.arena, filename);
                        base_json_object *jsonData = ParseJsonSimple(scratch.arena, &selectedFile);
                        i32 dirOffset = GetParentDirectoryOffset(filename);
                        ASSERT(dirOffset > 0);
                        ASSERT(filename[dirOffset] == '/');
                        gltf_context ctx = {};
                        ctx.arena = scratch.arena;
                        ctx.root = jsonData;
                        RawStringCopy(ctx.path, filename, (size_t)dirOffset + 1);

                        gltf_root gltf = gltf_ProcessGltf(&ctx);

                        ASSERT(gltf.defaultScene >= 0 && gltf.defaultScene < (i64)gltf.scenes.count);
                        for(size_t sIndex = 0; sIndex < gltf.scenes.count; ++sIndex)
                        {
                            scene_node *n = CreateSceneNodeTreeFromGltf(appState, rm, pbrScene, &gltf, (i64)sIndex);
                            sceneState->AddTopLevelNode(n);
                        }
                        sceneGuiState->fbCtx.init = false;
                        addingGltfMesh = false;
                    }
                }
            }


            // TODO(torgrim): Add functionality for renaming elements
            ImGui::BeginChild("left pane", ImVec2(0, 0), true);
            if(sceneState->firstNode != nullptr)
            {
                gui_CreateSceneNodeTreeList(sceneState->firstNode, &selected);

                // TODO(torgrim): Add a debug check here where we check that the selected
                // actually exists in the scene node list.

                if(selected == nullptr)
                {
                    selected = sceneState->firstNode;
                }
            }
            else
            {
                selected = nullptr;
            }

            ImGui::EndChild();

            ImGui::End();
        }

        if(ViewMenu_Properties)
        {
            ImGui::Begin("Properties", &ViewMenu_Properties);

            ASSERT(selected != nullptr || sceneState->firstNode != nullptr);
            if(selected != nullptr)
            {
                ImGui::Text("GroupID: %llu", selected->groupID);
                if(selected->type == SCENE_NODE_TYPE_Parent)
                {
                    ImGui::SetNextItemOpen(true);
                    if(ImGui::CollapsingHeader("Transform"))
                    {
                        ImGui::DragFloat3("Position", &selected->transform.translation[0]);
                        ImGui::DragFloat3("Rotation", &selected->transform.rotation[0]);
                        ImGui::DragFloat3("Scale", &selected->transform.scale[0]);
                    }

                }
                else if(selected->type == SCENE_NODE_TYPE_Mesh)
                {
                    auto mesh = pbrScene->GetMesh(tbs_opengl::VoidToHandle(selected->handle));
                    ASSERT(mesh != nullptr);

                    if(ImGui::CollapsingHeader("Primitive List"))
                    {
                        for(u32 pIndex = 0; pIndex < mesh->primitives.count; ++pIndex)
                        {
                            auto p = rm->GetPrimitive(mesh->primitives.data[pIndex].renderDataHandle);
                            auto mat = pbrScene->GetMaterial(mesh->primitives.data[pIndex].materialHandle);
                            ASSERT(mat != nullptr);
                            ImGui::PushID(p);
                            if(ImGui::TreeNode("Primitive"))
                            {
                                if(ImGui::BeginTabBar("prop_primitive_tab_bar"))
                                {
                                    if(ImGui::BeginTabItem("Primitive Info"))
                                    {
                                        ImGui::Text("Vertex Count %u", p->vertexCount); 
                                        ImGui::Text("Index Count %u", p->indexCount); 
                                        ImGui::EndTabItem();
                                    }
                                    if(ImGui::BeginTabItem("Primitive Material"))
                                    {
                                        float bc[] =
                                        {
                                            mat->baseColor.x,
                                            mat->baseColor.y,
                                            mat->baseColor.z,
                                            mat->baseColor.w
                                        };

                                        // NOTE(torgrim): Currently we don't allow editing of these
                                        // properties because in the current system one material
                                        // may be shared between multiple primitives. If we want to
                                        // be able to directly manipulate these, we should make all materials
                                        // have a one to one relationship with primitives.
                                        ImGui::ColorEdit4("Base Color", bc);
                                        ImGui::Text("Metallic Factor: %f", (f64)mat->metallicFactor);
                                        ImGui::Text("Rougness Factor: %f", (f64)mat->roughnessFactor);
                                        ImGui::Text("Emissive Factor: %f", (f64)mat->roughnessFactor);
                                        ImGui::Text("Normal Scale Factor: %f", (f64)mat->normalScaleFactor);
                                        ImGui::Text("Occlusion Strengh Factor: %f", (f64)mat->occlusionStrengthFactor);
                                        ImGui::Text("Alpha Cutoff: %f", (f64)mat->alphaCutoff);

                                        for(u32 tIndex = 0; tIndex < ARRAY_COUNT(mat->textures); ++tIndex)
                                        {
                                            auto texHandle = mat->textures[tIndex];
                                            auto texInfo = rm->GetTexture(texHandle);
                                            ImVec2 previewSize = ImVec2(128, 128);
                                            if(texInfo != nullptr && texInfo->loaded)
                                            {
                                                ImGui::Separator();
                                                gui_CreateTexturePreview(texInfo, previewSize);
                                                ImGui::Text(guiPbrTextureNames[tIndex]);
                                            }
                                        }

                                        ImGui::EndTabItem();
                                    }

                                    ImGui::EndTabBar();
                                }

                                ImGui::TreePop();
                            }

                            ImGui::PopID();
                        }
                    }

                    if(ImGui::CollapsingHeader("Transform##mesh_transform"))
                    {
                        ImGui::DragFloat3("Position", &selected->transform.translation[0]);
                        ImGui::DragFloat3("Rotation", &selected->transform.rotation[0]);
                        ImGui::DragFloat3("Scale", &selected->transform.scale[0]);
                    }

                    // TODO: Create a better version of this.
                    // Should also calculate the bounding box/center of the object
                    // on load instead of each time focus is given.
#if 0
                    if(ImGui::Button("Focus"))
                    {
                        matrix4D t = sceneState->CalculateCompleteNodeTransform(selected);
                        temp_memory scratch(appState);
                        // TODO(torgrim): For meshes with multiple primitives we need to
                        // collect all vertices from the mesh before processing.
                        auto p = r->GetPrimitive(mesh->primitives.data[0].renderDataHandle);
                        glBindBuffer(GL_ARRAY_BUFFER, p->posVBOID);
                        v3 *mappedVertices = (v3 *)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
                        v3 *vertices = nullptr;
                        i32 vertexCount = 0;
                        if(p->indexCount == 0)
                        {
                            vertices = AllocArray(scratch.arena, v3, p->vertexCount);
                            memcpy(vertices, mappedVertices, sizeof(v3) * p->vertexCount);
                            vertexCount = (i32)p->vertexCount;
                        }
                        else
                        {
                            vertices = AllocArray(scratch.arena, v3, p->indexCount);
                            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, p->indEBOID);
                            u16 *mappedIndices = (u16 *)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_READ_ONLY);
                            if(p->indexDataType == GL_UNSIGNED_SHORT)
                            {
                                for(u32 vIndex = 0; vIndex < p->indexCount; ++vIndex)
                                {
                                    vertices[vIndex] = mappedVertices[mappedIndices[vIndex]];
                                }
                            }
                            else
                            {
                                for(u32 vIndex = 0; vIndex < p->indexCount; ++vIndex)
                                {
                                    vertices[vIndex] = mappedVertices[((u32 *)mappedIndices)[vIndex]];
                                }
                            }

                            vertexCount = (i32)p->indexCount;

                            glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
                            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                        }

                        for(i32 vIndex = 0; vIndex < vertexCount; ++vIndex)
                        {
                            v3 vert = vertices[vIndex];
                            vertices[vIndex] = V4ToV3(t * v4(vert.x, vert.y, vert.z, 1.0f));
                        }

                        glUnmapBuffer(GL_ARRAY_BUFFER);
                        glBindBuffer(GL_ARRAY_BUFFER, 0);

                        i32 constexpr kDirectionCount = 13;
                        static const f32 directions[kDirectionCount][3] =
                        {
                            {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f},
                            {1.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 1.0f},
                            {1.0f, -1.0f, 0.0f}, {1.0f, 0.0f, -1.0f}, {0.0f, 1.0f, -1.0f},
                            {1.0f, 1.0f, 1.0f}, {1.0f, -1.0f, 1.0f}, {1.0f, 1.0f, -1.0f},
                            {1.0f, -1.0f, -1.0f},
                        };

                        f32 dMin[kDirectionCount];
                        f32 dMax[kDirectionCount];
                        i32 iMin[kDirectionCount];
                        i32 iMax[kDirectionCount];

                        for(i32 j = 0; j < kDirectionCount; ++j)
                        {
                            const f32 *u = directions[j];
                            dMin[j] = dMax[j] = u[0] * vertices[0].x + u[1] * vertices[0].y + u[2] * vertices[0].z;
                            iMin[j] = iMax[j] = 0;

                            for(i32 i = 1; i < vertexCount; ++i)
                            {
                                v3 vert = vertices[i];
                                f32 d = u[0] * vert.x + u[1] * vert.y + u[2] * vert.z;
                                if(d < dMin[j])
                                {
                                    dMin[j] = d;
                                    iMin[j] = i;
                                }
                                else if(d > dMax[j])
                                {
                                    dMax[j] = d;
                                    iMax[j] = i;
                                }
                            }
                        }

                        f32 d2 = LengthSquared(vertices[iMax[0]] - vertices[iMin[0]]);
                        i32 k = 0;
                        for(i32 j = 1; j < kDirectionCount; ++j)
                        {
                            v3 vertMax = vertices[iMax[j]];
                            v3 vertMin = vertices[iMin[j]];
                            f32 m2 = LengthSquared(vertMax - vertMin);
                            if(m2 > d2)
                            {
                                d2 = m2;
                                k = j;
                            }
                        }

                        v3 a = vertices[iMin[k]];
                        v3 b = vertices[iMax[k]];

                        v3 center = (a + b) * 0.5f;
                        f32 radius = SquareRoot(d2) * 0.5f;

                        for(i32 vIndex = 0; vIndex < vertexCount; ++vIndex)
                        {
                            v3 pv = vertices[vIndex] - center;
                            f32 m2 = LengthSquared(pv);
                            if(m2 > radius * radius)
                            {
                                v3 q = center - (pv * (radius / SquareRoot(m2)));
                                center = (q + vertices[vIndex]) * 0.5f;
                                radius = Length(q - center);
                            }
                        }

                        sceneState->editorCamera.target = center;
                        sceneState->editorCamera.hasFocus = true;
                    }

                    if(ImGui::Button("Calculate Bounding Sphere"))
                    {
                        matrix4D t = sceneState->CalculateCompleteNodeTransform(selected);
                        temp_memory scratch(appState);
                        // TODO(torgrim): For meshes with multiple primitives we need to
                        // collect all vertices from the mesh before processing.
                        ASSERT(mesh->primitives.count == 1);
                        auto p = r->GetPrimitive(mesh->primitives.data[0].renderDataHandle);
                        glBindBuffer(GL_ARRAY_BUFFER, p->posVBOID);
                        v3 *mappedVertices = (v3 *)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
                        v3 *vertices = nullptr;
                        i32 vertexCount = 0;
                        if(p->indexCount == 0)
                        {
                            vertices = AllocArray(scratch.arena, v3, p->vertexCount);
                            memcpy(vertices, mappedVertices, sizeof(v3) * p->vertexCount);
                            vertexCount = (i32)p->vertexCount;
                        }
                        else
                        {
                            vertices = AllocArray(scratch.arena, v3, p->indexCount);
                            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, p->indEBOID);
                            u16 *mappedIndices = (u16 *)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_READ_ONLY);
                            if(p->indexDataType == GL_UNSIGNED_SHORT)
                            {
                                for(u32 vIndex = 0; vIndex < p->indexCount; ++vIndex)
                                {
                                    vertices[vIndex] = mappedVertices[mappedIndices[vIndex]];
                                }
                            }
                            else
                            {
                                for(u32 vIndex = 0; vIndex < p->indexCount; ++vIndex)
                                {
                                    vertices[vIndex] = mappedVertices[((u32 *)mappedIndices)[vIndex]];
                                }
                            }

                            vertexCount = (i32)p->indexCount;

                            glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
                            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                        }

                        for(i32 vIndex = 0; vIndex < vertexCount; ++vIndex)
                        {
                            v3 vert = vertices[vIndex];
                            vertices[vIndex] = V4ToV3(t * v4(vert.x, vert.y, vert.z, 1.0f));
                        }

                        glUnmapBuffer(GL_ARRAY_BUFFER);
                        glBindBuffer(GL_ARRAY_BUFFER, 0);

                        i32 constexpr kDirectionCount = 13;
                        static const f32 directions[kDirectionCount][3] =
                        {
                            {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f},
                            {1.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 1.0f},
                            {1.0f, -1.0f, 0.0f}, {1.0f, 0.0f, -1.0f}, {0.0f, 1.0f, -1.0f},
                            {1.0f, 1.0f, 1.0f}, {1.0f, -1.0f, 1.0f}, {1.0f, 1.0f, -1.0f},
                            {1.0f, -1.0f, -1.0f},
                        };

                        f32 dMin[kDirectionCount];
                        f32 dMax[kDirectionCount];
                        i32 iMin[kDirectionCount];
                        i32 iMax[kDirectionCount];

                        for(i32 j = 0; j < kDirectionCount; ++j)
                        {
                            const f32 *u = directions[j];
                            dMin[j] = dMax[j] = u[0] * vertices[0].x + u[1] * vertices[0].y + u[2] * vertices[0].z;
                            iMin[j] = iMax[j] = 0;

                            for(i32 i = 1; i < vertexCount; ++i)
                            {
                                v3 vert = vertices[i];
                                f32 d = u[0] * vert.x + u[1] * vert.y + u[2] * vert.z;
                                if(d < dMin[j])
                                {
                                    dMin[j] = d;
                                    iMin[j] = i;
                                }
                                else if(d > dMax[j])
                                {
                                    dMax[j] = d;
                                    iMax[j] = i;
                                }
                            }
                        }


                        f32 d2 = LengthSquared(vertices[iMax[0]] - vertices[iMin[0]]);
                        i32 k = 0;
                        for(i32 j = 1; j < kDirectionCount; ++j)
                        {
                            v3 vertMax = vertices[iMax[j]];
                            v3 vertMin = vertices[iMin[j]];
                            f32 m2 = LengthSquared(vertMax - vertMin);
                            if(m2 > d2)
                            {
                                d2 = m2;
                                k = j;
                            }
                        }

                        v3 a = vertices[iMin[k]];
                        v3 b = vertices[iMax[k]];


                        v3 center = (a + b) * 0.5f;
                        f32 radius = SquareRoot(d2) * 0.5f;

                        sceneState->boundingSphereO[sceneState->boundingSphereCount] = center;
                        sceneState->boundingSphereR[sceneState->boundingSphereCount] = radius;
                        sceneState->boundingSphereColor[sceneState->boundingSphereCount] = v3(1.0f, 1.0f, 0.0f);
                        sceneState->boundingSphereCount++;

                        for(i32 vIndex = 0; vIndex < vertexCount; ++vIndex)
                        {
                            v3 pv = vertices[vIndex] - center;
                            f32 m2 = LengthSquared(pv);
                            if(m2 > radius * radius)
                            {
                                v3 q = center - (pv * (radius / SquareRoot(m2)));
                                center = (q + vertices[vIndex]) * 0.5f;
                                radius = Length(q - center);
                            }
                        }

                        sceneState->boundingSphereO[sceneState->boundingSphereCount] = center;
                        sceneState->boundingSphereR[sceneState->boundingSphereCount] = radius;
                        sceneState->boundingSphereColor[sceneState->boundingSphereCount] = v3(1.0f, 0.0f, 1.0f);
                        sceneState->boundingSphereCount++;

                        PrintDebugMsg("Primitive Diameter Squared: %f\n", d2);
                        PrintDebugMsg("Center: %f, %f, %f\n", center.x, center.y, center.z);
                        PrintDebugMsg("Radius: %f\n", radius);
                    }
#endif
                }
                else if(selected->type == SCENE_NODE_TYPE_Light)
                {
                    light_data *light = pbrScene->GetLight(tbs_opengl::VoidToHandle(selected->handle));
                    if(ImGui::CollapsingHeader("Transform##light_transform"))
                    {
                        ImGui::DragFloat3("Position", &light->position[0]);
                        ImGui::DragFloat3("Rotation", &light->rotation[0]);
                    }

                    ImGui::Checkbox("Enabled", &light->isEnabled);

                    if(light->type == LIGHT_TYPE_Point || light->type == LIGHT_TYPE_Spot)
                    {
                        if(ImGui::CollapsingHeader("Distances Attenuation"))
                        {
                            if(light->distAttFunc == DISTANCE_ATTENUATION_FUNCTION_InvSqrSim)
                            {
                                // TODO(torgrim): Set this disabled instead when attfunc != invsqr?
                                ImGui::DragFloat("Falloff Start", &light->falloffStartDist);
                            }
                            ImGui::DragFloat("Max Distance", &light->maxDist);

                            if(ImGui::BeginCombo("Function", guiAttenuationFunctionNames[light->distAttFunc]))
                            {
                                for(size_t funcIndex = 0; funcIndex < ARRAY_COUNT(guiAttenuationFunctionNames); ++funcIndex)
                                {
                                    if(ImGui::Selectable(guiAttenuationFunctionNames[funcIndex], light->distAttFunc == funcIndex))
                                    {
                                        light->distAttFunc = (distance_attenuation_function)funcIndex;
                                    }
                                }

                                ImGui::EndCombo();
                            }
                        }

                        if(light->type == LIGHT_TYPE_Spot)
                        {
                            if(ImGui::CollapsingHeader("Angle Attenuation"))
                            {
                                // TODO(torgrim): Should we check that values are inside the bounds
                                // after they are changed since dragfloat doesn't clamp manual values?
                                // We also probably want to check if min < max although this can make
                                // editing a bit tedious because if the use want a min > current max
                                // they need to edit the max value first?
                                ImGui::DragFloat("Minimum Angle", &light->spotMinCutoff, 1.0f, 0.0f, 90.0f);
                                ImGui::DragFloat("Maximum Angle", &light->spotMaxCutoff, 1.0f, 0.0f, 90.0f);
                            }
                        }
                    }

                    if(ImGui::CollapsingHeader("Color & Strength##light_color_and_strength"))
                    {
                        ImGui::ColorEdit3("Color##light_color_edit", &light->color[0], ImGuiColorEditFlags_Float);
                        ImGui::SliderFloat("Strength##light_strength", &light->strength, 1.0f, 1000.0f);
                    }

                    if(ImGui::CollapsingHeader("Shadow Maps"))
                    {

                        // TODO(torgrim): Implement this when we upgrade to opengl 4.5 since
                        // we then can create texture alias which makes cube map face rendering
                        // a lot easier.
                    }
                }
                else if(selected->type == SCENE_NODE_TYPE_Camera)
                {
                    // NOTE: camera transform is controlled by the node.
                    // The only camera where this the case is the editor camera
                    // but its not part of the scene anyway.
                    simple_camera *cam = sceneState->cameras + (u32)(uintptr_t)selected->handle;
                    ImGui::DragFloat3("Position", &cam->pos[0]);
                    ImGui::DragFloat3("Rotation", &cam->rot[0]);
                    ImGui::DragFloat("Phi", &cam->phi);
                    ImGui::DragFloat("Theta", &cam->theta);

                    ImGui::DragFloat("Aspect", &cam->aspect);
                    ImGui::DragFloat("Near Clip", &cam->clipNear);
                    ImGui::DragFloat("Far Clip", &cam->clipFar);
                    ImGui::DragFloat("Field of View", &cam->fov);
                }
            }

            ImGui::End();
        }

        if(sceneState->cameraCount > 0)
        {
            if(ImGui::Begin("Camera View"))
            {
                ImGui::Text("TODO");
            }

            ImGui::End();
        }

#endif

        // TODO(torgrim): Here we want some kind of notification/message overlay where
        // we can show invalid states/missing data etc(like using env map lighting but no env map set).

        ImGui::End();

        ImGui::Render();

        ImDrawData *imDD = ImGui::GetDrawData();
        tbs_gui::DefaultRenderImGuiDrawData(appState, imGuiState, rm, imDD);
    }
}
