/* date = September 15th 2020 9:31 pm */
#include "tbs_image.h"

#ifndef TBS_TEXTURE_WORKER_H

// TODO(torgrim): Make this more data agnostic since neither the queue nor the worker
// care about the data.
// Also note that we are using win32 specific types here.

constexpr size_t MAX_TEXTURE_QUEUE_COUNT = 1000;
#define TEXTURE_WORKER_HANDLER(name) unsigned __stdcall name(void *params)
typedef TEXTURE_WORKER_HANDLER(texture_worker_handler);

struct texture_queue_data
{
    void *handle;
    char *path;

    // NOTE: This is only part of the result.
    image_data image;
};

struct texture_worker_result
{
    bool hasValue;
    texture_queue_data data;
};

struct texture_queue
{
    texture_queue_data buff[MAX_TEXTURE_QUEUE_COUNT];
    size_t front;
    size_t back;
    size_t count;

    void Enqueue(void *handle, char *path)
    {
        Enqueue({handle, path, {}});
    }

    void Enqueue(texture_queue_data data)
    {
        ASSERT(count < MAX_TEXTURE_QUEUE_COUNT);
        buff[back++] = data;
        if(back == MAX_TEXTURE_QUEUE_COUNT)
        {
            back = 0;
        }

        ++count;
    }

    texture_queue_data Dequeue()
    {
        ASSERT(count > 0);
        texture_queue_data result = buff[front];
        buff[front++] = {};
        if(front == MAX_TEXTURE_QUEUE_COUNT)
        {
            front = 0;
        }

        --count;

        return result;
    }
};

struct texture_worker
{
    texture_queue inQueue;
    texture_queue outQueue;
    HANDLE inQueueSem;
    HANDLE outQueueSem;
    HANDLE inQueueMtx;
    HANDLE outQueueMtx;

    uintptr_t threadHandle;
};

#define TBS_TEXTURE_WORKER_H

#endif //TBS_TEXTURE_WORKER_H
