#include "tbs_light.h"
// TODO(torgrim): Put in opengl specific file?
internal matrix4D CreatePointLightCameraMatrix(GLenum face, v3 pos)
{
    matrix4D result = CreateZeroMatrix4D();
    switch(face)
    {
        case GL_TEXTURE_CUBE_MAP_POSITIVE_X:
        {
            result = matrix4D( 0,  0, -1, 0,
                               0, -1,  0, 0,
                              -1,  0,  0, 0,
                               0,  0,  0, 1);
        } break;
        case GL_TEXTURE_CUBE_MAP_NEGATIVE_X:
        {
            result = matrix4D( 0,  0,  1, 0,
                               0, -1,  0, 0,
                               1,  0,  0, 0,
                               0,  0,  0, 1);
        } break;
        case GL_TEXTURE_CUBE_MAP_POSITIVE_Y:
        {
            result = matrix4D( 1,  0,  0, 0,
                               0,  0,  1, 0,
                               0, -1,  0, 0,
                               0,  0,  0, 1);
        } break;
        case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y:
        {
            result = matrix4D( 1,  0,  0, 0,
                               0,  0, -1, 0,
                               0,  1,  0, 0,
                               0,  0,  0, 1);
        } break;
        case GL_TEXTURE_CUBE_MAP_POSITIVE_Z:
        {
            result = matrix4D( 1,  0,  0, 0,
                               0, -1,  0, 0,
                               0,  0, -1, 0,
                               0,  0,  0, 1);
        } break;
        case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z:
        {
            result = matrix4D(-1,  0,  0, 0,
                               0, -1,  0, 0,
                               0,  0,  1, 0,
                               0,  0,  0, 1);
        } break;
        default:
        {
            ASSERT(false);
        }
    }

    pos = -pos;
    result = result * CreateTranslate(pos);

    return result;
}

internal light_data CreateLight(light_type type)
{
    light_data light = {};

    v3 defaultRotation = v3(0.0f, 0.0f, 0.0f);
    v3 defaultPosition = v3(0.0f, 0.0f, 0.0f);
    v3 defaultColor = v3(1.0f, 1.0f, 1.0f);

    light.position = defaultPosition;
    light.rotation = defaultRotation;
    light.color = defaultColor;
    light.type = type;
    light.isEnabled = true;
    light.strength = 1.0f;

    switch(type)
    {
        case LIGHT_TYPE_Point:
        {

            light.distAttFunc = DISTANCE_ATTENUATION_FUNCTION_Exp;
            light.maxDist = 15.0f;
            light.falloffStartDist = 1.0f;

        } break;
        case LIGHT_TYPE_Spot:
        {
            light.distAttFunc = DISTANCE_ATTENUATION_FUNCTION_Exp;
            light.maxDist = 15.0f;
            light.falloffStartDist = 1.0f;
            light.spotMinCutoff = 13.0f;
            light.spotMaxCutoff = 18.0f;
        } break;
        case LIGHT_TYPE_Directional:
        {
        } break;
        default:
        {
            ASSERT(false);
        } break;
    }

    return light;
}
