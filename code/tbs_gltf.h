/* date = June 10th 2020 11:25 pm */

#ifndef TBS_GLTF_H

#define GLTF_ANIMATION_INTERP_LINEAR                "LINEAR"
#define GLTF_ANIMATION_INTERP_STEP                  "STEP"
#define GLTF_ANIMATION_INTERP_CUBIC_SPLINE          "CUBICSPLINE"

// TODO: enum?
#define GLTF_BUFFER_VIEW_TARGET_ARRAY_BUFFER            34962
#define GLTF_BUFFER_VIEW_TARGET_ELEMENT_ARRAY_BUFFER    34963

#define GLTF_CAMERA_TYPE_STR_Perspective                "perspective"
#define GLTF_CAMERA_TYPE_STR_Orthographic               "orthographic"

#define GLTF_MIME_TYPE_JPEG                         "image/jpeg"
#define GLTF_MIME_TYPE_PNG                          "image/png"

#define GLTF_ALPHA_MODE_STRING_Opaque               "OPAQUE"
#define GLTF_ALPHA_MODE_STRING_Mask                 "MASK"
#define GLTF_ALPHA_MODE_STRING_Blend                "BLEND"

#define GLTF_TARGET_PATH_TRANSLATION                "translation"
#define GLTF_TARGET_PATH_ROTATION                   "rotation"
#define GLTF_TARGET_PATH_SCALE                      "scale"
#define GLTF_TARGET_PATH_WEIGHTS                    "weights"

#define GLTF_ATTRIBUTE_TYPE_STRING_Scalar           "SCALAR"
#define GLTF_ATTRIBUTE_TYPE_STRING_Vec2             "VEC2"
#define GLTF_ATTRIBUTE_TYPE_STRING_Vec3             "VEC3"
#define GLTF_ATTRIBUTE_TYPE_STRING_Vec4             "VEC4"
#define GLTF_ATTRIBUTE_TYPE_STRING_Mat2             "MAT2"
#define GLTF_ATTRIBUTE_TYPE_STRING_Mat3             "MAT3"
#define GLTF_ATTRIBUTE_TYPE_STRING_Mat4             "MAT4"

#define GLTF_PRIMITIVE_ATTRIBUTE_Position           "POSITION"
#define GLTF_PRIMITIVE_ATTRIBUTE_Normal             "NORMAL"
#define GLTF_PRIMITIVE_ATTRIBUTE_Tangent            "TANGENT"
#define GLTF_PRIMITIVE_ATTRIBUTE_TexCoord0          "TEXCOORD_0"
#define GLTF_PRIMITIVE_ATTRIBUTE_TexCoord1          "TEXCOORD_1"
#define GLTF_PRIMITIVE_ATTRIBUTE_Color0             "COLOR_0"
#define GLTF_PRIMITIVE_ATTRIBUTE_Joints0            "JOINTS_0"
#define GLTF_PRIMITIVE_ATTRIBUTE_Weights0           "WEIGHTS_0"

enum gltf_primitive_mode
{
    GLTF_PRIMITIVE_MODE_Points,
    GLTF_PRIMITIVE_MODE_Lines,
    GLTF_PRIMITIVE_MODE_LineLoop,
    GLTF_PRIMITIVE_MODE_LineStrip,
    GLTF_PRIMITIVE_MODE_Triangles,
    GLTF_PRIMITIVE_MODE_TriangleStrip,
    GLTF_PRIMITIVE_MODE_TriangleFan,
};

enum gltf_sampler_wrap_type
{
    GLTF_SAMPLER_WRAP_TYPE_None             = 0,
    GLTF_SAMPLER_WRAP_TYPE_ClampToEdge      = 33071,
    GLTF_SAMPLER_WRAP_TYPE_MirroredRepeat   = 33648,
    GLTF_SAMPLER_WRAP_TYPE_Repeat           = 10497,
};

enum gltf_sampler_filter_type
{
    GLTF_SAMPLER_FILTER_TYPE_None                   = 0,
    GLTF_SAMPLER_FILTER_TYPE_Nearest                = 9728,
    GLTF_SAMPLER_FILTER_TYPE_Linear                 = 9729,
    GLTF_SAMPLER_FILTER_TYPE_NearestMipmapNearest   = 9984,
    GLTF_SAMPLER_FILTER_TYPE_LinearMipmapNearest    = 9985,
    GLTF_SAMPLER_FILTER_TYPE_NearestMipmapLinear    = 9986,
    GLTF_SAMPLER_FILTER_TYPE_LinearMipmapLinear     = 9987,
};

enum gltf_material_alpha_mode
{
    GLTF_MATERIAL_ALPHA_MODE_Opaque,
    GLTF_MATERIAL_ALPHA_MODE_Mask,
    GLTF_MATERIAL_ALPHA_MODE_Blend,
};


enum gltf_component_type
{
    GLTF_COMPONENT_TYPE_None                = 0,
    GLTF_COMPONENT_TYPE_Byte                = 5120,
    GLTF_COMPONENT_TYPE_UByte               = 5121,
    GLTF_COMPONENT_TYPE_Short               = 5122,
    GLTF_COMPONENT_TYPE_UShort              = 5123,
    GLTF_COMPONENT_TYPE_UInt                = 5125,
    GLTF_COMPONENT_TYPE_Float               = 5126,
};

enum gltf_internal_attribute_type
{
    GLTF_INTERNAL_ATTRIBUTE_TYPE_None = 0,
    GLTF_INTERNAL_ATTRIBUTE_TYPE_Scalar,
    GLTF_INTERNAL_ATTRIBUTE_TYPE_Vec2,
    GLTF_INTERNAL_ATTRIBUTE_TYPE_Vec3,
    GLTF_INTERNAL_ATTRIBUTE_TYPE_Vec4,
    GLTF_INTERNAL_ATTRIBUTE_TYPE_Mat2,
    GLTF_INTERNAL_ATTRIBUTE_TYPE_Mat3,
    GLTF_INTERNAL_ATTRIBUTE_TYPE_Mat4,

    GLTF_INTERNAL_ATTRIBUTE_TYPE_Count,
};


enum gltf_buffer_view_target
{
    GLTF_BUFFER_VIEW_TARGET_None = 0,
    GLTF_BUFFER_VIEW_TARGET_ArrayBuffer = 34962,
    GLTF_BUFFER_VIEW_TARGET_ElementArrayBuffer = 34963,
};

struct gltf_buffer_cache_data
{
    u8 *data;
    size_t size;
    bool loaded;
};

struct gltf_context
{
    memory_arena *arena;
    char path[MAX_PATH];
    base_json_object *root;
    tbs_array_sr<gltf_buffer_cache_data> bufferCache;
};

// TODO(torgrim): Not really a correct classification anymore
// since parents can also contain meshes etc. Find a better system
// instead.
enum gltf_node_type
{
    GLTF_NODE_TYPE_None,
    GLTF_NODE_TYPE_Mesh,
    GLTF_NODE_TYPE_Parent,
};

struct gltf_buffer_data
{
    u8 *data;
    size_t size;
    gltf_component_type componentType;
    i64 count;
};

struct gltf_asset
{
    char *copyright;
    char *generator;
    char *version;
    char *minVersion;
};

struct gltf_buffer_view
{
    i64 bufferIndex;
    i64 byteOffset;
    i64 byteLength;
    i64 byteStride;
    gltf_buffer_view_target target;
};

enum gltf_camera_type
{
    GLTF_CAMERA_TYPE_None,
    GLTF_CAMERA_TYPE_Perspective,
    GLTF_CAMERA_TYPE_Orthographic,
};

struct gltf_camera
{
    f64 aspectRatio;
    f64 yFov;
    f64 xMag;
    f64 yMag;
    f64 zFar;
    f64 zNear;

    gltf_camera_type type;
    char *name;
};

struct gltf_image
{
    // TODO: needs buffer data(if present else use uri)
    char *uri;
    char *mimeType;
    char *name;
    gltf_buffer_data imageBuffer;
};


struct gltf_primitive
{
    gltf_buffer_data positions;
    gltf_buffer_data normals;
    gltf_buffer_data tangents;
    gltf_buffer_data uv0;

    // TODO(torgrim): Not supported at the moment.
    //gltf_buffer_data uv_set_2;
    //gltf_buffer_data colors;
    //gltf_buffer_data joints;
    //gltf_buffer_data weights;

    gltf_buffer_data indices;
    i64 materialIndex;
    gltf_primitive_mode mode;

    // TODO(torgrim): Support morph targets
};

struct gltf_mesh
{
    tbs_array_sr<gltf_primitive> primitives;
    tbs_array_sr<f64> weights;
    char *name;
};

struct gltf_node
{
    i64 cameraIndex;
    tbs_array_sr<i64> children;
    i64 skinIndex;
    // TODO(torgrim): Do we always want to convert this to separate values?
    matrix4D matrix;
    i64 meshIndex;
    v3 rotation;
    v3 scale;
    v3 translation;
    tbs_array_sr<f64> weights;
    char *name;
};

struct gltf_scene
{
    tbs_array_sr<i64> rootNodes;
    char *name;
};

struct gltf_sampler
{
    char *name;

    gltf_sampler_filter_type magFilter;
    gltf_sampler_filter_type minFilter;
    gltf_sampler_wrap_type wrapS;
    gltf_sampler_wrap_type wrapT;
};

struct gltf_texture
{
    i64 samplerIndex;
    i64 imageIndex;
    char *name;
};

struct gltf_texture_info
{
    i64 texIndex;
    i64 texCoordsIndex;
};

struct gltf_pbr_metallic_roughness
{
    v4 baseColorFactor;
    gltf_texture_info baseColorTexture;
    f64 metallicFactor;
    f64 roughnessFactor;
    gltf_texture_info metallicRoughnessTexture;
};

struct gltf_material
{
    i64 normalTextureIndex;
    i64 normalTexCoordsIndex;
    f64 normalScaleFactor;

    i64 occlusionTextureIndex;
    i64 occlusionTexCoordsIndex;
    f64 occlusionStrengthFactor;

    gltf_pbr_metallic_roughness pbr;
    gltf_texture_info emissiveTexture;
    v3 emissiveFactor;
    gltf_material_alpha_mode alphaMode;
    f64 alphaCutoff;
    bool doubleSided;
    char *name;
};

struct gltf_root
{
    gltf_asset asset;
    tbs_array_sr<gltf_camera> cameras;
    tbs_array_sr<gltf_image> images;
    tbs_array_sr<gltf_mesh> meshes;
    tbs_array_sr<gltf_node> nodes;
    tbs_array_sr<gltf_scene> scenes;
    i64 defaultScene;
    tbs_array_sr<gltf_texture> textures;
    tbs_array_sr<gltf_sampler> samplers;
    tbs_array_sr<gltf_material> materials;

    // TODO:
    // tbs_array_sr<gltf_animation> animations;
    // tbs_array_sr<gltf_skin> skins;
};

const size_t gltf_component_type_byte_sizes[] =
{
    1,
    1,
    2,
    2,
    0,
    4,
    4,
};

const size_t gltf_internal_attribute_type_count[] =
{

    0,
    1,
    2,
    3,
    4,
    4,
    9,
    16,

    0,
};

#define TBS_GLTF_H
#endif //TBS_GLTF_H
