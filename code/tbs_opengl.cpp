#include "tbs_opengl.h"

internal inline GLint OpenGLGetVarI(GLenum id)
{
    GLint result;
    glGetIntegerv(id, &result);
    return result;
}

internal inline GLfloat OpenGLGetVarF(GLenum id)
{
    GLfloat result;
    glGetFloatv(id, &result);
    return result;
}

internal inline GLboolean OpenGLGetVarB(GLenum id)
{
    GLboolean result;
    glGetBooleanv(id, &result);
    return result;
}

internal void OpenGLResizeFramebufferAttachments(GLuint framebufferID, GLsizei width, GLsizei height, GLint textureFormat)
{
    ASSERT(framebufferID > 0);
    ASSERT(width > 0);
    ASSERT(height > 0);
    ASSERT(glIsFramebuffer(framebufferID));

    GLint currentDrawFbo;
    GLint currentReadFbo;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentDrawFbo);
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentReadFbo);

    glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);

    GLint colorType;
    GLint depthType;
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &colorType);
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &depthType);

    ASSERT(colorType == GL_TEXTURE);
    ASSERT(depthType == GL_RENDERBUFFER);

    GLint texID;
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &texID);

    GLint rboID;
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &rboID);

    glBindTexture(GL_TEXTURE_2D, (GLuint)texID);
    glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

    glBindRenderbuffer(GL_RENDERBUFFER, (GLuint)rboID);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

    OpenGLCheckFramebufferStatus(GL_READ_FRAMEBUFFER);
    OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, (GLuint)currentDrawFbo);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, (GLuint)currentReadFbo);
}

internal void OpenGLResizeFramebufferAttachmentsMsaa(GLuint framebufferID, GLsizei width, GLsizei height, GLenum textureFormat)
{
    ASSERT(framebufferID > 0);
    ASSERT(width > 0);
    ASSERT(height > 0);
    ASSERT(glIsFramebuffer(framebufferID));

    GLint currentDrawFbo;
    GLint currentReadFbo;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentDrawFbo);
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentReadFbo);

    glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);

    GLint colorType;
    GLint depthType;
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &colorType);
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &depthType);

    ASSERT(colorType == GL_TEXTURE);
    ASSERT(depthType == GL_RENDERBUFFER);

    GLint texID;
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &texID);

    GLint rboID;
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &rboID);

    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, (GLuint)texID);
    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, textureFormat, width, height, true);

    glBindRenderbuffer(GL_RENDERBUFFER, (GLuint)rboID);
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, 8, GL_DEPTH24_STENCIL8, width, height);

    OpenGLCheckFramebufferStatus(GL_READ_FRAMEBUFFER);
    OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, (GLuint)currentDrawFbo);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, (GLuint)currentReadFbo);
}

internal GLuint OpenGLGetFramebufferColorAttachment(GLuint framebufferID)
{
    ASSERT(framebufferID > 0);
    ASSERT(glIsFramebuffer(framebufferID));

    GLint currentDrawFbo;
    GLint currentReadFbo;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentDrawFbo);
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentReadFbo);

    glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);

    GLint colorType;
    GLint depthType;
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &colorType);
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &depthType);

    ASSERT(colorType == GL_TEXTURE);
    ASSERT(depthType == GL_RENDERBUFFER);

    glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);
    GLint texID;
    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &texID);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, (GLuint)currentDrawFbo);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, (GLuint)currentReadFbo);

    ASSERT(texID > 0);
    return (GLuint)texID;
}

internal GLuint OpenGLCreateShadowFramebuffer()
{
    GLuint fbo;
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return fbo;
}

internal GLuint OpenGLCreateFramebufferHDR()
{
    GLuint fbo;
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER,
                              rbo);

    OpenGLCheckFramebufferStatus(GL_READ_FRAMEBUFFER);
    OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return fbo;
}

internal GLuint OpenGLCreateFramebufferMsaa(GLsizei width, GLsizei height, GLenum textureFormat)
{
    GLuint fbo;
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    GLuint fboTexID;
    glGenTextures(1, &fboTexID);

    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, fboTexID);

    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, textureFormat, width, height, true);

    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, 8, GL_DEPTH24_STENCIL8, width, height);

    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,
                           GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D_MULTISAMPLE,
                           fboTexID,
                           0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_DEPTH_STENCIL_ATTACHMENT,
                              GL_RENDERBUFFER,
                              rbo);


    DEBUG_OpenGLCheckError();

    OpenGLCheckFramebufferStatus(GL_READ_FRAMEBUFFER);
    OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return fbo;
}

internal GLuint OpenGLCreateFramebufferDefault(GLsizei width, GLsizei height, GLint textureFormat)
{
    GLuint fbo;
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    GLuint fboTexID;
    glGenTextures(1, &fboTexID);

    glBindTexture(GL_TEXTURE_2D, fboTexID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, textureFormat,
                 width, height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

    glBindTexture(GL_TEXTURE_2D, 0);

    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,
                           GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D,
                           fboTexID,
                           0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_DEPTH_STENCIL_ATTACHMENT,
                              GL_RENDERBUFFER,
                              rbo);

    OpenGLCheckFramebufferStatus(GL_READ_FRAMEBUFFER);
    OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return fbo;
}

internal GLuint OpenGLCreateShadowMapTexture2DArray(GLsizei width, GLsizei height, i32 count)
{
    f32 shadowMapBorderColor[] = {1, 1, 1, 1};
    GLuint texID;
    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D_ARRAY, texID);

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glTexParameterfv(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BORDER_COLOR, shadowMapBorderColor);

    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_DEPTH_COMPONENT32, width, height, count, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

    return texID;
}

internal void OpenGLCreateShadowMapTextureCubeMap(GLsizei width, GLsizei height, i32 count, GLuint *result)
{
    // NOTE(torgrim): Point shadow map texture initialization
    for(i32 i = 0; i < count; ++i)
    {
        GLuint texID;
        glGenTextures(1, &texID);
        glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        for(i32 fIndex = GL_TEXTURE_CUBE_MAP_POSITIVE_X; fIndex <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++fIndex)
        {
            glTexImage2D((GLenum)fIndex, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);
        }

        result[i] = texID;
    }
}

internal inline GLenum OpenGLGetTextureFormatFromChannelCount(i32 count)
{
    GLenum result = GL_NONE;
    switch(count)
    {
        case 1:
            {
                result = GL_RED;
            }break;
        case 3:
            {
                result = GL_RGB;
            }break;
        case 4:
            {
                result = GL_RGBA;
            }break;
        default:
            {
                ASSERT(false);
            }
    }

    return result;
}

internal GLuint OpenGLCreateNDCQuad()
{
    const f32 vertAttribs[] =
    {
        -1.0f, -1.0f, 0.0f, 0.0f,
         1.0f, -1.0f, 1.0f, 0.0f,
         1.0f,  1.0f, 1.0f, 1.0f,

         1.0f,  1.0f, 1.0f, 1.0f,
        -1.0f,  1.0f, 0.0f, 1.0f,
        -1.0f, -1.0f, 0.0f, 0.0f,
    };

    GLuint VAOID;
    GLuint VBOID;
    glGenVertexArrays(1, &VAOID);
    glGenBuffers(1, &VBOID);
    glBindVertexArray(VAOID);
    glBindBuffer(GL_ARRAY_BUFFER, VBOID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertAttribs), vertAttribs, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(f32), nullptr);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(f32), (void *)(2 * sizeof(f32)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return VAOID;
}

internal inline void OpenGLSetActiveTextureUnit(GLuint unit)
{
    glActiveTexture(GL_TEXTURE0 + unit);
}

// TODO(torgrim): Create a image loader agnostic version of this where
// we just pass in the needed info including the pixels
internal GLuint OpenGLCreateTexture2D(const char *imgPath)
{
    image_data img = LoadImageData(imgPath);

    GLuint texID;
    glGenTextures(1, &texID);
    ASSERT(texID > 0);

    glBindTexture(GL_TEXTURE_2D, texID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLenum imgFormat = GL_RGBA;

    if(img.channelCount == 3)
    {
        imgFormat = GL_RGB;
    }
    else if(img.channelCount == 1)
    {
        imgFormat = GL_RED;
        // TODO(torgrim): do we need to restore previous pack alignment
        // when we are done with this one?
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
    }

    glTexImage2D(GL_TEXTURE_2D, 0, (GLint)imgFormat,
                 img.width, img.height,
                 0, imgFormat, GL_UNSIGNED_BYTE, img.pixels);

    FreeImageData(&img);


    return texID;
}

internal GLuint OpenGLCreateTextureCubemap(const char *leftFace,
                                           const char *rightFace,
                                           const char *topFace,
                                           const char *bottomFace,
                                           const char *backFace,
                                           const char *frontFace)
{

    const char *fileList[] =
    {
        rightFace,
        leftFace,
        topFace,
        bottomFace,
        frontFace,
        backFace,
    };

    GLuint texID;
    glGenTextures(1, &texID);
    ASSERT(texID > 0);

    glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    for(u32 faceIndex = 0; faceIndex < ARRAY_COUNT(fileList); ++faceIndex)
    {
        image_data face = LoadImageData(fileList[faceIndex]);
        ASSERT(face.channelCount == 4 || face.channelCount == 3);


        GLenum imgFormat = GL_RGBA;
        if(face.channelCount == 3)
        {
            imgFormat = GL_RGB;
        }


        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceIndex, 0, (GLint)imgFormat,
                face.width, face.height,
                0, imgFormat, GL_UNSIGNED_BYTE, face.pixels);

        FreeImageData(&face);
    }

    return texID;
}

// TODO(torgrim): have more info here like shaderid(currently not used), filename etc
#ifdef CHECK_FOR_UNKOWN_SHADER_UNIFORM
#define REPORT_UNKOWN_SHADER_UNIFORM(name, id) WriteDebugOutput("uniform not found: "); WriteDebugOutput(name); WriteDebugOutput("\n")
#else
#define REPORT_UNKOWN_SHADER_UNIFORM(name, id)
#endif

// TODO(torgrim): in most cases we want to cache the location
// instead of quering for it each time we set a shader uniform.
// so it would be better to pass in a location instead.
// TODO(torgrim): this is not really part of win32 either so should
// put this into a portable opengl file
internal void OpenGLSetUniform(GLuint shaderID, const char *name, matrix4D *mat)
{
    GLint loc = glGetUniformLocation(shaderID, name);
    if(loc != -1)
    {
        glUniformMatrix4fv(loc, 1, GL_FALSE, &(*mat)(0,0));
    }
    else
    {
        REPORT_UNKOWN_SHADER_UNIFORM(name, shaderID);
    }
}

internal void OpenGLSetUniform(GLuint shaderID, const char *name, v4 *data)
{
    GLint loc = glGetUniformLocation(shaderID, name);
    if(loc != -1)
    {
        glUniform4fv(loc, 1, &((*data)[0]));
    }
    else
    {
        REPORT_UNKOWN_SHADER_UNIFORM(name, shaderID);
    }
}

internal void OpenGLSetUniform(GLuint shaderID, const char *name, v3 *data)
{
    GLint loc = glGetUniformLocation(shaderID, name);
    if(loc != -1)
    {
        glUniform3fv(loc, 1, &((*data)[0]));
    }
    else
    {
        REPORT_UNKOWN_SHADER_UNIFORM(name, shaderID);
    }
}

internal void OpenGLSetUniform(GLuint shaderID, const char *name, v2 *data)
{
    GLint loc = glGetUniformLocation(shaderID, name);
    if(loc != -1)
    {
        glUniform2fv(loc, 1, &((*data)[0]));
    }
    else
    {
        REPORT_UNKOWN_SHADER_UNIFORM(name, shaderID);
    }
}

internal void OpenGLSetUniform(GLuint shaderID, const char *name, f32 a, f32 b)
{
    GLint loc = glGetUniformLocation(shaderID, name);
    if(loc != -1)
    {
        glUniform2f(loc, a, b);
    }
    else
    {
        REPORT_UNKOWN_SHADER_UNIFORM(name, shaderID);
    }
}

internal void OpenGLSetUniform(GLuint shaderID, const char *name, f32 a)
{
    GLint loc = glGetUniformLocation(shaderID, name);
    if(loc != -1)
    {
        glUniform1f(loc, a);
    }
    else
    {
        REPORT_UNKOWN_SHADER_UNIFORM(name, shaderID);
    }
}

internal void OpenGLSetUniform(GLuint shaderID, const char *name, i32 a)
{
    GLint loc = glGetUniformLocation(shaderID, name);
    if(loc != -1)
    {
        glUniform1i(loc, a);
    }
    else
    {
        REPORT_UNKOWN_SHADER_UNIFORM(name, shaderID);
    }
}

internal void OpenGLSetUniform(GLuint shaderID, const char *name, u32 a)
{
    GLint loc = glGetUniformLocation(shaderID, name);
    if(loc != -1)
    {
        glUniform1ui(loc, a);
    }
    else
    {
        REPORT_UNKOWN_SHADER_UNIFORM(name, shaderID);
    }
}

internal void OpenGLRestoreState(opengl_state_data *state)
{
    OPENGL_SET_BOOL_STATE(state->blend, GL_BLEND);
    OPENGL_SET_BOOL_STATE(state->cullFace, GL_CULL_FACE);
    OPENGL_SET_BOOL_STATE(state->depthTest, GL_DEPTH_TEST);
    OPENGL_SET_BOOL_STATE(state->scissorTest, GL_SCISSOR_TEST);

    glBlendFuncSeparate(state->blendSrcRGB,
                        state->blendDstRGB,
                        state->blendSrcAlpha,
                        state->blendDstAlpha);

    glBlendEquationSeparate(state->blendEquationRGB, state->blendEquationAlpha);

    glActiveTexture(state->activeTexture);
    glUseProgram(state->programID);
    glBindTexture(GL_TEXTURE_2D, state->texture2DBinding);

}

internal opengl_state_data OpenGLGetCurrentState()
{
    opengl_state_data state = {};
    state.blend = glIsEnabled(GL_BLEND);
    state.cullFace = glIsEnabled(GL_CULL_FACE);
    state.depthTest = glIsEnabled(GL_DEPTH_TEST);
    state.scissorTest = glIsEnabled(GL_SCISSOR_TEST);
    // NOTE(torgrim): not part of glIsEnabled
    //state.doubleBuffer = glIsEnabled(GL_DOUBLE_BUFFER);
    state.dither = glIsEnabled(GL_DITHER);
    state.lineSmooth = glIsEnabled(GL_LINE_SMOOTH);
    state.framebufferSRGB = glIsEnabled(GL_FRAMEBUFFER_SRGB);
    state.multisample = glIsEnabled(GL_MULTISAMPLE);
    state.polygonSmooth = glIsEnabled(GL_POLYGON_SMOOTH);
    state.stencilTest = glIsEnabled(GL_STENCIL_TEST);

    glGetBooleanv(GL_DOUBLEBUFFER, &state.doublebuffer);

    glGetIntegerv(GL_BLEND_SRC_RGB, (GLint *)&state.blendSrcRGB);
    glGetIntegerv(GL_BLEND_SRC_ALPHA, (GLint *)&state.blendSrcAlpha);
    glGetIntegerv(GL_BLEND_DST_RGB, (GLint *)&state.blendDstRGB);
    glGetIntegerv(GL_BLEND_DST_ALPHA, (GLint *)&state.blendDstAlpha);

    glGetIntegerv(GL_BLEND_EQUATION_RGB, (GLint *)&state.blendEquationRGB);
    glGetIntegerv(GL_BLEND_EQUATION_ALPHA, (GLint *)&state.blendEquationAlpha);

    glGetIntegerv(GL_CURRENT_PROGRAM, (GLint *)&state.programID);
    glGetIntegerv(GL_ACTIVE_TEXTURE, (GLint *)&state.activeTexture);
    glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint *)&state.texture2DBinding);

    GLint major;
    GLint minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);

    state.version = (major * 1000) + minor;

    glGetFloatv(GL_LINE_WIDTH, &state.lineWidth);
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &state.maxCombinedTextureImageUnits);

    return state;
}

internal void DEBUG_OpenGLFlushErrors()
{
    while(glGetError() != GL_NO_ERROR);
}

internal void DEBUG_OpenGLCheckError()
{
    GLenum e = glGetError();
    if(e != GL_NO_ERROR)
    {
        switch(e)
        {
            case GL_INVALID_ENUM:
                {
                    PrintDebugMsg("OpenGL::ERROR:: Invalid Enum\n");
                }break;
            case GL_INVALID_VALUE:
                {
                    PrintDebugMsg("OpenGL::ERROR:: Invalid Value\n");
                }break;
            case GL_INVALID_OPERATION:
                {
                    PrintDebugMsg("OpenGL::ERROR:: Invalid Operation\n");
                }break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                {
                    PrintDebugMsg("OpenGL::ERROR:: Invalid Framebuffer Operation\n");
                }break;
            case GL_OUT_OF_MEMORY:
                {
                    PrintDebugMsg("OpenGL::ERROR:: Out Of Memory\n");
                }break;
            case GL_STACK_UNDERFLOW:
                {
                    PrintDebugMsg("OpenGL::ERROR:: Stack Underflow\n");
                }break;
            case GL_STACK_OVERFLOW:
                {
                    PrintDebugMsg("OpenGL::ERROR:: Stack Overflow\n");
                }break;
            default:
                {
                    PrintDebugMsg("OpenGL::ERROR:: Unknown Error\n");
                }
        }
        ASSERT(false);
    }
}

internal bool OpenGLCheckFramebufferStatus(GLenum target, bool assertError)
{
    GLenum fboCheck = glCheckFramebufferStatus(target);
    bool result = (fboCheck == GL_FRAMEBUFFER_COMPLETE);
    if(assertError && !result)
    {

        switch(fboCheck)
        {
            case GL_FRAMEBUFFER_UNDEFINED:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer undefined\n");
                } break;
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer incomplete attachment\n");
                } break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer missing attachment\n");
                } break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer incomplete draw buffer\n");
                } break;
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer incomplete read buffer\n");
                } break;
            case GL_FRAMEBUFFER_UNSUPPORTED:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer unsupported\n");
                } break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer incomplete multisample\n");
                } break;
            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer incomplete layer targets\n");
                } break;
            default:
                {
                    PrintDebugMsg("GL_ERROR::framebuffer unknown error\n");
                } break;
        }

        ASSERT(false);
    }

    return result;
}


internal void OpenGLCompileShader(GLuint shaderID, const char *source[], i32 sourceCount)
{
    glShaderSource(shaderID, sourceCount, source, nullptr);
    glCompileShader(shaderID);

    GLint vertCompileResult;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &vertCompileResult);
    if(vertCompileResult == GL_FALSE)
    {
        GLsizei logLength;
        GLchar logBuffer[MAX_DEBUG_MSG_SIZE];
        glGetShaderInfoLog(shaderID, MAX_DEBUG_MSG_SIZE, &logLength, logBuffer);
        PrintDebugMsg("ERROR::SHADER_COMPILE:: could not compile vert shader\n");
        PrintDebugMsg(logBuffer);

        ASSERT(false);
    }
}

internal void OpenGLLinkProgram(GLuint programID, GLuint *shaderList, i32 shaderCount)
{
    for(i32 shaderIndex = 0; shaderIndex < shaderCount; ++shaderIndex)
    {
        glAttachShader(programID, shaderList[shaderIndex]);
    }

    glLinkProgram(programID);
    GLint linkStatus;
    glGetProgramiv(programID, GL_LINK_STATUS, &linkStatus);
    if(linkStatus == GL_FALSE)
    {
        GLsizei logLength;
        GLchar logBuffer[512];
        glGetProgramInfoLog(programID, 512, &logLength, logBuffer);
        PrintDebugMsg("ERROR::SHADER_LINK:: could not link program\n");
        PrintDebugMsg(logBuffer);

        ASSERT(false);
    }
}

internal opengl_shader_info OpenGLCreateProgram(const u8 *source)
{
    const char *vertSource[] =
    {
        GLSL_VERSION_PREFIX,
        GLSL_VERT_PREFIX,
        (const char *)source,
    };
    const char *fragSource[] =
    {
        GLSL_VERSION_PREFIX,
        GLSL_FRAG_PREFIX,
        (const char *)source,
    };

    GLuint vertShaderID = glCreateShader(GL_VERTEX_SHADER);

    OpenGLCompileShader(vertShaderID, vertSource, ARRAY_COUNT(vertSource));
    GLuint fragShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    OpenGLCompileShader(fragShaderID, fragSource, ARRAY_COUNT(fragSource));
    GLuint shaderList[] =
    {
        vertShaderID,
        fragShaderID,
    };

    GLuint programID = glCreateProgram();
    OpenGLLinkProgram(programID, shaderList, ARRAY_COUNT(shaderList));

    opengl_shader_info si = {};
    si.vertShaderID = vertShaderID;
    si.fragShaderID = fragShaderID;
    si.programID = programID;
    return si;
}

internal void OpenGLReloadShader(application_state *app, opengl_shader_info *shader)
{
    ASSERT(shader->programID > 0);
    ASSERT(shader->vertShaderID > 0);
    ASSERT(shader->fragShaderID > 0);
    ASSERT(shader->filename != nullptr);

    glDetachShader(shader->programID, shader->vertShaderID);
    glDetachShader(shader->programID, shader->fragShaderID);

    temp_memory scratch(app);
    file_data shaderFile = Win32ReadAllText(scratch.arena, shader->filename);
    ASSERT(shaderFile.length != 0);

    const char *vertSource[] =
    {
        GLSL_VERSION_PREFIX,
        GLSL_VERT_PREFIX,
        (char *)shaderFile.content,
    };
    const char *fragSource[] =
    {
        GLSL_VERSION_PREFIX,
        GLSL_FRAG_PREFIX,
        (char *)shaderFile.content,
    };

    OpenGLCompileShader(shader->vertShaderID, vertSource, ARRAY_COUNT(vertSource));
    OpenGLCompileShader(shader->fragShaderID, fragSource, ARRAY_COUNT(fragSource));

    GLuint shaderList[] =
    {
        shader->vertShaderID,
        shader->fragShaderID,
    };

    OpenGLLinkProgram(shader->programID, shaderList, ARRAY_COUNT(shaderList));

}


internal opengl_shader_info OpenGLCreateProgramFromSingleFile(application_state *app, const char *filename)
{
    temp_memory scratch(app);
    file_data shaderFile = Win32ReadAllText(scratch.arena, filename);
    ASSERT(shaderFile.length != 0);
    opengl_shader_info si = OpenGLCreateProgram(shaderFile.content);
    si.filename = RawStringCreate(filename);
    si.lastWriteTime = Win32GetLastWriteTime(filename);
    return si;
}

