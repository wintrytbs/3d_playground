/* date = June 20th 2020 4:12 am */

#ifndef TBS_IMAGE_H
#define TBS_IMAGE_H

// TODO(torgrim): Save the filename so that we know where the images comes from.
// We would want the image_data structures to own the name.
// We probably want a switch so where the caller can choose not to save the name where
// the default is false.
struct image_data
{
    i32 width;
    i32 height;
    i32 channelCount;
    i32 padding;
    u8 *pixels;
};

struct image_data_hdr
{
    char *filename;
    i32 width;
    i32 height;
    i32 channelCount;
    i32 padding;
    f32 *pixels;
};

internal image_data LoadImageDataFromMemory(u8 *buffer, int size, bool flip = false);
internal image_data LoadImageData(const char *filename, bool flip = false);
internal image_data_hdr LoadImageDataHDR(const char *filename, bool flip = false, bool storeFilename = false);
internal void FreeImageData(image_data *img);
internal void FreeImageData(image_data_hdr *img);

#endif //TBS_IMAGE_H
