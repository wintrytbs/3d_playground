#include "tbs_scene_renderer.h"

namespace tbs_scene
{
    internal shadow_map_render_result *RenderShadowMaps(memory_arena *arena,
                                                        simple_camera *camera,
                                                        draw_list *drawList,
                                                        pbr_scene *pbrScene,
                                                        tbs_opengl::opengl_resource_manager *manager)
    {
        ASSERT(drawList->lightCount <= tbs_opengl::local_constants::PBR_SHADER_MAX_LIGHT_COUNT);

        GLuint fbo;
        glGenFramebuffers(1, &fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);

        // TODO(torgrim): Currently both the spot and point shadow map textures have
        // the same resolution, if they differ we need to resize the viewport for different
        // types of light.
        glViewport(0, 0, local_constants::SHADOW_MAP_WIDTH, local_constants::SHADOW_MAP_HEIGHT);

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonOffset(4.0f, 6.0f);
        glClearDepth(1.0);

        GLuint spotDst = pbrScene->spotShadowTexArray;
        GLuint dirDst = pbrScene->dirShadowTexArray;
        GLuint *pointDst = pbrScene->pointShadowTexList;

        shadow_map_render_result *result = AllocStruct(arena, shadow_map_render_result);

        u32 spotShadowCount = 0;
        u32 pointShadowCount = 0;
        u32 directionalShadowCount = 0;

        constexpr f32 shadowMapRatio = local_constants::SHADOW_MAP_WIDTH / local_constants::SHADOW_MAP_HEIGHT;

        for(u32 lightIndex = 0; lightIndex < drawList->lightCount; ++lightIndex)
        {
            glViewport(0, 0, local_constants::SHADOW_MAP_WIDTH, local_constants::SHADOW_MAP_HEIGHT);
            light_data *light = drawList->lights[lightIndex];
            if(light->isEnabled)
            {
                if(light->type == LIGHT_TYPE_Spot)
                {
                    ASSERT(spotShadowCount < tbs_opengl::local_constants::PBR_SHADER_MAX_SPOT_COUNT);

                    v3 lightWorldDir = -CalcZAxisFromRot(light->rotation);
                    matrix4D lightPOV = LookAt(light->position, light->position + lightWorldDir, axisVectorY);
                    matrix4D lightProj = CreateRevFrustum(DegreeToRadians(light->spotMaxCutoff * 2.0f), shadowMapRatio, 0.1f, light->maxDist);
                    result->spotShadowMVPs[spotShadowCount] = lightProj * lightPOV;

                    glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, spotDst, 0, (i32)spotShadowCount);
                    OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

                    glClear(GL_DEPTH_BUFFER_BIT);

                    matrix4D projView = lightProj * lightPOV;
                    DrawListRender_Shadow(pbrScene, manager, drawList, projView);

                    ++spotShadowCount;
                }
                else if(light->type == LIGHT_TYPE_Point)
                {
                    ASSERT(pointShadowCount < tbs_opengl::local_constants::PBR_SHADER_MAX_POINT_COUNT);

                    matrix4D lightProj = CreateRevFrustum(DegreeToRadians(90.0f), shadowMapRatio, 0.1f, light->maxDist);
                    matrix4D lightModel = CreateTranslate(light->position);

                    result->pointShadowMVPs[pointShadowCount] = lightModel;
                    result->pointShadowDepthTransforms[pointShadowCount] = v2(lightProj(2,2), lightProj(2,3));
                    GLuint pointShadowTexID = pointDst[pointShadowCount];

#if 1
                    for(i32 cubeFace = GL_TEXTURE_CUBE_MAP_POSITIVE_X; cubeFace <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z; ++cubeFace)
                    {
                        matrix4D lightPOV = CreatePointLightCameraMatrix((GLenum)cubeFace, light->position);
                        ASSERT(cubeFace <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);
                        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, (GLenum)cubeFace, pointShadowTexID, 0);
                        OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

                        glClear(GL_DEPTH_BUFFER_BIT);

                        matrix4D projView = lightProj * lightPOV;
                        DrawListRender_Shadow(pbrScene, manager, drawList, projView);
                    }
#endif

                    ++pointShadowCount;
                }
                else if(light->type == LIGHT_TYPE_Directional)
                {
                    // TODO(torgrim): Try a different splitting scheme as well.
                    ASSERT(directionalShadowCount < tbs_opengl::local_constants::PBR_SHADER_MAX_DIR_LIGHT_COUNT);
                    glEnable(GL_DEPTH_CLAMP);
                    glViewport(0, 0, tbs_scene::local_constants::DIRECTIONAL_SHADOW_SIZE, tbs_scene::local_constants::DIRECTIONAL_SHADOW_SIZE);

                    f32 clipNear = camera->clipNear;
                    f32 clipFar = camera->clipFar;
                    f32 clipRange = clipFar - clipNear;
                    f32 minZ = clipNear;
                    f32 maxZ = clipNear + clipRange;
                    f32 zRange = (maxZ - minZ);
                    f32 ratio = maxZ / minZ;
                    f32 lambda = 0.95f;

                    f32 cascadeSplits[tbs_scene::local_constants::CASCADE_SHADOW_SPLIT_COUNT];
                    for(u32 si = 0; si < tbs_scene::local_constants::CASCADE_SHADOW_SPLIT_COUNT; ++si)
                    {
                        f32 p = (f32)(si + 1) / (f32)tbs_scene::local_constants::CASCADE_SHADOW_SPLIT_COUNT;
                        f32 log = minZ * PowF32(ratio, p);
                        f32 uniform = minZ + zRange * p;
                        f32 d = lambda * (log - uniform) + uniform;
                        cascadeSplits[si] = (d - clipNear) / clipRange;
                    }

                    v3 frustumCorners[8] =
                    {
                        v3(-1.0f,  1.0f, -1.0f),
                        v3( 1.0f,  1.0f, -1.0f),
                        v3( 1.0f, -1.0f, -1.0f),
                        v3(-1.0f, -1.0f, -1.0f),
                        v3(-1.0f,  1.0f,  1.0f),
                        v3( 1.0f,  1.0f,  1.0f),
                        v3( 1.0f, -1.0f,  1.0f),
                        v3(-1.0f, -1.0f,  1.0f),
                    };

                    matrix4D invViewProj = InvertMatrix4D(camera->proj * camera->view);

                    for(u32 i = 0; i < 8; ++i)
                    {
                        v4 inverseP = invViewProj * v4(frustumCorners[i], 1.0f);
                        frustumCorners[i] = V4ToV3(inverseP / inverseP.w);
                    }

                    float prevSplitDistance = 0.0f;
                    for(i32 k = 0; k < (i32)tbs_scene::local_constants::CASCADE_SHADOW_SPLIT_COUNT; ++k)
                    {
                        v3 frustumVectors[8];
                        f32 splitDistance = cascadeSplits[k];
                        for(u32 i = 0; i < 4; ++i)
                        {
                            v3 cornerRay = frustumCorners[i+4] - frustumCorners[i];
                            v3 nearCornerRay = cornerRay * prevSplitDistance;
                            v3 farCornerRay = cornerRay * splitDistance;
                            frustumVectors[i+4] = frustumCorners[i] + farCornerRay;
                            frustumVectors[i] = frustumCorners[i] + nearCornerRay;
                        }

                        v3 frustumCenter = zeroVector3D;
                        for(u32 i = 0; i < 8; ++i)
                        {
                            frustumCenter += frustumVectors[i];
                        }

                        frustumCenter /= 8.0f;

                        f32 radius = 0.0f;
                        for(u32 i = 0; i < 8; ++i)
                        {
                            f32 distance = Length(frustumVectors[i] - frustumCenter);
                            radius = Max(radius, distance);
                        }

                        radius = CeilF32(radius * 16.0f) / 16.0f;

                        v3 maxExtents = v3(radius, radius, radius);
                        v3 minExtents = -maxExtents;

                        v3 lightDir = -Normalize(CalcZAxisFromRot(light->rotation));
                        matrix4D lightView = LookAt(frustumCenter - lightDir * -minExtents.z, frustumCenter, axisVectorY);
                        matrix4D lightProj = CreateOrtho3D(minExtents.x, maxExtents.x, minExtents.y, maxExtents.y, 0.0f, maxExtents.z-minExtents.z);
                        matrix4D lightProjView = lightProj * lightView;

                        // NOTE: Make sure to move in texel sized increments so that we get a stable shadow.
#if 1
                        v4 shadowOrigin = v4(0.0f, 0.0f, 0.0f, 1.0f);
                        shadowOrigin = lightProjView * shadowOrigin;
                        shadowOrigin = shadowOrigin * tbs_scene::local_constants::DIRECTIONAL_SHADOW_SIZE / 2.0f;

                        v4 roundedOrigin = v4(RoundF32(shadowOrigin.x), RoundF32(shadowOrigin.y), RoundF32(shadowOrigin.z), RoundF32(shadowOrigin.w));
                        v4 roundOffset = roundedOrigin - shadowOrigin;
                        roundOffset = roundOffset * 2.0f / tbs_scene::local_constants::DIRECTIONAL_SHADOW_SIZE;
                        roundOffset.z = 0.0f;
                        roundOffset.w = 0.0f;
                        lightProj(0,3) += roundOffset.x;
                        lightProj(1,3) += roundOffset.y;

                        lightProjView = lightProj * lightView;
#endif

                        result->cascadeSplits[k] = (clipNear + splitDistance * clipRange) * -1.0f;
                        result->directionalShadowMVPs[k] = lightProjView;
                        prevSplitDistance = cascadeSplits[k];

                        glFramebufferTextureLayer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, dirDst, 0, k);
                        OpenGLCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

                        glClear(GL_DEPTH_BUFFER_BIT);
                        DrawListRender_Shadow(pbrScene, manager, drawList, lightProjView);
                    }

                    glDisable(GL_DEPTH_CLAMP);
                    directionalShadowCount++;
                }
            }
        }

        result->spotShadowTexArrayID = spotDst;
        result->pointShadowTextures = pointDst;
        result->directionalShadowTexArrayID = dirDst;
        result->spotCount = spotShadowCount;
        result->pointCount = pointShadowCount;
        result->directionalCount = directionalShadowCount;

        ASSERT(result->directionalCount <= 1);


        glDisable(GL_DEPTH_TEST);
        glDisable(GL_POLYGON_OFFSET_FILL);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDeleteFramebuffers(1, &fbo);

        return result;
    }

    internal void DrawListRender_Cascade(tbs_opengl::opengl_resource_manager *manager, pbr_scene *pbrScene, simple_camera *camera, light_data *light, draw_list *drawList)
    {
        if(drawList->meshCount == 0)
        {
            return;
        }

        v4 resultCascadeSplits;
        matrix4D resultCascadeMVPs[4];

        f32 clipNear = camera->clipNear;
        f32 clipFar = camera->clipFar;
        f32 clipRange = clipFar - clipNear;
        // NOTE: camera view is along the -z
        f32 minZ = clipNear;
        f32 maxZ = clipNear + clipRange;
        f32 zRange = (maxZ - minZ);
        f32 ratio = maxZ / minZ;
        f32 lambda = 0.95f;

        f32 cascadeSplits[tbs_scene::local_constants::CASCADE_SHADOW_SPLIT_COUNT];
        for(u32 si = 0; si < tbs_scene::local_constants::CASCADE_SHADOW_SPLIT_COUNT; ++si)
        {
            f32 p = (f32)(si + 1) / (f32)tbs_scene::local_constants::CASCADE_SHADOW_SPLIT_COUNT;
            f32 log = minZ * PowF32(ratio, p);
            f32 uniform = minZ + zRange * p;
            f32 d = lambda * (log - uniform) + uniform;
            cascadeSplits[si] = (d - clipNear) / clipRange;
        }

        v3 frustumCorners[8] =
        {
            v3(-1.0f,  1.0f, -1.0f),
            v3( 1.0f,  1.0f, -1.0f),
            v3( 1.0f, -1.0f, -1.0f),
            v3(-1.0f, -1.0f, -1.0f),
            v3(-1.0f,  1.0f,  1.0f),
            v3( 1.0f,  1.0f,  1.0f),
            v3( 1.0f, -1.0f,  1.0f),
            v3(-1.0f, -1.0f,  1.0f),
        };

        matrix4D invViewProj = InvertMatrix4D(camera->proj * camera->view);

        for(u32 i = 0; i < 8; ++i)
        {
            v4 inverseP = invViewProj * v4(frustumCorners[i], 1.0f);
            frustumCorners[i] = V4ToV3(inverseP / inverseP.w);
        }

        float prevSplitDistance = 0.0f;
        for(i32 k = 0; k < (i32)tbs_scene::local_constants::CASCADE_SHADOW_SPLIT_COUNT; ++k)
        {
            v3 frustumVectors[8];
            f32 splitDistance = cascadeSplits[k];
            for(u32 i = 0; i < 4; ++i)
            {
                v3 cornerRay = frustumCorners[i+4] - frustumCorners[i];
                v3 nearCornerRay = cornerRay * prevSplitDistance;
                v3 farCornerRay = cornerRay * splitDistance;
                frustumVectors[i+4] = frustumCorners[i] + farCornerRay;
                frustumVectors[i] = frustumCorners[i] + nearCornerRay;
            }

            v3 frustumCenter = zeroVector3D;
            for(u32 i = 0; i < 8; ++i)
            {
                frustumCenter += frustumVectors[i];
            }

            frustumCenter /= 8.0f;

            f32 radius = 0.0f;
            for(u32 i = 0; i < 8; ++i)
            {
                f32 distance = Length(frustumVectors[i] - frustumCenter);
                radius = Max(radius, distance);
            }

            radius = CeilF32(radius * 16.0f) / 16.0f;

            v3 maxExtents = v3(radius, radius, radius);
            v3 minExtents = -maxExtents;

            v3 lightDir = -Normalize(CalcZAxisFromRot(light->rotation));
            matrix4D lightView = LookAt(frustumCenter - lightDir * -minExtents.z, frustumCenter, axisVectorY);
            matrix4D lightProj = CreateOrtho3D(minExtents.x, maxExtents.x, minExtents.y, maxExtents.y, 0.0f, maxExtents.z-minExtents.z);
            matrix4D lightProjView = lightProj * lightView;

            // NOTE: Make sure to move in texel sized increments so that we get a stable shadow.
#if 1
            v4 shadowOrigin = v4(0.0f, 0.0f, 0.0f, 1.0f);
            shadowOrigin = lightProjView * shadowOrigin;
            shadowOrigin = shadowOrigin * tbs_scene::local_constants::DIRECTIONAL_SHADOW_SIZE / 2.0f;

            v4 roundedOrigin = v4(RoundF32(shadowOrigin.x), RoundF32(shadowOrigin.y), RoundF32(shadowOrigin.z), RoundF32(shadowOrigin.w));
            v4 roundOffset = roundedOrigin - shadowOrigin;
            roundOffset = roundOffset * 2.0f / tbs_scene::local_constants::DIRECTIONAL_SHADOW_SIZE;
            roundOffset.z = 0.0f;
            roundOffset.w = 0.0f;
            lightProj(0,3) += roundOffset.x;
            lightProj(1,3) += roundOffset.y;

            lightProjView = lightProj * lightView;
#endif

            resultCascadeSplits[k] = (clipNear + splitDistance * clipRange) * -1.0f;
            resultCascadeMVPs[k] = lightProjView;
            prevSplitDistance = cascadeSplits[k];
        }

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        GLuint progID = manager->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_CascadeVisualize);
        glUseProgram(progID);
        glUniformMatrix4fv(glGetUniformLocation(progID, "proj"), 1, GL_FALSE, Mat4Addr(camera->proj));
        glUniformMatrix4fv(glGetUniformLocation(progID, "view"), 1, GL_FALSE, Mat4Addr(camera->view));
        glUniform4fv(glGetUniformLocation(progID, "cascade_splits"), 1, &resultCascadeSplits[0]);
        glUniform3fv(glGetUniformLocation(progID, "cam_pos"), 1, &camera->pos[0]);
        glUniform1i(glGetUniformLocation(progID, "base_color_map"), 0);

        for(size_t mIndex = 0; mIndex < drawList->meshCount; ++mIndex)
        {
            tbs_pbr::pbr_mesh *mesh = drawList->meshes[mIndex];
            matrix4D model = drawList->meshTransforms[mIndex];
            glUniformMatrix4fv(glGetUniformLocation(progID, "model"), 1, GL_FALSE, Mat4Addr(model));
            for(size_t pIndex = 0; pIndex < mesh->primitives.count; ++pIndex)
            {
                tbs_opengl::renderable_primitive *prim = manager->GetPrimitive(mesh->primitives.data[pIndex].renderDataHandle);
                tbs_pbr::pbr_material *mat = pbrScene->GetMaterial(mesh->primitives.data[pIndex].materialHandle);

                glUniform4fv(glGetUniformLocation(progID, "base_color"), 1, &mat->baseColor[0]);

                OpenGLSetActiveTextureUnit(0);
                auto texHandle = mat->textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_BaseColor];
                tbs_opengl::texture_info *texInfo = manager->GetTexture(texHandle);
                glUniform1i(glGetUniformLocation(progID, "texture_present"), texInfo != nullptr);
                if(texInfo != nullptr && texInfo->loaded)
                {
                    ASSERT(texInfo->texID > 0);
                    ASSERT(texInfo->target != 0);
                    ASSERT(texInfo->target == GL_TEXTURE_2D);
                    glBindTexture(GL_TEXTURE_2D, texInfo->texID);
                }
                else
                {
                    glBindTexture(GL_TEXTURE_2D, 0);
                }

#if 1
                // TODO(torgrim): Handle blending properly.
                if(mat->alphaMode == tbs_pbr::MATERIAL_ALPHA_MODE_Mask || mat->alphaMode == tbs_pbr::MATERIAL_ALPHA_MODE_Blend)
                {
                    OpenGLSetUniform(progID, "alpha_cutoff", mat->alphaCutoff);
                    glEnable(GL_BLEND);
                }
                else
                {
                    OpenGLSetUniform(progID, "alpha_cutoff", 1.0f);
                    glDisable(GL_BLEND);
                }
#endif

                glBindVertexArray(prim->VAOID);
                if(prim->target == GL_ELEMENT_ARRAY_BUFFER)
                {
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, prim->indEBOID);
                    glDrawElements(prim->mode, (GLsizei)prim->indexCount, prim->indexDataType, nullptr);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                }
                else
                {
                    glDrawArrays(prim->mode, 0, (GLsizei)prim->vertexCount);
                }
            }
        }

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
    }

    internal void DrawListRender_Shadow(pbr_scene *pbrScene, tbs_opengl::opengl_resource_manager *manager, draw_list *drawList, matrix4D projView)
    {
        if(drawList->meshCount == 0)
        {
            return;
        }

        glDisable(GL_BLEND);
        glEnable(GL_CULL_FACE);

        GLuint progID = manager->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_ShadowMap);
        glUseProgram(progID);
        OpenGLSetUniform(progID, "proj_view", &projView);
        OpenGLSetUniform(progID, "base_color_map", 0);

        for(size_t mIndex = 0; mIndex < drawList->meshCount; ++mIndex)
        {
            tbs_pbr::pbr_mesh *mesh = drawList->meshes[mIndex];
            matrix4D model = drawList->meshTransforms[mIndex];
            for(size_t pIndex = 0; pIndex < mesh->primitives.count; ++pIndex)
            {
                tbs_opengl::renderable_primitive *prim = manager->GetPrimitive(mesh->primitives.data[pIndex].renderDataHandle);
                tbs_pbr::pbr_material *mat = pbrScene->GetMaterial(mesh->primitives.data[pIndex].materialHandle);

                OpenGLSetActiveTextureUnit(0);
                auto texHandle = mat->textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_BaseColor];
                tbs_opengl::texture_info *texInfo = manager->GetTexture(texHandle);
                if(texInfo != nullptr && texInfo->loaded)
                {
                    ASSERT(texInfo->texID > 0);
                    ASSERT(texInfo->target != 0);
                    ASSERT(texInfo->target == GL_TEXTURE_2D);
                    glBindTexture(GL_TEXTURE_2D, texInfo->texID);
                    OpenGLSetUniform(progID, "has_base_color", true);
                }
                else
                {
                    glBindTexture(GL_TEXTURE_2D, 0);
                    OpenGLSetUniform(progID, "has_base_color", false);
                }

                OpenGLSetUniform(progID, "model", &model);
#if 1
                // TODO(torgrim): Handle blending properly.
                if(mat->alphaMode == tbs_pbr::MATERIAL_ALPHA_MODE_Mask || mat->alphaMode == tbs_pbr::MATERIAL_ALPHA_MODE_Blend)
                {
                    OpenGLSetUniform(progID, "alpha_cutoff", mat->alphaCutoff);
                    glEnable(GL_BLEND);
                }
                else
                {
                    OpenGLSetUniform(progID, "alpha_cutoff", 1.0f);
                    glDisable(GL_BLEND);
                }
#endif

                glBindVertexArray(prim->VAOID);
                if(prim->target == GL_ELEMENT_ARRAY_BUFFER)
                {
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, prim->indEBOID);
                    glDrawElements(prim->mode, (GLsizei)prim->indexCount, prim->indexDataType, nullptr);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                }
                else
                {
                    glDrawArrays(prim->mode, 0, (GLsizei)prim->vertexCount);
                }
            }
        }

        glDisable(GL_CULL_FACE);
    }

    internal void DrawListRender_PBR(pbr_scene *pbrScene,
                                     tbs_opengl::opengl_resource_manager *manager,
                                     scene_state *scene,
                                     simple_camera *camera,
                                     draw_list *drawList,
                                     shadow_map_render_result *shadowMaps)
    {
        DEBUG_OpenGLFlushErrors();

        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        glEnable(GL_CULL_FACE);

        GLuint progID = manager->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_PrimitivePBR);
        glUseProgram(progID);

        S_ASSERT(ARRAY_COUNT(manager->pbrUniformSamplerLocations) == tbs_opengl::PBR_SAMPLER_BINDING_POINT_Count, "Invalid definition of sampler binding locations");
        for(i32 bindPoint = 0; bindPoint < tbs_opengl::PBR_SAMPLER_BINDING_POINT_Count; ++bindPoint)
        {
            GLint loc = manager->pbrUniformSamplerLocations[bindPoint];
            if(loc != -1)
            {
                glUniform1i(loc, bindPoint);
            }
        }

        OpenGLSetUniform(progID, "proj", &camera->proj);
        OpenGLSetUniform(progID, "view", &camera->view);
        OpenGLSetUniform(progID, "view_pos", &camera->pos);

        OpenGLSetUniform(progID, "spot_shadow_count", (i32)shadowMaps->spotCount);
        OpenGLSetUniform(progID, "point_shadow_count", (i32)shadowMaps->pointCount);
        OpenGLSetUniform(progID, "dir_shadow_count", (i32)shadowMaps->directionalCount);


        OpenGLSetUniform(progID, "use_spot_sample_shadow", scene->useSpotSampleShadow);
        OpenGLSetUniform(progID, "use_point_sample_shadow", scene->usePointSampleShadow);

        glUniform3f(glGetUniformLocation(progID, "cascadeScale[0]"), shadowMaps->cascadeScale[0].x, shadowMaps->cascadeScale[0].y, shadowMaps->cascadeScale[0].z);
        //OpenGLSetUniform(progID, "cascadeScale[0]", &shadowMaps->cascadeScale[0]);
        OpenGLSetUniform(progID, "cascadeScale[1]", &shadowMaps->cascadeScale[1]);
        OpenGLSetUniform(progID, "cascadeScale[2]", &shadowMaps->cascadeScale[2]);

        glUniform3f(glGetUniformLocation(progID, "cascadeOffset[0]"), shadowMaps->cascadeOffset[0].x, shadowMaps->cascadeOffset[0].y, shadowMaps->cascadeOffset[0].z);
        //OpenGLSetUniform(progID, "cascadeOffset[0]", &shadowMaps->cascadeOffset[0]);
        OpenGLSetUniform(progID, "cascadeOffset[1]", &shadowMaps->cascadeOffset[1]);
        OpenGLSetUniform(progID, "cascadeOffset[2]", &shadowMaps->cascadeOffset[2]);

        glUniform4f(glGetUniformLocation(progID, "f1"), shadowMaps->f1.x, shadowMaps->f1.y, shadowMaps->f1.z, shadowMaps->f1.w);
        glUniform4f(glGetUniformLocation(progID, "f2"), shadowMaps->f2.x, shadowMaps->f2.y, shadowMaps->f2.z, shadowMaps->f2.w);
        glUniform4f(glGetUniformLocation(progID, "f3"), shadowMaps->f3.x, shadowMaps->f3.y, shadowMaps->f3.z, shadowMaps->f3.w);

        u32 globalFlags = 0;
        if(scene->useAmbientEnvLighting && scene->envMap != invalidHandle)
        {
            globalFlags = tbs_opengl::PBR_SHADER_SETTINGS_FLAGS_UseAmbientEnvLighting;

            tbs_pbr::env_map_info *envMap = manager->GetEnvMap(scene->envMap);
            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_SAMPLER_BINDING_POINT_DiffPrefilterMap);
            glBindTexture(GL_TEXTURE_CUBE_MAP, envMap->diffPrefilterMap);

            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_SAMPLER_BINDING_POINT_SpecPrefilterMap);
            glBindTexture(GL_TEXTURE_CUBE_MAP, envMap->specPrefilterMap);

            OpenGLSetActiveTextureUnit(tbs_opengl::PBR_SAMPLER_BINDING_POINT_BrdfLUT);
            glBindTexture(GL_TEXTURE_2D, envMap->brdfLUT);
        }

        OpenGLSetUniform(progID, "glob_settings_flags", globalFlags);
        OpenGLSetUniform(progID, "ambient_strength", pbrScene->ambientStrength);


        char buff[255];
        {
            i32 activeLightCount = 0;
            for(u32 lightIndex = 0; lightIndex < drawList->lightCount; ++lightIndex)
            {
                light_data *light = drawList->lights[lightIndex];
                if(light->isEnabled)
                {
                    // NOTE(torgrim): really only directional and spot lights
                    // that have a main direction
                    v3 lightWorldDir = -CalcZAxisFromRot(light->rotation);

                    sprintf(buff, "light_list[%u].color", activeLightCount);
                    OpenGLSetUniform(progID, buff, &light->color);
                    sprintf(buff, "light_list[%u].position", activeLightCount);
                    OpenGLSetUniform(progID, buff, &light->position);
                    sprintf(buff, "light_list[%u].dist_att_func", activeLightCount);
                    OpenGLSetUniform(progID, buff, light->distAttFunc);
                    sprintf(buff, "light_list[%u].max_dist", activeLightCount);
                    OpenGLSetUniform(progID, buff, light->maxDist);
                    sprintf(buff, "light_list[%u].falloff_start_dist", activeLightCount);
                    OpenGLSetUniform(progID, buff, light->falloffStartDist);
                    sprintf(buff, "light_list[%u].isEnabled", activeLightCount);
                    OpenGLSetUniform(progID, buff, true);
                    sprintf(buff, "light_list[%u].prim_dir", activeLightCount);
                    OpenGLSetUniform(progID, buff, &lightWorldDir);
                    sprintf(buff, "light_list[%u].spot_min_cutoff", activeLightCount);
                    OpenGLSetUniform(progID, buff, Cos(DegreeToRadians(light->spotMinCutoff)));
                    sprintf(buff, "light_list[%u].spot_max_cutoff", activeLightCount);
                    OpenGLSetUniform(progID, buff, Cos(DegreeToRadians(light->spotMaxCutoff)));
                    sprintf(buff, "light_list[%u].type", activeLightCount);
                    OpenGLSetUniform(progID, buff, light->type);
                    sprintf(buff, "light_list[%u].strength", activeLightCount);
                    OpenGLSetUniform(progID, buff, light->strength);

                    ++activeLightCount;
                }
            }

            OpenGLSetUniform(progID, "light_count", activeLightCount);
        }


        OpenGLSetActiveTextureUnit(tbs_opengl::PBR_SAMPLER_BINDING_POINT_SpotShadowMap);
        glBindTexture(GL_TEXTURE_2D_ARRAY, shadowMaps->spotShadowTexArrayID);

        for(u32 shadowIndex = 0; shadowIndex < shadowMaps->spotCount; ++shadowIndex)
        {
            sprintf(buff, "spot_shadow_MVP_list[%u]", shadowIndex);
            OpenGLSetUniform(progID, buff, shadowMaps->spotShadowMVPs + shadowIndex);
        }

        OpenGLSetActiveTextureUnit(tbs_opengl::PBR_SAMPLER_BINDING_POINT_DirShadowMap);
        glBindTexture(GL_TEXTURE_2D_ARRAY, shadowMaps->directionalShadowTexArrayID);

        glUniformMatrix4fv(glGetUniformLocation(progID, "dir_shadow_MVP_list[0]"), 1, GL_FALSE, Mat4Addr(shadowMaps->directionalShadowMVPs[0]));
        glUniformMatrix4fv(glGetUniformLocation(progID, "dir_shadow_MVP_list[1]"), 1, GL_FALSE, Mat4Addr(shadowMaps->directionalShadowMVPs[1]));
        glUniformMatrix4fv(glGetUniformLocation(progID, "dir_shadow_MVP_list[2]"), 1, GL_FALSE, Mat4Addr(shadowMaps->directionalShadowMVPs[2]));
        glUniformMatrix4fv(glGetUniformLocation(progID, "dir_shadow_MVP_list[3]"), 1, GL_FALSE, Mat4Addr(shadowMaps->directionalShadowMVPs[3]));

        glUniform4fv(glGetUniformLocation(progID, "cascade_splits"), 1, &shadowMaps->cascadeSplits[0]);

        ASSERT(shadowMaps->pointCount <= ((tbs_opengl::PBR_SAMPLER_BINDING_POINT_PointShadowMap9 - tbs_opengl::PBR_SAMPLER_BINDING_POINT_PointShadowMap0) + 1));
        for(u32 shadowIndex = 0; shadowIndex < shadowMaps->pointCount; ++shadowIndex)
        {
            GLint textureUnit = (GLint)(tbs_opengl::PBR_SAMPLER_BINDING_POINT_PointShadowMap0 + shadowIndex);
            OpenGLSetActiveTextureUnit((GLuint)textureUnit);
            glBindTexture(GL_TEXTURE_CUBE_MAP, shadowMaps->pointShadowTextures[shadowIndex]);

            sprintf(buff, "point_shadow_model_list[%u]", shadowIndex);
            OpenGLSetUniform(progID, buff, shadowMaps->pointShadowMVPs + shadowIndex);

            sprintf(buff, "point_shadow_list[%u].trans", shadowIndex);
            OpenGLSetUniform(progID, buff, shadowMaps->pointShadowDepthTransforms + shadowIndex);
        }

        S_ASSERT(tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_Count == ARRAY_COUNT(tbs_pbr::pbr_material::textures), "Invalid PBR Material Texture Count");
        for(size_t mIndex = 0; mIndex < drawList->meshCount; ++mIndex)
        {
            tbs_pbr::pbr_mesh *mesh = drawList->meshes[mIndex];
            matrix4D model = drawList->meshTransforms[mIndex];
            for(size_t pIndex = 0; pIndex < mesh->primitives.count; ++pIndex)
            {
                tbs_opengl::renderable_primitive *prim = manager->GetPrimitive(mesh->primitives.data[pIndex].renderDataHandle);
                tbs_pbr::pbr_material *mat = pbrScene->GetMaterial(mesh->primitives.data[pIndex].materialHandle);
                u32 samplerFlags = 0;
                for(u32 texIndex = 0; texIndex < ARRAY_COUNT(mat->textures); ++texIndex)
                {
                    OpenGLSetActiveTextureUnit(tbs_opengl::PBR_SAMPLER_BINDING_POINT_BaseColor + texIndex);
                    auto texHandle = mat->textures[texIndex];
                    tbs_opengl::texture_info *texInfo = manager->GetTexture(texHandle);
                    if(texInfo != nullptr && texInfo->loaded)
                    {
                        ASSERT(texInfo->texID > 0);
                        ASSERT(texInfo->target != 0);
                        glBindTexture(texInfo->target, texInfo->texID);
                        samplerFlags |= tbs_opengl::samplerIndexToActiveSamplerFlag[texIndex];
                    }
                    else
                    {
                        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
                        glBindTexture(GL_TEXTURE_2D, 0);
                    }
                }

                OpenGLSetUniform(progID, "base_color", &mat->baseColor);
                OpenGLSetUniform(progID, "model", &model);
                OpenGLSetUniform(progID, "active_sampler_flag", samplerFlags);
                OpenGLSetUniform(progID, "metallic_factor", mat->metallicFactor);
                OpenGLSetUniform(progID, "roughness_factor", mat->roughnessFactor);
                OpenGLSetUniform(progID, "emissive_factor", &mat->emissiveFactor);

                // TODO(torgrim): Handle blending properly.
                if(mat->alphaMode == tbs_pbr::MATERIAL_ALPHA_MODE_Mask || mat->alphaMode == tbs_pbr::MATERIAL_ALPHA_MODE_Blend)
                {
                    OpenGLSetUniform(progID, "alpha_cutoff", mat->alphaCutoff);
                }
                else
                {
                    OpenGLSetUniform(progID, "alpha_cutoff", 1.0f);
                    glDisable(GL_BLEND);
                }

                glBindVertexArray(prim->VAOID);
                if(prim->target == GL_ELEMENT_ARRAY_BUFFER)
                {
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, prim->indEBOID);
                    glDrawElements(prim->mode, (GLsizei)prim->indexCount, prim->indexDataType, nullptr);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                }
                else
                {
                    glDrawArrays(prim->mode, 0, (GLsizei)prim->vertexCount);
                }
            }
        }

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);

        DEBUG_OpenGLCheckError();
    }

    internal void CreateDrawListFromSceneNodes(tbs_scene::pbr_scene *pbrScene, scene_node *node, draw_list *drawList, matrix4D parentTransform)
    {
        while(node != nullptr)
        {
            ASSERT(node->type != SCENE_NODE_TYPE_Mesh || (node->transform.scale.x != 0.0f && node->transform.scale.y != 0.0f && node->transform.scale.z != 0.0f));
            matrix4D currentTransform = parentTransform * CreateTranslate(node->transform.translation) * CreateRotate(node->transform.rotation) * CreateScale(node->transform.scale);
            if(node->type == SCENE_NODE_TYPE_Mesh)
            {
                auto handle = tbs_opengl::VoidToHandle(node->handle);
                ASSERT(handle != invalidHandle);
                drawList->meshes[drawList->meshCount] = pbrScene->GetMesh(handle);
                drawList->meshTransforms[drawList->meshCount] = currentTransform;
                drawList->meshCount++;
            }
            else if(node->type == SCENE_NODE_TYPE_Light)
            {
                auto handle = tbs_opengl::VoidToHandle(node->handle);
                ASSERT(handle != invalidHandle);
                drawList->lights[drawList->lightCount] = pbrScene->GetLight(handle);
                drawList->lightTransforms[drawList->lightCount] = currentTransform;
                drawList->lightCount++;
            }
            else if(node->type == SCENE_NODE_TYPE_Camera)
            {
                drawList->cameras[drawList->cameraCount] = (u32)(uintptr_t)node->handle;
                drawList->cameraTransforms[drawList->cameraCount] = currentTransform;
                drawList->cameraCount++;
            }

            if(node->firstChild)
            {
                CreateDrawListFromSceneNodes(pbrScene, node->firstChild, drawList, currentTransform);
            }

            node = node->next;
        }
    }

    internal draw_list CreateDrawList(memory_arena *arena, pbr_scene *pbrScene, scene_node *rootNode)
    {
        draw_list result = {};
        result.meshes = AllocPointerArray(arena, tbs_pbr::pbr_mesh, local_constants::MAX_SCENE_NODE_COUNT);
        result.meshTransforms = AllocArray(arena, matrix4D, local_constants::MAX_SCENE_NODE_COUNT);
        result.lights = AllocPointerArray(arena, light_data, local_constants::MAX_SCENE_NODE_COUNT);
        result.lightTransforms = AllocArray(arena, matrix4D, local_constants::MAX_SCENE_NODE_COUNT);
        result.cameras = AllocArray(arena, u32, 10);
        result.cameraTransforms = AllocArray(arena, matrix4D, 10);
        CreateDrawListFromSceneNodes(pbrScene, rootNode, &result, CreateIdentity4D());

        ASSERT(result.lightCount <= tbs_opengl::local_constants::PBR_SHADER_MAX_LIGHT_COUNT);

#if 1
        for(u32 i = 0; i < result.lightCount; ++i)
        {
            light_data *cl = result.lights[i];
            u32 minIndex = i;
            for(u32 j = i+1; j < result.lightCount; ++j)
            {
                if(result.lights[j]->type < result.lights[minIndex]->type)
                {
                    minIndex = j;
                }
            }

            light_data *tmp = cl;
            result.lights[i] = result.lights[minIndex];
            result.lights[minIndex] = tmp;
        }
#endif

        return result;
    }

    internal void RenderLightPrimitives(scene_state *scene, tbs_opengl::opengl_resource_manager *manager, draw_list *drawList)
    {
        glEnable(GL_DEPTH_TEST);
        GLuint progID = manager->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_Light);
        glUseProgram(progID);
        matrix4D proj = scene->editorCamera.proj;
        matrix4D view = scene->editorCamera.view;
        tbs_opengl::renderable_primitive *prim = manager->GetLightPrimitive();
        glBindVertexArray(prim->VAOID);
        // TODO(torgrim): Render different light sources with different draw data.
        // For lights with directions and angle constraints we should also
        // visualize those.
        for(u32 lightIndex = 0; lightIndex < drawList->lightCount; ++lightIndex)
        {
            light_data *light = drawList->lights[lightIndex];

            v3 scale = v3(0.1f, 0.1f, 0.1f);

            matrix4D rot = CreateRotate(light->rotation);
            matrix4D model = CreateIdentity4D();
            model *= CreateTranslate(light->position);
            model *= rot;
            model *= CreateScale(scale);

            OpenGLSetUniform(progID, "proj", &proj);
            OpenGLSetUniform(progID, "view", &view);
            OpenGLSetUniform(progID, "model", &model);
            OpenGLSetUniform(progID, "light_color", &light->color);

            glDrawArrays(GL_TRIANGLES, 0, (GLsizei)prim->vertexCount);
        }

        glBindVertexArray(0);

        glDisable(GL_DEPTH_TEST);
    }

    internal void DEBUG_RenderBoundingSphere(application_state *app, scene_state *scene)
    {
        static bool init = false;
        static opengl_shader_info shader;
        static GLuint VAOID;
        static GLuint VBOID;
        static i32 lineCount = 0;

        glLineWidth(1.0f);
        if(!init)
        {
            f32 step = PI / 30.0f;
            v3 linesData[60 * 2 * 3];
            u32 vertIndex = 0;
            for(u32 i = 0; i < 60; ++i)
            {
                f32 val1 = ((f32)i * step);
                f32 val2 = ((f32)(i+1) * step);
                f32 posX = Cos(val1);
                f32 posY = Sin(val1);

                linesData[vertIndex++] = v3(posX, posY, 0);
                posX = Cos(val2);
                posY = Sin(val2);
                linesData[vertIndex++] = v3(posX, posY, 0);
            }

            for(u32 i = 0; i < 60; ++i)
            {
                f32 val1 = ((f32)i * step);
                f32 val2 = ((f32)(i+1) * step);
                f32 posZ = Cos(val1);
                f32 posY = Sin(val1);

                linesData[vertIndex++] = v3(0, posY, posZ);
                posZ = Cos(val2);
                posY = Sin(val2);
                linesData[vertIndex++] = v3(0, posY, posZ);
            }

            for(u32 i = 0; i < 60; ++i)
            {
                f32 val1 = ((f32)i * step);
                f32 val2 = ((f32)(i+1) * step);
                f32 posX = Cos(val1);
                f32 posZ = Sin(val1);

                linesData[vertIndex++] = v3(posX, 0, posZ);
                posX = Cos(val2);
                posZ = Sin(val2);
                linesData[vertIndex++] = v3(posX, 0, posZ);
            }

            lineCount = ARRAY_COUNT(linesData);

            char shaderFullPath[MAX_PATH];
            RawStringConcat(shaderFullPath, app->shaderPath, "bounding_sphere.glsl");
            shader = OpenGLCreateProgramFromSingleFile(app, shaderFullPath);
            glGenVertexArrays(1, &VAOID);
            glGenBuffers(1, &VBOID);
            glBindVertexArray(VAOID);
            glBindBuffer(GL_ARRAY_BUFFER, VBOID);
            glBufferData(GL_ARRAY_BUFFER, sizeof(linesData), linesData, GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(0);
            init = true;
        }

        matrix4D worldToProj = scene->editorCamera.proj * scene->editorCamera.view;

        glUseProgram(shader.programID);
        glBindVertexArray(VAOID);
        for(i32 i = 0; i < scene->boundingSphereCount; ++i)
        {
            v3 o = scene->boundingSphereO[i];
            f32 r = scene->boundingSphereR[i];
            v3 c = scene->boundingSphereColor[i];
            glUniform1f(glGetUniformLocation(shader.programID, "scale"), r);
            glUniform3fv(glGetUniformLocation(shader.programID, "offset"), 1, &o.x);
            glUniformMatrix4fv(glGetUniformLocation(shader.programID, "world_to_proj"), 1, GL_FALSE, Mat4Addr(worldToProj));
            glUniform3fv(glGetUniformLocation(shader.programID, "color"), 1, &c.x);

            glDrawArrays(GL_LINES, 0, lineCount);

        }
    }

    internal void PostProcessTonemappingGamma(pbr_scene *pbrScene, tbs_opengl::opengl_resource_manager *GPUManager, GLuint texID)
    {
        GLuint programID = GPUManager->GetProgramID(tbs_opengl::TBS_SHADER_TYPE_TonemappingGamma);
        glUseProgram(programID);

        OpenGLSetActiveTextureUnit(0);
        glBindTexture(GL_TEXTURE_2D, texID);
        glUniform1i(glGetUniformLocation(programID, "tex"), 0);
        glUniform1i(glGetUniformLocation(programID, "use_gamma"), pbrScene->useGammaCorrection);
        glUniform1i(glGetUniformLocation(programID, "use_tonemapping"), pbrScene->useTonemapping);
        glUniform1f(glGetUniformLocation(programID, "max_white"), pbrScene->whitePoint);
        glUniform1i(glGetUniformLocation(programID, "tonemapping_type"), pbrScene->tonemappingType);

        f32 vertexAttribs[] =
        {
            -1.0f, -1.0f, 0.0f, 0.0f,
             1.0f, -1.0f, 1.0f, 0.0f,
             1.0f,  1.0f, 1.0f, 1.0f,

             1.0f,  1.0f, 1.0f, 1.0f,
            -1.0f,  1.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f,
        };

        GLuint fullscreenVAOID;
        GLuint fullscreenVBOID;
        glGenVertexArrays(1, &fullscreenVAOID);
        glBindVertexArray(fullscreenVAOID);
        glGenBuffers(1, &fullscreenVBOID);
        glBindBuffer(GL_ARRAY_BUFFER, fullscreenVBOID);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexAttribs), vertexAttribs, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(f32) * 4, nullptr);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(f32) * 4, (void *)(sizeof(f32) * 2));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);


        glDrawArrays(GL_TRIANGLES, 0, 6);


        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteVertexArrays(1, &fullscreenVAOID);
        glDeleteBuffers(1, &fullscreenVBOID);

    }

    internal void RenderScenePBR(application_state *app, tbs_gui::imgui_state *imGuiState, tbs_opengl::opengl_resource_manager *rm, pbr_scene *pbrScene, scene_state *scene)
    {
        draw_list drawList = CreateDrawList(&app->frameArena, pbrScene, scene->firstNode);

        if(drawList.meshCount == 0)
        {
            return;
        }

        switch(pbrScene->viewMode)
        {
            case tbs_scene::VIEW_MODE_Default:
            {
                if(pbrScene->msaaActive)
                {
                    glEnable(GL_MULTISAMPLE);
                }
                shadow_map_render_result *shadowMaps = RenderShadowMaps(&app->frameArena, &scene->editorCamera, &drawList, pbrScene, rm);

                GLuint renderFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_PBRSceneOffscreenMsaa);
                glBindFramebuffer(GL_FRAMEBUFFER, renderFBO);
                glViewport(0, 0, pbrScene->viewportSize.x, pbrScene->viewportSize.y);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                RenderLightPrimitives(scene, rm, &drawList);
                DrawListRender_PBR(pbrScene, rm, scene, &scene->editorCamera, &drawList, shadowMaps);

                DEBUG_RenderBoundingSphere(app, scene);

                if(scene->cameraCount > 0)
                {
                    // TODO(torgrim): Render scene from other cameras
                }

                GLuint swapFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_Swap);
                // NOTE: Blit MSAA pass
                glBindFramebuffer(GL_READ_FRAMEBUFFER, renderFBO);
                glBindFramebuffer(GL_DRAW_FRAMEBUFFER, swapFBO);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                glBlitFramebuffer(0, 0, pbrScene->viewportSize.x, pbrScene->viewportSize.y,
                                  0, 0, pbrScene->viewportSize.x, pbrScene->viewportSize.y,
                                  GL_COLOR_BUFFER_BIT,
                                  GL_LINEAR);


                GLuint tonemapGammaFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_PBRViewerTonemappingGamma);
                GLuint texID = OpenGLGetFramebufferColorAttachment(swapFBO);
                glBindFramebuffer(GL_READ_FRAMEBUFFER, tonemapGammaFBO);
                glBindFramebuffer(GL_DRAW_FRAMEBUFFER, tonemapGammaFBO);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                PostProcessTonemappingGamma(pbrScene, rm, texID);

                // NOTE: Blit to default framebuffer
                glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
                glViewport(0, 0, (GLsizei)app->clientW, (GLsizei)app->clientH);
                glBindFramebuffer(GL_READ_FRAMEBUFFER, tonemapGammaFBO);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                glBlitFramebuffer(0, 0,
                                  pbrScene->viewportSize.x, pbrScene->viewportSize.y,
                                  pbrScene->viewportPos.x, pbrScene->viewportPos.y,
                                  pbrScene->viewportPos.x + pbrScene->viewportSize.x, pbrScene->viewportPos.y + pbrScene->viewportSize.y,
                                  GL_COLOR_BUFFER_BIT,
                                  GL_LINEAR);

                glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

                glDisable(GL_MULTISAMPLE);
            } break;
            case tbs_scene::VIEW_MODE_Cascade:
            {
                light_data *light = nullptr;
                // NOTE(torgrim): Currently only support 1 directional light
                // so we just pick the first we find.
                for(u32 i = 0; i < ARRAY_COUNT(pbrScene->lights); ++i)
                {
                    if(pbrScene->lights[i].type == LIGHT_TYPE_Directional)
                    {
                        light = pbrScene->lights + i;
                        break;
                    }
                }
                if(light)
                {
                    GLuint renderFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_PBRSceneOffscreenMsaa);
                    glBindFramebuffer(GL_FRAMEBUFFER, renderFBO);
                    glViewport(0, 0, pbrScene->viewportSize.x, pbrScene->viewportSize.y);
                    DrawListRender_Cascade(rm, pbrScene, &scene->editorCamera, light, &drawList);
                    glViewport(0, 0, (GLsizei)app->clientW, (GLsizei)app->clientH);
                    glBindFramebuffer(GL_READ_FRAMEBUFFER, renderFBO);
                    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
                    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                    glBlitFramebuffer(0, 0,
                                      pbrScene->viewportSize.x, pbrScene->viewportSize.y,
                                      pbrScene->viewportPos.x, pbrScene->viewportPos.y,
                                      pbrScene->viewportPos.x + pbrScene->viewportSize.x, pbrScene->viewportPos.y + pbrScene->viewportSize.y,
                                      GL_COLOR_BUFFER_BIT,
                                      GL_LINEAR);

                    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
                }
            } break;
        }

        glViewport(0, 0, (GLsizei)app->clientW, (GLsizei)app->clientH);
        RenderSceneGUI(app, imGuiState, rm, pbrScene);

        glBindVertexArray(0);
        glUseProgram(0);

    }
}
