/* date = August 29th 2020 3:10 am */

#ifndef TBS_BASE_MEMORY_H

enum arena_type
{
    ArenaType_Base,
    ArenaType_App,
    ArenaType_Frame,
    ArenaType_Temp,
    ArenaType_Count,
};

// TODO(torgrim): If we have a temporary memory with big size we want to
// be able to decommit and recommit a range(on the end) where that range hasn't been used
// for a long time so that we don't actually hold a large amount of physical memory that we don't
// really use at the moment(This will most often be the case when loading large scenes)
struct memory_arena
{
    u8 *base;
    size_t offset;
    size_t size;

    // TODO(torgrim): Debug only?
    arena_type type;
};

// TODO(torgrim): Consider renaming this to something
// that better describes how it's supposed to be used.
// Currently this doesn't really make sense because we can
// only really have one that exists at any moment so that
// where ever this is used they might have just passed the arena
// directly??
// Need to find a better solution to temporary memory.
struct temp_memory
{
    memory_arena *arena;
    size_t cursor;
    u64 id;

    temp_memory(struct application_state *app);
    temp_memory(const temp_memory& src) = delete;
    ~temp_memory();
};

struct pool_free_chunk
{
    pool_free_chunk *next;
};

struct pool_memory
{
    u8 *memory;
    size_t size;
    pool_free_chunk *head;
    size_t chunkSize;
    u64 chunkCount;
};

#define CreateStructPool(arena, type, count) CreateStructPoolInternal(arena, sizeof(type), alignof(type), count)

#define AllocStruct(arena, type) (type *)AllocSizeInternal(arena, sizeof(type), alignof(type))
#define AllocSize(arena, size) AllocSizeInternal(arena, size, 16)
#define AllocArray(arena, type, count) (type *) AllocSizeInternal(arena, sizeof(type) * count, alignof(type))
#define AllocPointerArray(arena, type, count) (type**)AllocSizeInternal(arena, sizeof(type*) * count, alignof(void*))

// TODO(torgrim): Alignment
internal u8 *AllocSizeInternal(memory_arena *arena, size_t size, size_t alignment);
internal memory_arena MakeSubArena(memory_arena *arena, size_t size, arena_type type);
internal memory_arena MakeTailArena(memory_arena *arena, arena_type type);

internal void *PoolMemory_Allocate(pool_memory *p);

#define TBS_BASE_MEMORY_H

#endif //TBS_BASE_MEMORY_H
