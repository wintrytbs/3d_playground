#include "tbs_scene_gui.h"
#include "tbs_scene.h"

#include "tbs_scene_gui.cpp"
#include "tbs_scene_renderer.cpp"

namespace tbs_scene
{
    void scene_state::AddTopLevelNode(scene_node *node)
    {
        ASSERT(node != nullptr);
        ASSERT(node->groupID != 0);

        if(this->firstNode)
        {
            scene_node *prevNode = this->firstNode;
            while(prevNode->next)
            {
                prevNode = prevNode->next;
            }

            prevNode->next = node;
        }
        else
        {
            this->firstNode = node;
        }
    }

    scene_node *scene_state::CreateAndAddTopLevelNode(pool_memory *pool, void *dataHandle, const char *nodeName, scene_node_type type)
    {
        auto node = (scene_node *)PoolMemory_Allocate(pool);
        node->type = type;
        node->handle = dataHandle;
        node->groupID = groupIDCounter++;
        RawStringCopy(node->name, nodeName);
        this->AddTopLevelNode(node);
        return node;
    }

    matrix4D scene_state::CalculateCompleteNodeTransform(scene_node *node)
    {
        ASSERT(node->groupID != 0);
        scene_node *rootNode = this->firstNode;
#if 0
        while(rootNode)
        {
            if(rootNode->groupID == node->groupID)
            {
                break;
            }

            rootNode = rootNode->next;
        }
#endif

        matrix4D result = CreateIdentity4D();
        if(rootNode != nullptr)
        {

            struct stack_data
            {
                scene_node *node;
                bool childAdded;
            };

            stack_data stack[40];
            u32 stackCount = 0;
            stack[stackCount++] = {rootNode, false};
            while(stackCount > 0)
            {
                auto *data = stack + (stackCount-1);
                if(data->node == node)
                {
                    break;
                }
                if(data->node->firstChild && data->childAdded == false)
                {
                    stack[stackCount++] = {data->node->firstChild, false};
                    data->childAdded = true;
                }
                else
                {
                    if(data->node->next)
                    {
                        stack[stackCount-1] = {data->node->next, false};
                    }
                    else
                    {
                        --stackCount;
                    }
                }
            }

            ASSERT(stackCount > 0);
            for(u32 i = 0; i < stackCount; ++i)
            {
                auto *cNode = stack[i].node;
                result *= CreateTranslate(cNode->transform.translation) * CreateRotate(cNode->transform.rotation) * CreateScale(cNode->transform.scale);
            }
        }

        return result;
    }

    // TODO(torgrim): This doesn't really belong with the scene functionality
    internal tbs_opengl::opengl_resource_handle EnqueueGltfTexture(application_state *app,
                                                                   tbs_opengl::opengl_resource_manager *resourceManager,
                                                                   gltf_root *gltf, gltf_texture *gltfTex,
                                                                   tbs_opengl::texture_internal_format internalFormat = tbs_opengl::TEXTURE_INTERNAL_FORMAT_RGBA)
    {
        auto result = tbs_opengl::invalidHandle;
        if(gltfTex != nullptr)
        {
            tbs_opengl::sampler_config config;
            gltf_sampler *gltfSampler = gltf->samplers.GetItemOrNullI64(gltfTex->samplerIndex);
            if(gltfSampler)
            {
                config.magFilter = gltfSampler->magFilter;
                config.minFilter = gltfSampler->minFilter;
                config.wrapS = gltfSampler->wrapS;
                config.wrapT = gltfSampler->wrapT;
            }
            else
            {
                config = tbs_opengl::defaultSamplerConfig;
            }

            gltf_image *gltfImg = gltf->images.GetItemOrNullI64(gltfTex->imageIndex);
            if(gltfImg)
            {
                tbs_opengl::opengl_resource_handle handle = tbs_opengl::invalidHandle;
                if(gltfImg->uri != nullptr)
                {
                    ASSERT(gltfImg->uri != nullptr);
                    // TODO(torgrim): Currently only supports image files.
                    char *imagePath = RawStringCreate(gltfImg->uri);
                    handle = resourceManager->CreateUnloadedTexture(imagePath, config, GL_TEXTURE_2D, internalFormat);

                    ASSERT(handle != tbs_opengl::invalidHandle);

                    texture_queue_data queueInput = {tbs_opengl::HandleToVoid(handle), imagePath, {}};
                    Win32EnqueueTextureWork(app->textureWorker, queueInput);
                }
                else if(gltfImg->imageBuffer.data)
                {
                    image_data img = LoadImageDataFromMemory(gltfImg->imageBuffer.data, (int)gltfImg->imageBuffer.size);
                    // TODO(torgrim): Make this into one call that just creates the texture
                    // and return a handle.
                    handle = resourceManager->CreateUnloadedTexture(nullptr, config, GL_TEXTURE_2D, internalFormat);
                    resourceManager->LoadTexture(handle, &img);
                }

                ASSERT(handle != tbs_opengl::invalidHandle);

                result = handle;
            }
        }

        return result;
    }

    internal scene_node *CreateSceneNodeAndMesh(application_state *app,
                                                tbs_scene::pbr_scene *pbrScene,
                                                tbs_opengl::base_primitive_type type,
                                                const char *name)
    {
        scene_node *node = AllocStruct(&app->appArena, scene_node);
        node->type = SCENE_NODE_TYPE_Mesh;
        node->handle = tbs_opengl::HandleToVoid(pbrScene->CreateAndAddBuiltinMesh(&app->appArena, type));
        node->transform.translation = v3(0.0f, 0.0f, 0.0f);
        node->transform.scale = v3(1.0f, 1.0f, 1.0f);
        node->transform.rotation = v3(0.0f, 0.0f, 0.0f);
        RawStringCopy(node->name, name);

        return node;
    }

    internal scene_node *CreateSceneNodeTreeFromGltf(application_state *app, tbs_opengl::opengl_resource_manager *rm, pbr_scene *pbrScene, gltf_root *gltf, i64 sceneIndex)
    {
        temp_memory scratch(app);
        tbs_array_sr<tbs_opengl::opengl_resource_handle> materialMap(scratch.arena, gltf->materials.count);
        tbs_array_sr<tbs_opengl::opengl_resource_handle> meshMap(scratch.arena, gltf->meshes.count);
        tbs_array_sr<char*> meshNameMap(scratch.arena, gltf->meshes.count);

        for(size_t matIndex = 0; matIndex < gltf->materials.count; ++matIndex)
        {
            gltf_material *gltfMat = gltf->materials.GetItem(matIndex);
            tbs_pbr::pbr_material mat = {};

            // NOTE(torgrim): Initialize all textures to an invalid texture handle.
            for(size_t texIndex = 0; texIndex < ARRAY_COUNT(mat.textures); ++texIndex)
            {
                mat.textures[texIndex] = tbs_opengl::invalidHandle;
            }

            gltf_texture *gltfNorTex = gltf->textures.GetItemOrNullI64(gltfMat->normalTextureIndex);
            gltf_texture *gltfOccTex = gltf->textures.GetItemOrNullI64(gltfMat->occlusionTextureIndex);
            gltf_texture *gltfEmiTex = gltf->textures.GetItemOrNullI64(gltfMat->emissiveTexture.texIndex);
            gltf_texture *gltfBasTex = gltf->textures.GetItemOrNullI64(gltfMat->pbr.baseColorTexture.texIndex);
            gltf_texture *gltfMetTex = gltf->textures.GetItemOrNullI64(gltfMat->pbr.metallicRoughnessTexture.texIndex);

            // TODO(torgrim): All of these textures can have specific tex coord set that should
            // be used.
            auto texHandle = EnqueueGltfTexture(app, rm, gltf, gltfNorTex);
            if(texHandle != tbs_opengl::invalidHandle)
            {
                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_Normal] = texHandle;
                mat.normalScaleFactor = (f32)gltfMat->normalScaleFactor;
                ASSERT(mat.normalScaleFactor == 1.0f);
            }
            texHandle = EnqueueGltfTexture(app, rm, gltf, gltfOccTex);
            if(texHandle != tbs_opengl::invalidHandle)
            {
                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_Occlusion] = texHandle;
                mat.occlusionStrengthFactor = (f32)gltfMat->occlusionStrengthFactor;
                ASSERT(mat.occlusionStrengthFactor == 1);
            }
            texHandle = EnqueueGltfTexture(app, rm, gltf, gltfEmiTex, tbs_opengl::TEXTURE_INTERNAL_FORMAT_SRGBA);
            if(texHandle != tbs_opengl::invalidHandle)
            {
                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_Emissive] = texHandle;
                mat.emissiveFactor = gltfMat->emissiveFactor;
            }
            texHandle = EnqueueGltfTexture(app, rm, gltf, gltfBasTex, tbs_opengl::TEXTURE_INTERNAL_FORMAT_SRGBA);
            if(texHandle != tbs_opengl::invalidHandle)
            {
                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_BaseColor] = texHandle;
            }
            texHandle = EnqueueGltfTexture(app, rm, gltf, gltfMetTex);
            if(texHandle != tbs_opengl::invalidHandle)
            {
                mat.textures[tbs_pbr::PBR_MATERIAL_TEXTURE_TYPE_MetallicRoughness] = texHandle;
            }

            // TODO(torgrim): Implement double sided materials.
            //
            //ASSERT(gltfMat->doubleSided == false);
            mat.baseColor = gltfMat->pbr.baseColorFactor;
            mat.metallicFactor = (f32)gltfMat->pbr.metallicFactor;
            mat.roughnessFactor = (f32)gltfMat->pbr.roughnessFactor;
            mat.alphaCutoff = (f32)gltfMat->alphaCutoff;
            mat.alphaMode = (tbs_pbr::material_alpha_mode)gltfMat->alphaMode;

            auto handle = pbrScene->AddMaterial(mat);
            materialMap.Add(handle);
        }

        // NOTE(torgrim): Not sure we care about this one, maybe just put this directly on
        // the mesh since that's the only part that's using textures anyway.
        for(size_t meshIndex = 0; meshIndex < gltf->meshes.count; ++meshIndex)
        {
            gltf_mesh *gltfMesh = gltf->meshes.GetItem(meshIndex);
            ASSERT(gltfMesh->primitives.count > 0);
            tbs_pbr::pbr_mesh mesh;
            mesh.primitives = tbs_array_sr<tbs_pbr::pbr_primitive>(&app->appArena, gltfMesh->primitives.count);
            for(size_t primIndex = 0; primIndex < gltfMesh->primitives.count; ++primIndex)
            {
                gltf_primitive *gltfPrim = gltfMesh->primitives.GetItem(primIndex);
                // TODO(torgrim): Here we need to memcpy all the buffers since
                // we don't want the internal buffers belonging to the gltf
                // memory allocator.
                // TODO(torgrim): Positions are required so if we don't have
                // positions it's considered invalid.
                // TODO(torgrim): Do we want to always guarantee that a valid non_renderable_mesh
                // has bufferes so that when the backend needs to create gpu buffers it shouldn't
                // expect to have to create any buffers just copy the data directly or not create
                // any render data at all?

                ASSERT(gltfPrim->mode == GLTF_PRIMITIVE_MODE_Triangles);

                triangle_list triangleList = {};

                ASSERT(gltfPrim->positions.size % sizeof(v3) == 0);
                size_t vertexAttribCount = gltfPrim->positions.size / sizeof(v3);

                tbs_opengl::data_buffer posBuffer = {};
                tbs_opengl::data_buffer norBuffer = {};
                tbs_opengl::data_buffer tanBuffer = {};
                tbs_opengl::data_buffer uv0Buffer = {};
                tbs_opengl::data_buffer indBuffer = {};

                if(gltfPrim->indices.size > 0)
                {
                    indBuffer.buff = gltfPrim->indices.data;
                    indBuffer.size = gltfPrim->indices.size;
                    indBuffer.dataType = gltfPrim->indices.componentType;

                    if(indBuffer.dataType == GL_UNSIGNED_BYTE)
                    {
                        size_t count = gltfPrim->indices.size / sizeof(u8);
                        triangleList = CreateTriangleListU8(scratch.arena, (u8 *)indBuffer.buff, count);
                    }
                    else if(indBuffer.dataType == GL_UNSIGNED_SHORT)
                    {
                        size_t count = gltfPrim->indices.size / sizeof(u16);
                        triangleList = CreateTriangleListU16(scratch.arena, (u16 *)indBuffer.buff, count);
                    }
                    else if(indBuffer.dataType == GL_UNSIGNED_INT)
                    {
                        size_t count = gltfPrim->indices.size / sizeof(u32);
                        triangleList = CreateTriangleListU32(scratch.arena, (u32 *)indBuffer.buff, count);
                    }
                    else
                    {
                        ASSERT(false);
                    }
                }
                else
                {
                    triangleList = CreateSimpleTriangleList(scratch.arena, vertexAttribCount);
                }


                if(gltfPrim->positions.size > 0)
                {
                    posBuffer.buff = gltfPrim->positions.data;
                    posBuffer.size = gltfPrim->positions.size;
                }
                else
                {
                    ASSERT(false);
                }

                bool recalculateTangents = false;
                if(gltfPrim->normals.size > 0)
                {
                    norBuffer.buff = gltfPrim->normals.data;
                    norBuffer.size = gltfPrim->normals.size;
                }
                else
                {
                    recalculateTangents = true;

                    norBuffer.buff = AllocSize(scratch.arena, posBuffer.size);
                    norBuffer.size = posBuffer.size;
                    CalcFlatNormals((v3 *)posBuffer.buff, &triangleList, (v3 *)norBuffer.buff);
                }

                if(gltfPrim->uv0.size > 0)
                {
                    uv0Buffer.buff = gltfPrim->uv0.data;
                    uv0Buffer.size = gltfPrim->uv0.size;

                    if(!recalculateTangents && gltfPrim->tangents.size > 0)
                    {
                        tanBuffer.buff = gltfPrim->tangents.data;
                        tanBuffer.size = gltfPrim->tangents.size;
                    }
                    else
                    {
                        tanBuffer.size = vertexAttribCount * sizeof(v4);
                        tanBuffer.buff = AllocSize(scratch.arena, tanBuffer.size);
                        CalcMeshTangents(scratch.arena,
                                         (v3 *)posBuffer.buff, posBuffer.size / sizeof(v3),
                                         triangleList.triangles, triangleList.count,
                                         (v2 *)uv0Buffer.buff, uv0Buffer.size / sizeof(v2),
                                         (v3 *)norBuffer.buff,
                                         (v4 *)tanBuffer.buff);
                    }
                }

                tbs_opengl::primitive_buffer_collection bufferCollection = {};
                bufferCollection.posBuffer = posBuffer;
                bufferCollection.uv0Buffer = uv0Buffer;
                bufferCollection.norBuffer = norBuffer;
                bufferCollection.tanBuffer = tanBuffer;
                bufferCollection.indBuffer = indBuffer;

                auto material = materialMap.GetValueOrDefaultI64(gltfPrim->materialIndex, tbs_opengl::invalidHandle);
                auto primHandle = rm->CreateRenderablePrimitive(bufferCollection, gltfPrim->mode);

                tbs_pbr::pbr_primitive prim = {};
                prim.materialHandle = material;
                prim.renderDataHandle = primHandle;
                mesh.primitives.Add(prim);
            }

            auto handle = pbrScene->AddMesh(mesh);
            meshMap.Add(handle);
            meshNameMap.Add(gltfMesh->name);
            // TODO(torgrim): Support weights
        }

        gltf_scene *gltfScene = gltf->scenes.GetItemI64(sceneIndex);
        scene_node *firstRootNode = CreateSceneNodesRecurs(&app->sceneNodePool,
                                                           &gltf->nodes,
                                                           &meshMap,
                                                           &meshNameMap,
                                                           &gltfScene->rootNodes,
                                                           0);
        return firstRootNode;
    }

    internal scene_node *CreateSceneNodesRecurs(pool_memory *pool,
                                                tbs_array_sr<gltf_node> *completeNodeList,
                                                tbs_array_sr<tbs_opengl::opengl_resource_handle> *meshMap,
                                                tbs_array_sr<char*> *meshNameMap,
                                                tbs_array_sr<i64> *nodeIndices,
                                                u64 groupID)
    {
        scene_node *prevNode = nullptr;
        scene_node *firstNode = nullptr;
        for(size_t i = 0; i < nodeIndices->count; ++i)
        {
            gltf_node *gltfNode = completeNodeList->GetItemI64(nodeIndices->data[i]);
            scene_node *node = (scene_node *)PoolMemory_Allocate(pool);
            auto handle = meshMap->GetValueOrDefaultI64(gltfNode->meshIndex, tbs_opengl::invalidHandle);
            node->handle = tbs_opengl::HandleToVoid(handle);
            node->transform.rotation = gltfNode->rotation;
            node->transform.translation = gltfNode->translation;
            node->transform.scale = gltfNode->scale;
            if(groupID != 0)
            {
                node->groupID = groupID;
            }
            else
            {
                node->groupID = groupIDCounter++;
            }
            char *meshName = meshNameMap->GetItemOrNullI64(gltfNode->meshIndex);
            if(meshName != nullptr)
            {
                RawStringCopy(node->name, meshName);
            }
            else if(gltfNode->name != nullptr)
            {
                RawStringCopy(node->name, gltfNode->name);
            }
            else
            {
                RawStringCopy(node->name, "Default Node Name");
            }

            node->firstChild = CreateSceneNodesRecurs(pool, completeNodeList, meshMap, meshNameMap, &gltfNode->children, node->groupID);

            if(handle != tbs_opengl::invalidHandle)
            {
                node->type = SCENE_NODE_TYPE_Mesh;
            }
            else if(node->firstChild != nullptr)
            {
                node->type = SCENE_NODE_TYPE_Parent;
            }


            if(prevNode != nullptr)
            {
                prevNode->next = node;
            }
            else
            {
                firstNode = node;
            }

            prevNode = node;
        }

        return firstNode;
    }

    internal scene_state *CreateEmptyScene(application_state *app)
    {
        scene_state *scene = AllocStruct(&app->appArena, scene_state);
        memset(scene, 0, sizeof(scene_state));

        scene->editorCamera.pos = v3(0.0f, 3.0f, 5.0f);
        scene->editorCamera.rot = v3(20.0f, 90.0f, 0.0f);
        scene->editorCamera.rotationSpeed = 40.0f;
        scene->editorCamera.speed = 10.0f;
        scene->editorCamera.fov = 60.0f;
        scene->editorCamera.dir = v3(0.0f, 0.0f, 1.0f);
        scene->editorCamera.clipNear = 0.1f;
        scene->editorCamera.clipFar = 100.0f;

        scene->editorCamera.theta = 90.0f;

        // TODO(torgrim): This is just temporary, the name
        // should be passed as an argument
        local_persist i32 sceneID = 0;
        sprintf(scene->name, "scene %d", sceneID);
        ++sceneID;
        scene->envMap = tbs_opengl::invalidHandle;
        return scene;

    }

    internal scene_state *CreateScene(application_state *app, pbr_scene *pbrScene, scene_node *firstNode)
    {
        scene_state *scene = AllocStruct(&app->appArena, scene_state);
        memset(scene, 0, sizeof(scene_state));

        scene->firstNode = firstNode;

        light_data defaultLight = CreateLight(LIGHT_TYPE_Directional);
        defaultLight.position.x = 10.0f;
        defaultLight.position.y = 10.0f;
        auto defaultLightHandle = pbrScene->AddLight(defaultLight);
        scene->CreateAndAddTopLevelNode(&app->sceneNodePool, tbs_opengl::HandleToVoid(defaultLightHandle), "Default Light", SCENE_NODE_TYPE_Light);



        // NOTE(torgrim): For testing with more lights. This shows
        // how badly optimized the current pipeline is as it tanks
        // the performance.
#if 0
        for(u32 i = 0; i < 9; ++i)
        {
            auto sp = CreateLight(LIGHT_TYPE_Spot);
            auto spotHandle = app->pbrScene->AddLight(sp);
            scene->CreateAndAddTopLevelNode(&app->sceneNodePool, tbs_opengl::HandleToVoid(spotHandle), "TEST SPOT LIGHT", SCENE_NODE_TYPE_Light);
        }
        for(u32 i = 0; i < 9; ++i)
        {
            auto pt = CreateLight(LIGHT_TYPE_Point);
            auto spotHandle = app->pbrScene->AddLight(pt);
            scene->CreateAndAddTopLevelNode(&app->sceneNodePool, tbs_opengl::HandleToVoid(spotHandle), "TEST POINT LIGHT", SCENE_NODE_TYPE_Light);
        }
#endif

        scene->editorCamera.pos = v3(0.0f, 3.0f, 5.0f);
        scene->editorCamera.rot = v3(20.0f, 90.0f, 0.0f);
        scene->editorCamera.speed = 10.0f;
        scene->editorCamera.rotationSpeed = 40.0f;
        scene->editorCamera.fov = 60.0f;
        scene->editorCamera.dir = v3(0.0f, 0.0f, 1.0f);
        scene->editorCamera.clipNear = 0.1f;
        scene->editorCamera.clipFar = 100.0f;

        scene->editorCamera.theta = 90.0f;

        local_persist i32 sceneID = 0;
        sprintf(scene->name, "GLTF scene %d", sceneID);
        ++sceneID;
        scene->envMap = tbs_opengl::invalidHandle;
        return scene;
    }

    internal scene_state *CreateDefaultScene(application_state *app, pbr_scene *pbrScene)
    {
        scene_state *scene = AllocStruct(&app->appArena, scene_state);

        scene->usePointSampleShadow = true;

        scene_node *defaultCubeNode = CreateSceneNodeAndMesh(app, pbrScene, tbs_opengl::BASE_PRIMITIVE_TYPE_Cube, "Default Cube");

        defaultCubeNode->groupID = groupIDCounter++;

        scene_node *defaultPlaneNode = CreateSceneNodeAndMesh(app, pbrScene, tbs_opengl::BASE_PRIMITIVE_TYPE_Plane, "Default Plane");

        defaultPlaneNode->transform.scale = v3(100.0f, 1.0f, 100.0f);
        defaultPlaneNode->transform.translation = v3(0.0f, 0.0f, -50.0f);
        defaultPlaneNode->transform.rotation = v3(90.0f, 0.0f, 0.0f);

        defaultPlaneNode->groupID = groupIDCounter++;

        scene->AddTopLevelNode(defaultCubeNode);
        scene->AddTopLevelNode(defaultPlaneNode);

        light_data defaultSpot = CreateLight(LIGHT_TYPE_Spot);
        defaultSpot.rotation.y += 90.0f;
        defaultSpot.rotation.x += 26.0f;
        defaultSpot.spotMaxCutoff = 30.0f;
        defaultSpot.maxDist = 100.0f;
        defaultSpot.position.x = -12.0f;
        defaultSpot.position.y = 15.0f;

        light_data defaultPoint = CreateLight(LIGHT_TYPE_Point);
        defaultPoint.position.x = 4.0f;
        defaultPoint.position.y = 6.0f;

        light_data defaultDir = CreateLight(LIGHT_TYPE_Directional);
        defaultDir.position.y = 10.0f;
        defaultDir.position.x = 10.0f;

        auto spotHandle = pbrScene->AddLight(defaultSpot);
        auto pointHandle = pbrScene->AddLight(defaultPoint);
        auto dirHandle = pbrScene->AddLight(defaultDir);

        scene->CreateAndAddTopLevelNode(&app->sceneNodePool, tbs_opengl::HandleToVoid(spotHandle), "Default Spot Light", SCENE_NODE_TYPE_Light);
        scene->CreateAndAddTopLevelNode(&app->sceneNodePool, tbs_opengl::HandleToVoid(pointHandle), "Default Point Light", SCENE_NODE_TYPE_Light);
        scene->CreateAndAddTopLevelNode(&app->sceneNodePool, tbs_opengl::HandleToVoid(dirHandle), "Default Dir Light", SCENE_NODE_TYPE_Light);

        // NOTE(torgrim): For testing with more lights. This shows
        // how badly optimized the current pipeline is as it tanks
        // the performance.
#if 0
        for(u32 i = 0; i < 9; ++i)
        {
            auto sp = CreateLight(LIGHT_TYPE_Spot);
            auto lHandle = app->pbrScene->AddLight(sp);
            scene->CreateAndAddTopLevelNode(&app->sceneNodePool, tbs_opengl::HandleToVoid(lHandle), "TEST SPOT LIGHT", SCENE_NODE_TYPE_Light);
        }
        for(u32 i = 0; i < 9; ++i)
        {
            auto pt = CreateLight(LIGHT_TYPE_Point);
            auto lHandle = app->pbrScene->AddLight(pt);
            scene->CreateAndAddTopLevelNode(&app->sceneNodePool, tbs_opengl::HandleToVoid(lHandle), "TEST POINT LIGHT", SCENE_NODE_TYPE_Light);
        }
#endif

        scene->editorCamera.pos = v3(0.0f, 3.0f, 5.0f);
        scene->editorCamera.rot = v3(20.0f, 90.0f, 0.0f);
        scene->editorCamera.speed = 10.0f;
        scene->editorCamera.rotationSpeed = 40.0f;
        scene->editorCamera.fov = 60.0f;
        scene->editorCamera.dir = v3(0.0f, 0.0f, 1.0f);
        scene->editorCamera.clipNear = 0.1f;
        scene->editorCamera.clipFar = 1000.0f;

        scene->editorCamera.theta = 90.0f;

        scene->envMap = invalidHandle;

        RawStringCopy(scene->name, "Default Scene");

#if 0
        scene->cameras[0] = scene->editorCamera;
        scene->cameras[0].rot = v3(0.0f, 0.0f, 0.0f);
        scene->cameras[0].pos = v3(0.0f, 0.0f, 0.0f);
        scene->CreateAndAddNode(&app->sceneNodePool, (void *)(uintptr_t)(0), "Default Camera", SCENE_NODE_TYPE_Camera);
        scene->cameraCount++;
#endif

        return scene;
    }

    void pbr_scene::Init()
    {
        // NOTE(torgrim): We don't really care about the size here since we will resize the window
        // when it's ready and resize the framebuffer to the correct size then.
        this->SetupDefaultMaterialPBR();

        // TODO(torgrim): Not sure if this should really be GPU resource related.
        this->SetupShadowTextures();
    }

    void pbr_scene::SetupDefaultMaterialPBR()
    {
        ASSERT(this->materialCount == 0);
        this->defaultMaterialHandle = this->materialCount;
        tbs_pbr::pbr_material *mat = this->materials + this->materialCount;
        mat->baseColor = v4(0.5f, 0.5f, 0.5f, 1.0f);
        mat->roughnessFactor = 1.0f;
        mat->metallicFactor = 0.0f;

        for(size_t i = 0; i < ARRAY_COUNT(mat->textures); ++i)
        {
            mat->textures[i] = invalidHandle;
        }

        this->materialCount++;
    }


    // TODO(torgrim): GPU resource but special case. Not sure what to do here.
    void pbr_scene::SetupShadowTextures()
    {
        this->spotShadowTexArray = OpenGLCreateShadowMapTexture2DArray(local_constants::SHADOW_MAP_WIDTH, local_constants::SHADOW_MAP_HEIGHT, tbs_opengl::local_constants::PBR_SHADER_MAX_SPOT_COUNT);
        this->dirShadowTexArray = OpenGLCreateShadowMapTexture2DArray(local_constants::DIRECTIONAL_SHADOW_SIZE, local_constants::DIRECTIONAL_SHADOW_SIZE, local_constants::CASCADE_SHADOW_SPLIT_COUNT);
        OpenGLCreateShadowMapTextureCubeMap(local_constants::SHADOW_MAP_WIDTH, local_constants::SHADOW_MAP_HEIGHT, tbs_opengl::local_constants::PBR_SHADER_MAX_POINT_COUNT, this->pointShadowTexList);
    }

    resource_handle pbr_scene::AddLight(light_data light)
    {
        ASSERT(this->lightCount < local_constants::MAX_LIGHT_COUNT);
        auto handle = this->lightCount;
        this->lights[this->lightCount++] = light;
        return handle;
    }

    light_data *pbr_scene::GetLight(resource_handle handle)
    {
        ASSERT(handle != invalidHandle);
        ASSERT(handle < this->lightCount);

        auto result = this->lights + handle;
        return result;
    }

    resource_handle pbr_scene::AddMesh(tbs_pbr::pbr_mesh mesh)
    {
        ASSERT(this->meshCount < local_constants::MAX_MESH_COUNT);
        auto handle = this->meshCount;
        this->meshes[this->meshCount++] = mesh;
        return handle;
    }

    tbs_pbr::pbr_mesh *pbr_scene::GetMesh(resource_handle handle)
    {
        ASSERT(handle != invalidHandle);
        ASSERT(handle < this->meshCount);
        auto result = this->meshes + handle;
        return result;
    }

    resource_handle pbr_scene::AddMaterial(tbs_pbr::pbr_material mat)
    {
        ASSERT(this->materialCount < local_constants::MAX_MATERIAL_COUNT);

        auto result = this->materialCount;
        this->materials[this->materialCount++] = mat;
        return result;
    }

    tbs_pbr::pbr_material *pbr_scene::GetMaterial(resource_handle handle)
    {
        ASSERT(handle != invalidHandle);
        ASSERT(handle < this->materialCount);
        auto result = this->materials + handle;
        return result;
    }

    resource_handle pbr_scene::CreateAndAddBuiltinMesh(memory_arena *arena, tbs_opengl::base_primitive_type type)
    {
        ASSERT(type == tbs_opengl::BASE_PRIMITIVE_TYPE_Plane || type == tbs_opengl::BASE_PRIMITIVE_TYPE_Cube);
        tbs_pbr::pbr_mesh mesh = {};
        mesh.primitives = tbs_array_sr<tbs_pbr::pbr_primitive>(arena, 1);
        tbs_pbr::pbr_primitive prim = {};
        prim.renderDataHandle = type;
        prim.materialHandle = this->defaultMaterialHandle;
        mesh.primitives.Add(prim);
        auto handle = this->AddMesh(mesh);
        return handle;
    }

    scene_state *pbr_scene::GetActiveScene()
    {
        ASSERT(this->sceneCount <= local_constants::MAX_SCENE_COUNT);
        ASSERT(this->sceneCount > this->activeSceneIndex);
        scene_state *result = this->scenes[this->activeSceneIndex];
        return result;
    }

    void pbr_scene::AddScene(scene_state *scene)
    {
        this->scenes[this->sceneCount++] = scene;
    }

    internal void HandleInput(application_state *app, scene_state *scene)
    {
        auto *cam = &scene->editorCamera;
        v3 camZ = scene->editorCamera.dir;
        v3 camY = axisVectorY;
        v3 camX = Cross(camY, camZ);
        camY = Cross(camX, camZ);

        input_state *inputState = &app->inputState;
        f32 dt = app->dt;

        if(inputState->mouse.middleButtonDown)
        {
            v2 delta = inputState->mouse.pos - inputState->mouse.prevPos;

            // TODO(torgrim): Currently we just set the
            // orbit center at (0,0,0). When we add custom target we
            // need to take that into the account here as well.
            f32 p = Length(cam->pos);
            f32 phi = RadianToDegree(ArcTan2(cam->pos.x, cam->pos.z));
            f32 theta = RadianToDegree(ArcCos(Clamp(cam->pos.y / p, -1.0f, 1.0f)));

            theta -= delta.y * app->dt * cam->rotationSpeed;
            phi -= delta.x * app->dt * cam->rotationSpeed;

            theta = Clamp(theta, 0.1f, 179.0f);

            f32 radPhi = DegreeToRadians(phi);
            f32 radTheta = DegreeToRadians(theta);
            cam->pos.x = p * Sin(radPhi) * Sin(radTheta);
            cam->pos.z = p * Cos(radPhi) * Sin(radTheta);
            cam->pos.y = p * Cos(radTheta);

            cam->phi = phi;
            cam->theta = theta;
        }
        else if(inputState->mouse.rightButtonDown)
        {
            if(inputState->keyboard[INPUT_KEY_A].isDown)
            {
                cam->pos = cam->pos - (cam->speed * camX * dt);
            }

            if(inputState->keyboard[INPUT_KEY_D].isDown)
            {
                cam->pos = cam->pos + (cam->speed * camX * dt);
            }

            if(inputState->keyboard[INPUT_KEY_W].isDown)
            {
                cam->pos = cam->pos - (cam->speed * camZ * dt);
            }

            if(inputState->keyboard[INPUT_KEY_S].isDown)
            {
                cam->pos = cam->pos + (cam->speed * camZ * dt);
            }

            // TODO(torgrim): If shift is down as well while we scroll, increment by a smaller values.
            if(inputState->mouse.scrollDelta != 0)
            {
                cam->speed += (inputState->mouse.scrollDelta / 120.0f) * 1.0f;
                cam->speed = Clamp(cam->speed, 0.0f, 500.0f);
            }

            // TODO(torgrim): Windows specific
            POINT screenDownPos = {(i32)inputState->mouse.downPos.x, (i32)inputState->mouse.downPos.y};
            ClientToScreen(app->windowHandle, &screenDownPos);

            if(!inputState->mouse.pressedThisFrame)
            {
                f32 deltaX = inputState->mouse.pos.x - inputState->mouse.downPos.x;
                f32 deltaY = inputState->mouse.pos.y - inputState->mouse.downPos.y;
                cam->phi -= (deltaX * dt) * cam->rotationSpeed;
                cam->theta -= (deltaY * dt) * cam->rotationSpeed;
                cam->theta = Clamp(cam->theta, 0.1f, 179.0f);
            }

            // TODO(torgrim): Windows specific.
            SetCursorPos(screenDownPos.x, screenDownPos.y);
        }
        else if(inputState->mouse.scrollDelta != 0)
        {
            cam->pos = cam->pos - (camZ * (inputState->mouse.scrollDelta * dt));
            //cam->radius += inputState->mouse.scrollDelta * dt;
        }
    }

    internal void UpdateAndRenderPBRScene(application_state *app, tbs_gui::imgui_state *imGuiState, tbs_opengl::opengl_resource_manager *rm, pbr_scene *pbrScene)
    {
        scene_state *scene = pbrScene->GetActiveScene();
        simple_camera *camera = &scene->editorCamera;

        if(!tbs_gui::IsInputHandledByImGui())
        {
            HandleInput(app, scene);
        }

        // TODO(torgrim): Make this into an event system instead.
        if(pbrScene->NeedViewportResize)
        {
            // TODO(torgrim): Is this something the renderer should deal with
            // instead?
            // TODO(torgrim): This is not a very efficient way of resizing
            // framebuffers, especially when doing continous resizing by dragging
            // and imgui docked windows etc. Need to find a better method.
            GLuint renderFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_PBRSceneOffscreenMsaa);
            GLuint tonemapGammaFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_PBRViewerTonemappingGamma);
            GLuint swapFBO = rm->GetFramebuffer(tbs_opengl::FRAMEBUFFER_TYPE_Swap);
            OpenGLResizeFramebufferAttachmentsMsaa(renderFBO, pbrScene->viewportSize.x, pbrScene->viewportSize.y, GL_RGBA32F);
            OpenGLResizeFramebufferAttachments(swapFBO, pbrScene->viewportSize.x, pbrScene->viewportSize.y, GL_RGBA32F);
            OpenGLResizeFramebufferAttachments(tonemapGammaFBO, pbrScene->viewportSize.x, pbrScene->viewportSize.y, GL_RGBA8);

            pbrScene->NeedViewportResize = false;
        }

        camera->aspect = (f32)pbrScene->viewportSize.x / (f32)pbrScene->viewportSize.y;
        camera->proj = CreateRevFrustum(DegreeToRadians(camera->fov), camera->aspect, camera->clipNear, camera->clipFar);

        if(scene->editorCamera.hasFocus == false)
        {
            f32 phiRad = DegreeToRadians(camera->phi);
            f32 thetaRad = DegreeToRadians(camera->theta);
            v3 camZ;
            camZ.x = Sin(phiRad) * Sin(thetaRad);
            camZ.y = Cos(thetaRad);
            camZ.z = Cos(phiRad) * Sin(thetaRad);
            camera->dir = camZ;
        }
        else
        {
            camera->dir = Normalize(camera->pos - camera->target);
        }

        camera->view = LookAt(camera->pos, camera->pos - camera->dir, axisVectorY);

        RenderScenePBR(app, imGuiState, rm, pbrScene, scene);
    }
}

