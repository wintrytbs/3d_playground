/* date = June 3rd 2020 4:45 pm */

#ifndef TBS_TYPES_H

#include <cassert>
#define ASSERT(expr) assert(expr)
#define S_ASSERT(expr, mess) static_assert(expr, mess)
#define ARRAY_COUNT(array) (sizeof(array) / sizeof(array[0]))
#define TO_STRING(value) #value
#define BOOL_STRING(value) value ? TO_STRING(true) : TO_STRING(false)

#define KB(value) 1024ULL*value
#define MB(value) 1024ULL*KB(value)
#define GB(value) 1024ULL*MB(value)

typedef int8_t i8;
typedef uint8_t u8;
typedef int16_t i16;
typedef uint16_t u16;
typedef int32_t i32;
typedef uint32_t u32;
typedef int64_t i64;
typedef uint64_t u64;

typedef float f32;
typedef double f64;

#define internal static
#define local_persist static
#define global static


#define TBS_TYPES_H

#endif //TBS_TYPES_H
