#ifndef TBS_OPENGL_RESOURCE_MANAGER_H

namespace tbs_opengl
{
    typedef u64 opengl_resource_handle;

    namespace local_constants
    {
        constexpr u32 MAX_TEXTURE_COUNT                 = 1024;
        constexpr u32 MAX_PRIMITIVE_COUNT               = 1024;
        constexpr u32 MAX_MESH_COUNT                    = 1024;
        constexpr u32 MAX_PBR_MATERIAL_COUNT            = 1024;
        constexpr u32 MAX_ENVIRONMENT_MAP_COUNT         = 64;
        constexpr u32 MAX_LIGHT_COUNT                   = 1024;
        constexpr u32 PBR_SHADER_MAX_SPOT_COUNT         = 10;
        constexpr u32 PBR_SHADER_MAX_POINT_COUNT        = 10;
        constexpr u32 PBR_SHADER_MAX_DIR_LIGHT_COUNT    = 1;
        constexpr u32 PBR_SHADER_MAX_LIGHT_COUNT        = PBR_SHADER_MAX_SPOT_COUNT + PBR_SHADER_MAX_POINT_COUNT + PBR_SHADER_MAX_DIR_LIGHT_COUNT;
    }

    enum base_primitive_type
    {
        BASE_PRIMITIVE_TYPE_Cube,
        BASE_PRIMITIVE_TYPE_Plane,

        BASE_PRIMITIVE_TYPE_Count,
    };

    enum framebuffer_type
    {
        FRAMEBUFFER_TYPE_Default,
        FRAMEBUFFER_TYPE_PBRSceneOffscreenMsaa,
        FRAMEBUFFER_TYPE_PBRSceneOffscreen,
        FRAMEBUFFER_TYPE_PBRSceneCamera,
        FRAMEBUFFER_TYPE_PBRViewerOffscreenMsaa,
        FRAMEBUFFER_TYPE_Swap,
        FRAMEBUFFER_TYPE_PBRViewerTonemappingGamma,
        FRAMEBUFFER_TYPE_Count,
    };

    enum tbs_shader_type
    {
        TBS_SHADER_TYPE_None,
        TBS_SHADER_TYPE_PrimitivePBR,
        TBS_SHADER_TYPE_Line3D,
        TBS_SHADER_TYPE_Rect,
        TBS_SHADER_TYPE_FullscreenRect,
        TBS_SHADER_TYPE_ShadowMap,
        TBS_SHADER_TYPE_Skybox,
        TBS_SHADER_TYPE_Light,
        TBS_SHADER_TYPE_ShadowMapViewer,
        TBS_SHADER_TYPE_CubeMapViewer,
        TBS_SHADER_TYPE_DebugGui,
        TBS_SHADER_TYPE_EditorGrid,
        TBS_SHADER_TYPE_HdrToCubeMap,
        TBS_SHADER_TYPE_DiffPrefilter,
        TBS_SHADER_TYPE_SpecPrefilter,
        TBS_SHADER_TYPE_BrdfLut,
        TBS_SHADER_TYPE_Camera,
        TBS_SHADER_TYPE_Quad3D,
        TBS_SHADER_TYPE_PBRViewer,
        TBS_SHADER_TYPE_TonemappingGamma,
        TBS_SHADER_TYPE_CascadeVisualize,

        TBS_SHADER_TYPE_Count,
    };

    // NOTE(torgrim): We match this with the texture type enum
    // so that it easier to to use when rendering. Probably a
    // more robust solution to this.
    enum pbr_sampler_binding_point
    {
        PBR_SAMPLER_BINDING_POINT_BaseColor,
        PBR_SAMPLER_BINDING_POINT_Normal,
        PBR_SAMPLER_BINDING_POINT_MetallicRoughness,
        PBR_SAMPLER_BINDING_POINT_Emissive,
        PBR_SAMPLER_BINDING_POINT_Occlusion,
        PBR_SAMPLER_BINDING_POINT_DiffPrefilterMap,
        PBR_SAMPLER_BINDING_POINT_SpecPrefilterMap,
        PBR_SAMPLER_BINDING_POINT_BrdfLUT,
        PBR_SAMPLER_BINDING_POINT_SpotShadowMap,
        PBR_SAMPLER_BINDING_POINT_DirShadowMap,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap0,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap1,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap2,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap3,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap4,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap5,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap6,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap7,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap8,
        PBR_SAMPLER_BINDING_POINT_PointShadowMap9,

        PBR_SAMPLER_BINDING_POINT_Count,
    };


    enum pbr_shader_settings_flags
    {
        PBR_SHADER_SETTINGS_FLAGS_None                  = 0,
        PBR_SHADER_SETTINGS_FLAGS_UseAmbientEnvLighting = 1u << 0,
    };

    enum pbr_active_sampler_flag
    {
        PBR_ACTIVE_SAMPLER_FLAG_None                = 0,
        // TODO(torgrim): Replace this with the base_color one since they are the same,
        // but base color is a better description in pbr rendering.
        PBR_ACTIVE_SAMPLER_FLAG_BaseColor           = 1 << 0,
        PBR_ACTIVE_SAMPLER_FLAG_Normal              = 1 << 1,
        PBR_ACTIVE_SAMPLER_FLAG_MetallicRoughness   = 1 << 2,
        PBR_ACTIVE_SAMPLER_FLAG_Emissive            = 1 << 3,
        PBR_ACTIVE_SAMPLER_FLAG_Occlusion           = 1 << 4,

    };

    enum pbr_viewer_sampler_binding_point
    {
        PBR_VIEWER_SAMPLER_BINDING_POINT_BaseColor,
        PBR_VIEWER_SAMPLER_BINDING_POINT_NormalMap,
        PBR_VIEWER_SAMPLER_BINDING_POINT_MetallicRoughnessMap,
        PBR_VIEWER_SAMPLER_BINDING_POINT_EmissiveMap,
        PBR_VIEWER_SAMPLER_BINDING_POINT_OcclusionMap,
        PBR_VIEWER_SAMPLER_BINDING_POINT_IrradianceMap,
        PBR_VIEWER_SAMPLER_BINDING_POINT_PrefilterMap,
        PBR_VIEWER_SAMPLER_BINDING_POINT_BrdfLut,

        PBR_VIEWER_SAMPLER_BINDING_POINT_Count,
    };

    enum pbr_viewer_active_sampler_flag
    {
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_None                 = 0,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_BaseColor            = 1 << 0,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_Normal               = 1 << 1,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_MetallicRoughness    = 1 << 2,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_Occlusion            = 1 << 3,
        PBR_VIEWER_ACTIVE_SAMPLER_FLAG_Emissive             = 1 << 4,
        // TODO(torgrim): Add env map samplers
    };

    enum texture_internal_format
    {
        TEXTURE_INTERNAL_FORMAT_RGBA = GL_RGBA8,
        TEXTURE_INTERNAL_FORMAT_SRGBA = GL_SRGB8_ALPHA8,
    };

    struct sampler_config
    {
        GLint magFilter;
        GLint minFilter;
        GLint wrapS;
        GLint wrapT;
    };


    struct texture_info
    {
        sampler_config samplerConfig;
        char *path;
        GLuint texID;
        GLenum target;
        bool loaded;
        texture_internal_format internalFormat;
    };

    struct data_buffer
    {
        void *buff;
        u64 size;
        GLenum dataType;
    };

    struct primitive_buffer_collection
    {
        data_buffer posBuffer;
        data_buffer uv0Buffer;
        data_buffer uv1Buffer;
        data_buffer norBuffer;
        data_buffer tanBuffer;
        data_buffer indBuffer;
    };

    struct renderable_primitive
    {
        GLuint VAOID;
        GLuint posVBOID;
        GLuint norVBOID;
        GLuint tanVBOID;
        GLuint uv0VBOID;
        GLuint uv1VBOID;
        GLuint indEBOID;

        GLenum target;
        GLenum mode;
        u32 vertexCount;
        u32 indexCount;
        GLenum indexDataType;
    };

    struct uniform_info
    {
        const char *name;
        GLint location;
    };

    struct uniform_list
    {
        i32 count;
        uniform_info *uniforms;
    };

    struct opengl_resource_manager
    {
        opengl_shader_info shaders[TBS_SHADER_TYPE_Count];
        GLuint framebuffers[FRAMEBUFFER_TYPE_Count];
        GLint pbrUniformSamplerLocations[PBR_SAMPLER_BINDING_POINT_Count];
        GLint pbrViewerSamplerLocations[PBR_VIEWER_SAMPLER_BINDING_POINT_Count];

        u32 textureCount;
        texture_info textures[local_constants::MAX_TEXTURE_COUNT];
        u32 primitiveCount;
        renderable_primitive primitives[local_constants::MAX_PRIMITIVE_COUNT];
        u32 environmentMapCount;
        tbs_pbr::env_map_info environmentMaps[local_constants::MAX_ENVIRONMENT_MAP_COUNT];

        // NOTE: Init handling
        void Init(application_state *app);
        void SetupBasePrimitives(application_state *app);

        // NOTE: Shader handling
        GLuint GetProgramID(tbs_shader_type type);
        bool CheckAndUpdateModifiedShaders(application_state *app);
        void RelocateAllSamplers();

        // NOTE: Texture handling
        opengl_resource_handle CreateUnloadedTexture(char *imagePath, sampler_config config, GLenum textureTarget, texture_internal_format internalFormat);
        void LoadTexture(opengl_resource_handle handle, image_data *image);
        texture_info *GetTexture(opengl_resource_handle handle);

        // NOTE: Mesh/primitive/material handling
        opengl_resource_handle CreateRenderablePrimitive(primitive_buffer_collection buffers, GLenum mode);
        renderable_primitive *GetPrimitive(opengl_resource_handle handle);
        renderable_primitive *GetBasePrimitive(base_primitive_type type);
        renderable_primitive *GetLightPrimitive();

        // NOTE: Environment map handling
        opengl_resource_handle AddEnvMapHDR(application_state *app, image_data_hdr *image);
        tbs_pbr::env_map_info *GetEnvMap(opengl_resource_handle handle);

        // NOTE: Misc
        GLuint GetFramebuffer(framebuffer_type framebufferType);
    };

    global constexpr opengl_resource_handle invalidHandle = UINT32_MAX;
    global constexpr sampler_config defaultSamplerConfig = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};

    internal renderable_primitive Internal_CreateBasePrimitiveRenderData(application_state *app, base_primitive_type type);
    internal renderable_primitive Internal_CreateRenderablePrimitive(primitive_buffer_collection buffers, GLenum mode);
    internal primitive_buffer_collection Internal_CreateBasePrimitiveBuffers(memory_arena *arena, base_primitive_type type);
    internal tbs_pbr::env_map_info Internal_CreateEnvMapFromHDR(application_state *app, opengl_resource_manager *rm, image_data_hdr *hdr);

    internal inline void *HandleToVoid(opengl_resource_handle handle)
    {
        return ((void *)(uintptr_t)handle);
    }

    internal inline opengl_resource_handle VoidToHandle(void *handle)
    {
        return (opengl_resource_handle)(uintptr_t)handle;
    }

    // TODO(torgrim): We don't treat handles as anything special at the moment
    // so they are just direct indices.
    internal inline u32 HandleToIndexU32(opengl_resource_handle handle)
    {
        //ASSERT(handle != invalidHandle);
        return (u32)handle;
    }
    internal inline opengl_resource_handle IndexU32ToHandle(u32 index)
    {
        return (opengl_resource_handle)index;
    }
}

#define TBS_OPENGL_RESOURCE_MANAGER_H
#endif
