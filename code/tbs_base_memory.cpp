#include "tbs_base_memory.h"

// TODO(torgrim): Find a better method for allocating temporary
// memory.
global i32 tempMemoryStack = 0;
temp_memory::temp_memory(application_state *app)
{
    tempMemoryStack++;
    this->arena = &app->tempArena;
    this->cursor = this->arena->offset;
}

temp_memory::~temp_memory()
{
    ASSERT(tempMemoryStack > 0);
    tempMemoryStack--;
    if(tempMemoryStack == 0)
    {
        this->arena->offset = cursor;
    }
}

internal pool_memory CreateStructPoolInternal(memory_arena *arena,
                                              size_t chunkSize,
                                              size_t alignment,
                                              size_t chunkCount)
{
    size_t allocSize = chunkSize*chunkCount;
    pool_memory pool;
    pool.memory = AllocSizeInternal(arena, allocSize, alignment);
    pool.size = allocSize;
    pool.chunkSize = chunkSize;
    pool.chunkCount = chunkCount;
    pool.head = nullptr;

    for(size_t i = 0; i < chunkCount; ++i)
    {
        pool_free_chunk *c = (pool_free_chunk *)(pool.memory + i*pool.chunkSize);
        c->next = pool.head;
        pool.head = c;
    }

    return pool;
}

internal void *PoolMemory_Allocate(pool_memory *p)
{
    pool_free_chunk *c = p->head;

    if(c == nullptr)
    {
        // TODO(torgrim): Handle
        ASSERT(false);
    }

    p->head = c->next;

    memset(c, 0, p->chunkSize);
    return c;
}

// NOTE(torgrim): This is currently wasteful since we are allocating a full alignment
// when we already aligned. Not really a bit issue but can be improved.
internal u8 *AllocSizeInternal(memory_arena *arena, size_t size, size_t alignment)
{
    ASSERT(arena != nullptr);
    ASSERT(size > 0);
    ASSERT(alignment > 0);
    ASSERT(alignment <= 16);
    ASSERT((alignment & (alignment - 1)) == 0);

    size_t mask = (alignment - 1);
    uintptr_t baseUnaligned = (uintptr_t)(arena->base + arena->offset);
    uintptr_t padding = alignment - (baseUnaligned & mask);
    uintptr_t alignedBase = baseUnaligned + padding;
    size_t actualAllocSize = size + padding;

    u8 *mem = nullptr;
    if(arena->offset + actualAllocSize < arena->size)
    {
        mem = (u8 *)alignedBase;
        mem[-1] = (u8)padding;
        memset(mem, 0, size);

        arena->offset += actualAllocSize;
    }
    else
    {
        // TODO(torgrim): Handle proper.
        ASSERT(false);
    }

    ASSERT(((uintptr_t)mem & (alignment - 1)) == 0);
    return mem;
}

internal memory_arena MakeSubArena(memory_arena *arena, size_t size, arena_type type)
{
    ASSERT(arena != nullptr);
    ASSERT(size > 0);

    memory_arena result = {};
    result.type = type;
    if(arena->offset + size < arena->size)
    {
        result.base = arena->base + arena->offset;
        result.size = size;

        arena->offset += size;
    }
    else
    {
        // TODO(torgrim): Handle properly
        ASSERT(false);
    }

    return result;
}

internal memory_arena MakeTailArena(memory_arena *arena, arena_type type)
{
    ASSERT(arena != nullptr);
    memory_arena result = {};
    result.type = type;
    size_t size = arena->size - arena->offset;
    if(size > 0)
    {
        result.base = arena->base + arena->offset;
        result.size = size;
        result.offset = 0;
    }
    else
    {
        // TODO(torgrim): Handle properly
        ASSERT(false);
    }

    return result;
}
