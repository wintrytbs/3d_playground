/* date = June 9th 2020 4:37 pm */

#ifndef TBS_OPENGL_H
#define TBS_OPENGL_H

#include "tbs_types.h"

// TODO(torgrim): consider also adding the shader source names into the
// shader_info struct for easier reloading of the shaders
struct opengl_shader_info
{
    GLuint vertShaderID;
    GLuint fragShaderID;
    GLuint programID;

    char *filename;
    platform_file_time lastWriteTime;
};

// TODO(torgrim): find a better way of getting opengl
// state(probably need to do some kind of meta programming here.
struct opengl_state_data
{
    GLboolean blend;
    GLboolean cullFace;
    GLboolean depthTest;
    GLboolean scissorTest;

    GLenum blendSrcRGB;
    GLenum blendSrcAlpha;
    GLenum blendDstRGB;
    GLenum blendDstAlpha;

    GLenum blendEquationRGB;
    GLenum blendEquationAlpha;

    GLuint programID;
    GLenum activeTexture;
    GLuint texture2DBinding;

    GLint version;

    GLboolean doublebuffer;
    GLboolean dither;
    GLboolean lineSmooth;
    GLboolean framebufferSRGB;
    GLboolean multisample;
    GLboolean polygonSmooth;
    GLboolean stencilTest;

    GLfloat lineWidth;
    GLint maxCombinedTextureImageUnits;
};

// TODO(torgrim): This seems unnecessary. Remove in the future.
#define OPENGL_SET_BOOL_STATE(enable, name) if(enable) glEnable(name); else glDisable(name)

internal void DEBUG_OpenGLFlushErrors();
internal void DEBUG_OpenGLCheckError();
internal bool OpenGLCheckFramebufferStatus(GLenum target, bool assertError = true);

global const char *GLSL_VERSION_PREFIX = "#version 330 core\n";
global const char *GLSL_VERT_PREFIX    = "#define VERT_ACTIVE\n";
global const char *GLSL_FRAG_PREFIX    = "#define FRAG_ACTIVE\n";

#endif //TBS_OPENGL_H
