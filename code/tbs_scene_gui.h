#ifndef TBS_SCENE_GUI_H

namespace tbs_scene
{
    // NOTE(torgrim): these should be in the same order
    // as their corresponding enums.
    struct scene_gui_viewport
    {
        v2i size;
        v2i pos;
    };

    struct scene_gui_state
    {
        scene_gui_viewport viewport;
        tbs_gui::file_browser_context fbCtx;
        tbs_gui::env_map_chooser_context envMapChooserCtx;
    };

}

#define TBS_SCENE_GUI_H
#endif
