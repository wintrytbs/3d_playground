#ifndef TBS_IMGUI_WIDGETS_H
#define TBS_IMGUI_WIDGETS_H

namespace tbs_gui
{
    enum dialog_result
    {
        DIALOG_RESULT_None,
        DIALOG_RESULT_Cancel,
        DIALOG_RESULT_Open,
    };

    struct file_browser_result
    {
        dialog_result dialogResult;
        char fullFilename[255];
    };

    struct file_browser_context
    {
        bool init;
        char searchDir[255];
        char extension[5];
        file_browser_result result;
    };

    enum env_map_chooser_mode
    {
        ENV_MAP_CHOOSER_MODE_Normal,
        ENV_MAP_CHOOSER_MODE_FileBrowser,
    };

    struct env_map_chooser_context
    {
        bool init;
        bool open;
        file_browser_context fbCtx;
        env_map_chooser_mode mode;
        // TODO(torgrim): Don't really like that this needs
        // a ref to a resource handle...
        u32 activeIndex;
        u32 chooserIndex;
        tbs_opengl::opengl_resource_handle result;

    };
}

#endif
