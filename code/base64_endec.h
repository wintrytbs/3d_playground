#ifndef BASE64_ENDEC_H

#define BASE64_ENDEC_H

namespace tbs_base64
{

#define b11111100 0xFCu
#define b11110000 0xF0u
#define b00110000 0x30u
#define b11000000 0xC0u
#define b00000011 0x03u
#define b00001111 0x0Fu
#define b00111111 0x3Fu
#define b00111100 0x3Cu

    typedef struct b64_enc_result
    {
        char *data;
        size_t size;
    } b64_result;

    typedef struct b64_dec_result
    {
        u8 *data;
        size_t size;
    } b64_dec_result;

    internal char alphabet[65] =
    {
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z',

        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'w',
        'x',
        'y',
        'z',

        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '+',
        '/',
        '=',
    };

    // TODO: Make this into a direct mapping instead.
    static inline u8 inverse_alphabet(u8 v)
    {
        u8 result = 65u;
        if(v >= 'A' && v <= 'Z')
        {
            result = (u8)(v - 'A');
        }
        else if(v >= 'a' && v <= 'z')
        {
            result = v - 'a' + 26u;
        }
        else if(v >= '0' && v <= '9')
        {
            result = v - '0' + 52u;
        }
        else if(v == '+')
        {
            result = 62;
        }
        else if(v == '/')
        {
            result = 63;
        }
        else if(v == '=')
        {
            result = 64;
        }

        return result;
    }

    static b64_enc_result base64_encode(u8 *data, size_t length)
    {
        b64_enc_result result = {};
        result.data = (char *)malloc(length);
        size_t resultIndex = 0;
        size_t bytesLeft = length;
        size_t dataIndex = 0;
        while(bytesLeft >= 3u)
        {
            u8 b0 = data[dataIndex];
            u8 b1 = data[dataIndex+1];
            u8 b2 = data[dataIndex+2];

            u32 v0 = (b0 & b11111100) >> 2u;
            u32 v1 = ((b0 & b00000011) << 4u) | ((b1 & b11110000) >> 4u);
            u32 v2 = ((b1 & b00001111) << 2u) | ((b2 & b11000000) >> 6u);
            u32 v3 = (b2 & b00111111);

            result.data[resultIndex++] = alphabet[v0];
            result.data[resultIndex++] = alphabet[v1];
            result.data[resultIndex++] = alphabet[v2];
            result.data[resultIndex++] = alphabet[v3];

            dataIndex += 3;
            bytesLeft -= 3;
        }

        if(bytesLeft == 1)
        {
            u8 b0 = data[dataIndex];
            u32 v0 = (b0 & b11111100) >> 2u;
            u32 v1 = (b0 & b00000011) << 4u;

            result.data[resultIndex++] = alphabet[v0];
            result.data[resultIndex++] = alphabet[v1];
            result.data[resultIndex++] = alphabet[64];
            result.data[resultIndex++] = alphabet[64];

            dataIndex++;
        }
        else if(bytesLeft == 2)
        {
            u8 b0 = data[dataIndex];
            u8 b1 = data[dataIndex+1];
            u32 v0 = (b0 & b11111100) >> 2u;
            u32 v1 = ((b0 & b00000011) << 4u) | ((b1 & b11110000) >> 4u);
            u32 v2 = (b1 & b00001111) << 2u;

            result.data[resultIndex++] = alphabet[v0];
            result.data[resultIndex++] = alphabet[v1];
            result.data[resultIndex++] = alphabet[v2];
            result.data[resultIndex++] = alphabet[64];

            dataIndex += 2;
        }

        result.size = resultIndex;
        return result;
    }

    static b64_dec_result base64_decode(u8 *data, size_t length)
    {
        // NOTE: base64 encoded data will always be larger in size
        // that the original data.
        b64_dec_result result = {};
        result.data = (u8 *)malloc(length);

        size_t resultIndex = 0;
        size_t bytesLeft = length;
        size_t dataIndex = 0;
        while(bytesLeft > 4u)
        {
            u8 b0 = data[dataIndex];
            u8 b1 = data[dataIndex+1];
            u8 b2 = data[dataIndex+2];
            u8 b3 = data[dataIndex+3];

            u32 t0 = inverse_alphabet(b0);
            u32 t1 = inverse_alphabet(b1);
            u32 t2 = inverse_alphabet(b2);
            u32 t3 = inverse_alphabet(b3);

            u32 v0 = (t0 << 2u) | ((t1 & b00110000) >> 4u);
            u32 v1 = ((t1 & b00001111) << 4u) | ((t2 & b00111100) >> 2u);
            u32 v2 = ((t2 & b00000011) << 6u) | (t3 & b00111111);

            result.data[resultIndex++] = (u8)v0;
            result.data[resultIndex++] = (u8)v1;
            result.data[resultIndex++] = (u8)v2;

            dataIndex += 4;
            bytesLeft -= 4;
        }


        u8 b0 = data[dataIndex];
        u8 b1 = data[dataIndex+1];
        u8 b2 = data[dataIndex+2];
        u8 b3 = data[dataIndex+3];
        u32 t0 = inverse_alphabet(b0);
        u32 t1 = inverse_alphabet(b1);
        u32 t2 = inverse_alphabet(b2);
        u32 t3 = inverse_alphabet(b3);
        if(b2 == '=')
        {
            u32 v0 = (t0 << 2u) | ((t1 & b00110000) >> 4u);

            result.data[resultIndex++] = (u8)v0;
        }
        else if(b3 == '=')
        {
            u32 v0 = (t0 << 2u) | ((t1 & b00110000) >> 4u);
            u32 v1 = ((t1 & b00001111) << 4u) | ((t2 & b00111100) >> 2u);

            result.data[resultIndex++] = (u8)v0;
            result.data[resultIndex++] = (u8)v1;
        }
        else
        {
            u32 v0 = (t0 << 2u) | ((t1 & b00110000) >> 4u);
            u32 v1 = ((t1 & b00001111) << 4u) | ((t2 & b00111100) >> 2u);
            u32 v2 = ((t2 & b00000011) << 6u) | (t3 & b00111111);

            result.data[resultIndex++] = (u8)v0;
            result.data[resultIndex++] = (u8)v1;
            result.data[resultIndex++] = (u8)v2;
        }

        result.size = resultIndex;
        return result;
    }
}
#endif
