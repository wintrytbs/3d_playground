/* date = September 2nd 2020 5:00 pm */

#ifndef TBS_CONTAINERS_H

template <typename T, size_t L>
struct tbs_array_sc
{
    T data[L];
    size_t count = 0;

    void Add(T item)
    {
        ASSERT(this->count < this->Capacity());
        data[this->count++] = item;
    }

    constexpr size_t Capacity()
    {
        size_t result = ARRAY_COUNT(data);
        return result;
    }
};

template <typename T>
struct tbs_array_sr
{
    T *data;
    size_t count;
    size_t capacity;

    tbs_array_sr();
    tbs_array_sr(memory_arena *arena, size_t cap);

    void Add(T item);
    // TODO(torgrim): Not sure I like the naming here
    // since it makes it seem like the type of the result
    // is in the name rather than the parameter type.
    T *GetItem(size_t index);
    T *GetItemU64(u64 index);
    T *GetItemI64(i64 index);
    T *GetItemOrNull(size_t index);
    T *GetItemOrNullU64(u64 index);
    T *GetItemOrNullI64(i64 index);
    T *SetSlot(size_t index, T item);
    T *SetSlotI64(i64 index, T item);
    T *GetSlot(size_t index);
    T *GetSlotI64(i64 index);
    // TODO(torgrim): This should be done in a different way.
    T GetValueOrDefaultI64(i64 index, T defaultValue);
};

template <typename T>
struct tbs_array_sr<T*>
{
    T **data;
    size_t count;
    size_t capacity;

    tbs_array_sr();
    tbs_array_sr(memory_arena *arena, size_t cap);

    void Add(T *item);
    T *GetItem(size_t index);
    T *GetItemI64(i64 index);
    T *GetItemOrNull(size_t index);
    T *GetItemOrNullI64(i64 index);
};

constexpr size_t DEFAULT_DYN_ARRAY_COUNT = 16;
template <typename T>
struct ss_dynamic_array
{
};

template <typename T>
struct ss_dynamic_array<T*>
{
    T **data;
    size_t count;
    size_t capacity;
    memory_arena *arena;
    size_t relocationCount;

    ss_dynamic_array();
    ss_dynamic_array(memory_arena *arena);

    void Add(T *item);

    const T *operator [](size_t i) const;
};

constexpr size_t DEFAULT_DICT_COUNT = 1 << 8;

template <typename T>
struct simple_dict
{
};

template <typename T>
struct simple_dict<T*>
{
    struct pair
    {
        u32 key;
        T* value;
    };

    pair *data;

    // NOTE: This is really needed since count doesn't
    // mean anything special but can be useful if we ever
    // want to copy the elements into a fixed array.
    size_t count;

    simple_dict();
    simple_dict(memory_arena *arena);

    const T *operator [](u32 hash) const;
    const T *operator [](char *str) const;
    const T *operator [](const char *str) const;

    void Add(u32 hash, T *value);
};

template <typename T>
struct limit_stack
{
    T *data;
    u32 count;
    u32 maxCount;
    T *top;

    limit_stack(memory_arena *arena, u32 max_count);

    void pop();
    void push(T header);
};

template <typename T>
struct limit_stack<T*>
{
    T **data;
    u32 count;
    u32 maxCount;
    T *top;

    limit_stack(memory_arena *arena, u32 max_count);

    void pop();
    void push(T *header);
};


internal inline u32 DictHashDefault(const char *str);

#define TBS_CONTAINERS_H

#endif //TBS_CONTAINERS_H
